import yaml

import factory.cve.cve as cve

with open("/tmp/analyzer_params.yaml", encoding="utf-8") as f:
    analyzer_params = yaml.safe_load(f)

pa = cve.PackageAnalyzer(**analyzer_params)
pa.log(print_all=True)
print(f"CVEs with a severity higher {cve.CVSS_THRESHOLD}):")
pa.log()
