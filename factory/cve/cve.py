import logging
import logging.config

import apt

log = logging.getLogger(__name__)
logging.basicConfig(format="%(levelname)s %(message)s", level=logging.DEBUG)

ACCEPTED_VULNERABILITIES = ["None", "Low", "Medium"]
CVSS_THRESHOLD = 7
IGNORE_REASONS = [
    "runtime_unaffected",
    # The runtime was analysed with tools to ensure that
    # the image runtime is not affected by the CVE.
    # (internal justification)
    "upstream_ignores",
    # The upstream development team decided to ignore the CVE
    # (external justification)
    "risk_accepted",
    # Acceptance of the risk without justification
]

severity_values = {
    "Negligible": 0,
    "Low": 1,
    "Medium": 2,
    "High": 3,
    "Critical": 4,
}


class Vulnerability:
    """
    The Vulnerability class implements a vulnerability as a combination of a CVE and exactly
    one affected image. So if two images are affected, we also have two distinct Vulnerabilities.
    """

    def __init__(
        self,
        id: str,
        severity: str,
        cvss_basescore: str,
        image: str,
        image_dir: str,
        package: str,
        version: str,
        fix_state: str,
        fix_versions: str,
        can_be_removed: bool,
        package_priority: str,
        vulnerability_format: str,
        ignores: list[dict],
        upstream_status: str,
    ) -> None:
        self.id = id
        self.data_source = ""
        self.severity = severity
        self.cvss_basescore = cvss_basescore
        self.image = image
        self.ignores = ignores
        self.upstream_status = upstream_status
        self.image_dir = image_dir
        self.is_ignored = False
        self.is_ignored_reason = "not_handled"
        self.is_ignored_reason_description = "There is no ignore entry."
        self.load_ignored_setting()
        self.fix_state = fix_state
        self.fix_versions = ",".join(fix_versions)
        self.package = package
        self.version = version
        self.can_be_removed = can_be_removed
        self.package_priority = package_priority
        self.vulnerability_format = vulnerability_format
        possible_actions = ["ignore"]
        if self.package_priority != "essential":
            possible_actions.append("delete")
        if self.fix_state == "fixed":
            possible_actions.append("update")
        self.possible_actions = ",".join(possible_actions)

    def __str__(self) -> str:
        return self.vulnerability_format.format(**self.__dict__)

    def load_ignored_setting(self) -> None:
        for ignore in self.ignores:
            if ignore["id"] == self.id:
                self.is_ignored = True
                self.is_ignored_reason = ignore["reason"]
                self.is_ignored_reason_description = ignore["description"]

    def is_accepted(self) -> bool:
        if self.severity in ACCEPTED_VULNERABILITIES:
            return True
        else:
            return False

    def needs_action(self) -> bool:
        if self.is_accepted():
            log.info(f"The cve {self.id} has an accepted severity level.")
            return False
        elif self.is_ignored:
            log.info(f"The cve {self.id} is ignored for image {self.image}.")
            return False
        else:
            log.info(f"The cve {self.id} has a severity level, which is not accepted.")
            return True

    def get_manual_action(self) -> None:
        if self.fix_state == "fixed":
            log.info(
                f"The cve {self.id} is fixed in"
                f"{self.package} version {self.fix_versions}."
                f"Update or remove the affected package or add the cve in cve_ignores.yaml"
                f"of image {self.image}."
                f"Specify the CVE Id, a reason (possible values:"
                f'"runtime_unaffected", "upstream_ignores", "risk_accepted")'
                f"and a description of the internal or external justification."
            )
        else:
            log.info(
                f"Remove the affected package or add the cve in cve_ignores.yaml of image {self.image}."
                f"Specify the CVE Id, a reason (possible values:"
                f'"runtime_unaffected", "upstream_ignores", "risk_accepted")'
                f"and a description of the internal or external justification."
            )

    def process(self) -> None:
        if self.is_ignored:
            log.info(
                f"The cve {self.id} is ignored for image {self.image}"
                f"by the following reason: {self.is_ignored_reason}"
            )
        if self.needs_action():
            log.info(f"The cve {self.id} needs manual action.")
            self.get_manual_action()


class PackageAnalyzer:
    def __init__(
        self,
        image: str,
        image_dir: str,
        scan_result: list[dict],
        vulnerability_format: str,
        ok_vulnerabilities: list[str],
        ignores: list[dict],
    ):
        cache = apt.cache.Cache()

        self.vulnerabilities: list[Vulnerability] = list()
        self.image = image
        self.image_dir = image_dir
        self.scan_result = scan_result
        self.vulnerability_format = vulnerability_format
        self.ok_vulnerabilities = ok_vulnerabilities
        self.ignores = ignores

        installed_packages = [cache[pkg] for pkg in cache.keys() if cache[pkg].installed]

        essential_packages = list()
        required_packages = list()

        for pkg in installed_packages:
            if pkg.installed and pkg.installed.priority == "required":
                required_packages.append(pkg)

            if pkg.essential and pkg.installed:
                essential_packages.append(pkg)
                deps = pkg.installed.dependencies
                for dep in deps:
                    if dep._version.package.is_installed:
                        essential_packages.append(dep._version.package)
                    else:
                        # TODO: handle this case
                        pass

        self.essential_packages = sorted(list(set([pkg.name for pkg in essential_packages])))
        self.installed_packages = sorted(list(set([pkg.name for pkg in installed_packages])))
        self.required_packages = [pkg.name for pkg in required_packages]
        self.additional_packages = sorted(
            list(set([item.name for item in installed_packages if item not in required_packages]))
        )
        self.load_cves()

    def create_report(self, print_all: bool = False) -> list[str]:
        report = list()
        sorted_vulnerabilities = sorted(
            self.vulnerabilities, key=lambda obj: severity_values[obj.severity], reverse=True
        )
        for vulnerability in sorted_vulnerabilities:
            try:
                basescore = float(vulnerability.cvss_basescore.split()[0])
            except ValueError:
                basescore = 0
            if vulnerability.package_priority in ["essential", "required"]:
                vulnerability.is_ignored_reason = f"upstream_{vulnerability.upstream_status}"
            if basescore < CVSS_THRESHOLD:
                vulnerability.is_ignored_reason = "below_threshold"
            if basescore >= CVSS_THRESHOLD or print_all:
                if vulnerability.severity in self.ok_vulnerabilities:
                    report.append(f"W: {str(vulnerability)}")
                else:
                    report.append(f"E: {str(vulnerability)}")
        return report

    def log(self, print_all: bool = False) -> None:
        report = self.create_report(print_all)
        log.info("CVE report")
        header = {
            "id": "ID",
            "package": "Package",
            "version": "Version",
            "severity": "Severity",
            "cvss_basescore": "CVSS Score",
            "fix_state": "Fixed",
            "fix_versions": "Fixversions",
            "package_priority": "Priority",
            "possible_actions": "Possible actions",
            "is_ignored_reason": "Ignored reason",
        }
        print("   " + self.vulnerability_format.format(**header))
        for line in report:
            print(line)

    def can_be_removed(self, package_name: str) -> bool:
        if package_name not in self.essential_packages:
            return True
        else:
            return False

    def package_priority(self, package_name: str) -> str:
        if package_name in self.essential_packages:
            return "essential"
        if package_name in self.required_packages:
            return "required"
        if package_name in self.additional_packages:
            return "additional"
        return "none"

    def load_cves(self) -> None:
        self.vulnerabilities = list()
        data = self.scan_result
        for item in data:
            # package_type = match['artifact']['type']
            can_be_removed = self.can_be_removed(item["package"])
            package_priority = self.package_priority(item["package"])
            vulnerability = Vulnerability(
                id=item["id"],
                severity=item["severity"],
                cvss_basescore=item["cvss_basescore"],
                image=self.image,
                image_dir=self.image_dir,
                package=item["package"],
                version=item["version"],
                fix_state=item["fix_state"],
                fix_versions=item["fix_versions"],
                can_be_removed=can_be_removed,
                package_priority=package_priority,
                vulnerability_format=self.vulnerability_format,
                ignores=self.ignores,
                upstream_status=item["upstream_status"],
            )
            self.vulnerabilities.append(vulnerability)
