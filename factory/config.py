# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import re
from dataclasses import dataclass, field, fields
from typing import Any

from typing_extensions import Self

from factory.util.utils import get_tags_for_repository, replace_forbidden_chars

log = logging.getLogger(__name__)
logging.basicConfig(format="%(levelname)s %(message)s")


@dataclass
class DataclassHelper:
    """Base class with helper functions."""

    @classmethod
    def create_from_dict(cls, values_dict: dict[str, Any]) -> Self:
        """
        Custom function to create the class to ignore passed attributes from the dict that the
        class does not know and allow the parameters to have default values.
        """
        class_fields = {f.name for f in fields(cls)}
        return cls(**{k: v for k, v in values_dict.items() if k in class_fields})

    def update(self, new: dict[str, Any]) -> None:
        """Allow to update the dataclass with a given dict."""
        for key, value in new.items():
            if hasattr(self, key):
                setattr(self, key, value)


@dataclass
class ImageConfigAttributes(DataclassHelper):
    """
    Class for keeping track of all configuration items of ImageConfig.config.

    Primarily contains the configuration from the image.yaml of the image,
    additionally inherited attributes from the global configuration and
    attributes that are created before initialization.
    """

    basename: str
    name: str  # basename with tag
    distro: str
    distro_codename: str
    directory: str
    harbor_api_url: str
    project: str
    registry: str
    registry_url: str
    registry_user: str
    registry_password: str
    rel_path: str

    # optional and attributes with default values
    app_version: str | None = None
    disable_external_push: bool | None = None
    disable_buildversions: bool | None = None
    image_license: str = ""
    install_version: str | None = None
    safe_version: str | None = None
    skip_bom_stage: bool | None = None
    updated: bool = False

    # dicts and lists
    base_images: list[str] = field(default_factory=list)
    docker: dict[str, Any] = field(default_factory=dict)
    depends: list[str] = field(default_factory=list)
    tags: list[str] = field(default_factory=list)

    # image specific attributes, not direct in Python code used
    user: str | None = None
    uid: int | None = None
    gid: int | None = None
    port: int | None = None


class ImageConfig(object):
    def __init__(self, config: dict[str, Any]) -> None:
        self.config = ImageConfigAttributes.create_from_dict(config)
        self.buildversion: str | None = None
        self.buildversion_prod_ext: str | None = None
        self.buildversion_prod_int: str | None = None
        self.src_image_name: str | None = None
        self.build_name: str | None = None
        self.build_tag: str | None = None
        if os.environ.get("DEVELOP") == "1":
            self.verify = False
        else:
            self.verify = True
        if os.environ.get("DEBUG") == "1":
            log.setLevel(logging.DEBUG)
        else:
            log.setLevel(logging.INFO)

    def _add_project_and_registry(
        self, img_name: str, with_project: bool = False, with_registry: bool = False
    ) -> str:
        registry = self.config.registry
        project = self.config.project
        name = ""
        if with_registry:
            name += registry + "/"
        if with_project:
            name += project + "/"
        name += img_name
        return name

    def basename(self, with_project: bool = False, with_registry: bool = False) -> str:
        return self._add_project_and_registry(self.config.basename, with_project, with_registry)

    def name_with_tag(self, with_project: bool = False, with_registry: bool = False) -> str:
        return self._add_project_and_registry(self.config.name, with_project, with_registry)

    def baseimage_name_with_tag(
        self, with_project: bool = False, with_registry: bool = False
    ) -> str:
        return self.name_with_tag(with_project, with_registry) + "-unhardened"

    def name_and_tag(
        self, with_project: bool = False, with_registry: bool = False
    ) -> tuple[str, str]:
        name = self.name_with_tag(with_project, with_registry)
        splitted = name.rsplit(":", 1)
        return splitted[0], splitted[1]

    def tag(self) -> str:
        _, tag = self.name_and_tag()
        return tag

    def name(self) -> str:
        return self.config.name

    def get_safe_version(self) -> str | None:
        # let the user of the image template decide, if the safe_version line is used
        # so we catch the different cases where safe_version or app_version are defined or not
        if self.config.safe_version:
            # allow template creators to define another value for safe_version,
            # so we check chars again for safety
            safe_version = replace_forbidden_chars(self.config.safe_version)
        elif self.config.app_version:
            safe_version = replace_forbidden_chars(self.config.app_version)
        else:
            safe_version = None
        return safe_version

    def get_tags_for_repository(self, repository: str) -> list[str]:
        """Request list of tags for repository from registry."""
        registry = self.config.registry
        project = self.config.project
        user = self.config.registry_user
        password = self.config.registry_password
        tags = get_tags_for_repository(registry, project, repository, user, password)
        return tags

    def _get_latest_build_version(self) -> int | None:
        """
        Calculates the latest build version by requesting all tags of the repository and
        matching the dash-b-num pattern to get the highest build version of existing tags.
        We have to check all existing tags and cannot just check the tag defined in the image config,
        because we have to handle cases, where the app version included in the tag is changed in the config.
        We want to continue the line of existing build version numbers.
        """
        repository = self.basename()
        app_version = self.get_safe_version()
        tags = self.get_tags_for_repository(repository)
        log.debug("The following tags were found in repository %s: %s", repository, tags)
        distro_codename = self.config.distro_codename
        build_versions = []

        if app_version is None:
            # for images without app version (e.g. linux distribution base images), we match
            # the tag to be dash-b-num
            pattern = rf"^{distro_codename}-production-b(\d+)$"
            for tag in tags:
                match = re.match(pattern, tag)
                if match:
                    build_versions.append(int(match.group(1)))
        else:
            # match all tags beginning with app_version and return the build version integer in the list
            pattern = rf"(.*)-{distro_codename}-production-b(\d+)$"
            for tag in tags:  # NOSONAR
                match = re.match(pattern, tag)
                if match and match.group(1) == app_version:
                    build_versions.append(int(match.group(2)))  # NOSONAR

        log.debug("Found the following build versions: %s", build_versions)
        if not build_versions:
            return None
        latest_build_version = max(build_versions)
        return latest_build_version

    def _get_new_build_version(self) -> int:
        """Returns the incremented build version for a given app version"""
        existing_build_version = self._get_existing_build_version()
        return existing_build_version + 1

    def _get_existing_build_version(self) -> int:
        """Returns the incremented build version for a given app version"""
        latest_build_version = self._get_latest_build_version()
        if latest_build_version is None:
            latest_build_version = 0
        existing_build_version = latest_build_version
        return existing_build_version

    def _prefix_tag(self, build_version: int, image_type: str = "production") -> str:
        app_version = self.get_safe_version()
        distro_codename = self.config.distro_codename
        if app_version is None:
            build_tag = f"{distro_codename}-{image_type}-b{build_version}"
        else:
            app_version = replace_forbidden_chars(app_version)
            build_tag = f"{app_version}-{distro_codename}-{image_type}-b{build_version}"
        return build_tag

    def _prefix_new_build_tag(self, image_type: str = "production") -> str:
        """Return a prefixed build version tag for a given app version"""
        new_build_version = self._get_new_build_version()
        return self._prefix_tag(new_build_version, image_type)

    def _prefix_build_tag(self, image_type: str = "production") -> str:
        """Return a prefixed build version tag for a given app version"""
        build_version = self._get_existing_build_version()
        return self._prefix_tag(build_version, image_type)

    def _get_full_path(self, with_registry: bool = True) -> str:
        repository = self.basename()
        path = "{}/{}".format(self.config.project, repository)
        if with_registry:
            path = self.config.registry + "/" + path
        return path

    def get_new_image_name(self) -> tuple[str, str]:
        """Returns the name and build_tag for a given image config"""
        build_tag = self._prefix_new_build_tag()
        build_name = self._get_full_path(with_registry=True)
        return build_name, build_tag

    def _new_build_tag(self, image_type: str = "production") -> str:
        build_tag = self._prefix_new_build_tag(image_type)
        return build_tag

    def get_build_tag(self, image_type: str = "production") -> str:
        build_tag = self._prefix_build_tag(image_type)
        return build_tag

    def get_image_name_opencode(
        self, image_type: str = "production", cached: bool = False
    ) -> tuple[str, str]:
        """Return the opencode image name and tag"""
        if cached and self.build_name is not None and self.build_tag is not None:
            return self.build_name, self.build_tag
        else:
            opencode_path = os.environ["OPENCODE_CONTAINER_PATH"]
            build_tag = self._new_build_tag(image_type)
            img_name = self.config.basename
            build_name = f"{opencode_path}/{img_name}"
            self.build_name = build_name
            self.build_tag = build_tag
            return build_name, build_tag

    def get_target_tag(self) -> str:
        if self.config.safe_version:
            target_tag = str(self.get_safe_version()) + f"-{self.config.distro_codename}-production"
        else:
            target_tag = self.config.name.rsplit(":", 1)[1] + "-production"
        return target_tag

    def get_src_name(self) -> str:
        opencode_img_name, _ = self.get_image_name_opencode(image_type="src")
        return opencode_img_name

    def get_targetversion(self) -> str:
        target_tag = self.get_target_tag()
        opencode_img_name, _ = self.get_image_name_opencode()
        targetversion = f"{opencode_img_name}:{target_tag}"
        return targetversion

    def get_buildversion(self, cached: bool = False) -> str:
        if cached and self.buildversion:
            return self.buildversion
        else:
            opencode_img_name, build_version_tag = self.get_image_name_opencode()
            buildversion = f"{opencode_img_name}:{build_version_tag}"
            self.buildversion = buildversion
            return buildversion

    def get_prod_name_int(self) -> str:
        return self.basename(with_project=True, with_registry=True)

    def get_prod_name_ext(self) -> str:
        return self.get_image_name_opencode()[0]

    def get_targetversion_prod_int(self) -> str:
        prod_name_int = self.get_prod_name_int()
        target_tag = self.get_target_tag()
        targetversion_prod = f"{prod_name_int}:{target_tag}"
        return targetversion_prod

    def get_targetversion_prod_ext(self) -> str:
        prod_name_ext = self.get_prod_name_ext()
        target_tag = self.get_target_tag()
        targetversion_prod = f"{prod_name_ext}:{target_tag}"
        return targetversion_prod

    def get_buildversion_prod_ext(self, cached: bool = False) -> str:
        if cached and self.buildversion_prod_ext:
            return self.buildversion_prod_ext
        else:
            prod_name_ext = self.get_prod_name_ext()
            _, build_version_tag = self.get_image_name_opencode()
            buildversion_prod_ext = f"{prod_name_ext}:{build_version_tag}"
            self.buildversion_prod_ext = buildversion_prod_ext
            return buildversion_prod_ext

    def get_buildversion_prod_int(self, cached: bool = False) -> str:
        if cached and self.buildversion_prod_int:
            return self.buildversion_prod_int
        else:
            prod_name_int = self.get_prod_name_int()
            build_version_tag = self.get_build_tag()
            buildversion_prod_int = f"{prod_name_int}:{build_version_tag}"
            self.buildversion_prod_int = buildversion_prod_int
            return buildversion_prod_int

    def get_src_image_name(self, cached: bool = False) -> str:
        if cached and self.src_image_name:
            return self.src_image_name
        else:
            src_name = self.get_src_name()
            _, build_version_tag_src = self.get_image_name_opencode(image_type="src")
            src_image_name = f"{src_name}:{build_version_tag_src}"
            self.src_image_name = src_image_name
            return src_image_name

    def get_prod_image_name(self) -> str:
        prod_name_int = self.get_prod_name_int()
        target_tag = self.get_target_tag()
        prod_image_name = f"{prod_name_int}:{target_tag}"
        return prod_image_name
