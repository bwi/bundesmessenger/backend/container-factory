# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import re
from argparse import Namespace
from typing import Any, Iterable

from factory.PipelineWriter import PipelineWriter
from factory.update_images import ImageBuilder
from factory.util.utils import remove_duplicates, should_build_all


def should_process_image(
    image_name: str, images_list: list[str], changed_images: Iterable[str] | str
) -> bool:
    if (
        (len(images_list) == 0 and image_name in changed_images)
        or should_build_all()
        or image_name in images_list
        or image_name.split(":")[0] in images_list
    ):
        return True
    return False


def generator(
    image_configs: dict[str, Any],
    images: list[str] | str | None,
    changed_images: Iterable[str] | str,
    stages: str | None,
) -> None:
    with open("pipelines-gitlab-ci.yml", "w+", encoding="utf-8") as f:
        f.write(PipelineWriter.parent_job_template())

        if isinstance(images, list):
            images_list = images
        elif images is not None:
            images_list = [images]
        elif images is None:
            images_list = []

        num_childs = 0
        for image_name in image_configs:
            if should_process_image(image_name, images_list, changed_images):
                image_config = image_configs[image_name].config
                depends = image_config.depends
                image = image_config.name
                f.write(PipelineWriter.child_pipeline_job_template(stages, f"{image}", depends))
                num_childs += 1
        if num_childs == 0:
            f.write(PipelineWriter.child_pipeline_dummy_job_template())


def parse_arguments() -> tuple[Namespace, list[str]]:
    parser = argparse.ArgumentParser(description="update images")
    parser.add_argument("--config", default="config.yaml", help="configuration file name")
    parser.add_argument(
        "--keep", default=False, action="store_true", help="keep files generated from templates"
    )
    parser.add_argument(
        "--stages",
        default=None,
        help="stages to execute (check,build,test,strip,bom,push,scan), default: all",
    )
    parser.add_argument("--images", default=None, help="list of images to build, default: all")
    parser.add_argument(
        "--internal-scanner",
        default=False,
        action="store_true",
        help="use internal scanner instead of harbor",
    )
    args, unknown = parser.parse_known_args()
    return args, unknown


def main() -> None:
    args, unknown = parse_arguments()

    if args.images:
        images = args.images.split(",")
    else:
        # only if images are not set on command line add images from special tags in the commit message
        # f.e. [images=ubuntu:latest-jammy,python:3.9-jammy,synapse:1.94.0-jammy]
        commit_message = os.getenv("CI_COMMIT_MESSAGE", "")
        image_tag_pattern = re.compile(r"\[images=([^\]]+)\]")
        image_tags = []
        matches = image_tag_pattern.findall(commit_message)
        for match in matches:
            tags = match.split(",")
            image_tags.extend(tags)
        images = image_tags
    env_extras = {}
    for i in unknown:
        parts = i.split("=", 1)
        if len(parts) == 2:
            env_extras[parts[0]] = parts[1]

    builder = ImageBuilder(
        config=args.config, keep=args.keep, stages=args.stages, images=images, env_extras=env_extras
    )
    builder.load_image_definitions()
    changed_images = builder.retrieve_changed_images()
    image_tags = list()
    base_images = set()
    for img in images:
        if ":" in img:
            # with tag
            image_tags += [img]
            base_images.update(builder.get_all_base_images(builder.image_configs[img]))
        else:
            # without tag
            image_tags += [
                image_config.config.name
                for image_config in builder.get_all_configs_for_basename(img)
            ]
            base_images.update(builder.get_all_base_images_for_basename(img))
    images = list(base_images) + image_tags
    images = remove_duplicates(images)
    generator(builder.image_configs, images, changed_images, args.stages)


if __name__ == "__main__":
    main()
