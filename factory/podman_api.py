# Copyright 2025 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import time
from typing import Iterator

from podman import PodmanClient
from podman.domain.images import Image
from podman.errors.exceptions import APIError

from factory.util.exceptions import PushError, TransientError
from factory.util.utils import get_socket

log = logging.getLogger(__name__)
logging.basicConfig(format="%(levelname)s %(message)s")

PODMAN_API_TIMEOUT = 60 * 15

PODMAN_LOGIN_MAX_ATTEMPTS = 3
PODMAN_LOGIN_DELAY = 3

PUSH_MAX_ATTEMPTS = 3
PUSH_DELAY = 5

uri, _ = get_socket()
print(uri)


class BuilderPodmanAPI(object):
    def __init__(self) -> None:
        self.podman_client = PodmanClient(base_url=uri, timeout=PODMAN_API_TIMEOUT)
        self.auth_lookup: dict[str, tuple[str, str]] = {}

    def login(self, user: str, password: str, registry: str) -> str:
        """
        Attempt to log in to a given registry using the PodmanClient.
        This is a test/validation and saves the credentials in a dict.
        This login has no impact on the PodmanClient.
        podman-py does not save reliable the login information.

        Args:
            user: The username for authentication.
            password: The password for authentication.
            registry: The registry URL to log in to.
        Raises:
            TransientError: If a transient error is detected during the login attempt.
            APIError: If the Podman API returns an error during the login attempt.
        """
        for attempt in range(1, PODMAN_LOGIN_MAX_ATTEMPTS + 1):  # +1 for initial attempt
            try:
                response = self.podman_client.login(user, password, registry=registry)
                log.info("Podman login to %s successful: %s", registry, response)
                self.auth_lookup[registry] = (user, password)
                break
            except APIError as e:
                error_message = repr(e)
                log.exception("Login attempt %s failed: %s", attempt, error_message)

                if any(
                    msg in error_message for msg in ["POST operation failed", "Service Unavailable"]
                ):
                    if attempt == PODMAN_LOGIN_MAX_ATTEMPTS:
                        raise TransientError(f"Login failed after {attempt} attempts") from e

                    log.info("Retrying login in %s seconds...", PODMAN_LOGIN_DELAY)
                    time.sleep(PODMAN_LOGIN_DELAY)
                else:
                    # Re-raise if not a known transient error
                    raise
        return response

    def push(self, image: Image, name: str, tag: str, message: str) -> None:
        """
        Push image. Add function to retry pushing image to registry in case of failure,
        log message and authentication.

        Raises:
            PushError: If pushing the image to the registry fails
        """
        image.tag(name, tag)
        auth_config = self._get_auth_config(name)

        for attempt in range(1, PUSH_MAX_ATTEMPTS + 1):  # +1 for initial attempt
            try:
                log.info("%s (attempt #%s)", message, attempt)
                self.podman_client.images.push(name, tag, auth_config=auth_config)
                break
            except APIError as e:
                log.exception("Error attempt %s pushing to registry: %s", attempt, e)
                if attempt == PUSH_MAX_ATTEMPTS:
                    raise PushError(
                        f"Pushing image to registry failed after {attempt} attempts."
                    ) from e

                log.info("Retrying push in %s seconds...", PUSH_DELAY)
                time.sleep(PUSH_DELAY)

    def pull(self, name: str, tag: str | None = None) -> Image | list[Image] | Iterator[str]:
        """
        Pull image from registry and return it. Add function for authentication.

        Raises:
            APIError: If pulling the image from the registry fails
        """
        auth_config = self._get_auth_config(name)
        return self.podman_client.images.pull(name, tag, auth_config=auth_config)

    def _get_auth_config(self, name: str) -> dict[str, str] | None:
        """
        Split image name at first slash to get registry and return auth config
        for this when at login this was written.
        """
        # in feature improve this to use ImageConfig
        registry = name.split("/", maxsplit=1)[0]
        credentials = self.auth_lookup.get(registry)
        if credentials:
            user, pwd = credentials
            return {"username": user, "password": pwd}
        return None
