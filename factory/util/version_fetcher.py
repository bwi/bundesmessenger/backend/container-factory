# Copyright 2025 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
import subprocess
from abc import ABC, abstractmethod

import requests
from requests.auth import HTTPBasicAuth
from semver import Version

from factory.util.utils import get_semver_version


class VersionFetcher(ABC):
    """Abstract base class for retrieving versions from different sources."""

    @abstractmethod
    def get_latest_and_tags(self) -> tuple[Version, list[Version]]:
        """
        Gets the latest version and tags from a repository.

        Returns:
            Latest version and list of all versions.
        """

    @staticmethod
    def get_semver_version(version: str) -> Version | None:
        """
        Converts a tag to a SemVer version, if possible.
        Fix leading "v" or "V" in version string.
        Returns `None` if the format doesn't match.
        """
        return get_semver_version(version)


class GitVersionFetcher(VersionFetcher):
    """Fetcher for Git repositories."""

    def __init__(self, remote: str) -> None:
        self.remote = remote

    def _get_remote_tags(self, remote_url: str) -> list[str]:
        """Request tags and exclude dereferenced tags with --refs"""
        result = subprocess.run(
            ["git", "ls-remote", "--tags", "--refs", remote_url],
            stdout=subprocess.PIPE,
            text=True,
            check=False,
        )
        tags = []
        for line in result.stdout.splitlines():
            parts = line.split("\t")
            if len(parts) == 2:
                tag_name = parts[1].replace("refs/tags/", "")
                tags.append(tag_name)
        return tags

    def get_latest_and_tags(self) -> tuple[Version, list[Version]]:
        """Returns the latest version and all versions from a Git repository."""
        tags = [
            tag
            for tag in (self.get_semver_version(tag) for tag in self._get_remote_tags(self.remote))
            if tag is not None
        ]
        latest = max(tags)
        return latest, tags


class NexusVersionFetcher(VersionFetcher):
    """Fetcher for Nexus repositories."""

    def __init__(
        self, application: str, repository: str, server: str, user: str, password: str
    ) -> None:
        self.application = application
        self.artifact_server = server
        self.artifact_user = user
        self.artifact_password = password
        self.artifact_repo = repository

    def get_latest_and_tags(self) -> tuple[Version, list[Version]]:
        """Provides the latest version and all versions from a Nexus repository."""
        auth = HTTPBasicAuth(self.artifact_user, self.artifact_password)
        base_url = (
            f"https://{self.artifact_server}/service/rest/v1/assets?repository={self.artifact_repo}"
        )

        response = requests.get(base_url, auth=auth)
        data = response.json()
        paths = [item["path"] for item in data["items"]]

        # Pagination through Nexus API
        continuation_token = data.get("continuationToken")
        while continuation_token:
            url = f"{base_url}&continuationToken={continuation_token}"
            response = requests.get(url, auth=auth)
            continuation_token = data.get("continuationToken")
            data = response.json()
            paths += [item["path"] for item in data["items"]]

        files = [item.split("/")[-1] for item in paths]
        artifacts = [item for item in files if self.application in item and "-rc" not in item]

        tags = []
        for item in artifacts:
            res = re.match(rf"^{self.application}-(\d+\.\d+\.\d+).tgz", item)
            if res:
                version = self.get_semver_version(res.group(1))
                assert version  # the matching regex is a valid semver version
                tags.append(version)

        latest = max(tags)
        return latest, tags


class GitLabVersionFetcher(VersionFetcher):
    """Fetcher for GitLab registries."""

    def __init__(self, remote: str) -> None:
        """
        Args:
            remote: API URL for the project packages, e.g.,
                    https://gitlab.example.org/api/v4/projects/[project_id]/packages.
        """
        self.remote = remote

    def get_latest_and_tags(self) -> tuple[Version, list[Version]]:
        """Fetch the latest package version and all package versions from a GitLab registry."""
        response = requests.get(self.remote)
        response.raise_for_status()

        versions = [
            tag
            for tag in (self.get_semver_version(pkg["version"]) for pkg in response.json())
            if tag is not None
        ]
        latest = max(versions)
        return latest, versions
