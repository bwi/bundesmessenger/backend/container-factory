class ContainerFactoryError(Exception):
    """Base class for ContainerFactory exceptions."""


class BuildprocessError(ContainerFactoryError):
    """Error occurred during build operation."""


class PushError(ContainerFactoryError):
    """Error occurred during push to registry."""


class ContainerRuntimeError(ContainerFactoryError):
    """Error occurred during execute the container."""


class TransientError(ContainerFactoryError):
    """Raised for transient errors to trigger second run of build process."""


class DependencyRuntimeError(ContainerFactoryError):
    """Raised for errors for missing external dependencies at runtime."""


class BearerTokenError(ContainerFactoryError):
    """Exception raised when retrieving a bearer token fails."""
