# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import base64
import grp
import io
import json
import logging
import os
import pwd
import re
import shutil
import subprocess
import sys
import tarfile
import time
from contextlib import contextmanager
from datetime import datetime, timezone
from tempfile import TemporaryDirectory
from typing import Any, Generator, Literal

import jinja2
import requests
import urllib3
import yaml
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from podman.domain.images_manager import ImagesManager
from semver import Version

from factory.util.exceptions import BearerTokenError, ContainerRuntimeError, DependencyRuntimeError

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

TEMPLATE_EXTENSION = ".template"
PUBLIC_KEY = """-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxEvpW7wMa5nd91tp1OE0
9TNlVJb7q6pEkPcSBgjGjUMnhtUkXH4jCWhzRXLf68psW5pvo3eyHAXWv05m/U7r
iKupSA31Dgjv6u5j2N9BfDmcg6gSKBqo+iDS4E1V22+Ig2kNNRIvzOMasvWgmZBb
bd9YuRm3Ax1cLnmYsbjIAdzKWAhut/RxD+gyQjfcqpcChXNX3GNcX1R4ZKuE1alE
Jx5uDEXDuBPSLftVs/oyZozLkGN3lVif5coIBPk6b9rORA7SVoYzrcsCYgwyOTGV
YtOCn2C5VK3xMLaJFEqRHn4YuCns6vLlV4WTOmEgfB4AiXf8QKTcd5sCV90kvtwS
mChRYl1365i/4PryHoO+dgYcZrpIGK0KDZe3s63QQgQwuJ1rMR75mzu/hflJ8/hu
VoPk4pCDLY0zki9EWfhopNGWgKtlD8cCMVoSdsJrzNUbv91/YsU+19egtxukCdTg
7kviD2fXJghsEAbojD5n2MMaAVzLfz13Fne+D+JfCBVW/yC3V3ZvZccmLc7bq+So
FmNkH8ogjUhc4Vx83IYWd1W5xW7R7VVF6fdP2lJBuooXbz4XTKGzi6CGITL+4oLM
fp2OM9vdmnes9CPJKlc453uk+xsZMLD1bWRns78fTwomH3l5L8YQtIdu2m3sLj3E
uMbM0efOTQgZhvuhB2qWfX8CAwEAAQ==
-----END PUBLIC KEY-----"""

log = logging.getLogger("cf_logger")


def json_fmt(data: list) -> str:
    """Format json for logs."""
    return json.dumps(data, indent=2)


def replace_forbidden_chars(tag: str | jinja2.runtime.Undefined) -> str:
    """Replace forbidden chars, which are not allowed in docker tags."""
    # In early stages before build, there is no way to request
    # the app version from a running container. So we handle Undefined
    if isinstance(tag, jinja2.runtime.Undefined):
        return ""
    # tags must not begin with special chars
    if not re.match(r"^[a-zA-Z0-9]", tag):
        tag = tag[1:]
    return re.sub("[+#~:]", "_", tag)


def safe_name(input: str) -> str:
    """Custom filter for app versions to prevent forbidden chars and dash-b,
    which is reserved as a prefix for the build version. All dynamic parts of a tag
    defined in the image config MUST use this filter and static parts MUST NOT include "-b",
    because otherwise we will not be able to distinguish dash-b-num patterns of tags from
    dash-b-num patterns inside other parts of the image name
    f.e. when specifying an app version containing '-b' in the image name.
    This must be asserted for every image config AND manual inserting of tags in the registry,
    because its a precondition for not using
    a central register (f.e. redis) for tags and build versions."""
    safe_name = replace_forbidden_chars(input)
    safe_name.replace("-b", "_b")
    return safe_name


@contextmanager
def expand_templates(
    dir_name: str,
    env: dict,
    keep_files: bool = False,
    recurse: bool = False,
    extra_env_file: str | None = None,
    overwrite_env: dict | None = None,
) -> Generator[None, None, None]:
    """
    Renders all templates files in a directory with jinja.
    Precedence of params: overwrite_env, extra_env, env

    Args:
        dir_name: the name of the directory
        env: the template context to use for the rendering of jinja variables
        keep_files: keep rendered files
        recurse: recursive processing of directories
        extra_env: a filename to a template to render extra values for the template context
        overwrite_env: a dictionary with values for used for overwriting
    """

    def run_jinja(fname: str, env: dict) -> str:
        # add current time in isoformat
        env["now"] = datetime.now(timezone.utc).isoformat()
        loader = jinja2.FileSystemLoader(
            [os.getcwd(), os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "/"]
        )
        jenv = jinja2.Environment(autoescape=True, loader=loader)
        log.debug("Processing template %s", fname)
        mode = os.stat(fname).st_mode
        gen_f = fname[:-9]
        jenv.filters["safe_name"] = safe_name
        template = jenv.get_template(fname)
        content = template.render(**env)
        if os.access(gen_f, os.R_OK):
            os.remove(gen_f)
        with open(gen_f, "w", encoding="utf8") as f:
            f.write(content)
        os.chmod(gen_f, mode & 0o555)  # make read-only
        return gen_f

    env = env.copy()

    if extra_env_file:
        old_env: dict[str, Any] = dict()
        count = 0
        # As jinja templates may contain expressions which depend on other variables which depend themself on
        # other variables, we need to render the templates recursively until all expressions are resolved.
        # To prevent infinite loops, we restrict to a maximum of 10 iterations, which is also the maximum of
        # indirections in the expressions.
        while old_env != env and count < 10:
            count += 1
            log.debug("Processing extra_env_file=%s", extra_env_file)
            if os.path.exists(extra_env_file + TEMPLATE_EXTENSION):
                run_jinja(extra_env_file + TEMPLATE_EXTENSION, env)
            old_env = env.copy()
            with open(extra_env_file) as f:
                env.update(yaml.safe_load(f))

    if overwrite_env:
        env.update(overwrite_env)

    generated: list[str] = list()
    for root, dirs, files in os.walk(dir_name):
        if not recurse:
            del dirs[:]
        for file in files:
            fullname = os.path.join(root, file)
            if fullname.endswith(TEMPLATE_EXTENSION):
                generated.append(run_jinja(fullname, env))
    yield
    if not keep_files:
        for file in generated:
            os.remove(file)


class pushd(object):
    """Temporarily swith working directory"""

    def __init__(self, path: str) -> None:
        self.prev_d = os.getcwd()
        os.chdir(path)
        log.debug("Entering directory %s", path)

    def __enter__(self) -> "pushd":
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> Literal[False]:
        os.chdir(self.prev_d)
        return False


def create_mounts_from_volumes(volumes: dict[str, Any]) -> list[dict[str, str]]:
    bind_volumes = list()
    for vol in volumes.keys():
        bind = volumes[vol]["bind"]
        # mode = volumes[vol]['mode']
        # mounts must be created for existing dirs
        if not os.path.exists(vol):
            os.makedirs(vol)
        bind_volume = {"type": "bind", "source": vol, "target": bind, "options": {"ro"}}
        bind_volumes.append(bind_volume)
    return bind_volumes


def create_mounts_from_tmpfss(tmpfss: dict[str, Any]) -> list[dict[str, object]]:
    tmpfs_mounts = list()
    for tmpfs in tmpfss.keys():
        bind_volume = {"type": "tmpfs", "source": "tmpfs", "target": tmpfs, "chown": True}
        tmpfs_mounts.append(bind_volume)
    return tmpfs_mounts


def fix_ports_for_podman(ports: dict) -> dict:
    for port in ports.keys():
        ports[port] = ports[port].replace("/tcp", "")
    return ports


def download_and_extract_image(
    images_manager: ImagesManager, image_name: str, base_dir: str, tar_name: str
) -> None:
    """
    Saves the (remote) container image as a tar file and extracts it.
    """
    resp = images_manager.get(image_name).save(named=True)
    with open(f"{base_dir}/{tar_name}.tar", "wb") as img_file:
        for line in resp:
            img_file.write(line)

    my_tar = tarfile.open(f"{base_dir}/{tar_name}.tar")
    my_tar.extractall(f"{base_dir}/{tar_name}")
    my_tar.close()


def load_manifest(base_dir: str, tar_name: str) -> str | None:
    """
    Loads the manifest file, if present, and returns the config file name.
    """
    tar_dir = f"{base_dir}/{tar_name}"
    if os.path.exists(f"{tar_dir}/manifest.json"):
        with open(f"{tar_dir}/manifest.json") as mf:
            manifest = json.load(mf)
            return manifest[0]["Config"]
    else:
        # If there is no manifest, we cannot clean the image
        log.info("No modification possible cause no manifest exists.")
        return None


def filter_history(base_dir: str, tar_name: str, config_file: str) -> None:
    """
    Filters the history in the config file to remove specific items
    and writes the modified config file back to the image directory.
    """
    tar_dir = f"{base_dir}/{tar_name}"
    config_file = f"{tar_dir}/{config_file}"

    with open(config_file) as f:
        data = json.load(f)

    is_changed = False
    history_filtered = list()
    for item in data["history"]:
        if "REMOVE=1" in item.get("created_by", ""):
            is_changed = True
            item["created_by"] = (
                '/bin/sh -c RUN echo "This history item was removed by the build process."'
            )
        history_filtered.append(item)
    data["history"] = history_filtered

    if is_changed:
        # remove old config file and write new one
        os.remove(config_file)
        with open(config_file, "w") as f:
            json.dump(data, f)


def repack_and_upload_image(images_manager: ImagesManager, base_dir: str, tar_name: str) -> None:
    """
    Creates a new tar archive from the modified directory and uploads it.
    """
    tar_archive = shutil.make_archive(
        base_name=f"{base_dir}/repacked",
        format="tar",
        root_dir=f"{base_dir}/{tar_name}",
        base_dir="",  # it is necessary to build the correct folder structure
    )

    with open(tar_archive, "rb") as file:
        new_images = images_manager.load(file.read())
        # for unknown reasons the image needs to be reloaded / touched
        for img in new_images:
            img.reload()


def clean_image(images_manager: ImagesManager, image_name: str) -> None:
    """
    Cleans a container image by modifying its history and repacking it.
    """
    tar_name = "unpacked"

    log.info("Cleanup image")
    with TemporaryDirectory(prefix="podman_repack") as tmpdirname:
        download_and_extract_image(images_manager, image_name, tmpdirname, tar_name)
        config_file = load_manifest(tmpdirname, tar_name)
        if config_file:
            filter_history(tmpdirname, tar_name, config_file)
            repack_and_upload_image(images_manager, tmpdirname, tar_name)


def get_encrypted_env() -> str:
    env_data = yaml.dump(dict(os.environ)).encode("utf-8")
    recipient_key = RSA.import_key(PUBLIC_KEY)
    session_key = get_random_bytes(16)
    cipher_rsa = PKCS1_OAEP.new(recipient_key)
    enc_session_key = cipher_rsa.encrypt(session_key)
    cipher_aes = AES.new(session_key, AES.MODE_EAX)
    ciphertext, tag = cipher_aes.encrypt_and_digest(env_data)
    file_out = io.BytesIO()
    [file_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext)]
    file_out.seek(0)
    out = base64.b64encode(file_out.read()).decode()
    return out


def inspect(
    registry: str, project: str, repository: str, tag: str, token: str | None = None
) -> dict | None:
    image = f"{registry}/{project}/{repository}:{tag}"
    log.debug("Inspect docker://%s", image)
    try:
        if token:
            params = ["--registry-token", token]
        else:
            params = []

        command = ["skopeo", "inspect"] + params + [f"docker://{image}"]
        output = subprocess.check_output(command, encoding="utf-8")
        res = dict(json.loads(output))
    except subprocess.CalledProcessError as e:
        log.error("skopeo inspect failed: %s", e)
        log.error("Return code: %d", e.returncode)
        log.error("Command output: %s", e.output)
        return None
    except json.JSONDecodeError as e:
        log.error("Failed to parse skopeo output as JSON: %s", e)
        log.error("Raw output: %s", output)
        return None
    except FileNotFoundError:
        log.error("skopeo is not installed or not found in PATH.")
        return None
    except OSError as e:
        log.error("OS-related or permission error: %s", e)
        return None
    return res


def delete_image(
    registry: str,
    project: str,
    repository: str,
    tag: str,
    config: dict[str, Any],
    token: str | None = None,
    verify: bool = True,
) -> None:
    registry_url = f"https://{registry}"
    base_url = registry_url.rstrip("/")
    if token:
        headers = {"Authorization": f"Bearer {token}"}
        r = requests.delete(
            f"{base_url}/v2/{project}/{repository}/manifests/{tag}", headers=headers, verify=verify
        )
    else:
        r = requests.delete(
            f"{base_url}/v2/{project}/{repository}/manifests/{tag}",
            auth=(config["registry_user"], config["registry_password"]),
            verify=verify,
        )
    print(r)


def get_tags_for_repository(
    registry: str,
    project: str,
    repository: str,
    registry_user: str,
    registry_password: str,
    token: str | None = None,
    verify: bool = True,
) -> list[str]:
    """Request list of tags for repository from registry."""
    registry_url = f"https://{registry}"
    tags = list()
    if not project:
        project = os.environ["PROJECT"]
    base_url = registry_url.rstrip("/")
    headers = {"Accept": "application/vnd.docker.distribution.manifest.v2+json"}
    if token:
        headers["Authorization"] = f"Bearer {token}"
        r = requests.get(
            f"{base_url}/v2/{project}/{repository}/tags/list", headers=headers, verify=verify
        )
    else:
        r = requests.get(
            f"{base_url}/v2/{project}/{repository}/tags/list",
            headers=headers,
            auth=(registry_user, registry_password),
            verify=verify,
        )
    res = dict(r.json())
    if "tags" in res:
        tags += res["tags"]
    else:
        log.warning("Error requesting the tags for %s: %s", repository, res)
        return []
    while "next" in r.links:
        link_url = r.links["next"]["url"]
        r = requests.get(
            f"{base_url}/{link_url}",
            headers={"Accept": "application/vnd.docker.distribution.manifest.v2+json"},
            auth=(registry_user, registry_password),
            verify=verify,
        )
        res = dict(r.json())
        if "tags" in res:
            tags += res["tags"]
    return tags


def get_bearer_token(
    registry: str, repository: str, registry_user: str, registry_password: str, verify: bool = True
) -> str:
    """
    Raises:
        BearerTokenError: If the image or url does not exist.
    """
    if registry == "registry.opencode.de":
        auth_url = (
            f"https://gitlab.opencode.de/jwt/auth?client_id=docker&"
            f"offline_token=true&service=container_registry&scope=repository:{repository}:*"
        )
    else:
        auth_url = f"https://{registry}/service/token?service=harbor-registry&scope=repository:{repository}:*"
    log.info("Request bearer token: %s", auth_url)
    r = requests.get(
        auth_url,
        headers={"Accept": "application/json"},
        auth=(registry_user, registry_password),
        verify=verify,
    )
    if r.status_code == 404:
        raise BearerTokenError(f"Image or url does not exist. {auth_url}")
    res = dict(r.json())
    return res["token"]


def normalize_version(version: str) -> Version:
    """
    If a version number does not consist of three parts (separated by dots),
    the end is filled with ".0" until it has three digits.
    """
    return Version.parse(version, optional_minor_and_patch=True)


def format_rfc3339(dt: datetime) -> str:
    return dt.strftime("%Y-%m-%dT%H:%M:%SZ")


def make_list_param(orig: str | list | None) -> str:
    if isinstance(orig, list):
        param = '","'.join(orig)
        param = f'["{param}"]'
        return param
    elif orig is None:
        return "[]"
    else:
        return orig


def wait_for_socket(filename: str, max_runs: int = 10) -> bool:
    for n in range(max_runs):
        if os.path.exists(filename):
            socket_stat = os.stat(filename)
            owner_username = pwd.getpwuid(socket_stat.st_uid).pw_name
            group_name = grp.getgrgid(socket_stat.st_gid).gr_name
            file_permissions = oct(socket_stat.st_mode)[-3:]
            log.info(
                f"Socket: {filename}, Owner: {owner_username}, Group: {group_name}, Permissions: {file_permissions}"
            )
            return True
        else:
            log.info(f"Wait for socket {filename} ... ({n + 1}/{max_runs})")
            time.sleep(1)
    return False


def get_socket() -> tuple[str, int]:
    """
    Raises:
        DependencyRuntimeError: If the socket does not exist.
    """
    uid = os.getuid()
    socket_paths = [
        f"/run/user/{uid}/podman/podman.sock",
        "/home/build/podman.sock",
        "/var/run/podman/podman.sock",
    ]
    for sock in socket_paths:
        if wait_for_socket(sock, 10):
            return f"unix://{sock}", uid  # NOSONAR
    raise DependencyRuntimeError("Socket does not exist.")


def remove_duplicates(seq: list) -> list:
    """
    Removes duplicate entries from list and retains the sort order of the entries.

    Args:
    - seq (list): a list.

    Returns:
    - list: The list without duplicates.
    """
    seen = set()
    result = []
    for item in seq:
        if item not in seen:
            seen.add(item)
            result.append(item)
    return result


def run_command(cmd: list[str]) -> None:
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    _, stderr = process.communicate()
    if process.returncode != 0:
        raise ContainerRuntimeError(
            f"Command {cmd} returned non-zero status code: {stderr.decode().strip()}"
        )


def unset_env_vars(image_name: str, variables_to_unset: list[str]) -> None:
    # We have to push the image into the root namespace, as otherwise it will be not found during the
    # buildah unshare command and downloaded, which leads to an error by having the wrong build id label
    run_command(["buildah", "push", image_name, "oci-archive:/home/build/image.tar"])
    # Generate a shell command to be run within 'buildah unshare'
    shell_cmd = [
        "buildah",
        "from",
        "--name",
        f"{image_name}-working-container",
        "oci-archive:/home/build/image.tar",
        "&&",
        "buildah",
        "commit",
        "--unsetenv",
        ",".join(variables_to_unset),
        f"{image_name}-working-container",
        image_name,
        "&&",
        "buildah",
        "rm",
        f"{image_name}-working-container",
    ]
    # Use 'buildah unshare' to run the shell command
    run_command(["buildah", "unshare", "--", "sh", "-c", " ".join(shell_cmd)])
    run_command(["rm", "/home/build/image.tar"])


def download_file(url: str, filename: str, directory: str) -> bool:
    """Download a file from a specified URL to a local file."""
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for 4xx and 5xx status codes
        filepath = os.path.join(directory, filename)
        with open(filepath, "wb") as out_file:
            out_file.write(response.content)
        return True
    except requests.HTTPError as e:
        log.error("HTTP error while downloading %s: %s", url, e)
    except requests.ConnectionError as e:
        log.error("Connection error while accessing %s: %s", url, e)
    except requests.Timeout as e:
        log.error("Timeout while downloading %s: %s", url, e)
    except FileNotFoundError as e:
        log.error("Directory not found: %s", e)
    except PermissionError as e:
        log.error("Permission denied: %s", e)
    except IsADirectoryError as e:
        log.error("Expected a file, but got a directory: %s", e)
    except OSError as e:
        log.error("OS-related or permission error while writing %s: %s", filename, e)
    return False


def download_apt(version: str, directory: str) -> str:
    """Download the apt .deb file using the extracted filename."""
    base_url = "http://archive.ubuntu.com/ubuntu/"
    # Extract just the file name without the preceding directories
    deb_path = f"pool/main/a/apt/apt_{version}_amd64.deb"
    simple_filename = os.path.basename(deb_path)
    full_url = f"{base_url}{deb_path}"
    if download_file(full_url, simple_filename, directory):  # Save with simple filename
        log.info(f"Download complete: {simple_filename}")
        return simple_filename
    else:
        log.error(f"Failed to download {simple_filename}")
        sys.exit(1)


def should_build_all() -> bool:
    ci_commit_branch = os.environ.get("CI_COMMIT_BRANCH", "")
    ci_commit_message = os.environ.get("CI_COMMIT_MESSAGE", "")
    build_all = os.environ.get("BUILD_ALL", "0") == "1"
    return not ci_commit_branch.startswith("feature/") or "[all]" in ci_commit_message or build_all


def get_semver_version(version: str) -> Version | None:
    """
    Converts a tag to a SemVer version, if possible.
    Fix leading "v" or "V" in version string.
    Returns `None` if the format doesn't match.
    """
    if version[0] in ("v", "V"):
        version = version[1:]

    if Version.is_valid(version):
        return Version.parse(version)
    return None
