# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re
from typing import List

RUNNER = "sde-common"


class PipelineWriter:
    @staticmethod
    def parent_job_template() -> str:
        builder_image = os.environ["BUILDER_IMAGE"]
        parent_job_template = f"""
cache:
  - key: grype
    paths:
      - ".cache/grype"
    when: always
  - key:
      files:
        - requirements.txt
    paths:
      - ".cache/pip"
    when: always

workflow:
  rules:
    - when: always

variables:
  GRYPE_DB_CACHE_DIR: "$CI_PROJECT_DIR/.cache/grype"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

image:
  name: {builder_image}
  entrypoint: [""]
stages:
  - build-images
.basic:
  interruptible: false
  allow_failure: false
  retry:
    max: 2
    when:
      - runner_system_failure
"""
        return parent_job_template

    @staticmethod
    def child_pipeline_job_template(stages: str | None, image: str, depends: List[str] = []) -> str:
        image_job = re.sub("[+#~:]", "_", image)
        child_pipeline_job_template = f"""
build-{image_job}-image:
  extends: .basic
  tags:
    - {RUNNER}
  stage: build-images
  variables:
     BUILD_IMAGE: {image}
     no_proxy: $HTTP_NO_PROXY,podman.sock,localhost
  before_script:
    - export PATH="/home/build/.local/bin:$PATH"
    - pip install -r requirements.txt
    - 'source .default.env'
    - 'if [ $CI_COMMIT_REF_NAME != "main" ]; then source .develop.env; fi'
    - '[ -f .${{CI_COMMIT_REF_NAME}}.env ] && source .${{CI_COMMIT_REF_NAME}}.env'
    - 'echo "Starting build process for ref $CI_COMMIT_REF_NAME."'
    - 'echo "Test build: $TEST_BUILD."'
  script:
    - echo "Starting build process."
    - echo $BUILDER_VERSION
    - echo $FORCE_BUILD
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTPS_PROXY
    - podman system service --time 0 &
    # workaround for CVE-2022-24767 mitigation when the container is not running in the context of root
    - git config --global --add safe.directory $CI_PROJECT_DIR
    - echo $BUILD_IMAGE
    - python3 -m factory.update_images --stages={stages} --images={image}
  artifacts:
    paths:
      - dependency_updated_*
    expire_in: 3 hours"""
        if len(depends) > 0:
            needs_list = ["build-{}-image".format(re.sub("[+#~:]", "_", item)) for item in depends]
            child_pipeline_job_template += f"""
  needs: {needs_list}
"""
        return child_pipeline_job_template

    @staticmethod
    def child_pipeline_dummy_job_template() -> str:
        child_pipeline_job_template = f"""
build-dummy-image:
  extends: .basic
  tags:
    - {RUNNER}
  stage: build-images
  before_script:
    - source .default.env
    - if [ $CI_COMMIT_REF_NAME != "main" ]; then source .develop.env; fi
    - '[ -f .${{CI_COMMIT_REF_NAME}}.env ] && source .${{CI_COMMIT_REF_NAME}}.env'
  script:
    - 'echo "Starting dummy build process for ref $CI_COMMIT_REF_NAME."'
    - 'echo "Test build: $TEST_BUILD."'
"""
        return child_pipeline_job_template
