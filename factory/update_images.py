# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import base64
import glob
import json
import logging
import logging.config
import os
import re
import secrets
import shutil
import subprocess
import sys
import tarfile
import tempfile
import time
import uuid
from contextlib import contextmanager
from dataclasses import asdict
from datetime import datetime, timezone
from graphlib import TopologicalSorter
from http.client import HTTPConnection
from pprint import pformat
from typing import Any, Generator, Iterable, cast

import git
import pexpect
import requests
import yaml
from dateutil.parser import parse
from podman.domain.containers import Container
from podman.domain.images import Image
from podman.errors import NotFound
from podman.errors.exceptions import APIError, ImageNotFound

from factory.config import ImageConfig
from factory.podman_api import BuilderPodmanAPI
from factory.util.exceptions import BuildprocessError, ContainerRuntimeError, TransientError
from factory.util.utils import (
    clean_image,
    create_mounts_from_tmpfss,
    create_mounts_from_volumes,
    download_apt,
    expand_templates,
    fix_ports_for_podman,
    format_rfc3339,
    get_bearer_token,
    get_encrypted_env,
    get_socket,
    inspect,
    json_fmt,
    make_list_param,
    normalize_version,
    pushd,
    replace_forbidden_chars,
    should_build_all,
    unset_env_vars,
)

log = logging.getLogger("cf_logger")
logging.basicConfig(format="%(levelname)s %(message)s")

# renovate: datasource=repology depName=ubuntu_22_04/apt versioning=semver
APT_VERSION = "2.4.13"
IMAGE_CONFIG_FILE = "image.yaml"
VERSION_BOT_CONFIG_FILE = "images.yaml"

_, uid = get_socket()
HTTPConnection.debuglevel = 0

# set runtime dir to be used with podman (e.g. to store the auth credentials)
os.environ["XDG_RUNTIME_DIR"] = f"/run/user/{uid}"


class ImageBuilder(object):
    def __init__(
        self,
        config: str = "config.yaml",
        keep: bool = False,
        stages: list[str] = [],
        images: list[str] = [],
        types: set[str] = set([]),
        env_extras: dict[str, Any] = {},
    ) -> None:
        if os.environ.get("DEBUG") == "1":
            log.setLevel(logging.DEBUG)
        else:
            log.setLevel(logging.INFO)
        self.image_configs: dict[str, ImageConfig] = {}
        self.version_bot_data: dict[str, Any] = {}
        self.sorted_names: list[str] = []
        self.build_id: str | None = None
        self.keep = keep
        self.stages = self.init_stages(stages)
        self.types = self.init_types(types)
        self.disable_external_push = False
        self.disable_buildversions = False
        self.images = images
        if os.environ.get("DEVELOP") == "1":
            self.verify = False
        else:
            self.verify = True
        project_path = os.path.dirname(os.path.dirname(__file__))
        os.chdir(project_path)
        if "test" in env_extras and env_extras["test"] is True:
            logging.disable(sys.maxsize)
        self.git_repo = git.Repo(project_path)
        self.env: dict[str, Any] = {i[0]: i[1] for i in env_extras}
        self.env.update(os.environ)
        self.project_dir = project_path.rstrip("./")
        self.podman_api = BuilderPodmanAPI()
        self.podman_client = self.podman_api.podman_client
        self.load_config(config_file=config)

    def authenticate(self) -> None:
        """Authenticate against the ubuntu mirror and the registry."""
        if not self.podman_client.secrets.exists("mirror_url"):
            self.podman_client.secrets.create("mirror_url", os.environ.get("UBUNTU_MIRROR", ""))
            self.podman_client.secrets.create(
                "mirror_user", os.environ.get("UBUNTU_MIRROR_USER", "")
            )
            self.podman_client.secrets.create(
                "mirror_password", os.environ.get("UBUNTU_MIRROR_PASSWORD", "")
            )
        self.podman_api.login(
            self.config["registry_user"],
            self.config["registry_password"],
            self.config["registry"],
        )
        # login with podman client to create auth config in runtime dirs container/auth directory
        subprocess.run(
            [
                "podman",
                "login",
                self.config["registry"],
                "-u",
                self.config["registry_user"],
                "-p",
                self.config["registry_password"],
            ]
        )
        auths = json.loads(os.environ.get("DOCKER_AUTH_CONFIG", "{}")).get("auths", dict())
        for reg, cred in auths.items():
            user, pwd = base64.b64decode(cred["auth"]).decode("latin1").split(":", 1)
            self.podman_api.login(user, pwd, reg)

    def load_config(self, config_file: str) -> None:
        """Load global config from config file."""
        with expand_templates(self.project_dir, self.env, self.keep, extra_env_file=config_file):
            self.config = self.read_config(config_file)
            logging.config.dictConfig(self.config["logging"])
            self.env.update(self.config)

    def load_image_definitions(self) -> None:
        """
        Load image definitions.

        To do this
        - the images.yaml of the version bot is loaded and
        - the ImageConfig are created from the directory structure and the version bot.
        - Remove the "latest" image from the directory structure when that is also part of the version bot.
          Does not build the latest version if image is under version control of the version bot or
          the images.yaml file.
        """
        self.version_bot_data = self.read_config(
            os.path.join(self.config["base_dir"], VERSION_BOT_CONFIG_FILE)
        )
        image_configs_dir = self.get_image_configs_from_directory()
        image_configs_bot = self.get_image_configs_from_version_bot()

        unique_keys = {
            (item.basename(), item.config.distro_codename) for item in image_configs_bot.values()
        }
        for basename, distro_codename in unique_keys:
            remove_image = f"{basename}:latest-{distro_codename}"
            image_configs_dir.pop(remove_image, None)
            log.debug("Removing image from list: %s", remove_image)

        self.image_configs = image_configs_dir | image_configs_bot
        self.sorted_names = self.sort_image_configs(self.image_configs)
        log.debug("Sorted image list: %s", self.sorted_names)

    def clone_git_repository(self, feature_branch: str) -> git.Repo:
        git_server = os.environ["GIT_SERVER"]
        project = os.environ["PROJECT"]
        remote_path = f"{git_server}/{project}/container-images/base-images.git"
        local_path = "/tmp/base-images-repo"
        username = os.environ["BOT_USER"]
        password = os.environ["BOT_PASSWORD"]
        remote = f"https://{username}:{password}@{remote_path}"
        if os.path.exists(local_path):
            shutil.rmtree(local_path)
        return git.Repo.clone_from(remote, local_path, branch=feature_branch)

    def setup_git_repository(self) -> tuple[str, git.Repo]:
        """
        Raises:
            Exception: If no branch is specified.
        """
        feature_branch = os.environ.get("CI_COMMIT_BRANCH")
        if not feature_branch:
            raise Exception("No branch specified.")  # NOSONAR
        repo = self.clone_git_repository(feature_branch)
        return feature_branch, repo

    def get_branch_diff(self, repo: git.Repo, feature_branch: str) -> git.diff.DiffIndex:
        base_branch = "origin/develop"
        origin = repo.remote(name="origin")
        origin.fetch()
        base_branch_commit = repo.commit(base_branch)
        feature_branch_commit = repo.commit(feature_branch)
        return base_branch_commit.diff(feature_branch_commit)

    def get_changed_rel_paths(self, diff_index: git.diff.DiffIndex) -> set[str]:
        changed_files = [item.a_path for item in diff_index]
        changed_image_paths = [
            item.split("/") for item in changed_files if item.startswith("images")
        ]
        changed_rel_paths = set()
        for image_path in changed_image_paths:
            if len(image_path) >= 4:
                rel_path = os.path.join("images", *image_path[1:4])
                changed_rel_paths.add(rel_path)
        return changed_rel_paths

    def get_changed_image_names(self, changed_rel_paths: Iterable[str]) -> set[str]:
        changed_image_configs = set()
        for image_config in self.image_configs.values():
            if image_config.config.rel_path in changed_rel_paths:
                changed_image_configs.add(image_config)
                # Assuming you want to collect names here as well
        return set([config.config.name for config in changed_image_configs])

    def retrieve_changed_images(self) -> set[str]:
        """
        Retrieve images that have changed in the feature branch.
        Also retrieve images that base images of changed images.
        """
        if should_build_all():
            return set()

        feature_branch, repo = self.setup_git_repository()
        diff_index = self.get_branch_diff(repo, feature_branch)
        changed_rel_paths = self.get_changed_rel_paths(diff_index)

        changed_images = self.get_changed_image_names(changed_rel_paths)

        for image_config in self.image_configs.values():
            base_images = self.get_all_base_images(image_config)
            changed_images.update(base_images)

        return changed_images

    def get_all_configs_for_basename(self, basename: str) -> list[ImageConfig]:
        return [
            image_config
            for image_config in self.image_configs.values()
            if image_config.config.basename == basename
        ]

    def get_all_base_images_for_basename(self, basename: str) -> set[str]:
        image_configs = self.get_all_configs_for_basename(basename)
        base_images = set()
        for image_config in image_configs:
            base_images.update(self.get_all_base_images(image_config))
        return base_images

    def get_all_base_images(self, img_config: ImageConfig) -> set[str]:
        base_images = set()
        for base_image in img_config.config.base_images:
            base_images.add(base_image)
            base_images.update(self.get_all_base_images(self.image_configs[base_image]))
        return base_images

    def read_config(self, name: str) -> dict[str, Any]:
        """Read config from yaml file"""
        with open(name, encoding="utf-8") as file:
            return yaml.safe_load(file)

    def create_image_config_from_dir(
        self,
        image: str,
        distro: str,
        distro_codename: str,
        additional_vars: dict[str, Any] | None = None,
    ) -> ImageConfig | None:
        """
        Create an image config object from the given directory.
            additional_vars: Additional variables to be used in the image config
                             and are not part of the builder environment.
        """
        dir_name = self.config["image_dir"]
        prefix = self.config["image_prefix"]
        img_dir = os.path.join(dir_name, distro, distro_codename, image)
        rel_path = os.path.join(prefix, distro, distro_codename, image)
        log.debug("Updating configuration from '%s'", img_dir)
        if not additional_vars:
            additional_vars = {}
        additional_vars.update({"distro": distro, "distro_codename": distro_codename})

        try:
            with pushd(img_dir):
                with expand_templates(
                    ".",
                    self.env | additional_vars,
                    self.keep,
                    recurse=False,
                    extra_env_file=IMAGE_CONFIG_FILE,
                ):
                    img_conf = self.config.copy()
                    img_conf.update(self.read_config(IMAGE_CONFIG_FILE))
                    # set additional config for image
                    img_conf.update(
                        {
                            "directory": img_dir,
                            "rel_path": rel_path,
                            "updated": False,
                            "distro": distro,
                            "distro_codename": distro_codename,
                        }
                    )
        except OSError as e:
            log.exception(f"No image config file in %s {e}", img_dir)
            return None

        return ImageConfig(img_conf)

    def get_image_configs_from_directory(self) -> dict[str, ImageConfig]:
        """
        Runs through the directory tree of the images and creates the configurations.

        Return a table of image configurations.
        """
        dir_name = self.config["image_dir"]
        configs: dict[str, ImageConfig] = dict()
        distros = os.listdir(dir_name)

        for distro in distros:
            distro_codenames = os.listdir(os.path.join(dir_name, distro))
            for distro_codename in distro_codenames:
                images = os.listdir(os.path.join(dir_name, distro, distro_codename))
                for image in images:
                    image_config = self.create_image_config_from_dir(image, distro, distro_codename)
                    if image_config:
                        configs[image_config.config.name] = image_config
        log.debug("Configs found:\n%s", json_fmt(list(configs.keys())))
        return configs

    def get_image_configs_from_version_bot(self) -> dict[str, ImageConfig]:
        """
        Reads all images from the configuration of the version bot (images.yaml)
        and creates the configuration from the image directory using the image configuration.

        Return a table of image configurations.
        """
        configs: dict[str, ImageConfig] = dict()
        for distro in self.version_bot_data["images"].keys():
            for distro_codename in self.version_bot_data["images"][distro]:
                for image in self.version_bot_data["images"][distro][distro_codename]:
                    image_segment = image.replace("-", "_")
                    versions = self.version_bot_data["images"][distro][distro_codename][image][
                        "versions"
                    ]

                    for version in versions:
                        version_number = version["version"]
                        additional_vars = {
                            "install_version": f"{version_number}",
                            "base_images": version["base_images"],
                        }

                        image_config = self.create_image_config_from_dir(
                            image_segment, distro, distro_codename, additional_vars
                        )
                        if image_config:
                            configs[image_config.config.name] = image_config

        return configs

    def sort_image_configs(self, image_configs: dict[str, ImageConfig]) -> list[str]:
        """Return a list of image names sorted by dependencies."""
        graph = {c.name: c.depends for c in [config.config for config in image_configs.values()]}
        ts = TopologicalSorter(graph)
        log.debug("Sorting graph %s", graph)
        return list(ts.static_order())

    def can_load_image_from_registry(self, img_config: ImageConfig) -> bool:
        img = img_config.name()
        remote_name, remote_tag = img_config.name_and_tag(with_project=True, with_registry=True)
        try:
            # we have to pull, as before we only pulled nightly (tag in config) not latest (tag in img)
            # check if both production and unhardened image are published
            for tag in [f"{remote_tag}-production", f"{remote_tag}-unhardened"]:
                log.info("Pulling image %s:%s", remote_name, tag)
                image = self.podman_api.pull(remote_name, tag)
                # "unknown: repository ... not found" does not raise an exception
                # but returns an empty Image
                if repr(image) == "<Image: ''>":
                    log.info("Failed to load %s", img)
                    return False
        except ImageNotFound:
            log.error("Image %s not found in the registry.", img)
            return False
        except ConnectionError:
            log.error("Failed to connect to the Podman daemon.")
            return False
        except NotFound:
            log.error("Podman daemon or container runtime not found.")
            return False
        except APIError as e:
            log.exception("Podman API error while pulling %s: %s", img, e)
            return False
        except OSError as e:
            log.error("OS-related error while interacting with Podman: %s", e)
            return False

        return True

    def needs_security_update(self, img_config: ImageConfig) -> bool:
        img = img_config.name()
        distro = img_config.config.distro

        remote_name, remote_tag = img_config.name_and_tag(with_project=True, with_registry=True)
        with self.run_podman(
            f"{remote_name}:{remote_tag}-unhardened", img_config=img_config
        ) as container:
            try:
                with pushd(os.path.join(self.project_dir, "common", distro)):
                    subprocess.check_call(
                        [
                            os.path.join(
                                self.project_dir, "common", distro, "needs_security_update.sh"
                            ),
                            container.id,
                        ]
                    )
                log.info("Image %s needs security update.", img + "-unhardened")
                return True
            except subprocess.CalledProcessError as e:
                log.error(f"CalledProcessError. {e}")
                return False

    def needs_update(self, img_config: ImageConfig) -> bool:
        """
        Check if image need update by comparing timestamps of repo files and
        calling need_update script in container.
        """
        img = img_config.name()
        distro = img_config.config.distro

        if os.environ.get("DEVELOP", "") == "1":
            return True

        # if the image does not exist it must be updated anyway
        if not self.can_load_image_from_registry(img_config):
            return True

        force_build = os.environ.get("FORCE_BUILD", "0")
        log.info("FORCE_BUILD: %s", force_build)
        if force_build == "1":
            log.info("Force build.")
            return True

        if self.needs_security_update(img_config):
            log.info("Security update needed.")
            return True

        for dep in img_config.config.depends:
            if self.image_configs[dep].config.updated:
                log.info(
                    "Image %s needs an update because a dependency was marked for update.", img
                )
                return True

        log.debug("project_dir:  %s", self.project_dir)
        log.debug("files in project_dir:  %s", os.listdir(self.project_dir))
        if glob.glob(f"{self.project_dir}/dependency_updated_*"):
            log.info(
                "Image %s needs an update because a dependency was marked for update by passing an artifact.",
                img,
            )
            return True
        else:
            log.info("No artifact found.")
        img_ctime = self.image_timestamp(img_config)
        if img_ctime is None:
            log.info("Image %s does not exist", img)
            return True

        log.info("Image %s created %s", img, time.ctime(img_ctime))

        mtime = self.latest_subtree_change(img_config.config.rel_path)
        if mtime > img_ctime:
            log.info("Image %s is older than build instructions", img)
            return True

        try:
            # we have to pull, as before we only pulled nightly (tag in config) not latest (tag in img)
            remote_name, remote_tag = img_config.name_and_tag(with_registry=True)
            log.info("Pulling image %s:%s", remote_name, remote_tag)
            self.podman_api.pull(remote_name, remote_tag)
            with self.run_podman(img + "-unhardened", img_config=img_config) as container:
                try:
                    script_path = os.path.join(
                        self.project_dir, "common", distro, "needs_update.sh"
                    )
                    with pushd(os.path.join(self.project_dir, "common", distro)):
                        subprocess.check_call(
                            [
                                script_path,
                                container.id,
                            ]
                        )
                    log.info("Image %s needs update by script.", img + "-unhardened")
                    return True
                except subprocess.CalledProcessError as e:
                    log.error("CalledProcessError. %s", e)
                    return False
                except FileNotFoundError:
                    log.error("Script %s not found.", script_path)
                    return False
                except PermissionError:
                    log.error("Permission denied while executing script %s.", script_path)
                    return False
        # APIError from pulling images
        # ContainerRuntimeError from run_podman
        # AttributeError if run_podman returns None
        except (APIError, ContainerRuntimeError, AttributeError) as e:
            log.exception("Failed to run %s", img, exc_info=True)
            log.exception(e)
            return True

    def latest_subtree_change(self, prefix: str) -> float:
        """Return latest commit date of git repo"""
        newest: float = 0
        tree = self.git_repo.tree()
        for blob in tree.list_traverse():
            path = str(blob.path)
            if path.startswith(prefix):
                for commit in self.git_repo.iter_commits(paths=path):
                    newest = max(newest, commit.committed_date)

        return newest

    def is_stable_version(self, img_config: ImageConfig, tag: str) -> bool:
        """Check if given tag is marked as stable version."""
        return self._is_version("stable", img_config, tag)

    def is_latest_version(self, img_config: ImageConfig, tag: str) -> bool:
        """Check if given tag is marked as latest version."""
        return self._is_version("latest", img_config, tag)

    def _is_version(self, version_type: str, img_config: ImageConfig, tag: str) -> bool:
        """check if the given tag is marked as the expected version type."""
        basename = img_config.config.basename
        distro = img_config.config.distro
        distro_codename = img_config.config.distro_codename

        img_data = (
            self.version_bot_data["images"].get(distro, {}).get(distro_codename, {}).get(basename)
        )
        if img_data and str(tag) == img_data.get(version_type):
            return True
        return False

    def is_latest_in_range(self, img_config: ImageConfig, tag: str) -> bool:
        """
        Checks whether a given tag is the highest version number within its range.
        If given tag not in list, it will return False.
        e.g.
        - "1.2.4" in ["1.2.1", "1.2.4", "1.3.3"]
        - "1.2" in ["1.2", "1.1", "2.0"]
        """
        basename = img_config.config.basename
        distro = img_config.config.distro
        distro_codename = img_config.config.distro_codename

        # remove distro_codename appendix and discard distro
        tag, _ = tag.rsplit("-", maxsplit=1)
        img_data = (
            self.version_bot_data["images"].get(distro, {}).get(distro_codename, {}).get(basename)
        )
        if img_data is None:
            return False

        delimiter = "."
        if len(tag.split(delimiter)) == 0:  # simple version like "16"
            rolling_tag = tag
        else:  # version has min. one separator, like "1.0" or "1.0.0"
            # truncation of the last digit (and dot) of the version number
            rolling_tag = delimiter.join(tag.rsplit(delimiter)[:-1])

        versions = [
            normalize_version(version["version"])
            for version in img_data["versions"]
            if str(version["version"]).startswith(rolling_tag)
        ]
        latest_in_range = max(versions)

        return normalize_version(tag) == latest_in_range

    def image_timestamp(self, img_config: ImageConfig) -> float | None:
        """Return the newest "created" timestamp from all parts."""
        basename = img_config.config.basename
        registry = img_config.config.registry
        project = img_config.config.project
        remote_name = f"{registry}/{project}/{basename}"
        # TODO: define if we check timestamp of latest tag or the tag defined in the image config
        # currently we dont push the tag nightly-jammy to the registry.
        # decide, if we need the nightly tag in the registry
        # tag = self.config['tag']
        distro_codename = img_config.config.distro_codename
        tag = f"latest-{distro_codename}-production"
        try:
            log.info("Pulling image %s:%s", remote_name, tag)
            # pulling a tag should always get an Image
            image = cast(
                Image,
                self.podman_api.pull(remote_name, tag),
            )
            if image.id:
                image.tag(f"localhost/{basename}", tag)
            else:
                log.exception("Failed to pull image %s:%s", remote_name, tag, exc_info=True)
        except (APIError, ContainerRuntimeError, AttributeError) as e:
            log.exception("Failed to run %s:%s: %s", remote_name, tag, e)
            return True
        except OSError as e:
            log.exception("OS-related error while executing container or script: %s", e)
            return True

        repository = f"{project}/{basename}:{tag}"

        if registry == "registry.opencode.de" or "harbor" in registry:
            token = get_bearer_token(
                registry,
                repository,
                img_config.config.registry_user,
                img_config.config.registry_password,
                self.verify,
            )
        else:
            token = None

        data = inspect(registry, project, basename, tag, token)
        if not data:
            return None
        creation_date = data["Created"]
        creation_date = parse(creation_date)
        return creation_date.timestamp()

    def fix_kwargs_for_podman(
        self, img_config: ImageConfig, user: str | None, privileged: bool
    ) -> dict[str, Any]:
        # list of kwargs values:
        #   https://github.com/containers/podman-py/blob/c7356dcff7d15fd0da61e9ffb226e789c2d7d9c4/podman/domain/containers_create.py#L30-L260
        kwargs: dict[str, Any] = {"detach": True}
        kwargs["environment"] = {
            "http_proxy": os.environ.get("http_proxy", ""),
            "https_proxy": os.environ.get("https_proxy", ""),
            "no_proxy": os.environ.get("no_proxy", ""),
        }
        for arg in ["command", "entrypoint", "tmpfs", "volumes", "ports", "privileged", "env"]:
            if arg in img_config.config.docker:
                docker_config = img_config.config.docker[arg]
                if arg == "volumes":
                    bind_mounts = create_mounts_from_volumes(docker_config)
                    kwargs.setdefault("mounts", []).extend(bind_mounts)
                elif arg == "tmpfs":
                    tmpfs_mounts = create_mounts_from_tmpfss(docker_config)
                    kwargs.setdefault("mounts", []).extend(tmpfs_mounts)
                elif arg == "env":
                    kwargs["environment"].update(docker_config)
                elif arg == "ports":
                    kwargs["ports"] = fix_ports_for_podman(docker_config)
                else:
                    kwargs[arg] = docker_config

        if user:
            kwargs["user"] = user
        kwargs["privileged"] = privileged
        kwargs["secret_env"] = {
            "UBUNTU_MIRROR": "mirror_url",
            "UBUNTU_MIRROR_USER": "mirror_user",
            "UBUNTU_MIRROR_PASSWORD": "mirror_password",
        }

        if "TEST_BUILD" in os.environ:
            kwargs["environment"]["TEST_BUILD"] = os.environ["TEST_BUILD"]
        return kwargs

    def create_podman_changes_command(self, remote_name: str, img_config: ImageConfig) -> list[str]:
        image = self.podman_client.images.get(remote_name)
        entrypoint = image.attrs["Config"].get("Entrypoint")
        command = image.attrs["Config"].get("Cmd")
        changes = list()
        entrypoint_param = make_list_param(entrypoint)
        command_param = make_list_param(command)
        if entrypoint:
            changes.append(f"ENTRYPOINT {entrypoint_param}")
        changes.append(f"CMD {command_param}")
        changes.append('LABEL org.opencontainers.image.authors="BWI GmbH"')
        image_license = img_config.config.image_license
        changes.append(f'LABEL org.opencontainers.image.licenses="{image_license}"')
        changes.append(
            'LABEL org.opencontainers.image.url="https://gitlab.opencode.de/bwi/bundesmessenger/backend/container-factory"'
        )
        current_datetime = datetime.now(timezone.utc)
        now = format_rfc3339(current_datetime)
        changes.append(f'LABEL org.opencontainers.image.created="{now}"')
        if img_config.config.app_version:
            app_version = img_config.config.app_version
            changes.append(f'LABEL org.opencontainers.image.version="{app_version}"')
        return changes

    def check_build_id(self, image_name: str) -> None:
        """
        Raises:
            BuildprocessError: If the build id of the image does not equal the unique build id
        """
        image = self.podman_client.images.get(image_name)
        build_id_label = "de.bwi.baseimage.build-id"
        labels = image.labels
        image_build_id = labels.get(build_id_label, "no id")
        if build_id_label in labels and labels[build_id_label] != self.build_id:
            raise BuildprocessError(
                f"The build id of the image ({image_build_id}) does not equal the unique build id "
                "set at the start of the build process ({self.build_id})."
            )

    def check_mandatory_image_definition_properties(self, img_config: ImageConfig) -> None:
        if not img_config.config.image_license:
            image_name = img_config.name()
            log.error(f"There is no image_license defined for image {image_name}.")
            sys.exit(1)

    def copy_image_license_to_container(
        self, img_config: ImageConfig, container: Container
    ) -> None:
        image_license = img_config.config.image_license
        license_file = os.path.join("common", "licenses", image_license)
        self.copy_files_to_container(container, [(license_file, "/LICENSE_IMAGE")])

    @contextmanager
    def run_podman(
        self,
        name: str,
        img_config: ImageConfig,
        commit: bool = False,
        user: str | None = None,
        privileged: bool = False,
        commit_name: str | None = None,
    ) -> Generator[Container, None, None]:
        """
        Raises:
            ContainerRuntimeError: If the container is not running.
        """
        remote_name = img_config.name_with_tag(with_project=True, with_registry=True)
        remote_name += "-unhardened"
        container = None
        try:
            log.info("Running container %s", name)
            kwargs = self.fix_kwargs_for_podman(img_config, user, privileged)
            container = cast(Container, self.podman_client.containers.run(name, **kwargs))
            time.sleep(5)
            log.info("Container logs:")
            try:
                for line in container.logs(stdout=True, stderr=True, tail="all"):
                    log.info("%s", line)
            except Exception as e:
                log.exception(e)
            if container.inspect()["State"]["Status"] == "exited":
                raise ContainerRuntimeError("Container is not running.")
            yield container
        finally:
            if container is not None:
                log.info("Stopping container: %s with id %s", container.logs(), container.id)
                if container.status == "running":
                    try:
                        container.stop(ignore=True, timeout=3)
                    except APIError as e:
                        log.info("The stopping of the container had a timeout.")
                        log.info(e)

                if commit:
                    if commit_name is None:
                        commit_name = name
                    img_name, img_tag = commit_name.rsplit(":", 1)
                    changes = self.create_podman_changes_command(remote_name, img_config)
                    log.info("Command for changes: %s", changes)
                    container.commit(repository=img_name, tag=img_tag, changes=changes)
                    variables_to_unset = [
                        "http_proxy",
                        "https_proxy",
                        "HTTP_PROXY",
                        "HTTPS_PROXY",
                        "no_proxy",
                        "TEST_BUILD",
                    ]
                    variables_to_unset += kwargs["environment"].keys()
                    unset_env_vars(commit_name, variables_to_unset)
                log.info("Removing container")
                container.remove()

    def run(self) -> None:
        """Build all configured images, if needed"""
        pre_stages = ["render", "lintcheck", "spellcheck"]
        remaining_stages = [stage for stage in self.stages if stage not in pre_stages]
        self.load_image_definitions()
        if "render" in self.stages:
            # rendering is part of load_image_definitions (expand_templates)
            log.info("Stage render started.")

        if "lintcheck" in self.stages:
            log.info("Stage lintcheck started.")
            command = (
                "find . -name 'Dockerfile*' ! -name '*.template' -print0 | xargs -n1 -0 "
                "hadolint --ignore DL3006 --ignore DL3008 --ignore SC2015 --ignore DL3007"
            )
            subprocess.check_call(command, shell=True)

        if "spellcheck" in self.stages:
            log.info("Stage spellchecks started.")
            subprocess.check_call(["scripts/test_shell.sh"])

        if remaining_stages:
            self.authenticate()
        else:
            exit()

        # do not move this to the __init__ function as we cannot mock out the repo call so easily then
        changed_images = self.retrieve_changed_images()

        for img in self.sorted_names:
            img_config = self.image_configs[img]
            if self.check_skip(img_config):
                log.debug("Skipping image %s", img)
                continue
            if len(changed_images) > 0 and img not in changed_images:
                continue
            # fetch_image_configs and add_version_bot_versions only parse image configurations
            # for running the build it is necessary to build all files of the image directory
            with pushd(img_config.config.directory):
                with expand_templates(
                    ".", self.env, self.keep, recurse=True, overwrite_env=asdict(img_config.config)
                ):
                    self.run_stages(img_config)

    def trigger_scan(self, img_config: ImageConfig) -> bool:
        if self.config.get("vulnerability_scanner", "grype") in [None, "grype"]:
            log.debug("Skipping scan trigger, using internal grype")
            return True

        repository = img_config.basename()
        tag = img_config.get_build_tag()
        base_url = img_config.config.harbor_api_url
        project = img_config.config.project
        resp = requests.post(
            f"{base_url}/projects/{project}/repositories/" f"{repository}/artifacts/{tag}/scan",
            headers={
                "Accept": "application/json",
                "X-Request-Id": "%d" % secrets.randbelow(1000000),
            },
            auth=(img_config.config.registry_user, img_config.config.registry_password),
        )
        log.info("Scan trigger response: %d %s", resp.status_code, resp.reason)
        return resp.ok

    def internal_scan(self) -> list[dict]:
        # we use the value which was set in the push buildversion stage to prevent retrieving
        # wrong incremented values from parallel feature branch pipelines building the same image.
        output = subprocess.check_output(["grype", "-o", "json", self.prod_img_name])
        data = json.loads(output)
        items = list()
        for match in data["matches"]:
            item = dict()
            item["id"] = match["vulnerability"]["id"]
            item["severity"] = match["vulnerability"]["severity"]
            item["package"] = match["artifact"]["name"]
            item["version"] = match["artifact"]["version"]
            item["fix_state"] = match["vulnerability"]["fix"]["state"]
            item["fix_versions"] = match["vulnerability"]["fix"]["versions"]
            try:
                cvs_lists = [vuln["cvss"] for vuln in match["relatedVulnerabilities"]]
                cvs = [item for sublist in cvs_lists for item in sublist]

                if len(cvs) > 0:
                    max_cvss = max(cvs, key=lambda x: x["version"])
                    version = max_cvss["version"]
                    score = max_cvss["metrics"]["baseScore"]
                else:
                    version = "null"
                    score = "null"
                item["cvss_basescore"] = f"{score} [v{version}]"
                items.append(item)
            except KeyError:
                item["cvss_basescore"] = ""
        return items

    def external_scan(self, img_config: ImageConfig) -> list[dict] | None:
        repository = img_config.basename()
        # we use the value which was set in the push buildversion stage to prevent retrieving
        # wrong incremented values from parallel feature branch pipelines building the same image.
        tag = self.prod_name_int.split(":")[1]
        base_url = img_config.config.harbor_api_url
        project = img_config.config.project

        # "X-Accept-Vulnerabilities":
        # "application/vnd.scanner.adapter.vuln.report.harbor+json; version=1.0",

        resp = requests.get(
            f"{base_url}/projects/{project}/repositories/{repository}"
            f"/artifacts/{tag}/additions/vulnerabilities",
            headers={
                "Accept": "application/json",
                "X-Request-Id": "%d" % secrets.randbelow(1000000),
            },
            auth=(img_config.config.registry_user, img_config.config.registry_password),
        )
        if resp.ok:
            data = resp.json()
            log.info("External scan log:")
            log.info(data)
            if "application/vnd.security.vulnerability.report; version=1.1" not in data:
                return None
            data = data["application/vnd.security.vulnerability.report; version=1.1"]
            items = list()
            for vulnerability in data["vulnerabilities"]:
                item = dict()
                item["id"] = vulnerability["id"]
                item["severity"] = vulnerability["severity"]
                item["package"] = vulnerability["package"]
                item["version"] = vulnerability["version"]
                item["fix_versions"] = [vulnerability["fix_version"]]
                try:
                    version = vulnerability["vendor_attributes"]["CVSS"]["nvd"]["V3Vector"][5:8]
                    score = vulnerability["vendor_attributes"]["CVSS"]["nvd"]["V3Score"]
                    item["cvss_basescore"] = f"{score} [v{version}]"
                except (TypeError, KeyError):
                    item["cvss_basescore"] = ""
                if len(item["fix_versions"]) > 0:
                    item["fix_state"] = True
                else:
                    item["fix_state"] = False
                items.append(item)
            return items
        else:
            return None

    def download_scan(self, img_config: ImageConfig) -> list[dict] | None:
        scan_config = self.config.get("vulnerability_scanner")
        if scan_config in [None, "", "grype"]:
            return self.internal_scan()
        else:
            return self.external_scan(img_config)

    def remove_package(
        self, container: Container, pattern: Iterable, pkg: str
    ) -> tuple[bool, set[str]]:
        success_pkg = set()
        file_removed = False
        for p in pattern:
            if re.match(p, pkg):
                log.info("Removing %s", pkg)
                ret_code, _ = container.exec_run(
                    "apt -y --allow-remove-essential remove " + pkg, demux=False
                )
                if ret_code == 0:
                    log.info("Removed %s", pkg)
                    success_pkg.add(pkg)
                    file_removed = True
                else:
                    log.warning("Failed to remove %s: Unable to parse output", pkg)
        return file_removed, success_pkg

    def remove_deb_packages(self, img: str, container: Container, pattern: Iterable) -> None:
        """
        Raises:
            OSError: If dpkg-query fails
        """
        ret_code, stdout_data = container.exec_run(
            "dpkg-query --showformat '${binary:Package}\n' --show", demux=False
        )
        if ret_code != 0:
            log.warning("Failed to strip image %s", img)
            raise OSError("dpkg-query failed")

        assert isinstance(stdout_data, bytes)

        file_removed = True
        success_pkg = set()
        while file_removed:
            file_removed = False
            for pkg in stdout_data.decode("latin1").split("\n"):
                if not pkg or pkg in success_pkg:
                    continue
                file_removed, pkgs = self.remove_package(container, pattern, pkg)
                success_pkg.update(pkgs)

        ret_code, stdout_data = container.exec_run("apt -y autoremove", demux=False)
        if ret_code != 0:
            assert isinstance(stdout_data, bytes)
            log.warning("Failed to run autoremove:\n%s", stdout_data.decode("utf-8"))

    def remove_apt(self, container: Container) -> None:
        ret_code, _ = container.exec_run("dpkg -P apt-transport-https apt")
        if ret_code != 0:
            log.warning("Failed to remove apt.")
        else:
            log.info("Removed apt")

    def run_script(self, container: Container, params: dict[str, Any]) -> list[str]:
        spwn = pexpect.spawn(
            f"podman exec -i {container.id} " + params["shell"], timeout=30, encoding="latin1"
        )
        spwn.logfile = sys.stdout
        for line in params["script"]:
            spwn.sendline(line)
        spwn.send("exit\r\n")
        response = spwn.readlines()
        log.debug("Script output:\n%s", response)
        return response

    def strip(self, img: str, container: Container) -> None:
        if not os.access("strip.yaml", os.R_OK):
            return
        with open("strip.yaml") as f:
            actions = yaml.safe_load(f)
        for step in actions["steps"]:
            action = actions[step]["action"]
            params = actions[step]["params"]
            if action == "remove_deb_packages":
                self.remove_deb_packages(img, container, params)
            if action == "script":
                self.run_script(container, params)
            if action == "remove_apt":
                self.remove_apt(container)

    def init_stages(self, stages: list[str] | str = "") -> list[str]:
        """Setup and fix processing steps"""
        if stages is None or len(stages) == 0:
            stages = [
                "render",
                "lintcheck",
                "spellcheck",
                "check",
                "build",
                "test",
                "strip",
                "ref",
                "src",
                "bom",
                "qa",
                "push",
                "scan",
                "sign",
            ]
        elif isinstance(stages, str):
            stages = stages.split(",")
        if "push" in stages and "test" not in stages:  # tags might be derived from test results
            stages.append("test")

        log.info("using stages: %s", ",".join(stages))
        return stages

    def init_types(self, types: set[str]) -> set[str]:
        """Setup and fix build types"""
        if len(types) == 0:
            types = set(["standard", "production", "src"])
        log.info("Types initialized: %s", ",".join(types))
        return types

    def is_bot_version(self, img_config: ImageConfig) -> bool:
        basename = img_config.config.basename
        distro = img_config.config.distro
        distro_codename = img_config.config.distro_codename

        img_data = (
            self.version_bot_data["images"].get(distro, {}).get(distro_codename, {}).get(basename)
        )
        if img_data:
            return True
        return False

    def run_image_and_check_mandatory_files(
        self, image_name: str, mandatory_files: list[str], img_config: ImageConfig
    ) -> None:
        """
        Raises:
            BuildprocessError: If the image is missing a mandatory file
            APIError: If running the image fails (exec_run)
        """
        with self.run_podman(image_name, img_config=img_config) as container:
            for file in mandatory_files:
                ret_code, _ = container.exec_run(["/usr/bin/test", "-f", file], user="root")
                if ret_code != 0:
                    raise BuildprocessError(f"The image is missing a mandatory file: {file}")

    def check_mandatory_files(
        self, prod_image_name: str, src_image_name: str, img_config: ImageConfig
    ) -> None:
        # check production files
        mandatory_files = [
            "/container_data.tar.gz",
            "/legal-info-bin.txt",
            "/readme_references.txt",
        ]
        self.check_build_id(prod_image_name)
        self.run_image_and_check_mandatory_files(prod_image_name, mandatory_files, img_config)

        # check src files
        mandatory_files = ["/tmp/sources.tar.gz"]
        self.check_build_id(src_image_name)
        self.run_image_and_check_mandatory_files(src_image_name, mandatory_files, img_config)

    def compare_motd_with_legal_info(self, prod_image_name: str, img_config: ImageConfig) -> None:
        """
        Raises:
            BuildprocessError: If the motd is not matching the legal info
            ContainerRuntimeError: If the container is not running (run_podman)
            OSError: If opening the motd or legal-info-bin.txt fails (open)
            subprocess.SubprocessError: If copying the files from the container fails (subprocess.run)
        """
        self.check_build_id(prod_image_name)
        with self.run_podman(prod_image_name, img_config=img_config) as container:
            subprocess.run(["podman", "cp", f"{container.id}:/etc/motd", "/tmp/motd"])
            subprocess.run(
                [
                    "podman",
                    "cp",
                    f"{container.id}:/legal-info-bin.txt",
                    "/tmp/legal-info-bin.txt",
                ]
            )
        with open("/tmp/motd") as f:
            motd = f.read()
        with open("/tmp/legal-info-bin.txt") as f:
            legal_info = f.read()
        if motd not in legal_info:
            raise BuildprocessError("The motd is not matching the legal info.")

    def check_image_exists(self, image_name: str) -> bool:
        try:
            # Run Skopeo to inspect the image
            result = subprocess.run(
                ["skopeo", "inspect", f"docker://{image_name}"],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            # Check if the command was successful
            if result.returncode == 0:
                return True
            return False
        except FileNotFoundError:
            log.error("skopeo is not installed or not found in PATH.")
            return False
        except PermissionError:
            log.error("Permission denied while executing skopeo.")
            return False
        except OSError as e:
            log.error("OS-related error occurred while running skopeo: %s", e)
            return False
        except subprocess.SubprocessError as e:
            log.error("An error occurred: %s", e)
            return False

    def check_published_tags(self, img_config: ImageConfig) -> None:
        """
        Raises:
            BuildprocessError: If the reference tags do not exist in the registry
        """
        # sync this function with so that the following tags align with the ones in process_references
        tags = {
            "target_version": img_config.get_targetversion(),
            "prod_target_version": img_config.get_targetversion_prod_ext(),
        }
        if "src" in self.stages and "src" in self.types:
            tags["src_version"] = img_config.get_src_image_name(cached=True)
        if not self.disable_buildversions:
            tags["build_version"] = img_config.get_buildversion(cached=True)
            tags["prod_build_version"] = img_config.get_buildversion_prod_ext(cached=True)
        for reference_name, tag in tags.items():
            if self.check_image_exists(tag):
                log.info(
                    f"The reference for the {reference_name} leads to an existing image in the registry: {tag}"
                )
            else:
                raise BuildprocessError(
                    f"The reference for the {reference_name} leads not to an existing image in the registry: {tag}"
                )

    def process_qa(self, img_config: ImageConfig) -> None:
        src_image_name = img_config.get_src_image_name(cached=True)
        prod_image_name = img_config.get_prod_image_name()
        self.check_mandatory_files(prod_image_name, src_image_name, img_config)
        self.compare_motd_with_legal_info(prod_image_name, img_config)
        # only check published tags on main branch, on non main branches the reference tags will not be created
        # if external push is disabled, the check for published tags is not executed
        if os.environ.get("CI_COMMIT_BRANCH", "") == "main" and not self.disable_external_push:
            self.check_published_tags(img_config)

    def add_label_to_image(self, image_name: str, label_key: str, label_value: str) -> None:
        """Adds a label to an existing image using buildah."""
        try:
            buildah_container_id = (
                subprocess.check_output(["buildah", "from", image_name]).strip().decode("utf-8")
            )
            subprocess.run(
                [
                    "buildah",
                    "config",
                    "--label",
                    f"{label_key}={label_value}",
                    buildah_container_id,
                ],
                check=True,
            )
            subprocess.run(
                ["buildah", "commit", buildah_container_id, image_name],
                check=True,
            )
        except subprocess.CalledProcessError as e:
            log.error(f"Error: {e}")

    def process_build(self, img_config: ImageConfig) -> None:
        img = img_config.name()
        log.info("Image %s needs an update", img)
        prod_name_int = img_config.get_prod_name_int()
        remote_name = img_config.basename(with_project=True, with_registry=True)
        basename = img_config.basename()
        config_tag = img_config.tag()
        assert self.build_id
        subprocess.check_call(["./build", self.build_id])
        img_config.config.updated = True
        # create artifact to pass updated-information to pipelines of dependant images
        with open(f"{self.project_dir}/dependency_updated_{basename}", "w") as f:
            f.write("updated")
        log.info(
            "Created artifact to pass updated-information for image %s to pipelines of dependant images.",
            img,
        )
        try:
            image = self.podman_client.images.get(img + "-unhardened")
            if image.id:
                log.info("Image %s found in repository", img + "-unhardened")
                # tag for registry
                log.info("Tagged image to %s:latest-unhardened", remote_name)
                image.tag(remote_name, "latest-unhardened")
                log.info("Tagged image to %s:%s-unhardened", remote_name, config_tag)
                image.tag(remote_name, config_tag + "-unhardened")
                log.info("Tagged image to %s:%s-unhardened", prod_name_int, config_tag)
                image.tag(prod_name_int, config_tag + "-unhardened")
        except ImageNotFound:
            log.exception("Image %s not found in repository", img + "-unhardened")
        except APIError as e:
            log.exception("Podman API error while fetching/tagging image: %s", e)
        if os.environ.get("ADD_PIPELINE_ID", "0") == "1":
            self.add_label_to_image(
                f"localhost/{img}-unhardened",
                "de.bwi.baseimage.pipeline-id",
                os.environ.get("CI_PIPELINE_ID", ""),
            )
        with self.run_podman(
            f"localhost/{img}-unhardened", img_config=img_config, commit=True
        ) as container:
            self.copy_image_license_to_container(img_config, container)

    def process_tests(self, img_config: ImageConfig) -> None:
        """
        Raises:
            subprocess.CalledProcessError: If the tests fail
        """
        img = img_config.name()
        self.check_build_id(f"localhost/{img}-unhardened")
        try:
            # kill and remove old leftover test
            container = self.podman_client.containers.get("app")
            if container.status == "running":
                container.kill()
            container.remove(force=True)
        except NotFound:
            log.info('No running container with name "app" to kill.')
        try:
            output = subprocess.check_output(
                ["./tests", f"localhost/{img}-unhardened"], encoding="latin1"
            )
        except subprocess.CalledProcessError as e:
            log.error("Tests in %s failed", img + "-unhardened")
            log.error("Return code: %s", e.returncode)
            log.error("Output: %s", e.output)
            raise
        log.info("Tests passed: %s", output)
        try:
            # kill and remove container from test if it has not been deleted
            container = self.podman_client.containers.get("app")
            if container.status == "running":
                container.kill()
            container.remove(force=True)
        except NotFound:
            log.info('No running container to with name "app" to kill.')

        # be sure do not overwrite distro and distro_codename
        additional_vars: dict[str, Any] = {
            "distro": img_config.config.distro,
            "distro_codename": img_config.config.distro_codename,
        }

        for m in re.finditer("(?m)^ENV ([0-9A-Z_]+)=(\\S+)\n", output):
            log.info("Updating environment %s=%s", m.group(1), m.group(2))
            additional_vars[m.group(1)] = m.group(2)

        # if bot version image, overwrite env with version bot data
        if self.is_bot_version(img_config):
            additional_vars.update(
                {
                    "install_version": img_config.config.install_version,
                    "base_images": img_config.config.base_images,
                }
            )

        with expand_templates(
            ".", self.env | additional_vars, True, extra_env_file=IMAGE_CONFIG_FILE
        ):
            log.info("Updating configuration from templates")
            img_config.config.update(self.read_config(IMAGE_CONFIG_FILE))

    def process_references(self, img_config: ImageConfig) -> None:
        img = img_config.name()
        buildversion = img_config.get_buildversion()
        targetversion = img_config.get_targetversion()
        targetversion_prod = img_config.get_targetversion_prod_ext()
        buildversion_prod = img_config.get_buildversion_prod_ext()
        src_image_name = img_config.get_src_image_name()
        self.check_build_id(f"localhost/{img}-unhardened")
        with self.run_podman(
            f"localhost/{img}-unhardened", img_config=img_config, commit=True
        ) as container:
            output = subprocess.check_output(
                [
                    "./build_ref",
                    container.id,
                    targetversion,
                    buildversion,
                    targetversion_prod,
                    buildversion_prod,
                    src_image_name,
                ]
            )
            log.info(
                "Creating reference information: targetversion:%s buildversion:%s %s",
                targetversion,
                buildversion,
                output,
            )

    def process_sources(self, img_config: ImageConfig) -> None:
        if "src" not in self.types:
            log.info("Skip building not requested src image.")
            return None

        img = img_config.name()
        img_conf = img_config.config
        src_image_name = img_config.get_src_image_name(cached=True)
        self.check_build_id(f"localhost/{img}-unhardened")
        with self.run_podman(
            f"localhost/{img}-unhardened",
            img_config=img_config,
            commit=True,
            commit_name=src_image_name,
        ) as container:
            if img_conf.install_version:
                install_version = str(img_conf.install_version)
            else:
                install_version = "none"
            try:
                subprocess.check_output(
                    [
                        os.path.join(self.project_dir, "common", "build_src"),
                        container.id,
                        install_version,
                        img_conf.directory,
                        img_conf.distro,
                        img_conf.distro_codename,
                    ]
                )
            except subprocess.CalledProcessError as e:
                log.exception("Sources could not be loaded.")
                log.exception(e)
                exit(1)

    def process_strip(self, img_config: ImageConfig) -> None:
        img = img_config.name()
        src_image_name = img_config.get_src_image_name(cached=True)
        prod_image_name = img_config.get_prod_image_name()
        clean_image(self.podman_client.images, f"localhost/{img}-unhardened")
        self.check_build_id(f"localhost/{img}-unhardened")
        with self.run_podman(
            f"localhost/{img}-unhardened",
            img_config=img_config,
            commit=True,
            commit_name=prod_image_name,
        ) as container:
            self.strip(img, container)
            log.info("Production Container stripped")
        self.check_build_id(src_image_name)
        if src_image_name:
            with self.run_podman(
                src_image_name, img_config=img_config, commit=True, commit_name=src_image_name
            ) as container:
                self.strip(src_image_name, container)
                log.info("Source Container stripped")

    def process_bom(self, img_config: ImageConfig) -> None:
        build_script = "./build_bom"
        if img_config.config.skip_bom_stage:
            log.info("build_bom skipped: %s", img_config.config.skip_bom_stage)
            return None
        if not os.access(build_script, os.R_OK):
            log.info("Skip build bom due to problems with th build script %s", build_script)

        remote_name = img_config.basename(with_project=True, with_registry=True)
        prod_image_name = img_config.get_prod_image_name()
        config_tag = img_config.tag()
        self.check_build_id(prod_image_name)
        with self.run_podman(prod_image_name, img_config=img_config, commit=True) as container:
            log.info("Fetching BoM")
            # use standard image for bom, as these are still unstripped and include apt
            subprocess.check_output(
                [build_script, container.id, f"{remote_name}:{config_tag}-unhardened"]
            )

    def push_latest_tag(self, img_config: ImageConfig, image: Image) -> None:
        log.info("Push latest tag")
        config_tag = img_config.tag()
        remote_name = img_config.basename(with_project=True, with_registry=True)
        prod_image_name = img_config.get_prod_image_name()
        prod_image = self.podman_client.images.get(prod_image_name)
        distro_codename = img_config.config.distro_codename
        # push to latest if bot defined latest version
        # remove distro_codename from config_tag
        # this works only as long we build only one distribution
        # TODO: when we use more then one distribution, we must
        # define the lastest versions with the distribution appendix f.e. "-jammy" in the images.yaml file
        # so we have one latest tag which point to one of the distributions latest version.
        # The distribution which is used for the latest version can be defined by setting the
        # latest property for only one chosen distribution by hand.
        # The version must than refer to this property to set the latest tag only for this chosen distribution.
        if self.is_latest_version(img_config, config_tag) or not self.is_bot_version(img_config):
            self.podman_api.push(
                image,
                remote_name,
                "latest-unhardened",
                f"Pushing unhardened image to latest tag: {remote_name}:latest-unhardened",
            )
            self.podman_api.push(
                image,
                remote_name,
                f"latest-{distro_codename}-unhardened",
                f"Pushing unhardened image to latest tag: {remote_name}:latest-{distro_codename}-unhardened",
            )
            self.podman_api.push(
                prod_image,
                remote_name,
                "latest-production",
                f"Pushing production image to latest tag: {remote_name}:latest-production",
            )
            self.podman_api.push(
                prod_image,
                remote_name,
                f"latest-{distro_codename}-production",
                f"Pushing production image to latest tag: {remote_name}:latest-{distro_codename}-production",
            )

    def push_stable_tag(self, img_config: ImageConfig, image: Image) -> None:
        log.info("Push stable tag")
        config_tag = img_config.tag()
        remote_name = img_config.basename(with_project=True, with_registry=True)
        prod_image_name = img_config.get_prod_image_name()
        prod_image = self.podman_client.images.get(prod_image_name)
        distro_codename = img_config.config.distro_codename
        if self.is_stable_version(img_config, config_tag) or not self.is_bot_version(img_config):
            self.podman_api.push(
                image,
                remote_name,
                "stable-unhardened",
                f"Pushing unhardened image to stable tag: {remote_name}:stable-unhardened",
            )
            self.podman_api.push(
                image,
                remote_name,
                f"stable-{distro_codename}-unhardened",
                f"Pushing unhardened image to stable tag: {remote_name}:stable-{distro_codename}-unhardened",
            )
            self.podman_api.push(
                prod_image,
                remote_name,
                "stable-production",
                f"Pushing production image to stable tag: {remote_name}:stable-production",
            )
            self.podman_api.push(
                prod_image,
                remote_name,
                f"stable-{distro_codename}-production",
                f"Pushing production image to stable tag: {remote_name}:stable-{distro_codename}-production",
            )

    def extract_rolling_tag(self, version: str) -> str:
        """Extracts the rolling tag from version."""
        return ".".join(version.rsplit(".")[:-1])

    def push_tags(
        self, prod_image: Image, prod_name_int: str, prod_name_ext: str, tag: str
    ) -> None:
        """Pushes tags to internal/external repos based on disable_external_push."""
        self.podman_api.push(
            prod_image, prod_name_int, tag, f"Pushing tag to internal: {prod_name_int}:{tag}"
        )
        if not self.disable_external_push:
            self.podman_api.push(
                prod_image, prod_name_ext, tag, f"Pushing tag to external {prod_name_ext}:{tag}"
            )

    def handle_rolling_tag(
        self,
        img_config: ImageConfig,
        config_tag: str,
        prod_image: Image,
        prod_name_int: str,
        prod_name_ext: str,
    ) -> None:
        """Handles pushing of rolling tag if latest in range."""
        if self.is_latest_in_range(img_config, config_tag):
            rolling_tag = self.extract_rolling_tag(config_tag)
            self.push_tags(prod_image, prod_name_int, prod_name_ext, rolling_tag)

    def handle_system_package_version_tag(
        self,
        img_config: ImageConfig,
        target_tag: str,
        prod_image: Image,
        prod_name_int: str,
        prod_name_ext: str,
    ) -> None:
        """Handles pushing of system package version tags."""
        if not self.is_bot_version(img_config):
            version_match = re.match(r"(\d+_)*(\d+\.\d+\.\d+)(.*)", target_tag)
            if version_match:
                version = version_match.group(2)
                self.push_tags(prod_image, prod_name_int, prod_name_ext, version)
                rolling_tag = "r" + self.extract_rolling_tag(version)
                self.push_tags(prod_image, prod_name_int, prod_name_ext, rolling_tag)

    def push_rolling_and_system_package_version_tags(
        self,
        img_config: ImageConfig,
        config_tag: str,
        prod_image: Image,
        prod_name_int: str,
        prod_name_ext: str,
        target_tag: str,
    ) -> None:
        """Pushes rolling/system package version tags based on configuration."""
        self.handle_rolling_tag(img_config, config_tag, prod_image, prod_name_int, prod_name_ext)
        self.handle_system_package_version_tag(
            img_config, target_tag, prod_image, prod_name_int, prod_name_ext
        )

    def push_commit_tag(self, prod_image: Image, prod_name_int: str, prod_name_ext: str) -> None:
        """Pushes commit tag if CI_COMMIT_TAG variable is set."""
        commit_tag = os.environ.get("CI_COMMIT_TAG")
        if commit_tag:
            self.podman_api.push(
                prod_image,
                prod_name_int,
                commit_tag,
                f"Pushing image to commit tag {prod_name_int}:{commit_tag}",
            )
            if not self.disable_external_push:
                self.podman_api.push(
                    prod_image,
                    prod_name_ext,
                    commit_tag,
                    f"Pushing image to commit tag {prod_name_ext}:{commit_tag}",
                )

    def push_source_tag(self, img_config: ImageConfig, image: Image) -> None:
        """Pushes source tag."""
        src_name = img_config.get_src_name()
        _, build_version_tag = img_config.get_image_name_opencode("src", cached=True)

        self.podman_api.push(
            image,
            src_name,
            build_version_tag,
            f"Pushing image to src tag {src_name}:{build_version_tag}",
        )

    def push_production_tag(self, img_config: ImageConfig, image: Image) -> None:
        """Pushes production tag."""
        prod_name_int = img_config.basename(with_project=True, with_registry=True)
        prod_name_ext, _ = img_config.get_image_name_opencode("src", cached=True)
        target_tag = img_config.get_target_tag()

        self.podman_api.push(
            image,
            prod_name_int,
            target_tag,
            f"Pushing image to production tag (internal) {prod_name_int}:{target_tag}",
        )
        if not self.disable_external_push:
            self.podman_api.push(
                image,
                prod_name_ext,
                target_tag,
                f"Pushing image to production tag {prod_name_ext}:{target_tag}",
            )

    def push_image_config_tag(self, tag: str, image: Image) -> None:
        registry = self.config["registry"]
        project = self.config["project"]
        log.info("Tag in image config: %s", tag)
        top_level_path_segment = tag.split("/")[0]

        img_name, img_tag = tag.rsplit(":", 1)
        img_name = replace_forbidden_chars(img_name)
        img_tag = replace_forbidden_chars(img_tag)

        if "/" in tag:
            if (
                self.disable_external_push
                and top_level_path_segment in self.config["external_registries"]
            ):
                log.info("Skipping push to remote registry: %s", tag)
                return

            self.podman_api.push(
                image,
                img_name,
                img_tag + "-unhardened",
                f"Pushing image to tag definition {img_name}:{img_tag}-unhardened",
            )
        else:
            img_name = f"{registry}/{project}/{img_name}"

            self.podman_api.push(
                image,
                img_name,
                img_tag + "-unhardened",
                f"Pushing image to internal registry (tag) {img_name}:{img_tag}-unhardened",
            )

    def push_image_config_tags(self, img_config: ImageConfig, image: Image) -> None:
        """Pushes image config tags."""
        for tag in img_config.config.tags:
            self.push_image_config_tag(tag, image)

    def push_build_version_tags(self, img_config: ImageConfig, image: Image) -> None:
        """Pushes build version tags."""
        prod_name_int = img_config.basename(with_project=True, with_registry=True)
        prod_name_ext, _ = img_config.get_image_name_opencode("src", cached=True)

        if not self.disable_buildversions:
            # push build version to harbor
            image_name, build_version_tag = img_config.get_new_image_name()
            self.prod_img_name = f"{image_name}:{build_version_tag}"
            # we must get the new image name for opencode before we push to main registry
            # otherwise the buildversion will be increased a second time
            _, build_version_tag = img_config.get_image_name_opencode()
            self.prod_name_int = f"{prod_name_int}:{build_version_tag}"
            self.podman_api.push(
                image,
                prod_name_int,
                build_version_tag,
                f"Pushing buildversion to internal registry {prod_name_int}:{build_version_tag}",
            )
            # push build version to opencode
            if not self.disable_external_push:
                self.podman_api.push(
                    image,
                    prod_name_ext,
                    build_version_tag,
                    f"Pushing buildversion to external registry {prod_name_ext}:{build_version_tag}",
                )

    def process_push(self, img_config: ImageConfig) -> None:
        """Process push stage."""
        img = img_config.name()
        config_tag = img_config.tag()
        target_tag = img_config.get_target_tag()
        prod_image_name = img_config.get_prod_image_name()
        image = self.podman_client.images.get(img + "-unhardened")
        prod_image = self.podman_client.images.get(prod_image_name)
        self.check_build_id(prod_image_name)
        opencode_img_name, _ = img_config.get_image_name_opencode("src", cached=True)
        prod_name_int = img_config.basename(with_project=True, with_registry=True)
        prod_name_ext = opencode_img_name

        self.push_latest_tag(img_config, image)
        self.push_stable_tag(img_config, image)
        self.push_rolling_and_system_package_version_tags(
            img_config, config_tag, prod_image, prod_name_int, prod_name_ext, target_tag
        )
        self.push_commit_tag(prod_image, prod_name_int, prod_name_ext)

        if "src" in self.stages and "src" in self.types and not self.disable_external_push:
            src_image_name = img_config.get_src_image_name(cached=True)
            src_image = self.podman_client.images.get(src_image_name)
            self.push_source_tag(img_config, src_image)

        self.push_production_tag(img_config, prod_image)
        self.push_image_config_tags(img_config, image)
        self.push_build_version_tags(img_config, prod_image)

    def load_ignores(self, img_config: ImageConfig) -> list[dict]:
        base_dir = self.config["base_dir"]
        distro = img_config.config.distro
        distro_codename = img_config.config.distro_codename
        image_dir = img_config.config.directory
        distro_ignores_filename = os.path.join(
            base_dir, "common", distro, distro_codename, "cve_ignores.yaml"
        )
        try:
            with open(distro_ignores_filename) as f:
                distro_ignores_data = yaml.safe_load(f)
            if distro_ignores_data is None:
                distro_ignores_data = list()
        except FileNotFoundError:
            log.warning("Ignore file not found: %s", distro_ignores_filename)
            distro_ignores_data = []
        except PermissionError:
            log.error("Permission denied when reading %s", distro_ignores_filename)
            distro_ignores_data = []
        except IsADirectoryError:
            log.error("Expected a file but found a directory: %s", distro_ignores_filename)
            distro_ignores_data = []
        except OSError as e:
            log.error("OS error while reading %s: %s", distro_ignores_filename, e)
            distro_ignores_data = []
        except yaml.YAMLError as e:
            log.error("Invalid YAML format in %s: %s", distro_ignores_filename, e)
            distro_ignores_data = []

        ignores_filename = os.path.join(image_dir, "cve_ignores.yaml")
        try:
            with open(ignores_filename) as f:
                image_ignores_data = yaml.safe_load(f)
            if image_ignores_data is None:
                image_ignores_data = list()
        except (OSError, yaml.YAMLError):
            image_ignores_data = list()
        distro_ignores_data_mapping = {item["id"]: item for item in distro_ignores_data}
        image_ignores_data_mapping = {item["id"]: item for item in image_ignores_data}
        distro_ignores_data_mapping.update(image_ignores_data_mapping)
        ignores_data = list(distro_ignores_data_mapping.values())
        return ignores_data

    def add_upstream_status(self, scan_result: list[dict]) -> list[dict]:
        # API: https://ubuntu.com/security/api/docs
        url = "https://ubuntu.com/security/cves/{}.json"

        new_result: list[dict] = []
        for item in scan_result:
            item_id = item["id"]
            if item_id.startswith("CVE-"):
                stati = []
                logging.info(f"Request CVE information from: {url.format(item_id)}")
                resp = requests.get(url=url.format(item_id))

                if resp.status_code != 200:
                    logging.error(f"HTTP response code {resp.status_code}")
                    item["upstream_status"] = "undefined"
                    new_result.append(item)
                    continue

                try:
                    data = resp.json()
                except json.decoder.JSONDecodeError as e:
                    logging.error(f"{e.msg} for {item_id}")
                    item["upstream_status"] = "undefined"
                    new_result.append(item)
                    continue

                if data.get("status") != "active":
                    item["upstream_status"] = data.get("status")
                    new_result.append(item)
                    continue

                for package in data.get("packages"):
                    # print(package.get("name"))
                    for release in package.get("statuses"):
                        if release.get("release_codename") == "jammy":
                            if release.get("description"):
                                status_info = f" ({release.get('description')})"
                            else:
                                status_info = ""
                            stati.append(f"{release.get('status')}{status_info}")
                status = ",".join(stati)
            else:
                logging.info(f"Skip request CVE information for: {item_id}")
                status = "undefined"

            item["upstream_status"] = status
            new_result.append(item)
        return new_result

    def perform_scan(self, img_config: ImageConfig) -> list[dict] | None:
        """
        Download scan from internal or external scanner.

        Raises:
            BuildprocessError: If scan timeout is reached
        """
        if not self.trigger_scan(img_config):
            return None

        now = time.time()
        while time.time() < now + self.config["scan_timeout"]:
            scan_result = self.download_scan(img_config)
            if scan_result is not None:
                return scan_result
            time.sleep(3)
        raise BuildprocessError("Scan timeout reached.")

    def prepare_analyzer_params(
        self, img_config: ImageConfig, scan_result: list[dict]
    ) -> dict[str, Any]:
        """Prepare analyer params."""
        image = img_config.config.basename
        image_dir = img_config.config.directory
        ignores = self.load_ignores(img_config)
        ok_vulnerabilities = self.config["ok_vulnerabilities"].split()
        vulnerability_format = self.config["vulnerability_format"]
        return {
            "image": image,
            "image_dir": image_dir,
            "scan_result": scan_result,
            "vulnerability_format": vulnerability_format,
            "ok_vulnerabilities": ok_vulnerabilities,
            "ignores": ignores,
        }

    def dump_analysis_parameters(self, img_config: ImageConfig, scan_result: list[dict]) -> None:
        """Dump analysis parameters to analyzer_params yaml file."""
        analyzer_params = self.prepare_analyzer_params(img_config, scan_result)
        with open(f"{self.project_dir}/analyzer_params.yaml", "w", encoding="utf-8") as file:
            yaml.dump(analyzer_params, file)

    def process_scan_result(self, img_config: ImageConfig, scan_result: list[dict]) -> str:
        """Process the scan result."""
        self.dump_analysis_parameters(img_config, scan_result)
        output = self.create_cve_report(img_config)
        return output

    def copy_files_to_container(
        self, container: Container, files_to_copy: list[tuple[str, str | None]]
    ) -> None:
        """
        Copy files to container. List of files is a tuple of src and dst file.
        src_file is relative to project_dir and dst_file is an absolute path in the container.
        If dst_file is None, src_path is the target directory within /tmp/

        Raises:
            BuildprocessError: If copying the files to the container fails
        """
        if not files_to_copy:
            return None
        log.info("Copy files to container")
        with tempfile.NamedTemporaryFile(
            mode="w+b", suffix=".tar", prefix="podman_cp_"
        ) as tmp_file:
            with tarfile.open(fileobj=tmp_file, mode="w") as tar_file:
                for src_file, dst_file in files_to_copy:
                    if dst_file is None:
                        dst_file = f"tmp/{src_file}"
                    else:
                        dst_file = dst_file.lstrip("/")
                    log.debug("Copy file: %s to %s", src_file, dst_file)
                    tar_file.add(f"{self.project_dir}/{src_file}", arcname=f"{dst_file}")
            tmp_file.flush()
            tmp_file.seek(0)
            success = container.put_archive("/", tmp_file.read())
        if not success:
            raise BuildprocessError(
                "Put file %s to container %s failed"
                % (src_file, container.attrs["Config"]["Image"])
            )

    def execute_container_commands(
        self, container: Container, installation_commands: list[list[str]]
    ) -> None:
        """
        Execute commands inside container.

        Raises:
            ContainerRuntimeError: If a command fails
        """
        log.info("Execute container commands.")
        for cmd in installation_commands:
            log.debug("Command %s", cmd)
            ret_code, output = container.exec_run(cmd, user="root", tty=True)
            # mypy does not know the correct type
            if isinstance(output, bytes):
                log.info(output.decode("utf-8"))
            else:
                log.warning("Unexpected output type: %s", type(output))
            if ret_code != 0:
                raise ContainerRuntimeError(f"Run command {cmd} failed.")

    def run_cve_agent(self, container: Container) -> str:
        ret_code, output = container.exec_run(
            ["sh", "-c", "cd /tmp && python3.10 -m factory.cve.cve_agent"], user="root", tty=True
        )
        if ret_code != 0:
            log.warning(f"Running cve_agent failed with status code: {ret_code}")
        # mypy does not know the correct type
        if isinstance(output, bytes):
            log.info(output.decode("utf-8"))
            result = output.decode("utf-8")
        else:
            result = f"Unexpected output type: {type(output)}"
            log.warning(result)
        return result

    def create_cve_report(self, img_config: ImageConfig) -> str:
        """Execute tasks with podman."""
        # we use the value which was set in the push buildversion stage to prevent retrieving
        # wrong incremented values from parallel feature branch pipelines building the same image.
        prod_img_name = self.prod_img_name
        self.check_build_id(prod_img_name)
        setup_script = "/tmp/add_custom_apt_config.sh"
        with self.run_podman(prod_img_name, img_config=img_config) as container:
            try:
                deb_filename_apt = download_apt(APT_VERSION, self.project_dir)
                files_to_copy: list[tuple[str, str | None]] = [
                    ("analyzer_params.yaml", None),
                    ("factory/__init__.py", None),
                    ("factory/cve/__init__.py", None),
                    ("factory/cve/cve_agent.py", None),
                    ("factory/cve/cve.py", None),
                    ("common/ubuntu/add_custom_apt_config.sh", setup_script),
                    (deb_filename_apt, None),
                ]
                self.copy_files_to_container(container, files_to_copy)
                installation_commands = [
                    ["chmod", "+x", "/usr/bin/dpkg"],
                    ["chmod", "+x", setup_script],
                    [setup_script],
                    ["dpkg", "-i", f"/tmp/{deb_filename_apt}"],
                    ["apt", "update"],
                    ["apt", "install", "python3.10"],
                ]
                self.execute_container_commands(container, installation_commands)
                output = self.run_cve_agent(container)

            except (
                BuildprocessError,
                ContainerRuntimeError,
                APIError,
                AttributeError,
                OSError,
                subprocess.CalledProcessError,
            ) as e:
                log.exception("The installation of the CVE agent failed: %s", e)
                sys.exit(1)
        return output

    def process_scan(self, img_config: ImageConfig) -> None:
        """
        Process scan stage. Add upstream cve status to scan results.

        Raises:
            BuildprocessError: If scan fails
        """
        # The scan would also work for other images. however,
        # an output-ready format is created within the image in the `create_cve_report` function.
        # So there is no need to perform a scan that cannot output any results.
        if img_config.config.distro_codename != "jammy":
            log.info("CVE scan is only supported for Ubuntu Jammy images.")
            return None

        scan_result = self.perform_scan(img_config)
        if scan_result is None:
            raise BuildprocessError("Scan failed.")
        scan_result = self.add_upstream_status(scan_result)
        self.process_scan_result(img_config, scan_result)

    def get_signature(self, img_config: ImageConfig, digest: str) -> dict:
        """Get signature for image by digest."""
        base_url = img_config.config.registry_url.rstrip("/")
        project = img_config.config.project
        base_name = img_config.config.basename
        sig_url = f"{base_url}/v2/{project}/{base_name}/manifests/sha256-{digest}.sig"
        log.info(f"Downloading signature {sig_url}")
        res = requests.get(
            sig_url,
            headers={"Accept": "application/vnd.oci.image.manifest.v1+json"},
            auth=(self.config["registry_user"], self.config["registry_password"]),
            verify=self.verify,
        )
        return res.json()

    def setup_docker_auth(self, docker_dir: str = "/home/build/.docker") -> None:
        """
        Sets up Docker authentication by copying the auth.json file from XDG_RUNTIME_DIR to the docker config directory.
        """
        runtime_dir = os.environ.get("XDG_RUNTIME_DIR", "")
        os.makedirs(docker_dir, exist_ok=True)
        output = subprocess.check_output(
            f"cat {runtime_dir}/containers/auth.json > {docker_dir}/config.json", shell=True
        )
        log.info(output)

    def prepare_image_signature(self, image_name: str, img_config: ImageConfig) -> tuple[str, str]:
        """
        Prepares the image signature by pulling the image, getting the digest, and setting up the full digest string.

        Raises:
            BuildprocessError: If the image inspection fails
            APIError: If the image pull fails (images.pull)
        """
        registry = img_config.config.registry
        project = img_config.config.project
        base_name = img_config.config.basename
        tag = image_name.rsplit(":", 1)[1]

        repository = f"{project}/{base_name}:{tag}"
        if registry == "registry.opencode.de" or "harbor" in registry:
            token = get_bearer_token(
                registry,
                repository,
                img_config.config.registry_user,
                img_config.config.registry_password,
                self.verify,
            )
        else:
            token = None
        inspect_data = inspect(registry, project, base_name, tag, token)
        if inspect_data is None:
            raise BuildprocessError("Could not inspect image.")
        digest = inspect_data["Digest"]
        # we expect getting one Image
        image_obj = cast(Image, self.podman_api.pull(image_name))
        digest_wrong = image_obj.attrs["Digest"]
        sha_sum = digest.split(":")[1]
        full_digest = f"{registry}/{project}/{base_name}@{digest}"
        log.info(base64.b64encode(full_digest.encode("utf-8")))
        log.info("digest: %s", full_digest)
        log.info("wrong digest: %s", digest_wrong)
        return full_digest, sha_sum

    def sign_and_log_image(
        self,
        registry: str,
        username: str,
        password: str,
        tuf_url: str,
        fulcio_url: str,
        rekor_url: str,
        full_digest: str,
        sha_sum: str,
        img_config: ImageConfig,
    ) -> None:
        """
        Signs the image using cosign and logs the signature. Handles errors related to signing process.
        """
        try:
            output = subprocess.check_output(
                f"cosign login {registry} -u {username} -p {password}", shell=True
            )
            log.info(output)
            output = subprocess.check_output(
                f"cosign initialize --root {tuf_url}/root.json --mirror {tuf_url}", shell=True
            )
            log.info(output.decode("utf-8"))
            output = subprocess.check_output(
                f"cosign sign -d -y --fulcio-url={fulcio_url} --rekor-url={rekor_url} "
                f"--allow-insecure-registry {full_digest}",
                shell=True,
            )
            log.info(output)
            log.info(pformat(self.get_signature(img_config, sha_sum), sort_dicts=False))
        except subprocess.CalledProcessError as e:
            log.error("Cosign command failed: %s", e)
            log.error("Return code: %d", e.returncode)
            log.error("Command output: %s", e.output)
        except (subprocess.SubprocessError, UnicodeDecodeError) as e:
            log.exception("An exception occured: %s", e)
            log.exception("Maybe you have to initialize your tuf root with:")
            log.exception("cosign initialize --root %s/root.json --mirror %s", tuf_url, tuf_url)
        except FileNotFoundError:
            log.exception("Cosign is not installed or not found in PATH.")
        except OSError as e:
            log.exception("OS-related or permission error occurred: %s", e)

    def sign_image(
        self,
        image_name: str,
        img_config: ImageConfig,
        fulcio_url: str,
        rekor_url: str,
        tuf_url: str,
    ) -> None:
        """
        Main function to sign an image. This function organizes the workflow of image signing by calling other sub-functions.
        """
        # We have to fix cosigns dependency on the docker auth file
        self.setup_docker_auth()
        full_digest, sha_sum = self.prepare_image_signature(image_name, img_config)
        self.sign_and_log_image(
            img_config.config.registry,
            self.config["registry_user"],
            self.config["registry_password"],
            tuf_url,
            fulcio_url,
            rekor_url,
            full_digest,
            sha_sum,
            img_config,
        )

    def get_id_token(
        self, url: str, client_id: str, client_secret: str, username: str, password: str
    ) -> str | None:
        """
        Retrieves an ID token from the given URL using the provided credentials.

        Args:
            url: The token URL to authenticate against.
            client_id: Client ID for authentication.
            client_secret: Client secret for authentication.
            username: Username for authentication.
            password: Password for authentication.

        Returns:
            The retrieved ID token.
        """
        response = requests.post(
            url=url,
            data={
                "grant_type": "password",
                "client_id": client_id,
                "client_secret": client_secret,
                "username": username,
                "password": password,
                "scope": "openid",
            },
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        if not response.ok:
            return None

        data = response.json()
        id_token = data.get("id_token")
        # Keep the following lines for future debugging purposes:
        # access_token = data.get('access_token')
        # claims = jwt.get_unverified_claims(id_token)
        # header = jwt.get_unverified_header(id_token)
        # pprint(header)
        # pprint(claims)
        # claims = jwt.get_unverified_claims(access_token)
        # header = jwt.get_unverified_header(access_token)
        # pprint(header)
        # pprint(claims)
        return id_token

    def process_image_signing(
        self, img_config: ImageConfig, id_token: str, fulcio_url: str, rekor_url: str, tuf_url: str
    ) -> None:
        """Processes the signing of images based on the given configuration and token."""
        os.environ["SIGSTORE_ID_TOKEN"] = id_token
        log.info("ID token: %s", id_token)

        # sign targetversion production image internal
        targetversion_int = img_config.get_targetversion_prod_int()
        self.sign_image(targetversion_int, img_config, fulcio_url, rekor_url, tuf_url)

        # sign buildversion image
        if not self.disable_buildversions:
            buildversion_prod_int = img_config.get_buildversion_prod_int(cached=True)
            self.sign_image(buildversion_prod_int, img_config, fulcio_url, rekor_url, tuf_url)

        log.info("Imagesigning finished.")

    def process_sign(self, img_config: ImageConfig) -> None:
        """
        Main function to handle the signing process of images.

        Args:
            img_config: The image configuration object.
        """
        self.set_build_options(img_config)

        keycloak_server = os.environ["KEYCLOAK_SERVER"]
        keycloak_realm = os.environ["KEYCLOAK_REALM"]
        token_url = (
            f"https://{keycloak_server}/realms/{keycloak_realm}/protocol/openid-connect/token"
        )
        client_id = os.environ["KEYCLOAK_CLIENT_ID"]
        client_secret = os.environ["KEYCLOAK_CLIENT_SECRET"]
        username = os.environ["KEYCLOAK_USERNAME"]
        password = os.environ["KEYCLOAK_PASSWORD"]
        fulcio_url = os.environ["FULCIO_URL"]
        rekor_url = os.environ["REKOR_URL"]
        tuf_url = os.environ["TUF_URL"]

        id_token = self.get_id_token(token_url, client_id, client_secret, username, password)
        if id_token:
            self.process_image_signing(img_config, id_token, fulcio_url, rekor_url, tuf_url)

    def process_dependencies(self, img_config: ImageConfig) -> None:
        """
        Raises:
            APIError: If the image pull fails (images.pull)
        """
        img_conf = img_config.config
        for dep in img_conf.depends:
            dep_config = self.image_configs[dep]
            repo_tag = dep_config.baseimage_name_with_tag(with_project=True, with_registry=True)
            log.info("Pulling dependency %s", repo_tag)
            # we expect getting one Image
            image = cast(Image, self.podman_api.pull(repo_tag))
            if image.id:
                image_tag = self.image_configs[dep].tag()
                dependency_repo = self.image_configs[dep].basename(with_project=True)
                image.tag(dependency_repo, image_tag)

    def set_build_options(self, img_config: ImageConfig) -> None:
        """Set the build options based on the environment variables and image configuration."""
        self.disable_external_push = False
        self.disable_buildversions = False
        if os.environ.get("DISABLE_EXTERNAL_PUSH") == "1":
            self.disable_external_push = True
        if os.environ.get("DISABLE_BUILDVERSIONS") == "1":
            self.disable_buildversions = True
        # disable_external_push in img_conf has precedence over environment variable
        # so we can exclude f.e. builder from being pushed even in main environment,
        # where pushs are not disabled in environment variable
        if img_config.config.disable_external_push:
            self.disable_external_push = img_config.config.disable_external_push
        if img_config.config.disable_buildversions:
            self.disable_buildversions = img_config.config.disable_buildversions

    def check_skip(self, img_config: ImageConfig) -> bool:
        """
        This function checks if a canonical name of an image is in the list of images which can be provided with
        the images parameter of the container factory. It allows the comparison against a list of image basenames or
        alternatively a list of tags including a colon which are saved in the images property of the ImageBuilder
        during the initialisation of the ImageBuilder.
        """
        log.debug("check skip Image: %s", img_config.name())
        basenames = [image for image in self.images if ":" not in image]
        log.debug("basenames %s", basenames)
        tagnames = [image for image in self.images if ":" in image]
        log.debug("tagnames %s", tagnames)

        if (
            (tagnames and img_config.name() in tagnames)
            or (basenames and img_config.basename() in basenames)
            or (not (tagnames or basenames))  # both must be None
        ):
            skip = False
        else:
            skip = True
        return skip

    def run_stages(self, img_config: ImageConfig) -> None:
        img = img_config.name()
        stage_methods = [
            ("test", self.process_tests),
            ("ref", self.process_references),
            ("src", self.process_sources),
            ("strip", self.process_strip),
            ("bom", self.process_bom),
            ("push", self.process_push),
            ("scan", self.process_scan),
            ("sign", self.process_sign),
            ("qa", self.process_qa),
        ]

        log.info("Processing image %s", img)
        log.info("Stages: %s", self.stages)

        self.check_mandatory_image_definition_properties(img_config)
        self.set_build_options(img_config)
        self.process_dependencies(img_config)

        if ("check" not in self.stages or self.needs_update(img_config)) and "build" in self.stages:
            # create unique build id
            self.build_id = str(uuid.uuid4())
            self.process_build(img_config)

        else:
            log.info("Image %s checked, no rebuild needed", img)
            return None

        for stage, method in stage_methods:
            # conditions for skipping a stage are part of the stage functions
            if stage in self.stages:
                log.info("Running stage: %s", stage)
                method(img_config)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="update images")
    parser.add_argument("--config", default="config.yaml", help="configuration file name")
    parser.add_argument(
        "--keep", default=False, action="store_true", help="keep files generated from templates"
    )
    parser.add_argument(
        "--stages",
        default=None,
        help="stages to execute (render, lintcheck, spellcheck, check, build, test, strip, ref, src, "
        "bom, qa, push, scan, sign), default: all",
    )
    parser.add_argument("--images", default=None, help="list of images to build, default: all")
    parser.add_argument(
        "--types", default=None, help="types to build (standard,production,src), default: all"
    )
    parser.add_argument(
        "--internal-scanner",
        default=False,
        action="store_true",
        help="use internal scanner instead of harbor",
    )
    args, unknown = parser.parse_known_args()
    env_extras = dict(map(lambda i: i.split("=", 1), unknown))

    if os.environ.get("DEBUG") == "1":
        log.info("SECURELOG" + "*" * 62)
        log.info(get_encrypted_env())
        log.info("*" * 72)

    if args.images:
        images = args.images.split(",")
    else:
        images = []
    if args.stages:
        stages = args.stages.split(",")
    else:
        stages = []
    try:
        ib = ImageBuilder(
            config=args.config, keep=args.keep, stages=stages, images=images, env_extras=env_extras
        )
        ib.run()
    except TransientError as e:
        log.warning("Transient error: %s", repr(e))
        log.error("Program will exit.")
