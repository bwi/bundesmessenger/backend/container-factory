# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import base64
import json
import logging
import logging.config
import os
from datetime import datetime, timezone

import urllib3
import yaml
from dateutil.parser import parse
from podman import PodmanClient

from factory.util.utils import (
    delete_image,
    get_bearer_token,
    get_encrypted_env,
    get_tags_for_repository,
    inspect,
)

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logging.basicConfig(level=logging.INFO)
log = logging.getLogger

RETENTION_DAYS = 30
RETENTION_CONFIG = "retention_config.yaml"
MAINTENANCE_LIST = "maintenance_list.yaml"


def get_retention_ids(retention_list: list[str], registry: str, project: str) -> list[str]:
    retention_ids = list()
    for image in retention_list:
        image_name, tag = image.split(":")
        data = inspect(registry, project, image_name, tag)
        if not data:
            continue
        image_id = data["Digest"]
        log(__name__).info(f"Added digest {image_id} of {image_name}:{tag} to retention list.")
        retention_ids.append(image_id)
    return retention_ids


def process_all_images(
    all_images: list[str],
    registry: str,
    project: str,
    config: dict,
    verify: bool,
    retention_ids: list[str],
) -> None:
    for image in all_images:
        log(__name__).info("Get tags for repository.")
        if registry == "registry.opencode.de" or "harbor" in registry:
            token = get_bearer_token(
                registry,
                f"{project}/{image}",
                config["registry_user"],
                config["registry_password"],
                verify,
            )
        else:
            token = None
        tags = get_tags_for_repository(
            registry, project, image, config["registry_user"], config["registry_password"], token
        )

        for tag in tags:
            remote_name = f"{registry}/{project}/{image}:{tag}"
            log(__name__).info(f"Processing {remote_name}.")
            data = inspect(registry, project, image, tag, token)
            if not data:
                continue
            creation_date = data["Created"]
            creation_date = parse(creation_date)
            digest = data["Digest"]
            if digest in retention_ids:
                log(__name__).info(
                    f"Skipped image {image}:{tag} with id {digest} from retention list"
                )
                continue
            days = (datetime.now(timezone.utc) - creation_date).days
            retention_days = RETENTION_DAYS
            if days > retention_days:
                log(__name__).info(
                    f"{image}:{tag} is older than {retention_days} days. Delete image."
                )
                delete_image(registry, project, image, digest, config, token, verify)
            else:
                log(__name__).info(f"{image}:{tag} is younger than {retention_days} days")


def delete_process() -> None:
    with open(RETENTION_CONFIG) as f:
        retention_config = yaml.safe_load(f)
        retention_list = retention_config["retention_list"]
    if not isinstance(retention_list, list):
        print(
            "You must specifiy a retention_list in the retention_list property of the retention config. (it can be empty ([])"
        )
        exit()
    print("Retention list:")
    print(retention_list)

    time_string = datetime.now().strftime("%Y%m%d-%H%_M_%S")
    logging.basicConfig(
        encoding="utf-8",
        level=logging.INFO,
        handlers=[logging.FileHandler(f"delete_images_{time_string}.log"), logging.StreamHandler()],
    )
    log = logging.getLogger

    auths = json.loads(os.environ.get("DOCKER_AUTH_CONFIG", "{}")).get("auths", dict())
    auth_lookup = dict()
    for reg, cred in auths.items():
        user, pwd = base64.b64decode(cred["auth"]).decode("latin1").split(":", 1)
        auth_lookup[reg] = (user, pwd)

    if os.environ["TARGET"] == "opencode":
        registry = "registry.opencode.de"
        project = "bwi/bundesmessenger/backend/container-images"
        registry_user, registry_password = auth_lookup[registry]
    else:
        registry = os.environ["DOCKER_REGISTRY"]
        project = os.environ["PROJECT"]
        registry_user = os.environ["REGISTRY_USER"]
        registry_password = os.environ["REGISTRY_PASSWORD"]

    registry_url = f"https://{registry}"
    config = {"registry_user": registry_user, "registry_password": registry_password}

    uid = os.getuid()
    user_sock = f"/run/user/{uid}/podman/podman.sock"
    service_sock = "/home/build/podman.sock"
    host_sock = "/var/run/podman/podman.sock"
    if os.path.exists(user_sock):
        uri = "unix://" + user_sock
    elif os.path.exists(service_sock):
        uri = "unix://" + service_sock
    else:
        uri = "unix://" + host_sock

    if os.environ.get("DEVELOP", "1") == "0":
        verify = False
    else:
        verify = True

    podman_client = PodmanClient(base_url=uri)
    log(__name__).info(f"Login with reauth to {registry_url}")
    response = podman_client.login(
        username=registry_user, password=registry_password, registry=registry_url, reauth=True
    )
    log(__name__).info(response)

    with open(MAINTENANCE_LIST) as f:
        maintenance_data = yaml.safe_load(f)
        all_images = maintenance_data["maintained_images"]
        if len(all_images) < 1:
            print("You must define at least one image in the maintenance_list.yaml file.")
            exit()

    retention_ids = get_retention_ids(retention_list, registry, project)

    process_all_images(all_images, registry, project, config, verify, retention_ids)


if __name__ == "__main__":
    if os.environ.get("DEBUG") == "1":
        log(__name__).info("SECURELOG" + "*" * 62)
        log(__name__).info(get_encrypted_env())
        log(__name__).info("*" * 72)
    delete_process()
