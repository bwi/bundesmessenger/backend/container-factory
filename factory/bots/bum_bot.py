import logging
import os
from datetime import datetime, timedelta
from tempfile import TemporaryDirectory

import yaml
from git import Repo
from semver import Version
from yaml.nodes import ScalarNode

from factory.bots.version_bot import VERSIONS_FILE, VersionBot

log = logging.getLogger(__name__)
logging.basicConfig(format="%(levelname)s %(message)s", level=logging.INFO)

CHART_SUPPORT_DAYS = 365 / 2


class BumBot(VersionBot):

    def get_tags_from_version_file(self, full_path: str) -> list[str]:
        """
        Extracts the tags from a versions file of the BundesMessenger helm chart.
        The versions file contain the used tags of a specific helm chart version.
        Return a list of tags.
        """

        # Override the timestamp constructor to treat all dates as strings
        yaml.SafeLoader.add_constructor(
            "tag:yaml.org,2002:timestamp",
            lambda loader, node: (
                loader.construct_scalar(node) if isinstance(node, ScalarNode) else None
            ),
        )
        with open(full_path, encoding="utf-8") as f:
            data = yaml.load(f, Loader=yaml.SafeLoader)
        tags = list()
        chart_date = datetime.strptime(data[0]["date"], "%Y-%m-%d").date()
        if chart_date >= (datetime.today().date() - timedelta(days=CHART_SUPPORT_DAYS)):
            for image in data[0]["images"]:
                name = image["image"].split("/")[-1]
                version_tag = image["tag"]
                tags.append(f"{name}:{version_tag}")
        return tags

    def get_used_tags_from_helm_chart_repo(self) -> list[str]:
        """
        Load all used tags of all releases of helm charts from version files of the helm chart repo.
        """
        try:
            username = os.environ["BOT_HELM_CHART_REPO_USER"]
            password = os.environ["BOT_HELM_CHART_REPO_PASSWORD"]
            remote = f"https://{username}:{password}@{os.environ['HELMCHART_REPO']}"
        except KeyError:
            raise Exception(
                "The BOT_HELM_CHART_REPO_USER, BOT_HELM_CHART_REPO_PASSWORD and HELMCHART_REPO environment variable must be set."
            )

        with TemporaryDirectory(prefix="version_bot_helmchart") as helmchart_path:
            Repo.clone_from(remote, helmchart_path, branch="develop", single_branch=True, depth=1)
            version_dir = os.path.join(helmchart_path, "ci", "versions")
            filenames = [
                file
                for file in os.listdir(version_dir)
                if file.startswith("v") and file.endswith(".yaml")
            ]
            tags = list()
            for filename in filenames:
                full_path = os.path.join(helmchart_path, "ci", "versions", filename)
                tags_of_version_file = self.get_tags_from_version_file(full_path)
                tags += tags_of_version_file
            return tags

    def pre_process_pre_hook(self) -> None:
        used_tags = self.get_used_tags_from_helm_chart_repo()
        self.retention_config["retention_list"] += used_tags

    def update_versions_file(self) -> None:
        """
        Updates the images.yaml by adding all used tags of all releases of a helm chart and removing deprecated versions.
        """
        maintenance_dict = self.get_versions_file()
        used_tags = self.get_used_tags_from_helm_chart_repo()
        used_tags = self.remove_deprecated_versions(used_tags)
        for tag in used_tags:
            image_name, version_tag = tag.split(":")
            used_version = self.extract_version(version_tag)
            if used_version is None:
                log.info("The tag %s contains no version information.", tag)
                continue
            if image_name in maintenance_dict["images"]["ubuntu"]["jammy"]:
                version_items = maintenance_dict["images"]["ubuntu"]["jammy"][image_name][
                    "versions"
                ]
                maintained_versions = [item["version"] for item in version_items]
                if used_version not in maintained_versions:
                    base_images = self.retrieve_base_images(
                        Version.parse(used_version),
                        maintenance_dict["images"]["ubuntu"]["jammy"][image_name]["versions"],
                    )
                    version_item = {"base_images": base_images, "version": used_version}
                    maintenance_dict["images"]["ubuntu"]["jammy"][image_name]["versions"].append(
                        version_item
                    )
        self.write_versions_file(maintenance_dict)
        # When we update the file, we must also update self.data so
        # coming changes are based on the new data
        self.data = maintenance_dict

    def pre_process_post_hook(self) -> None:
        self.update_versions_file()
        if self.version_repo.is_dirty(path=self.versions_file_filename):
            self.index.add(VERSIONS_FILE)
            if self.index.diff("HEAD"):
                self.index.commit("version-bot: updated maintenance dict")
                self.changed = True


if __name__ == "__main__":
    bot = BumBot()
    bot.run()
