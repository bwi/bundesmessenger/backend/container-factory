# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import re
import shutil
from typing import Any

import requests
import yaml
from git import Repo
from natsort import natsorted
from semver import Version

from factory.util.utils import pushd
from factory.util.version_fetcher import (
    GitLabVersionFetcher,
    GitVersionFetcher,
    NexusVersionFetcher,
    VersionFetcher,
)

log = logging.getLogger(__name__)
logging.basicConfig(format="%(levelname)s %(message)s", level=logging.INFO)

VERSIONS_FILE = "images.yaml"
RETENTION_CONFIG = "retention_config.yaml"
CHART_SUPPORT_DAYS = 365 / 2

BASE_IMAGES_REPO_PATH = "/tmp/base-image-versions"


class QuotedDumper(yaml.SafeDumper):
    """YAML Dumper to force string quotes."""

    def represent_scalar(self, tag: str, value: Any, style: str | None = None) -> yaml.ScalarNode:
        if isinstance(value, str):
            # Force single quotes for all strings
            style = "'"
        return super().represent_scalar(tag, value, style=style)


class NoAliasDumper(yaml.SafeDumper):
    def ignore_aliases(self, data: Any) -> bool:
        return True


class QuotedNoAliasDumper(QuotedDumper, NoAliasDumper):
    pass


class VersionBot:

    def __init__(self, branch: str = "main") -> None:
        self.branch = branch
        self.data: dict[str, Any] = {}
        self.changed = False
        self.mr_allowed = True
        self.git_server = os.environ["GIT_SERVER"]
        self.project = os.environ["PROJECT"]
        self.project_id = os.environ["CI_PROJECT_ID"]
        self.remote_path = f"{self.git_server}/{self.project}/container-images/base-images.git"
        self.local_path = BASE_IMAGES_REPO_PATH
        self.versions_file_filename = os.path.join(self.local_path, VERSIONS_FILE)
        self.retention_config_filename = os.path.join(self.local_path, RETENTION_CONFIG)
        self.retention_config: dict = dict()
        self.username = os.environ["BOT_USER"]
        self.password = os.environ["BOT_PASSWORD"]
        self.remote = f"https://{self.username}:{self.password}@{self.remote_path}"

        self.download_images_repo()

    def checkout_mr_branch(self) -> None:
        """Checks out a branch for and creates a merge request."""
        merge_requests = self.get_merge_requests_by_prefix(
            self.git_server, self.project_id, self.password, f"bot/version-update-{self.branch}"
        )
        open_merge_requests = [mr["title"] for mr in merge_requests if mr["state"] == "opened"]
        if len(open_merge_requests) > 0:
            print(
                "There is already an open merge request with the prefix bot/version-update.\n"
                "Please merge or close the existing merge request before starting again."
            )
            self.mr_allowed = False
        else:
            mr_ids = [int(mr["title"].split("-")[-1]) for mr in merge_requests]
            if len(mr_ids):
                mr_id = max(mr_ids) + 1
            else:
                mr_id = 1
            self.version_repo.git.checkout(self.branch)
            self.mr_branch = self.version_repo.create_head(
                f"bot/version-update-{self.branch}-{mr_id}"
            )
            self.version_repo.git.checkout(self.mr_branch)

    def load_data(self) -> None:
        """Loads the data from the version file."""
        with pushd(self.local_path):
            self.data = self.get_versions_file()

    def create_gitlab_merge_request(
        self,
        git_server: str,
        project_id: str,
        source_branch: str,
        target_branch: str,
        title: str,
        description: str,
        private_token: str,
    ) -> dict:
        """
        Create a GitLab merge request.

        Args:
        - project_id: The ID of the GitLab project.
        - source_branch: The name of the source branch for the merge request.
        - target_branch: The name of the target branch for the merge request.
        - title: The title of the merge request.
        - description: The description for the merge request.
        - private_token: The private token for GitLab API authentication.

        Returns:
        - The response from the GitLab API (either the merge request details or an error message).
        """

        # Define the GitLab API endpoint for merge requests
        url = f"https://{git_server}/api/v4/projects/{project_id}/merge_requests"

        # Define the payload
        payload = {
            "source_branch": source_branch,
            "target_branch": target_branch,
            "title": title,
            "description": description,
        }

        # Define headers with the private token for authentication
        headers = {"PRIVATE-TOKEN": private_token}

        # Send the POST request to create the merge request
        response = requests.post(url, data=payload, headers=headers)

        if response.status_code == 201:
            return response.json()  # Return merge request details
        else:
            raise Exception(
                f"Failed to create merge request. Status code: {response.status_code}, Message: {response.json()}"
            )

    def get_merge_requests_by_prefix(
        self, git_server: str, project_id: str, private_token: str, prefix: str
    ) -> list[dict]:
        """
        Fetch all merge requests from a GitLab project with a title starting with a given prefix.

        Args:
            git_server: The GitLab git_server, e.g., "https://gitlab.com"
            project_id: The project's ID.
            private_token: Personal access token.
            prefix: Prefix to filter merge requests by title.

        Returns:
            List of merge requests with titles starting with the given prefix.
        """

        # Base URL for Merge Requests API
        base_url = f"https://{git_server}/api/v4/projects/{project_id}/merge_requests"

        # Headers including the private token
        headers = {"PRIVATE-TOKEN": private_token}

        # Placeholder to store merge requests
        merge_requests_with_prefix = []

        # Handle pagination
        page = 1
        per_page = 20  # Default and max per page for GitLab API
        while True:
            response = requests.get(
                base_url, headers=headers, params={"page": page, "per_page": per_page}
            )
            response.raise_for_status()  # Raise an error for bad responses
            merge_requests = response.json()

            # Filter merge requests based on the prefix
            for mr in merge_requests:
                if mr["title"].startswith(prefix):
                    merge_requests_with_prefix.append(mr)

            # If we've retrieved less than `per_page` merge requests, it means we're on the last page
            if len(merge_requests) < per_page:
                break

            page += 1

        return merge_requests_with_prefix

    def download_images_repo(self) -> None:
        """
        Clones the images repository to get retention list und versions file.
        """
        if os.path.exists(self.local_path):
            shutil.rmtree(self.local_path)

        self.version_repo = Repo.clone_from(self.remote, self.local_path)
        self.version_repo.config_writer().set_value("user", "name", self.username).release()
        self.version_repo.config_writer().set_value(
            "user", "email", f"{self.username}@noreply.{self.git_server}"
        ).release()
        self.origin = self.version_repo.remote(name="origin")
        self.index = self.version_repo.index

    def get_versions_file(self) -> dict[str, Any]:
        """Retrieves the images.yaml and returns it."""
        with open(self.versions_file_filename, encoding="utf-8") as f:
            maintenance_dict: dict[str, Any] = yaml.safe_load(f)
        return maintenance_dict

    def write_versions_file(self, data: dict[str, Any]) -> None:
        """Writes the images.yaml."""
        with open(self.versions_file_filename, "w", encoding="utf-8") as file:
            yaml.dump(data, file, Dumper=NoAliasDumper)

    def extract_version(self, tag: str) -> str | None:
        """
        Extracts a version number pattern (e.g., "1.2.3") from a given string.
        Returns the version number as a string if found, otherwise returns None.

        Example:
        >>> extract_version("version 2.4.1 is released")
        '2.4.1'
        """
        pattern = r"\d+\.\d+\.\d+"
        match = re.search(pattern, tag)
        if match:
            return match.group(0)
        else:
            return None

    def remove_deprecated_versions(self, tag_list: list[str]) -> list[str]:
        """
        Remove deprecated versions from the retention list.
        Deprecated version can be defined by setting the min_versions setting in the retention_config.yaml file.
        All versions lower the specified version will be removed from the retention list.
        """
        min_versions = self.retention_config.get("min_versions")
        # if no min_versions defined, we do not remove versions
        if min_versions is None:
            return tag_list
        for tag in tag_list[:]:
            image_name, version_tag = tag.split(":")
            if image_name in min_versions.keys():
                min_version = min_versions[image_name]
                tag_version = self.extract_version(version_tag)
                if tag_version is None:
                    log.info(
                        "The tag %s does not contain a version and is ignored for removal from the retention list.",
                        tag,
                    )
                elif Version.parse(tag_version).compare(min_version) < 0:
                    log.info(
                        "The tag version %s is smaller than the minimum version %s. "
                        "It is therefore removed from the retention list.",
                        tag,
                        min_version,
                    )
                    tag_list.remove(tag)
        return tag_list

    def retrieve_base_images(
        self, used_version: Version, version_items: list[dict[str, Any]]
    ) -> list:
        """
        Retrieves the base images of a version.
        It first tries to get a predecessor version to retrieve the base images.
        If that fails, it tries to get the base images of a successor version.
        This retrieval of base versions for a version is necessary in full automated use cases,
        where for a newly added version is no base image defined.
        """
        lower_versions: list[dict[str, Any]] = []
        higher_versions: list[dict[str, Any]] = []

        for version_item in version_items:
            # Find all lower versions
            if Version.parse(version_item["version"]) < used_version:
                lower_versions.append(version_item)
            # Find all higher or equal versions
            else:
                higher_versions.append(version_item)

        if lower_versions:
            # Get the highest lower version
            highest_version_item = max(lower_versions, key=lambda x: Version.parse(x["version"]))
            return highest_version_item["base_images"]

        if higher_versions:
            # Get the lowest higher or equal version
            lowest_version_item = min(higher_versions, key=lambda x: Version.parse(x["version"]))
            return lowest_version_item["base_images"]

        raise Exception(  # NOSONAR
            "There are no versions configured from which we could retrieve the base images."
        )

    def load_retention_config(self) -> None:
        """Loads the retention list."""
        with pushd(self.local_path):
            with open(self.retention_config_filename, encoding="utf-8") as f:
                self.retention_config = yaml.safe_load(f)

    def add_missing_source_tags_to_retention_list(self, retention_list: list[str]) -> list[str]:
        """
        Add missing source tags to the retention list.
        This function checks, if for a production image a corresponding src image tag is already in the list.
        """
        for tag in retention_list[:]:
            image_name, version_tag = tag.split(":")
            if "production" in version_tag:
                source_tag = version_tag.replace("production", "src")
                if f"{image_name}:{source_tag}" not in retention_list:
                    retention_list.append(f"{image_name}:{source_tag}")
        return retention_list

    def update_retention_config(self) -> None:
        """
        Updates the retention list by adding used tags of the helm chart releases and removing reprecated tags.
        The retention list will be sorted and converted in an unique list.
        """
        retention_list = self.retention_config["retention_list"]
        retention_list = self.remove_deprecated_versions(retention_list)
        retention_list = self.add_missing_source_tags_to_retention_list(retention_list)
        retention_list = list(set(retention_list))
        self.retention_config["retention_list"] = natsorted(retention_list)
        with open(self.retention_config_filename, "w", encoding="utf-8") as f:
            # We use the QuotedNoAliasDumper to force single Quotes for all strings in order to keep the retention list
            # updates consistent, so that no changes in quoting lead to an update of otherwise unchanged lines.
            # Also prevent the usage of aliases for the same reason.
            yaml.dump(self.retention_config, f, Dumper=QuotedNoAliasDumper)

    def previous_version(self, version: str) -> str:
        """Calculates the preceding version of a semver version by decrementing the patch version."""
        # Split the version string into its components
        components = version.split(".")

        # Check if the version string has at least two components (X.Y)
        if len(components) < 2:
            raise ValueError("Invalid version string format. It should be at least X.Y")

        try:
            # Reduce the last part and convert it back to an integer
            last_part = int(components[-1])
            last_part -= 1

            # Reassemble the version string with the reduced last part
            previous_version = ".".join(components[:-1] + [str(last_part)])

            return previous_version
        except ValueError:
            raise ValueError("Invalid version string format. Last part must be an integer.")

    def is_used_version(self, image: str, version: str) -> bool:
        """Checks if image with the version is used in the retention list."""
        for record in self.retention_config["retention_list"]:
            record_image, record_tag = record.split(":")
            if image == record_image and version in record_tag:
                return True
        return False

    def update_latest_version(
        self, distro: str, distro_codename: str, image: str, latest: Version
    ) -> None:
        """Updates the latest version in the version file and commits the change."""
        latest_str = str(latest)
        latest_with_dist = f"{latest_str}-{distro_codename}"
        image_data = self.data["images"][distro][distro_codename][image]

        # skip updating when new latest is already the latest version
        if image_data["latest"] == latest_with_dist:
            return None

        log.info("Found new latest version for %s.", image)
        # when we add more distros we will read the distro from the tag
        # lookup the list for the entry with the information to the current/old latest
        old_latest = image_data["latest"].replace(f"-{distro_codename}", "")
        old_latest_item = next(
            version_item
            for version_item in image_data["versions"]
            if version_item["version"] == old_latest
        )
        latest_base_images = old_latest_item["base_images"]
        image_data["latest"] = latest_with_dist
        # add new latest to list of versions
        if latest_str not in image_data["versions"]:
            image_data["versions"].append(
                {"version": latest_str, "base_images": latest_base_images}
            )

            # write to file and commit
            self.write_versions_file(self.data)
            if self.mr_allowed and self.version_repo.is_dirty(path=self.versions_file_filename):
                self.index.add(VERSIONS_FILE)
                self.index.commit(f"version-bot: updated latest version of {image} to {latest_str}")
                self.changed = True

    def remove_unused_version(
        self,
        version_item: dict,
        distro: str,
        distro_codename: str,
        image: str,
        latest: Version,
        stable: str | None,
    ) -> bool:
        """
        Remove unused version of an image in the version file and commits the change.
        Returns whether the version_item has been removed.
        """
        version = version_item["version"]
        image_data = self.data["images"][distro][distro_codename][image]

        # remove not used version
        if (
            not (
                self.is_used_version(image, version)
                or self.is_used_version(image, self.previous_version(version))
            )
            and version != latest
            and version != stable
        ):
            log.info("Removed unused version %s %s", image, version)
            image_data["versions"].remove(version_item)

            # write to file and commit
            self.write_versions_file(self.data)
            if self.mr_allowed and self.version_repo.is_dirty(path=self.versions_file_filename):
                self.index.add(VERSIONS_FILE)
                self.index.commit(f"version-bot: removed unused version {version} of {image}")
                self.changed = True

            return True
        return False

    def update_version(
        self,
        version_item: dict,
        versions: list[str],
        distro: str,
        distro_codename: str,
        image: str,
        tags: list[Version],
    ) -> bool:
        """
        Updates the versions of an image in the version file and commits the change.
        Returns whether a new version has been added.
        """
        version = version_item["version"]
        image_data = self.data["images"][distro][distro_codename][image]

        # add new version
        base_images = version_item["base_images"]
        current_version = Version.parse(version)
        next_version_semver = current_version.bump_patch()
        next_version = str(next_version_semver)
        if (
            next_version_semver in tags
            and next_version not in versions
            and (
                self.is_used_version(image, next_version)
                or self.is_used_version(image, str(current_version))
            )
        ):
            log.info("Found new patch version for %s.", image)
            image_data["versions"].append({"version": next_version, "base_images": base_images})
            versions.append(next_version)

            # write to file and commit
            self.write_versions_file(self.data)
            if self.mr_allowed and self.version_repo.is_dirty(path=self.versions_file_filename):
                self.index.add(VERSIONS_FILE)
                self.index.commit(
                    f"version-bot: appended new patch version {next_version} to {image}"
                )
                self.changed = True

            return True
        return False

    def update_versions(
        self,
        distro: str,
        distro_codename: str,
        image: str,
        latest: Version,
        tags: list[Version],
    ) -> None:
        """
        Initializes updating all versions of an image
        Call updates for every version for image that updates the version file and commits the change.
        """
        image_data = self.data["images"][distro][distro_codename][image]
        image_versions = image_data.get("versions")

        if image_versions:
            # get all version numbers of an image ("version" in dict "versions")
            versions = [version_item["version"] for version_item in image_versions]
            stable = image_data.get("stable", None)
            # calls an update for every "version" entry in dict "versions"
            for version_item in image_versions.copy():
                removed = self.remove_unused_version(
                    version_item, distro, distro_codename, image, latest, stable
                )
                if removed:
                    continue  # a removed version no longer needs an update
                self.update_version(version_item, versions, distro, distro_codename, image, tags)

    def create_merge_request(self) -> None:
        """Creates a merge request."""
        result = self.create_gitlab_merge_request(
            self.git_server,
            self.project_id,
            self.mr_branch.name,
            self.branch,
            self.mr_branch.name,
            "Version Bot Updates",
            self.password,
        )
        log.info(result)

    def process(self) -> None:
        """Processes all images and starts the update process for each."""
        for distro in self.data["images"].keys():
            for distro_codename in self.data["images"][distro].keys():
                for image in self.data["images"][distro][distro_codename].keys():
                    log.info(image)
                    if image in self.retention_config["manual_updates"]:
                        log.info("Skip image from manual updates list: %s", image)
                        continue

                    resource_type = self.data["images"][distro][distro_codename][image].get(
                        "type", "git-repository"
                    )
                    repository = self.data["images"][distro][distro_codename][image].get(
                        "repository", ""
                    )
                    remote = self.data["images"][distro][distro_codename][image].get("remote", "")
                    # The resource type defines the type of system in which the versions for the image are published.
                    # The resource type can be "nexus", "gitlab-registry" or "git-repository",
                    # which is the default when no "type" property for the image is defined in the images.yaml.
                    fetcher: VersionFetcher
                    if resource_type == "nexus":
                        fetcher = NexusVersionFetcher(
                            application=repository,
                            repository=os.environ["ARTIFACT_REPO"],
                            server=os.environ["ARTIFACT_SERVER"],
                            user=os.environ["UBUNTU_MIRROR_USER"],
                            password=os.environ["UBUNTU_MIRROR_PASSWORD"],
                        )
                    elif resource_type == "gitlab-registry":
                        fetcher = GitLabVersionFetcher(remote)
                    elif resource_type == "git-repository":
                        fetcher = GitVersionFetcher(remote)
                    else:
                        raise ValueError(f"Unknown source: {resource_type}")

                    latest, tags = fetcher.get_latest_and_tags()
                    self.update_latest_version(distro, distro_codename, image, latest)
                    self.update_versions(distro, distro_codename, image, latest, tags)

    def pre_process_pre_hook(self) -> None:
        """Preprocessing hook for extensions of the VersionBot"""
        pass

    def pre_process_post_hook(self) -> None:
        """Preprocessing hook for extensions of the VersionBot"""
        pass

    def pre_process(self) -> None:
        """Preprocesses the retention list and versions file."""
        self.pre_process_pre_hook()
        self.update_retention_config()
        if self.version_repo.is_dirty(path=self.retention_config_filename):
            self.index.add(RETENTION_CONFIG)
            if self.index.diff("HEAD"):
                self.index.commit("version-bot: updated retention_list")
                self.changed = True
        self.pre_process_post_hook()

    def push(self) -> None:
        """Pushes the merge request branch."""
        if self.changed:
            self.origin.push(self.mr_branch)

    def run(self) -> None:
        for branch in ["develop", "main"]:
            log.info('Run VersionBot on branch "%s".', branch)
            self.branch = branch
            self.checkout_mr_branch()
            self.load_data()
            self.load_retention_config()
            self.pre_process()
            if self.mr_allowed:
                self.process()
                self.create_merge_request()
                self.push()
            else:
                log.info("Skipped creation of merge request, one mr already opened.")


if __name__ == "__main__":
    bot = VersionBot()
    bot.run()
