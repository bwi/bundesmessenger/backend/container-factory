#!/bin/bash

set -x

if [ $(id -u) != 0 ]; then
    echo run me as root!
    sudo $0 "$@"
    exit 0
fi

if [ $# != 2 ]; then
    echo iusage $0 container_id workdir
    exit 1
fi

WD=$2
APT_USER=_apt
chown $APT_USER $WD

pushd $WD

apt-get update

podman exec -i $1 dpkg-query -f '${source:Package} ${source:Version} \n' -W | sort -u | \
    while read -a line; do

        name=${line[0]}
        version=${line[1]}
        dirname=${name}_${version}

        if [ ! -d $dirname ]; then
            runuser -u $APT_USER -- mkdir $dirname

            pushd $dirname
            apt-get source -y --download-only ${name}=${version}
            dpkg-source -x *.dsc
            rm *.tar.* *.dsc
            popd

        fi

    done

popd
