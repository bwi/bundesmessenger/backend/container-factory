#!/usr/bin/env sh
set -x

# For system packages we dont need to download sources
# curl -L -o /tmp/sources/somepackage-$1.tar.gz https://github.com/some/package/archive/refs/tags/somepackage-$1.tar.gz
