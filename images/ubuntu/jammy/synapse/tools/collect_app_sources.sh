#!/bin/bash
set -x

curl -L -o /tmp/sources/synapse-$1.tar.gz https://github.com/element-hq/synapse/archive/refs/tags/v$1.tar.gz
