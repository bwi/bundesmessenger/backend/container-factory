#!/bin/bash
set -x

curl -L -o /tmp/sources/bumap-ui-$1.tar.gz https://gitlab.opencode.de/bwi/bundesmessenger/admin-portal/bumap-ui/-/archive/v$1/bumap-ui-v$1.tar.gz
