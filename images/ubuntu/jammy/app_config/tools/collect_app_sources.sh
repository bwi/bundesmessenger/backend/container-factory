#!/bin/bash
set -x

curl -L -o /tmp/sources/app-config-$1.tar.gz https://gitlab.opencode.de/bwi/bundesmessenger/backend/app-config/-/archive/v$1/app-config-v$1.tar.gz
