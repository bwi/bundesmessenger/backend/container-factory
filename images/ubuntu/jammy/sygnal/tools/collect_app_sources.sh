#!/bin/bash
set -x

curl -L -o /tmp/sources/sygnal-$1.tar.gz https://github.com/matrix-org/sygnal/archive/refs/tags/v$1.tar.gz
