#!/bin/bash
set -xe

curl -o /tmp/sources/bundesmessenger-web-v$1.tar.gz https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web/-/archive/v$1/bundesmessenger-web-v$1.tar.gz
