#!/bin/bash
set -x

curl -L -o /tmp/sources/sadm-$1.tar.gz https://github.com/Awesome-Technologies/synapse-admin/archive/refs/tags/$1.tar.gz
