#!/bin/bash
set -x

curl -L -o /tmp/sources/mcs-$1.tar.gz https://github.com/vector-im/matrix-content-scanner-python/archive/refs/tags/$1.tar.gz
