#!/bin/sh
set -x

curl -L -o /tmp/sources/livekit-management-service.tar.gz https://github.com/element-hq/lk-jwt-service/archive/refs/tags/v$1.tar.gz
