#!/bin/bash
set -x

curl -L -o /tmp/sources/kubctl-$1.tar.gz https://github.com/kubernetes/kubectl/archive/refs/tags/kubernetes-$1.tar.gz
