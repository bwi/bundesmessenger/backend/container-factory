#!/bin/bash
set -x

curl -L -o /tmp/sources/bumap-core-$1.tar.gz https://gitlab.opencode.de/bwi/bundesmessenger/admin-portal/bumap-core/-/archive/v$1/bumap-core-v$1.tar.gz
