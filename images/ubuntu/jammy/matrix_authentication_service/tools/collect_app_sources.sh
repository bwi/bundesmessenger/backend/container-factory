#!/bin/bash
set -x

curl -L -o /tmp/sources/mas-$1.tar.gz https://github.com/element-hq/matrix-authentication-service/archive/refs/tags/v$1.tar.gz
