# Builder stage to download artifact
FROM {{ registry }}/{{ project }}/ubuntu:latest-{{ distro_codename }}-unhardened AS builder

ARG ARTIFACT_SERVER
ARG ARTIFACT_PATH_CALL

USER root

RUN mkdir -p /tmp/bum_install
WORKDIR /tmp/bum_install/
RUN --mount=type=secret,id=apt_auth,target=/etc/apt/auth.conf \
    --mount=type=secret,id=apt_list,target=/etc/apt/sources.list \
    apt-get update && apt-get -y upgrade && apt-get -y --no-install-recommends install curl && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN --mount=type=secret,id=curl_auth \
    curl --netrc-file /run/secrets/curl_auth  -L -o /tmp/bum_install/call_artifact.tgz https://$ARTIFACT_SERVER/$ARTIFACT_PATH_CALL/bundesmessenger-call-{{ install_version }}.tgz || exit 1 && \
    tar xzf /tmp/bum_install/call_artifact.tgz -C /tmp/bum_install/ && \
    rm /tmp/bum_install/package/package.json && \
    curl -L -o /LICENSE https://raw.githubusercontent.com/element-hq/element-call/livekit/LICENSE

USER $UID

# runtime stage
FROM {{ registry }}/{{ project }}/{{ base_images[0] }}-unhardened

ARG BUILD_ID
ARG UID=1001

USER root

# Override default nginx config
COPY /default.conf /etc/nginx/sites-available/default

COPY --from=builder /tmp/bum_install/package /app
COPY --from=builder /LICENSE /LICENSE

RUN rm -rf /usr/share/nginx/html \
  && mkdir -p /usr/share/nginx/ \
  && ln -s /app /usr/share/nginx/html \
  && chown -R $UID:0 /app

WORKDIR /app/

EXPOSE 8080

LABEL de.bwi.baseimage.build-id=$BUILD_ID

USER $UID
