# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import base64
import io
import os
from pprint import pprint

import yaml
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA

if not os.path.isfile("encrypted_log"):
    print("There is no encrypted_log file in this directory.")
    exit()
if not os.path.isfile("private_key.pem"):
    print("There is no private_key.pem in this directory.")
    exit()
with open("encrypted_log") as f:
    cipher = f.read()
log_message = base64.b64decode(cipher)
private_key = RSA.import_key(open("private_key.pem").read())
file_in = io.BytesIO(log_message)
enc_session_key, nonce, tag, ciphertext = [
    file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1)
]
# Decrypt the session key with the private RSA key
cipher_rsa = PKCS1_OAEP.new(private_key)
session_key = cipher_rsa.decrypt(enc_session_key)
# Decrypt the data with the AES session key
cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
data = cipher_aes.decrypt_and_verify(ciphertext, tag)
res = yaml.full_load(data.decode("utf-8"))
pprint(res)
