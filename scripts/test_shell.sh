#!/bin/bash

set -x

find images -name 'entrypoint.sh' | xargs -I {} shellcheck -S error {} ;
