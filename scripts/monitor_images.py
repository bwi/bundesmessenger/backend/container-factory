# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import logging.config
import os
import subprocess

import urllib3
from podman import PodmanClient

from factory.util.utils import get_tags_for_repository


def inspect(registry: str, project: str, repository: str, tag: str) -> dict:
    output = subprocess.check_output(
        ["skopeo", "inspect", "--raw", f"docker://{registry}/{project}/{repository}:{tag}"],
        encoding="utf-8",
    )
    res = dict(json.loads(output))
    return res


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


if "DEVELOP" not in os.environ:
    verify = True
elif os.environ.get("DEVELOP") == "1":
    verify = True
elif os.environ.get("DEVELOP") == "0":
    verify = False

logging.basicConfig(level=logging.INFO)
log = logging.getLogger

registry = os.environ["DOCKER_REGISTRY"]
registry_user = os.environ["REGISTRY_USER"]
registry_password = os.environ["REGISTRY_PASSWORD"]
registry_url = f"https://{registry}"
config = {
    "registry_user": registry_user,
    "registry_password": registry_password,
}

all_images = [
    "ansible",
    "builder",
    "clamav",
    "content-scanner",
    "element",
    "icap",
    "kubectl",
    "nginx",
    "node",
    "python",
    "redis",
    "sygnal",
    "synapse",
    "synapse-admin",
    "ubuntu",
]

project = os.environ["PROJECT"]

uid = os.getuid()
user_sock = f"/run/user/{uid}/podman/podman.sock"
service_sock = "/home/build/podman.sock"
host_sock = "/var/run/podman/podman.sock"
if os.path.exists(user_sock):
    uri = "unix://" + user_sock
elif os.path.exists(service_sock):
    uri = "unix://" + service_sock
else:
    uri = "unix://" + host_sock


podman_client = PodmanClient(base_url=uri)
response = podman_client.login(
    username=registry_user, password=registry_password, registry=registry_url
)
print(response)

project_size = 0
for image in all_images:
    tags = get_tags_for_repository(
        registry,
        project,
        image,
        config["registry_user"],
        config["registry_password"],
        verify=verify,
    )
    image_size = 0
    for tag in tags:
        remote_name = f"{registry}/{project}/{image}:{tag}"
        data = inspect(registry, project, image, tag)
        layers = data["layers"]
        tag_size = 0
        for layer in layers:
            tag_size += layer["size"]
        tag_size = round(tag_size / 1024 / 1024)
        print(f"{image}:{tag} : {tag_size} MB")
        image_size += tag_size
    print("_" * 70)
    print(f"{image} : {image_size} MB")
    print()
    project_size += image_size
print(f"project : {project_size} MB")
