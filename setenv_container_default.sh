#!/usr/bin/env bash

# Show env vars
grep -v '^#' .env_container

# Export env vars
export $(grep -v '^#' .env_container | xargs )
echo DOCKER_AUTH_CONFIG='{"auths": {"registry.example.com": {"auth": "base64encodedcredentials"}}}'
export DOCKER_AUTH_CONFIG='{"auths": {"registry.example.com": {"auth": "base64encodedcredentials"}}}'
