# Changelog

## 1.12.2 (2025-03-05)

### 🐛 Bugfixes

- Pin `podman-py` to `5.2.0`
- Increase timeout for pushing images

## 1.12.1 (2025-02-22)

### 🐛 Bugfixes

- Fix authentication with `podman-py`
- Fix building Debian image
- Remove obsolet debug output
- Fix wrong message for pushing images

## 1.12.0 (2025-02-21)

### ✨ Features

- Make image rentention a configuration
- App-config image
- Decouple version bot from BundesMessenger bot
- Debian bookworm base image

### 🐛 Bugfixes

- Fix test for tagging pipelines

### 📝 Other changes

- Extend usage of semver
- Split code and image ci pipelines
- Unit test isolation
- Make image push attempts configurable
- Reduce logins for container registry
- Reduce sending custom auth config
- Refactor versions retrieval for external systems
- Improve exception handling

## 1.11.0 (2025-01-30)

### ✨ Features

- Use pasta
- Move maintenance_list into yaml config
- Add mount options needed for podman 5
- Bundesmessenger Call Multi-Stage Dockerfile
- Normalize uid, gid and port settings
- Decouple autentication and loading from ImageBuilder constructor
- Synapse Admin Multi-Stage Dockerfile
- Set support time to 6 month
- Livekit Management Service image
- ImageConfig uses dataclass for better typing
- Use pip cache for sygnal and matrix content scanner
- Use binaries in builder image
- Simplify builder image setting
- Rename admin portal images

### 🐛 Bugfixes

- Fix BundesMessenger Web license download
- Fix missing param vulnerability_format for cve scanner

### 📚 Documentation

- Documentation of security scan

### 📝 Other changes

- Remove sliding sync image
- Update BundesMessenger Call license
- Decouple tests from image definitions
- Remove install_tag property from image definitions

## 1.10.0 (2024-12-10)

### ✨ Features

- Publish python image
- Reduce use of proxy for apt
- Restrict support of version bot to one year old chart versions
- Extend synapse downloader for new versions
- Update builder dependencies
- Keep rendered files as default
- Artifact repository scanning for new admin portal image versions
- Update to podman 5 in builder image
- Decouple tests fromimage definitions
- Separate maintenance list from delete images script
- Use pasta instead of slirp4netns

### 🐛 Bugfixes

- Fix tests and typos

### 📚 Documentation

- Improve documentation

### 📝 Other changes

- Improve code
- Prepare separation of code and image configuration
- Improve modularization of tests
- Extend use of podman module instead of podman subprocess
- Isolate unit tests
- Improve tests
- Change license of content scanner to AGPL
- Update matrix authentication service repository
- Remove nightly chart builder
- Update bundesmessenger call licenses to AGPL
- Remove sliding sync image

## 1.9.0 (2024-10-07)

### ✨ Features

- Add image licenses to image labels and license text to image
- Use poetry for sygnal
- Pipeline uses grype database cache
- Remove weekly restriction from code
- Minimize bundesmessenger-web image
- Add Admin Portal images
- Version switch for matrix content scanner

### 🐛 Bugfixes

- Allow FORCE_BUILD to force
- Add catatonit to builder
- Fix local cve scanner tests

### 📚 Documentation

- Improve documentation

### 📝 Other changes

- Move monitor images to scripts directory
- Move mandatory files to root directory
- Remove references to old jammy images notation
- Remove U volume mount option
- Move version bot util functions to version bot module
- Remove additional python in python image
- Simplify git add safe.directory
- Move ChartBuilder tests in separate file
- Move ImageConfig tests in separate file
- Refactor building of /etc/motd
- Update publiccode yaml
- Split unit tests
- Improve structure of code and tests
- Move workdir of builder to var directory

## 1.8.6 (2024-09-25)

### 🐛 Bugfixes

- Update dependencies for matrix authentication service version 0.11.0

## 1.8.5 (2024-09-24)

### 🐛 Bugfixes

- Adapt apt version to the currently supported version

## 1.8.4 (2024-09-24)

### 🐛 Bugfixes

- Add admin portal images to list of images with manual version discovery in version bot

## 1.8.3 (2024-08-20)

### 🐛 Bugfixes

- Adapt no_proxy variable definition to be used in child pipelines

## 1.8.2 (2024-08-06)

### 🐛 Bugfixes

- Fix dockerfile linting

## 1.8.1 (2024-07-19)

### 🐛 Bugfixes

- Allow to force builds with `FORCE_BUILD`

## 1.8.0 (2024-07-18)

### ✨ Features

- Add Matrix Authentication Service image
- Add Sliding Sync image
- Remove packages from base image (`logsave`, `e2fsprogs`, `libext2fs2`, `libss2`)
- Better semver compatibility

### 🐛 Bugfixes

- Disable cache from installing python pip
- Remove circular import of `Builder` and `ImageConfig`
- Do not update finalised images
- Improve creation of SBOM and build it for all stages

### 📚 Documentation

- Split README.md in multiple documentation
- How to add an image

### 📝 Other changes

- Refactor icap image
- Refactor clamav image
- Refactor Matrix Content Scanner image
- Remove helm image
- Add Python type hints
- Replace deprecated command `syft packages` with `syft scan`
- Remove not needed configuration for nginx tests
- Bump python version for multiple images to `3.12`
- Bump python version for Sygnal image to `3.11`
- Bump Syft
- Bump grype
- Bump conmon
- Bump Python dependencies
- Bump goss
- Bump skopeo

## 1.7.0 (2024-05-07)

### ✨ Features

- Project converted from Python script to Python module.
- Add support for authenticated ubuntu apt mirror.
- Compress base image by 25% and optimised the build process.
- apt cleanup process embedded in base image via apt configuration.
  As a result, the images become smaller. Synapse shrinks by approx. 30%.
- Move generation ubuntu mirror list outside the images and mount via secret.

### 🐛 Bugfixes

- Disable check of published tags for images with `disable_external_push`.
- Fix source collection.
- Fix check published tags test.
- Fix synapse-admin download.
- Prevent `getcwd` warning in source collection.
- Timeout error handling for container stop calls.
- Fix deprecated Python semver calls
- Fix deprecated escaped string (`W605`) in hadolint CI.
- Fix update checks (`check_security_updates.sh` and `check_updates.sh`)
- Propagate ci job variables to triggered child pipelines.
- Remove old file environment variables `.master.env`
- Access environment variables with `os.environ.get()`
- Remove unused parameter `remove_after` from `run_podman`
- Moved `image_timestamp` and `get_all_base_images` from `ImageConfig` to `ImageBuilder`
- Fix removing build information in `add_custom_apt_config.j2`

### 📝 Other changes

- Bump Syft
- Bump cosign
- Bump grype
- Bump conmon
- Bump Python dependencies
- Code style with black
- Revert "Postbuffer workaround for network issues." (from `v1.6.0`)
- Update SonarQube CI to use global config.
- Move ShellCheck script from tests to scripts.
- Replace deprecated command `syft packages` with `syft scan`
- Remove old example file `icap/freshclam.conf.sample`
- Remove not needed cleanup task from images
- Remove not used assets from builder image
- Add [Goss](https://github.com/goss-org/goss) to builder image
- Move basic actions from base image to `mmdebstrap`.

## 1.6.0 (2024-04-02)

### ✨ Features

- Mirror all tagged versions to OpenCoDE
- Pipeline for mypy and flake8
- Raise exception if image repository is not found
- Added rsync to ansible image
- Make tests mandatory
- Migrate from dependabot to Renovate
- Dependency management for builder tools
- Refactor Synapse-admin nginx conf
- Reduce git clone overhead
- Set debconf to Noninteractive
- Add black pipeline for code style
- Migrate some commands from bash to sh
- Replace wget with curl and remove wget from images
- Add environment variable to build all images
- Use the JSON API for the request of the upstream CVE status
- Remove volumes from all dockerfiles
- Remove additional python versions from Synapse
- Type hints
- New Synapse remote
- Allow override of entrypoint for running containers in all stages
- Error handling for source collection
- Dynamic apt installation
- Manage apt package with Renovate
- Update python base version for Sygnal
- Replace git checkouts with http downloads in builder image
- Add requirements for builder

### 🐛 Bugfixes

- Fix image tag to be scanned
- Remove volumes from Sygnal image
- Fix project root permissions for Synapse config
- Remove chown from pipeline
- Remove ldap from strip for clamav
- Remove libldap from strip for Synapse image
- Fix test for cases where the all special commit tag is used
- Fix version bot file path
- Fix whl suffix for Synapse 1.89.0
- Remove redundant entrypoints
- Bugfix for sync to main
- Fix cve agent injection
- Check published images in image registry
- Get tags with ls remote instead of git clone
- Fix version type in image definitions

### 📚 Documentation

- Restructure README.md
- Chapter about adding images to the container factory

### 📝 Other changes

- Refactor process functions of all stages to apply visitor pattern
- Postbuffer workaround for network issues
- Workaround for proxy issues
- Improve code quality for cve agent
- Distinct apt repositories for updates and security
- Temporarily remove errexit from source collection to prevent pipeline fails on nexus issues

## 1.5.0 (2023-12-08)

### ✨ Features

- Version updates for dependencies of the container factory by dependabot
- New versions added by version bot
- Disable scan and sign stage when image is not updated
- Move image headers and footers for custom apt config to include file
- Refactor Synapse image to use python base image
- Refactor redis image
- Optimize parallel build processes
- Unique build ids improve image identification across build stages
- Restrict version bot to one merge request at a time
- Add yamlllinter and fixes
- New pipeline structure
- Refactor Sygnal image
- Improve pipelines and build process control features
- Isort stage
- Restrict pipelines to changed images in feature branches
- Force build feature
- Improve proxy handling
- Add checksum checks for all binary downloads
- CVE mitigation for all BundesMessenger images

### 🐛 Bugfixes

- Fix version bot to process all existing versions when removing versions
- Add missing clamdscan
- Fix Sygnal volume mount issue
- Fix remove_deb_packages for versions with invalid characters
- Propagate variables to child pipelines
- Fix empty pipelines when no images are changed

### 📚 Documentation

- Restriction of the build process to changed images in feature branches
- Enhanced description of restricting the build process to build a single or a defined set of images
- In-depth view into the intricacies of proxy environment variables during the build process and during runtime

### 📝 Other changes

- Improve code coverage
- SonarQube fixings

## 1.4.0 (2023-09-07)

### ✨ Features

- Generalize key error handling for nvd data
- Include TypeErrors in nvd data error handling.
- Adapt Synapse image to upstream image
- Remove manuals and docs in base image
- Remove curl from nginx image
- Remove unnecessary apt config
- Improve code with linting
- Improve dockerfile linting with hadolint
- Use unhardened tag when pulling dependencies
- Add dependabot to build process
- Migrate dind runner to sde-common
- Generic directory structure for image definition directories
- Restrict build process to latest and used versions

### 🐛 Bugfixes

- Increase waiting time for scan log
- Handle invalid responses from scanner
- Handle missing NVD infos in scanner data
- Handle missing docker config directory for scanner usage
- Adapt render stage to the template rendering process of the other stages
- Improve removal of docs in images
- Add missing mocks to unittests

### 📚 Documentation

- Add chapter about render stage to documentation
- Added documentation for environment variables
- Added documentation for scanner selection

### 📝 Other changes

- Temporary deactivation of sign stage

## 1.3.0 (2023-07-31)

### ✨ Features

- Modular rendering of dockerfiles
- Linting of dockerfiles
- New image: BundesMessenger-call
- Improve DVS conform tagging
- Create CVE reports to support the CVE handling process
- Add NVD severity to CVE report
- Make nightly commits optional
- Waiting time before the creation of container logs
- Use included gpg key for python installation
- Python installation without software-properties-common to mitigate CVE
- Install ansible with pip instead of the system package to mitigate CVE
- Setting for the CVE scanner selection
- Sign images with cosign (keyless signing)

### 🐛 Bugfixes

- Increase scan timeout

### 📚 Documentation

- Chapter about the sign stage
- Documentation of the Keyless Signing Process
- Chapter about the Advantages of Keyless Signing
- Chapter about the The Sigstore Stack
- Documentation about the verification of images with the generated signatures
- Chapter for the installation of signing requirements
- Image signing with cosign (keyless signing)

### 📝 Other changes

- Synape image without su

## 1.2.0 (2023-07-17)

### ✨ Features

- Update syft and scancode
- Setting for cve scanner selection
- Install ansible with pip
- Remove CVE introduced by software-properties-common
- Use included gpg key for python installation
- Add short waiting time before container logs
- Make commits of nightly image tags optional
- RFC3339 conform timestamps for image labels
- Changed Synapse container to work without su calls in entrypoint
- Package analyzer for information collection to support the cve handling process
- Vulnerability class to support a semi-automated cve handling process

### 🐛 Bugfixes

- Fix source tags

### 📚 Documentation

- Technical guidelines for deployments and BSI compliance

## 1.1.0 (2023-06-12)

### ✨ Features

- Introduce unhardened appendix to tags for intermediary and development images (DVS MUST requirement)
- Check that the container login prompt equals the legal info file
- Pin pip requirements for each Synapse version via upstream poetry lock file
- Introduced a final stage for canary tests
- Prioritized security updates (unscheduled builds)
- Restart build jobs on transient errors
- Vault image
- Nightly helm chart builder
- Exit on wget 404 during image builds
- Add publiccode.yaml
- Added a nightly tags file (which shows the versions of the development registry currently)
- Separate retention list from code into yaml file

### 🐛 Bugfixes

- Remove pip-get binaries where not needed
- Make path to get pip script generic

### 📚 Documentation

- 1.1.0 Changelog
- Documentation for the build schedule and retention

### 📝 Other changes

- Improve code coverage
- Add integration tests
- Add functional tests
- Reduced image sizes for all images (reduced size of ubuntu base image to 59MB)
- Renamed content scanner to matrix content scanner
- Removed element image

## 1.0.0 (2023-03-17)

### ✨ Features

- New tagging system (new tags f.e. Synapse:1.77.0-jammy-production-b1)
- Add modules to Synapse (OpenCoDE issue-3)
- Update npm in node images
- Jammy wget image
- Integrate external repository base image versions
- Add pip, ansible-lint, ansible-lint-junit to ansible image.
- Version bot BundesMessenger web auto update
- Create c-icap user
- Add skopeo to builder.
- Add images to rentention list.
- Helm image.
- Test mandatory files.
- Log container errors during build.
- Double push for additional pushs to external registry.
- Registry mode switch for image delete process.
- BundesMessenger web image
- Add file with references to production images, non-rolling-tags and immutable tags.
- Add non system package images to version bot.
- Rolling tags
- Push latest version from version bot data.
- Logs for delete images script.
- Add latest and maintained versions from base-image-versions repo to image configs.
- Add icap and python production images.
- Concurrent pipelines with artifacts
- Concurrent pipelines.
- Version bot base.
- Test registry
- Disable buildversions setting.
- Image delete and registry analyser scripts.
- Use second image layer for bom step
- Use unstripped layer for apt tasks.
- Content scanner python
- Jammy python image
- Build node from nodesource
- Syntax check for entrypoints
- Individual push policy per branch
- Improve pipeline security
- Modify image history without removing items.
- Disable external push with environment variable.
- Add api endpoint test to matrix content scanner.
- Disable external push.
- Jammy clamav image .
- Jammy node image.
- Rootless and reproducible builds
- Build versioning
- Synapse admin image
- Jammy Sygnal image
- Jammy redis image
- Jammy nginx image
- More jammy images
- Jammy signal image
- Jammy kubectl image
- Clamav virus scanner image
- Focal-Synapse image based on Python3.8
- Bom and licenses
- Strip images
- Scan images for vulnerabilities
- Python image
- Nginx image
- Make gitlab ci running

### 🐛 Bugfixes

- Disable external push for helm image and add it to deletion process.
- Add request if running as root and added exec directly for python3 call
- Changed runtime to added user
- Remove created freshclam.conf by apt with internal environment
- Handle cool-down for clamav.
- Prevent early exit for source collection.
- Fix image timestamp function.
- Set http proxy vars in subcontainers
- Fix wget for element sources.
- Fix webclient entrypoint.
- Remove installed freshclam
- Fix nginx var lib permissions.
- Fixed implement changes required to run nginx as an unprivileged user
- Add secure-delete package (srm) for secure rm with content-scanner
- Workaround for insufficient token duration.

### 📚 Documentation

- Initial Changelog
- Improve documentation for buildsystem

### 📝 Other changes

- Security review fixings.
- Remove executable bit from dpkg.
- Create junit reports with pytest for gitlab-ci integration.
- Junit reports.
- Sonarqube code quality improvements for bugs and vulnerabilities.
- Final osadl fixings.
- Clean code refactoring and tests.
- Disable external push corresponding to whitelist.
- Enhance delete images script for OpenCoDE.
