#!/usr/bin/env sh

echo "Acquire::http::Proxy \"$http_proxy\";"> /etc/apt/apt.conf.d/00proxy

apt-get update

test $(apt list --upgradable 2>/dev/null | grep "\-security" | wc -l) -gt 0
