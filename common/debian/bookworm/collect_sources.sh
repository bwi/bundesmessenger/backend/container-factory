#!/bin/bash

set -x

echo "Acquire::http::Proxy \"$http_proxy\";"> /etc/apt/apt.conf.d/00proxy
# echo "machine $(echo $UBUNTU_MIRROR | sed -r 's#([^/])/[^/].*#\1#') login ${UBUNTU_MIRROR_USER} password ${UBUNTU_MIRROR_PASSWORD}" > /etc/apt/auth.conf

echo "deb-src https://ftp.debian.org/debian/ bookworm contrib main non-free non-free-firmware" > /etc/apt/sources.list.d/debian-src.list
echo "deb-src https://ftp.debian.org/debian/ bookworm-updates contrib main non-free non-free-firmware" >> /etc/apt/sources.list.d/debian-src.list
echo "deb-src https://security.debian.org/debian-security/ bookworm-security contrib main non-free non-free-firmware" >> /etc/apt/sources.list.d/debian-src.list

export http_proxy=$2
export https_proxy=$2

# collecting package sources
echo "Collecting package sources"

WD=/tmp/sources
CD=${WD}.tar.gz
mkdir $WD
chown _apt $WD
pushd $WD

apt-get update -y && apt-get install -y --no-install-recommends dpkg-dev curl

dpkg-query -f '${source:Package} ${source:Version} \n' -W | sort -u | \
    while read -a line; do

        name=${line[0]}
        version=${line[1]}
        dirname=${name}_${version}
        runuser -u _apt mkdir $dirname
        pushd $dirname >/dev/null 2>&1
	set -x
	echo "Collecting sources ${name}=${version}"

        apt-get -qq source -y --download-only ${name}=${version}
        dpkg-source -x *.dsc >/dev/null 2>&1
        rm -rf *.tar.* *.dsc | echo $?
        popd >/dev/null 2>&1 ;
        if [ "$TEST_BUILD" == "1" ]; then
            echo "Test build break."
            echo $TEST_BUILD
            break
        fi
    done

# collecting app sources
echo "Collecting app sources"
/usr/sbin/collect_app_sources.sh $1

echo "Creating source tar ..."

tar czf $CD $WD >/dev/null 2>&1

apt-get -y purge curl && apt-get -y autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*
rm -rf $WD
rm -f /etc/apt/apt.conf.d/00proxy
rm -f /etc/apt/auth.conf
rm -f /etc/apt/sources.list.d/debian-src.list
unset http_proxy
unset https_proxy
exit 0
