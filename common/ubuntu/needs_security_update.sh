#!/bin/bash

set -x

podman cp check_security_updates.sh $1:/tmp

podman exec -u root $1 /tmp/check_security_updates.sh
