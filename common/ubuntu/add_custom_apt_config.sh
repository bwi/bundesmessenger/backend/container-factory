#!/bin/bash

cp -r /root/apt /etc/apt
echo "machine $(echo $UBUNTU_MIRROR | sed -r 's#([^/])/[^/].*#\1#') login ${UBUNTU_MIRROR_USER} password ${UBUNTU_MIRROR_PASSWORD}" > /etc/apt/auth.conf && \
base_url=$(echo "${UBUNTU_MIRROR}" | sed 's#-jammy-main/ubuntu##') && \
for repo in jammy-main jammy-security jammy-updates; do \
echo "deb $base_url-$repo ${repo%-main} main universe" > /etc/apt/sources.list.d/${repo}.list; \
done && \
mv /etc/apt/sources.list /etc/apt/sources.list.orig

# apt --fix-broken install
apt-get upgrade
apt-get update
dpkg --configure -a
# install gnupg2 (with libldap-2.5-0, which was removed by cve mitigation) to check signing of packages to allow python installation
apt-get install -y gnupg2
apt-get install -y apt-utils python3 python3-yaml python3-apt
# remove gnupg2 for cve mitigation
apt-get remove -y libldap-2.5-0
