#!/bin/bash

cp -r /root/apt /etc/apt
echo "Acquire::http::Proxy \"$http_proxy\";"> /etc/apt/apt.conf.d/00proxy && \
echo "machine $(echo $UBUNTU_MIRROR | sed -r 's#([^/])/[^/].*#\1#') login ${UBUNTU_MIRROR_USER} password ${UBUNTU_MIRROR_PASSWORD}" > /etc/apt/auth.conf && \
base_url=$(echo "${UBUNTU_MIRROR}" | sed 's#-jammy-main/ubuntu##') && \
for repo in jammy-main jammy-security jammy-updates; do \
echo "deb $base_url-$repo ${repo%-main} main universe" > /etc/apt/sources.list.d/${repo}.list; \
done && \
mv /etc/apt/sources.list /etc/apt/sources.list.orig

apt-get update

test $(apt list --upgradable | wc -l) -gt 1
