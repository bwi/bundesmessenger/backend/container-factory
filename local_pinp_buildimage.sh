#!/bin/bash

cd `dirname $0`

echo "creating ci_project_dir and chowning needs to be run as root"
sudo cp -r . /ci_project_dir
sudo chown -R 297607.297607 /ci_project_dir

podman run -d --name builder --privileged -v /ci_project_dir:/var/build ${BUILDER_IMAGE} podman system service --time 0 unix:///home/build/podman.sock
podman cp ~/.gitconfig builder:/home/build/.gitconfig

podman exec -ti builder /bin/bash

