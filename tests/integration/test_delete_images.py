# Copyright 2024 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from unittest.mock import Mock, patch

from factory.bots.delete_images import delete_process


class TestDeleteImagesIntegration:

    @patch("factory.bots.delete_images.get_retention_ids")
    @patch("factory.bots.delete_images.logging.getLogger")
    @patch("factory.bots.delete_images.delete_image")
    @patch(
        "factory.bots.delete_images.MAINTENANCE_LIST", new="tests/test_data/maintenance_list.yaml"
    )
    def test_delete_process(
        self, mock_delete_image: Mock, mock_log: Mock, mock_get_retention_ids: Mock
    ) -> None:
        # ONLY SET RETENTION LIST EMPTY, IF DELETE_IMAGE IS MOCKED!
        mock_get_retention_ids.return_value = []
        with patch.dict("os.environ", {"TARGET": "harbor"}):
            delete_process()
        assert mock_log.called
