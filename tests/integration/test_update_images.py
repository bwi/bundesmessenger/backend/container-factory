# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import glob
import logging
import os
import subprocess
from dataclasses import asdict
from datetime import datetime, timedelta
from typing import Generator
from unittest.mock import Mock, mock_open, patch

import pytest
from _pytest.fixtures import FixtureRequest

from factory.update_images import ImageBuilder, ImageConfig
from factory.util.utils import expand_templates, pushd
from tests.conftest import ibu, image_config_from_builder

logging.basicConfig(level=logging.DEBUG)

IMAGECONFIG_UBUNTU = "ubuntu:latest-jammy"
IMAGECONFIG_SYNAPSE = "synapse:1.118.0-jammy"

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


@pytest.fixture(scope="class")
def integration_setup(request: FixtureRequest) -> Generator[None, None, None]:
    sibu = ibu()
    request.cls.ibu = sibu
    request.cls.image_config_ubuntu = image_config_from_builder(sibu, IMAGECONFIG_UBUNTU)
    request.cls.image_config_synapse = image_config_from_builder(sibu, IMAGECONFIG_SYNAPSE)
    yield


@pytest.mark.usefixtures("integration_setup")
class TestBuildsystemIntegration:
    image_config_ubuntu: ImageConfig
    image_config_synapse: ImageConfig
    ibu: ImageBuilder

    def test_get_tags_for_repository(self) -> None:
        tags = self.image_config_ubuntu.get_tags_for_repository("ubuntu")
        assert isinstance(tags, list)

    @patch("factory.update_images.log.exception")
    def test_image_timestamp(self, mock_log: Mock) -> None:
        tstamp = self.ibu.image_timestamp(self.image_config_ubuntu)
        assert isinstance(tstamp, float)
        basename = self.image_config_ubuntu.config.basename
        self.image_config_ubuntu.config.basename = "doesnotexist"
        self.ibu.image_timestamp(self.image_config_ubuntu)
        assert mock_log.called
        # reset image config
        self.image_config_ubuntu.config.basename = basename

    def test_latest_subtree_change(self) -> None:
        path = self.image_config_ubuntu.config.rel_path
        latest_change = self.ibu.latest_subtree_change(path)
        assert isinstance(latest_change, int)

    def test_run_script(self) -> None:
        params = {"shell": "/bin/bash", "script": ['echo "one\ntwo"', 'echo "three\nfour"']}
        img = IMAGECONFIG_UBUNTU
        with self.ibu.run_podman(img, img_config=self.image_config_ubuntu) as container:
            response = self.ibu.run_script(container, params)
        assert any("one" in item for item in response)
        assert any("two" in item for item in response)
        assert any("three" in item for item in response)
        assert any("four" in item for item in response)

    @patch("factory.update_images.subprocess.check_call")
    @patch("factory.update_images.log.info")
    @patch("builtins.open", mock_open())
    def test_process_build_logs_message(self, mock_log: Mock, mock_check_call: Mock) -> None:
        self.ibu.build_id = "some_id"
        self.ibu.process_build(self.ibu.image_configs["ubuntu:latest-jammy"])
        assert mock_log.called

    @patch("factory.update_images.log.info")
    @patch("factory.update_images.ImageBuilder.check_build_id")
    def test_process_tests_logs_message(self, mock_check_build_id: Mock, mock_log: Mock) -> None:
        img = self.ibu.sorted_names[0]
        with pushd(self.ibu.image_configs[img].config.directory):
            with expand_templates(
                ".",
                self.ibu.env,
                self.ibu.keep,
                recurse=True,
                overwrite_env=asdict(self.ibu.image_configs[img].config),
            ):
                self.ibu.process_tests(self.image_config_ubuntu)
        assert mock_log.called

    @patch("factory.update_images.subprocess.check_output")
    @patch("factory.update_images.log.info")
    @patch("factory.update_images.ImageBuilder.check_build_id")
    def test_process_tests_exception_logs_message(
        self, mock_check_build_id: Mock, mock_log: Mock, mock_check_output: Mock
    ) -> None:
        mock_check_output.side_effect = subprocess.CalledProcessError(returncode=2, cmd=["bad"])
        img = self.ibu.sorted_names[0]
        with pushd(self.ibu.image_configs[img].config.directory):
            with expand_templates(
                ".",
                self.ibu.env,
                self.ibu.keep,
                recurse=True,
                overwrite_env=asdict(self.ibu.image_configs[img].config),
            ):
                try:
                    self.ibu.process_tests(self.image_config_ubuntu)
                except Exception:
                    pass
        assert mock_log.called

    @patch("factory.update_images.log.info")
    @patch("factory.update_images.ImageBuilder.check_build_id")
    def test_process_tests_bot_image_logs_message(
        self, mock_check_build_id: Mock, mock_log: Mock
    ) -> None:
        # we need to mock check_build_id as the build id is not set when testing the test stage without the build stage
        with pushd(self.image_config_synapse.config.directory):
            with expand_templates(
                ".",
                self.ibu.env,
                self.ibu.keep,
                recurse=True,
                overwrite_env=asdict(self.image_config_synapse.config),
            ):
                self.ibu.process_tests(self.image_config_synapse)
        assert mock_log.called

    @patch("factory.update_images.ImageBuilder.compare_motd_with_legal_info")
    @patch("factory.update_images.ImageBuilder.process_qa")
    @patch("factory.update_images.ImageBuilder.process_scan")
    @patch("factory.update_images.ImageBuilder.process_push")
    @patch("factory.update_images.ImageBuilder.check_mandatory_files")
    @patch("factory.update_images.ImageBuilder.process_bom")
    @patch("factory.update_images.ImageBuilder.process_strip")
    @patch("factory.update_images.ImageBuilder.process_sources")
    @patch("factory.update_images.ImageBuilder.process_references")
    @patch("factory.update_images.ImageBuilder.process_tests")
    @patch("factory.update_images.ImageBuilder.process_build")
    @patch("factory.update_images.ImageBuilder.needs_update")
    def test_run_stages(
        self,
        mock_needs_update: Mock,
        mock_process_build: Mock,
        mock_process_tests: Mock,
        mock_process_references: Mock,
        mock_process_sources: Mock,
        mock_process_strip: Mock,
        mock_process_bom: Mock,
        mock_check_mandatory_files: Mock,
        mock_process_push: Mock,
        mock_process_scan: Mock,
        mock_process_qa: Mock,
        mock_compare_motd: Mock,
    ) -> None:
        mock_needs_update.return_value = True
        img_config = self.image_config_ubuntu
        with pushd(img_config.config.directory):
            img_config.config.updated = True
            with expand_templates(
                ".",
                self.ibu.env,
                self.ibu.keep,
                recurse=True,
                overwrite_env=asdict(img_config.config),
            ):
                self.ibu.run_stages(img_config)
                assert mock_process_tests.called

    @patch("factory.update_images.ImageBuilder.process_tests")
    @patch("factory.update_images.ImageBuilder.process_build")
    @patch("factory.update_images.ImageBuilder.needs_update")
    def test_run_stages_returns_early(
        self,
        mock_needs_update: Mock,
        mock_process_build: Mock,
        mock_process_tests: Mock,
    ) -> None:
        mock_needs_update.return_value = False
        img_config = self.image_config_ubuntu
        self.ibu.run_stages(img_config)
        assert mock_process_tests.not_called

    def test_process_scan_result_logs_cve_report(self) -> None:
        self.ibu.prod_img_name = self.image_config_ubuntu.config.name
        # mock check_build_id as this check is not relevant for this test,
        # we dont want to run all stages for this test
        with patch.object(self.ibu, "check_build_id", return_value=True):
            output = self.ibu.process_scan_result(self.image_config_ubuntu, [])
            assert "CVE report" in output

    @patch("factory.podman_api.PodmanClient.images")
    @patch("factory.update_images.ImageBuilder.run_podman")
    @patch("factory.update_images.ImageBuilder.image_timestamp")
    @patch("factory.update_images.subprocess.check_call")
    @patch("factory.update_images.ImageBuilder.latest_subtree_change")
    @patch("factory.update_images.ImageBuilder.needs_security_update")
    def test_needs_update_given_no_artifact_and_no_repo_change_and_no_update_needed_returns_false(
        self,
        mock_needs_security_update: Mock,
        mock_latest_subtree_change: Mock,
        mock_check_call: Mock,
        mock_image_timestamp: Mock,
        mock_run_podman: Mock,
        mock_client_images: Mock,
    ) -> None:
        mock_client_images.pull.return_value = None
        mock_run_podman.iter.return_value.__iter__.return_value = None
        mock_image_timestamp.return_value = (datetime.now() - timedelta(days=1)).timestamp()
        mock_needs_security_update.return_value = False
        mock_latest_subtree_change.return_value = 0
        mock_check_call.side_effect = subprocess.CalledProcessError(returncode=2, cmd=["bad"])
        # remove all depedency-updated-artifacts
        fileList = glob.glob(f"{PROJECT_DIR}/dependency_updated_*")
        for filePath in fileList:
            os.remove(filePath)

        with patch.dict("os.environ", {"DEVELOP": "0", "FORCE_BUILD": "0"}):
            needs_update = self.ibu.needs_update(self.image_config_ubuntu)
        assert needs_update is False

    @patch("factory.podman_api.PodmanClient.images")
    @patch("factory.update_images.ImageBuilder.run_podman")
    @patch("factory.update_images.ImageBuilder.image_timestamp")
    @patch("factory.update_images.subprocess.check_call")
    @patch("factory.update_images.ImageBuilder.latest_subtree_change")
    @patch("factory.update_images.ImageBuilder.needs_security_update")
    def test_needs_update_given_no_artifact_and_no_repo_change_and_update_needed_returns_true(
        self,
        mock_needs_security_update: Mock,
        mock_latest_subtree_change: Mock,
        mock_check_call: Mock,
        mock_image_timestamp: Mock,
        mock_run_podman: Mock,
        mock_client_images: Mock,
    ) -> None:
        """
        This test checks to return true given no artifact is existing,
        latest code change is long ago and an the needs_update check returns 0.
        """
        mock_client_images.pull.return_value = None
        mock_run_podman.return_value.__enter__.return_value = Mock()
        mock_image_timestamp.return_value = (datetime.now() - timedelta(days=1)).timestamp()
        mock_needs_security_update.return_value = False
        mock_latest_subtree_change.return_value = 0
        mock_check_call.return_value = 0
        # remove all depedency-updated-artifacts
        fileList = glob.glob(f"{PROJECT_DIR}/dependency_updated_*")
        for filePath in fileList:
            os.remove(filePath)
        needs_update = self.ibu.needs_update(self.image_config_ubuntu)
        assert needs_update is True

    @patch("factory.podman_api.PodmanClient.images")
    @patch("factory.update_images.ImageBuilder.run_podman")
    @patch("factory.update_images.ImageBuilder.image_timestamp")
    @patch("factory.update_images.ImageBuilder.latest_subtree_change")
    @patch("factory.update_images.ImageBuilder.needs_security_update")
    def test_needs_update_given_no_artifact_and_no_repo_change_and_exception_run_podman_returns_true(
        self,
        mock_needs_security_update: Mock,
        mock_latest_subtree_change: Mock,
        mock_image_timestamp: Mock,
        mock_run_podman: Mock,
        mock_client_images: Mock,
    ) -> None:
        """
        This test checks to return true given no artifact is existing,
        latest code change is long ago and an exception
        is occurring during run podman (returns None).
        """
        mock_client_images.pull.return_value = None
        mock_run_podman.return_value.__enter__.return_value = None
        mock_image_timestamp.return_value = (datetime.now() - timedelta(days=1)).timestamp()
        mock_needs_security_update.return_value = False
        mock_latest_subtree_change.return_value = 0
        # remove all depedency-updated-artifacts
        fileList = glob.glob(f"{PROJECT_DIR}/dependency_updated_*")
        for filePath in fileList:
            os.remove(filePath)
        needs_update = self.ibu.needs_update(self.image_config_ubuntu)
        assert needs_update is True

    @patch("factory.podman_api.PodmanClient.images")
    @patch("factory.update_images.ImageBuilder.run_podman")
    @patch("factory.update_images.ImageBuilder.image_timestamp")
    @patch("factory.update_images.subprocess.check_call")
    @patch("factory.update_images.ImageBuilder.latest_subtree_change")
    @patch("factory.update_images.ImageBuilder.needs_security_update")
    def test_needs_update_given_no_artifact_and_no_repo_change_and_exception_needs_update_returns_false(
        self,
        mock_needs_security_update: Mock,
        mock_latest_subtree_change: Mock,
        mock_check_call: Mock,
        mock_image_timestamp: Mock,
        mock_run_podman: Mock,
        mock_client_images: Mock,
    ) -> None:
        """
        This test checks to return false given no artifact is existing,
        latest code change is long ago and an exception
        is occurring during the needs_update check.
        """
        mock_client_images.pull.return_value = None
        mock_run_podman.return_value.__enter__.return_value = Mock()
        mock_image_timestamp.return_value = (datetime.now() - timedelta(days=1)).timestamp()
        mock_needs_security_update.return_value = False
        mock_latest_subtree_change.return_value = 0
        mock_check_call.side_effect = subprocess.CalledProcessError(
            returncode=1, cmd="fake command"
        )
        # remove all depedency-updated-artifacts
        fileList = glob.glob(f"{PROJECT_DIR}/dependency_updated_*")
        for filePath in fileList:
            os.remove(filePath)
        needs_update = self.ibu.needs_update(self.image_config_ubuntu)
        assert needs_update is False

    def test_add_label_to_image(self) -> None:
        test_image = "alpine:latest"
        self.ibu.add_label_to_image(test_image, "key", "value")
        image = self.ibu.podman_client.images.get(test_image)
        assert image.labels["key"] == "value"
        image.remove(force=True)
