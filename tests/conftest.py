# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import pathlib
import subprocess
from copy import deepcopy
from typing import Any, Generator
from unittest.mock import MagicMock, mock_open, patch

import git
import pytest
import yaml
from _pytest.fixtures import FixtureRequest
from podman import PodmanClient

from factory.bots.bum_bot import BumBot
from factory.bots.version_bot import VersionBot
from factory.update_images import ImageBuilder, ImageConfig

MOCK_IMAGECONFIG_UBUNTU = "ubuntu:latest-jammy"
MOCK_IMAGECONFIG_SYNAPSE = "synapse:1.118.0-jammy"
MOCK_IMAGECONFIG_PYTHON = "python:3.12-jammy"


@pytest.fixture(scope="class")
def version_bot_images_data() -> str:
    file = pathlib.Path("tests/test_data/images.yaml")
    with open(file, encoding="utf-8") as f:
        input_data = f.read()
    return input_data


@pytest.fixture(params=["VersionBot", "BumBot"])
def load_bot(request: FixtureRequest, version_bot_images_data: str) -> VersionBot:
    """Shares the initialization for both bots."""

    def side_effect(remote: str, local_path: str) -> MagicMock:
        try:
            os.mkdir(local_path)
        except FileExistsError:
            pass
        return MagicMock()

    with (
        patch("factory.bots.version_bot.VersionBot.get_merge_requests_by_prefix"),
        patch("factory.bots.version_bot.Repo.clone_from", side_effect=side_effect),
    ):
        bot: VersionBot
        if request.param == "BumBot":
            bot = BumBot(branch="main")
        else:
            bot = VersionBot(branch="main")
        bot.checkout_mr_branch()

    with patch("builtins.open", mock_open(read_data=version_bot_images_data)):
        bot.load_data()

    return bot


@pytest.fixture
def mock_git_repo() -> Generator[MagicMock, None, None]:
    mock_repo = MagicMock(spec=git.Repo)
    mock_origin = MagicMock()
    mock_commit = MagicMock()
    mock_diff_index = MagicMock()

    mock_repo.clone_from.return_value = mock_repo
    mock_repo.remote.return_value = mock_origin
    mock_repo.commit.return_value = mock_commit
    mock_commit.diff.return_value = mock_diff_index

    diff_item_mock = MagicMock()
    diff_item_mock.a_path = "images/ubuntu/jammy/synapse/somefile.yaml"
    mock_diff_index.__iter__.return_value = [diff_item_mock]

    with patch("git.Repo", return_value=mock_repo):
        yield mock_repo


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="update images")
    parser.add_argument("--config", default="config.yaml", help="configuration file name")
    parser.add_argument(
        "--keep", default=False, action="store_true", help="keep files generated from templates"
    )
    parser.add_argument(
        "--stages",
        default=None,
        help="stages to execute (check,build,test,strip,bom,push,scan), default: all",
    )
    parser.add_argument("--images", default=None, help="list of images to build, default: all")
    parser.add_argument(
        "--types", default=None, help="types to build (standard,production,src), default: all"
    )
    parser.add_argument(
        "--internal-scanner",
        default=False,
        action="store_true",
        help="use internal scanner instead of harbor",
    )
    return parser


def create_image_builder(config: str = "config.yaml") -> ImageBuilder:
    parser = create_parser()
    args = parser.parse_args([])
    args.images = list()
    env_extras = dict()
    env_extras["test"] = True
    return ImageBuilder(config=config, keep=args.keep, images=args.images, env_extras=env_extras)


def create_ib() -> ImageBuilder:
    image_configs = {
        MOCK_IMAGECONFIG_UBUNTU: image_config_ubuntu(),
        MOCK_IMAGECONFIG_SYNAPSE: image_config_synapse(),
        MOCK_IMAGECONFIG_PYTHON: image_config_python(),
    }

    with (
        patch.object(PodmanClient, "login", return_value="Response by mock"),
        patch.object(subprocess, "run", return_value="Response by mock"),  # Mock podman login
        # do not render templates with jinja for every test
        patch(
            "factory.update_images.ImageBuilder.get_image_configs_from_directory",
            return_value=image_configs,
        ),
        patch(
            "factory.update_images.ImageBuilder.get_image_configs_from_version_bot", return_value={}
        ),
    ):
        builder = create_image_builder(config="tests/test_data/config.yaml")
        builder.load_image_definitions()
        return builder


# the same as create_ib() but as fixture
@pytest.fixture
def ib() -> ImageBuilder:
    return create_ib()


# unmocked image builder for integration tests
def ibu() -> ImageBuilder:
    builder = create_image_builder(config="tests/test_data/config.yaml")
    builder.load_image_definitions()
    builder.authenticate()
    return builder


def image_config_from_builder(builder: ImageBuilder, image_name: str) -> ImageConfig:
    return deepcopy(builder.image_configs[image_name])


def image_config_ubuntu() -> ImageConfig:
    data = _load_yaml("tests/test_data/config_rendered.yaml")
    image_definition = _load_yaml("tests/test_data/images/ubuntu/jammy/base/image_rendered.yaml")
    data.update(image_definition)
    return ImageConfig(data)


def image_config_synapse() -> ImageConfig:
    data = _load_yaml("tests/test_data/config_rendered.yaml")
    image_definition = _load_yaml("tests/test_data/images/ubuntu/jammy/synapse/image_rendered.yaml")
    data.update(image_definition)
    return ImageConfig(data)


def image_config_python() -> ImageConfig:
    data = _load_yaml("tests/test_data/config_rendered.yaml")
    image_definition = _load_yaml("tests/test_data/images/ubuntu/jammy/python/image_rendered.yaml")
    data.update(image_definition)
    return ImageConfig(data)


def _load_yaml(path: str) -> dict[str, Any]:
    file_path = pathlib.Path(path)
    with open(file_path, encoding="utf-8") as file:
        input_data = file.read()
    return yaml.safe_load(input_data)
