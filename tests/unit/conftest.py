# Copyright 2025 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from io import StringIO

from dotenv import load_dotenv

ENV_FILE = ".env_default"
PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Configures the environment variables for the unit tests.
# CI_PROJECT_DIR is dynamically dependent on the environment and must therefore be adapted.
# Used a stream to avoid having to edit the file.

raw_config = open(ENV_FILE, "r", encoding="utf-8").read()
raw_config += f'CI_PROJECT_DIR="{PROJECT_DIR}"'

config = StringIO(raw_config)
load_dotenv(stream=config, verbose=True)
