# Copyright 2024 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from factory.cve.cve import PackageAnalyzer


class TestPackageAnalyzerUnit:

    def test_package_analyzer_create_report(self) -> None:
        scan_result = [
            {
                "id": "CVE-2021-31879",
                "severity": "Medium",
                "package": "wget",
                "cvss_basescore": "1",
                "version": "1.21.2-2ubuntu1",
                "fix_state": "not-fixed",
                "fix_versions": [],
                "upstream_status": "deferred",
            }
        ]
        pa = PackageAnalyzer(
            image="ubuntu",
            image_dir="ubuntu_jammy_base",
            scan_result=scan_result,
            vulnerability_format="{id:20} {package:25} {version:35} {severity:10} {fix_state:10} {fix_versions:25} "
            "{package_priority:10} {possible_actions:20} {is_ignored_reason:12}",
            ok_vulnerabilities=["Negligible", "Low"],
            ignores=[],
        )
        report = pa.create_report(print_all=True)
        line = report[0]
        # Assert that CVEs which have a severity which are not on the ok_vulnerabilities list
        # are logged as errors.
        assert line.startswith("E:")
