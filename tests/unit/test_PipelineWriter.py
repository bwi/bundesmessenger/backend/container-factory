# Copyright 2024 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from factory.PipelineWriter import PipelineWriter


class TestPipelineWriterUnit:
    def test_template_generation_without_dependencies(self) -> None:
        stages = "test-stages"
        image = "test-image"
        expected_start = (
            "\nbuild-test-image-image:\n"  # Adjusted to include the newline at the start
        )
        result = PipelineWriter.child_pipeline_job_template(stages, image)
        assert result.startswith(expected_start)
        assert "needs:" not in result
        assert f"--stages={stages}" in result
        assert f"--images={image}" in result

    def test_template_generation_with_dependencies(self) -> None:
        stages = "test-stages"
        image = "test-image"
        depends = ["dep1", "dep2"]
        result = PipelineWriter.child_pipeline_job_template(stages, image, depends)
        assert "needs:" in result
        for dep in depends:
            assert (
                f"build-{dep.replace('+', '_').replace('#', '_').replace('~', '_').replace(':', '_')}-image"
                in result
            )

    def test_special_characters_handling_in_image_name(self) -> None:
        stages = "test-stages"
        image = "test+image#name:version~1"
        sanitized_image_name = "test_image_name_version_1"
        result = PipelineWriter.child_pipeline_job_template(stages, image)
        assert f"build-{sanitized_image_name}-image:" in result
