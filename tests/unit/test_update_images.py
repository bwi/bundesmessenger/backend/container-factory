# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import glob
import json
import logging
import os
import subprocess
from datetime import datetime, timedelta
from io import BytesIO
from tempfile import TemporaryDirectory
from typing import Any, cast
from unittest.mock import MagicMock, Mock, call, mock_open, patch

import pytest
import yaml
from podman.domain.containers import Container
from podman.domain.images import Image
from podman.errors import NotFound
from podman.errors.exceptions import APIError, ImageNotFound

from factory.config import ImageConfig
from factory.update_images import ImageBuilder
from tests.conftest import (
    MOCK_IMAGECONFIG_SYNAPSE,
    MOCK_IMAGECONFIG_UBUNTU,
    create_ib,
    image_config_ubuntu,
)

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


class MockContainer(object):
    id: str
    attrs: dict[str, Any] | None

    def __init__(self) -> None:
        self.id = "someid"
        self.attrs = {"Config": {"Image": "image_name"}}

    def exec_run(
        self, cmd: str | list[str], tty: bool = False, user: str | None = None, demux: bool = False
    ) -> tuple[int, bytes]:
        return 0, "somepackage 1.0".encode("latin-1")

    def put_archive(self, path: str, data: bytes | None = None) -> bool:
        return True


class MockContainerExecFail(object):

    def exec_run(
        self, cmd: str | list[str], tty: bool = False, user: str | None = None, demux: bool = False
    ) -> tuple[int, bytes]:
        return 1, "output".encode("latin-1")


class TestImageBuilderUnit:
    ib: ImageBuilder

    @pytest.fixture(autouse=True)
    def before_test(self, ib: ImageBuilder) -> None:
        """Function to replace __init__."""
        self.ib = ib

    @patch("factory.update_images.ImageBuilder.needs_security_update")
    def test_needs_update_given_artifact_returns_true(
        self, mock_needs_security_update: Mock
    ) -> None:
        mock_needs_security_update.return_value = False
        img_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        # remove all dependency-updated-artifacts
        fileList = glob.glob(f"{PROJECT_DIR}/dependency_updated_*")
        for filePath in fileList:
            os.remove(filePath)
        # create test artifact
        with open("dependency_updated_something", "w") as f:
            f.write("updated")
        needs_update = self.ib.needs_update(img_config)
        # remove test artifact
        os.remove("dependency_updated_something")
        assert needs_update is True

    @patch("factory.update_images.ImageBuilder.image_timestamp")
    @patch("factory.update_images.ImageBuilder.latest_subtree_change")
    @patch("factory.update_images.ImageBuilder.needs_security_update")
    def test_needs_update_given_no_artifact_and_repo_change_returns_true(
        self,
        mock_needs_security_update: Mock,
        mock_latest_subtree_change: Mock,
        mock_image_timestamp: Mock,
    ) -> None:
        mock_needs_security_update.return_value = False
        mock_latest_subtree_change.return_value = datetime.timestamp(datetime.now())
        mock_image_timestamp.return_value = (datetime.now() - timedelta(days=1)).timestamp()
        img_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        # remove all dependency-updated-artifacts
        fileList = glob.glob(f"{PROJECT_DIR}/dependency_updated_*")
        for filePath in fileList:
            os.remove(filePath)
        needs_update = self.ib.needs_update(img_config)
        assert needs_update is True

    def test_fix_kwargs_for_podman(self) -> None:
        for image_config_item in self.ib.image_configs.values():
            testuser = "testuser"
            privileged = True
            image_config_item.config.docker["env"] = {"key": "value"}

            with patch.dict("os.environ", {"TEST_BUILD": "1"}):
                image_config_mod = self.ib.fix_kwargs_for_podman(
                    image_config_item, user=testuser, privileged=privileged
                )
            assert image_config_mod["user"] == testuser
            assert image_config_mod["privileged"] == privileged
            assert image_config_mod["environment"]["key"] == "value"

    @pytest.mark.parametrize(
        "image_attrs",
        [
            {
                "Config": {
                    "Cmd": ["somecommand", "-someparam"],
                }
            },
            {
                "Config": {
                    "Cmd": ["somecommand", "-someparam"],
                    "Entrypoint": ["someentrypoint.sh"],
                }
            },
        ],
    )
    def test_create_podman_changes_command(self, image_attrs: dict[str, dict[str, list]]) -> None:
        mocked_image = MagicMock()
        mocked_image.attrs = image_attrs
        entry_point = image_attrs["Config"].get("Entrypoint")
        cmd = image_attrs["Config"].get("Cmd")

        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        image_license = image_config.config.image_license
        remote_name = image_config.baseimage_name_with_tag(with_project=True, with_registry=True)
        with patch.object(self.ib.podman_client.images, "get", return_value=mocked_image):
            changes = self.ib.create_podman_changes_command(remote_name, image_config)
        assert f'LABEL org.opencontainers.image.licenses="{image_license}"' in changes
        # pytest uses spaces, single and double quotes in a different way
        assert f"CMD {cmd}".replace("'", '"').replace(", ", ",") in changes
        if entry_point:
            assert f"ENTRYPOINT {entry_point}".replace("'", '"') in changes

    def test_remove_package(self) -> None:
        container = cast(Container, MockContainer())
        _, success_pkg = self.ib.remove_package(container, ["curl"], "curl")
        assert "curl" in success_pkg

    def test_remove_deb_packages(self) -> None:
        container = cast(Container, MockContainer())
        self.ib.remove_deb_packages("someimg", container, ["curl"])
        # TODO: add assertions

    def test_remove_apt(self) -> None:
        container_good = cast(Container, MockContainer())
        self.ib.remove_apt(container_good)

        container_fail = cast(Container, MockContainer())
        self.ib.remove_apt(container_fail)
        # TODO: add assertions

    @pytest.mark.parametrize(
        "init_param, expected_stages",
        [
            # check if all stages are added, if no stages are provided
            (
                "",
                [
                    "render",
                    "lintcheck",
                    "spellcheck",
                    "check",
                    "build",
                    "test",
                    "strip",
                    "ref",
                    "src",
                    "bom",
                    "qa",
                    "push",
                    "scan",
                    "sign",
                ],
            ),
            # check if test is added for push
            ("check,build,push", ["check", "build", "push", "test"]),
        ],
    )
    def test_init_stages(self, init_param: str, expected_stages: list[str]) -> None:
        stages = self.ib.init_stages(init_param)
        assert stages == expected_stages

    def test_init_types(self) -> None:
        """check if all types are added, if no types are provided"""
        types = self.ib.init_types(set([]))
        assert types == {"standard", "production", "src"}

    def test_check_build_id(self) -> None:
        mocked_image = MagicMock()
        mocked_image.labels = {"de.bwi.baseimage.build-id": "someimageid"}
        self.ib.build_id = "someimageid"
        with patch.object(self.ib.podman_client.images, "get", return_value=mocked_image):
            self.ib.check_build_id("some_imagename_not_needed_while_mocked")

        self.ib.build_id = "doesnotmatch"
        with (
            pytest.raises(Exception),
            patch.object(self.ib.podman_client.images, "get", return_value=mocked_image),
        ):
            self.ib.check_build_id("some_imagename_not_needed_while_mocked")

    @patch("factory.update_images.ImageBuilder.run_stages")
    @patch("factory.update_images.subprocess.check_call")
    @patch("factory.update_images.log.info")
    def test_run(self, mock_log: Mock, mock_check_call: Mock, mock_run_stages: Mock) -> None:
        self._patch_image_paths()
        with (
            patch("factory.update_images.ImageBuilder.authenticate"),
            patch("factory.update_images.ImageBuilder.load_image_definitions"),
        ):
            self.ib.run()
        assert mock_log.called

        self.ib.stages = ["check"]
        with (
            patch("factory.update_images.ImageBuilder.authenticate"),
            patch("factory.update_images.ImageBuilder.load_image_definitions"),
        ):
            self.ib.run()
        assert mock_log.called

    @pytest.mark.parametrize(
        "check_skip_return, should_run_stages",
        [
            (True, False),  # If check_skip returns True, run_stages should not be called
            (False, True),  # If check_skip returns False, run_stages should be called
        ],
    )
    def test_run_stages_based_on_skip(
        self, check_skip_return: bool, should_run_stages: bool
    ) -> None:
        self._patch_image_paths()
        with (
            patch("factory.update_images.ImageBuilder.authenticate"),
            patch("factory.update_images.ImageBuilder.load_image_definitions"),
            patch("factory.update_images.ImageBuilder.check_skip", return_value=check_skip_return),
            patch("factory.update_images.ImageBuilder.run_stages") as mock_run_stages,
            patch("factory.update_images.subprocess.check_call"),
            patch("factory.update_images.log.info"),
        ):
            self.ib.run()
            assert mock_run_stages.called == should_run_stages

    @patch("factory.update_images.log.info")
    @patch("factory.update_images.log.debug")
    def test_trigger_scan(self, mock_log_debug: Mock, mock_log_info: Mock) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with patch("requests.get"), patch("requests.post"):
            self.ib.config["vulnerability_scanner"] = "trivy"
            self.ib.trigger_scan(image_config)
            assert mock_log_info.called

        with patch("requests.get"):
            self.ib.config["vulnerability_scanner"] = "grype"
            self.ib.trigger_scan(image_config)
            assert mock_log_debug.called

    @patch("factory.update_images.subprocess.check_output")
    def test_internal_scan(self, mock_check_output: Mock) -> None:
        scan_data = {
            "matches": [
                {
                    "vulnerability": {
                        "id": "someid",
                        "severity": "medium",
                        "fix": {
                            "state": "fixed",
                            "versions": ["1.4.0"],
                        },
                    },
                    "artifact": {
                        "name": "somepackage",
                        "version": "1.2.3",
                    },
                    "relatedVulnerabilities": [
                        {
                            "cvss": [
                                {
                                    "version": "3.1",
                                    "metrics": {"baseScore": "5"},
                                }
                            ]
                        }
                    ],
                }
            ]
        }
        mock_check_output.return_value = json.dumps(scan_data)
        self.ib.prod_img_name = "somename"

        with (
            patch("os.makedirs"),
            patch("shutil.copy"),
        ):
            items = self.ib.internal_scan()

        assert isinstance(items, list)
        assert items[0]["cvss_basescore"] == "5 [v3.1]"

    @patch("factory.podman_api.PodmanClient.images")
    @patch("factory.update_images.ImageBuilder.run_podman")
    @patch("factory.update_images.subprocess.check_call")
    @patch("factory.update_images.log.info")
    def test_needs_security_update(
        self, mock_log: Mock, mock_check_call: Mock, mock_run_podman: Mock, mock_client_images: Mock
    ) -> None:
        mock_client_images.pull.return_value = None
        mock_run_podman.iter.return_value.__iter__.return_value = None
        img_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        res = self.ib.needs_security_update(img_config)
        assert mock_log.called
        assert res is True

        mock_check_call.side_effect = subprocess.CalledProcessError(returncode=2, cmd=["bad"])
        res = self.ib.needs_security_update(img_config)
        assert res is False

    @patch("requests.get")
    def test_external_scan(self, mock_requests_get: Mock) -> None:
        # test with version string not set
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        mock_response = MagicMock()
        mock_response.ok = True
        mock_response.json.return_value = {}
        mock_requests_get.return_value = mock_response
        self.ib.prod_name_int = "some:tag"
        res = self.ib.external_scan(image_config)
        assert res is None

        # test with version string set
        base_score = "123"
        vulnerability = {
            "id": "CVE-2023-0001",
            "severity": "3",
            "package": "wget",
            "version": "1.1",
            "fix_version": ["1.2"],
            "vendor_attributes": {"CVSS": {"nvd": {"V3Vector": "xxxxx123", "V3Score": base_score}}},
        }
        mock_response.json.return_value = {
            "application/vnd.security.vulnerability.report; version=1.1": {
                "vulnerabilities": [vulnerability]
            }
        }
        mock_requests_get.return_value = mock_response
        self.ib.prod_name_int = "some:tag"
        res = self.ib.external_scan(image_config)
        assert res
        line = res[0]
        assert line["id"] == vulnerability["id"]
        assert base_score in line["cvss_basescore"]
        assert isinstance(res, list)

        # test with missing nvc data
        vulnerability["vendor_attributes"] = {"CVSS": {"nvd": {}}}
        mock_response.json.return_value = {
            "application/vnd.security.vulnerability.report; version=1.1": {
                "vulnerabilities": [vulnerability]
            }
        }
        mock_requests_get.return_value = mock_response
        self.ib.prod_name_int = "some:tag"
        res = self.ib.external_scan(image_config)
        assert isinstance(res, list)

    @pytest.mark.parametrize(
        "scanner, expected_call",
        [
            ("trivy", "mock_external_scan"),
            ("grype", "mock_internal_scan"),
            ("", "mock_internal_scan"),
            (None, "mock_internal_scan"),
        ],
    )
    @patch("factory.update_images.ImageBuilder.internal_scan")
    @patch("factory.update_images.ImageBuilder.external_scan")
    def test_download_scan(
        self, mock_external_scan: Mock, mock_internal_scan: Mock, scanner: str, expected_call: str
    ) -> None:
        self.ib.config["vulnerability_scanner"] = scanner
        self.ib.download_scan(self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU])

        if expected_call == "mock_external_scan":
            assert mock_external_scan.called
        else:
            assert mock_internal_scan.called

    @pytest.mark.parametrize(
        "file_exists, expected_call",
        [
            (True, True),
            (False, False),
        ],
    )
    @patch("os.access")
    @patch("factory.update_images.ImageBuilder.remove_deb_packages")
    @patch("factory.update_images.ImageBuilder.run_script")
    @patch("factory.update_images.ImageBuilder.remove_apt")
    def test_strip(
        self,
        mock_remove_apt: Mock,
        mock_run_script: Mock,
        mock_remove_deb_packages: Mock,
        mock_access: Mock,
        file_exists: bool,
        expected_call: bool,
    ) -> None:
        file_content = """
            steps:
            - remove_deb_packages
            - remove_apt
            - remove_files
            remove_deb_packages:
                action: remove_deb_packages
                params:
                    - logsave(:amd64)?
                    - libss2(:amd64)?
            remove_apt:
                action: remove_apt
                params: null
            remove_files:
                action: script
                params:
                    shell: /bin/bash
                    script:
                    - rm -rf /var/lib/apt
                    - rm -rf /etc/apt
        """

        container = cast(Container, MockContainer())
        with patch("builtins.open", mock_open(read_data=file_content)):
            mock_access.return_value = file_exists
            self.ib.strip("undefined", container)
            assert mock_remove_deb_packages.called == expected_call
            assert mock_run_script.called == expected_call
            assert mock_remove_apt.called == expected_call

    @patch("factory.update_images.ImageBuilder.check_build_id")
    @patch("factory.update_images.ImageBuilder.run_podman")
    def test_check_mandatory_files(self, mock_run_podman: Mock, mock_check_build_id: Mock) -> None:
        mock_run_podman.return_value.__enter__.return_value = MockContainer()
        img_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        self.ib.check_mandatory_files("", "", img_config=img_config)

        mock_run_podman.return_value.__enter__.return_value = MockContainerExecFail()
        with pytest.raises(Exception):
            self.ib.check_mandatory_files("", "", img_config=img_config)

    # we have to overwrite the commit message, as it may include an [all] special tag,
    # which would make this test fail as ib.changed_imaged would be empty
    @patch.dict(
        "os.environ",
        {
            "CI_COMMIT_BRANCH": "feature/x",
            "CI_COMMIT_MESSAGE": "Some commit message without all tag.",
        },
    )
    def test_retrieve_changed_images_with_feature_branch(self, mock_git_repo: Mock) -> None:
        changed_images = self.ib.retrieve_changed_images()
        assert isinstance(changed_images, set)

    # we have to overwrite the commit message, as it may include an [all] special tag,
    # which would make this test fail as ib.changed_imaged would be empty
    @patch.dict(
        "os.environ",
        {
            "CI_COMMIT_BRANCH": "someother",
            "CI_COMMIT_MESSAGE": "Some commit message without all tag.",
        },
    )
    def test_retrieve_changed_images_with_non_feature_branch(self, mock_git_repo: Mock) -> None:
        changed_images = self.ib.retrieve_changed_images()
        assert isinstance(changed_images, set)
        assert len(changed_images) == 0

    @pytest.mark.parametrize(
        "image_attrs, expected",
        [
            ({"attrs": {"RepoTags": ["ubuntu-latest-jammy"]}}, True),
            (None, False),
        ],
    )
    def test_can_load_image_from_registry(self, image_attrs: dict | None, expected: bool) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with patch.object(self.ib.podman_api, "podman_client") as mock_podman_client:
            if image_attrs is not None:
                mocked_image = Image(**image_attrs)
            else:
                mocked_image = Image()
            mock_podman_client.images.pull.return_value = mocked_image
            result = self.ib.can_load_image_from_registry(image_config)
            assert result == expected

    @pytest.mark.parametrize(
        "exception",
        [
            ImageNotFound("some error"),
            ConnectionError,
            NotFound("some error"),
            APIError("some error"),
            OSError,
        ],
    )
    def test_can_load_image_from_registry_exception(self, exception: Exception) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with patch.object(self.ib.podman_client.images, "pull", side_effect=exception):
            assert not self.ib.can_load_image_from_registry(image_config)

    def _some_method_that_triggers_push(self, image: Image, remote_name: str) -> None:
        """Example calls that should be made to self.ib.podman_api.push"""
        self.ib.podman_api.push(image, remote_name, "latest-unhardened", "message")
        self.ib.podman_api.push(image, remote_name, "latest-jammy-unhardened", "message")
        self.ib.podman_api.push(image, remote_name, "latest-production", "message")
        self.ib.podman_api.push(image, remote_name, "latest-jammy-production", "message")

    def _verify_push_calls(self, img_config: ImageConfig) -> None:
        distro_codename = img_config.config.distro_codename
        remote_name = img_config.basename(with_project=True, with_registry=True)
        image = self.ib.podman_client.images.get(img_config.get_prod_image_name())
        expected_calls = [
            call(image, remote_name, "latest-unhardened"),
            call(image, remote_name, f"latest-{distro_codename}-unhardened"),
            call(image, remote_name, "latest-production"),
            call(image, remote_name, f"latest-{distro_codename}-production"),
        ]

        with patch.object(self.ib.podman_api, "push") as mock_push:
            # Simulate the method that triggers the push calls.
            self._some_method_that_triggers_push(image, remote_name)

            # Convert expected_calls to a set of tuples with only the first three arguments
            expected_calls_set = {tuple(c.args[:3]) for c in expected_calls}
            # Extract only the first three arguments from the actual calls and convert to tuples
            actual_calls_set = {tuple(c.args[:3]) for c in mock_push.call_args_list}

            # Assert that the sets of expected and actual calls match
            assert actual_calls_set == expected_calls_set

    @pytest.mark.parametrize(
        "is_latest_version, is_bot_version, should_push",
        [
            (True, True, True),
            (False, False, True),
            (False, True, False),
        ],
    )
    def test_push_latest_tag(
        self, is_latest_version: bool, is_bot_version: bool, should_push: bool
    ) -> None:
        img_config = MagicMock()
        img_config.get_prod_image_name.return_value = "ubuntu"
        img_config.config.distro = "ubuntu"
        img_config.config.distro_codename = "jammy"
        img_config.config.basename = "fake_image"
        image = MagicMock()

        with (
            patch(
                "factory.update_images.ImageBuilder.is_latest_version",
                return_value=is_latest_version,
            ),
            patch("factory.update_images.ImageBuilder.is_bot_version", return_value=is_bot_version),
            patch.object(self.ib.podman_client.images, "get", return_value=MagicMock()),
            patch.object(self.ib.podman_api, "push", autospec=True) as mock_push,
        ):
            # Call the method that should trigger the push
            self.ib.push_latest_tag(img_config, image)

            if should_push:
                self._verify_push_calls(img_config)
            else:
                assert not mock_push.called

    @patch("factory.update_images.ImageBuilder.trigger_scan")
    @patch("factory.update_images.ImageBuilder.download_scan")
    def test_perform_scan_triggers_scan(
        self, mock_download_scan: Mock, mock_trigger_scan: Mock
    ) -> None:
        img_config = Mock()
        img_config.config = {"updated": True}

        self.ib.config["scan_timeout"] = 1
        mock_trigger_scan.return_value = False
        assert self.ib.perform_scan(img_config) is None

        with pytest.raises(Exception):
            mock_trigger_scan.return_value = True
            mock_download_scan.return_value = None
            result = self.ib.perform_scan(img_config)

        mock_download_scan.return_value = [{"id": "CVE-2022-3219"}]
        result = self.ib.perform_scan(img_config)
        assert result
        assert result[0]["id"] == "CVE-2022-3219"

        mock_trigger_scan.assert_called()
        mock_download_scan.assert_called()

    @patch("factory.update_images.ImageBuilder.dump_analysis_parameters")
    @patch("factory.update_images.ImageBuilder.create_cve_report")
    def test_process_scan_result(
        self, mock_create_cve_report: Mock, mock_dump_analysis_parameters: Mock
    ) -> None:
        img_config = Mock()
        scan_result = [{"result": "data"}]

        self.ib.process_scan_result(img_config, scan_result)

        mock_dump_analysis_parameters.assert_called_once_with(img_config, scan_result)
        mock_create_cve_report.assert_called_once()

    @patch("factory.update_images.requests.get")
    def test_add_upstream_status_success(self, mock_requests_get: Mock) -> None:
        """Test that getting a valid response from remote API."""
        resp = """
        {
            "id": "CVE-2022-3219",
            "packages": [
                {
                    "statuses": [
                        {
                            "description": null,
                            "release_codename": "jammy",
                            "status": "deferred"
                        }
                    ]
                }
            ],
            "status": "active"
        }
        """
        mock_requests_get.return_value.status_code = 200
        mock_requests_get.return_value.json.return_value = json.loads(resp)
        data = self.ib.add_upstream_status([{"id": "CVE-2022-3219"}])
        assert data[0]["upstream_status"] == "deferred"

    @patch("factory.update_images.requests.get")
    def test_add_upstream_status_success_with_description(self, mock_requests_get: Mock) -> None:
        """Test that getting a valid repsonse (with description) from remote API."""
        resp = """
        {
            "id": "CVE-2022-3219",
            "packages": [
                {
                    "statuses": [
                        {
                            "description": "2022-09-28",
                            "release_codename": "jammy",
                            "status": "deferred"
                        }
                    ]
                }
            ],
            "status": "active"
        }
        """
        mock_requests_get.return_value.status_code = 200
        mock_requests_get.return_value.json.return_value = json.loads(resp)
        data = self.ib.add_upstream_status([{"id": "CVE-2022-3219"}])
        assert data[0]["upstream_status"] == "deferred (2022-09-28)"

    @patch("factory.update_images.requests.get")
    def test_add_upstream_status_error_response(self, mock_requests_get: Mock) -> None:
        """Test that a CVE is not found by remote API."""
        mock_requests_get.return_value.status_code = 404
        data = self.ib.add_upstream_status([{"id": "CVE-2022-3219"}])
        assert data[0]["upstream_status"] == "undefined"

    @patch("factory.update_images.requests.get")
    def test_add_upstream_status_json_error(self, mock_requests_get: Mock) -> None:
        """Test that remote API returns an invalid JSON reponse."""
        mock_requests_get.return_value.status_code = 200
        mock_requests_get.return_value.json.side_effect = json.decoder.JSONDecodeError(
            "Expecting value", "", 0
        )
        data = self.ib.add_upstream_status([{"id": "CVE-2022-3219"}])
        assert data[0]["upstream_status"] == "undefined"

    @patch("factory.update_images.requests.get")
    def test_add_upstream_status_not_active(self, mock_requests_get: Mock) -> None:
        """Test that CVE is not active anymore."""
        status = "unactive"
        mock_requests_get.return_value.status_code = 200
        mock_requests_get.return_value.json.return_value = {"status": status}
        data = self.ib.add_upstream_status([{"id": "CVE-2022-3219"}])
        assert data[0]["upstream_status"] == status

    def test_add_upstream_status_invalid_cve(self) -> None:
        """Test that CVE has not a valid name for this remote API."""
        data = self.ib.add_upstream_status([{"id": "OTHER-2022-3219"}])
        assert data[0]["upstream_status"] == "undefined"

    def test_dump_analysis_parameters(self) -> None:
        config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU].config
        config.update(
            {
                "basename": "example_image",
                "directory": "/path/to/directory",
                "vulnerability_format": "format1",
                "updated": True,
            }
        )
        self.ib.project_dir = "/tmp/testdir"
        img_config = MagicMock()
        img_config.config = config
        scan_result = [{"result": "data"}]

        os.makedirs(self.ib.project_dir, exist_ok=True)
        self.ib.dump_analysis_parameters(img_config, scan_result)
        with open(os.path.join(self.ib.project_dir, "analyzer_params.yaml"), encoding="utf-8") as f:
            data = yaml.safe_load(f)
        assert isinstance(data, dict)
        assert data["scan_result"] == scan_result

    def test_setup_docker_auth(self) -> None:
        with (
            TemporaryDirectory() as tmpdir_auth,
            TemporaryDirectory() as tmpdir_config,
            patch.dict("os.environ", {"XDG_RUNTIME_DIR": tmpdir_auth}),
        ):
            # Simulate the existence of auth.json with some content
            auth_content = '{"auth": "token"}'
            os.makedirs(f"{tmpdir_auth}/containers", exist_ok=True)
            with open(f"{tmpdir_auth}/containers/auth.json", "w", encoding="utf-8") as file:
                file.write(auth_content)

            # be sure to use a non existing path, the temp path is already existing
            test_path = os.path.join(tmpdir_config, "something")
            self.ib.setup_docker_auth(docker_dir=test_path)

            # Check if the Docker config directory is created
            assert os.path.isdir(test_path)

            # Check if auth.json is copied correctly to the Docker config directory
            with open(f"{test_path}/config.json", "r", encoding="utf-8") as file:
                copied_content = file.read()
            assert copied_content == auth_content

    @patch("factory.update_images.get_bearer_token")
    @patch("factory.update_images.inspect")
    def test_prepare_image_signature(self, mock_inspect: Mock, mock_get_bearer_token: Mock) -> None:
        inspect_digest = "sha256:12345"
        mock_inspect.return_value = {"Digest": inspect_digest}
        mock_podman_client = MagicMock()
        digest = "sha256:67890"
        mock_podman_client.images.pull.return_value.attrs = {"Digest": digest}

        with patch.object(self.ib.podman_api, "podman_client", mock_podman_client):
            img_config_mock = MagicMock()
            img_config_mock.config.basename = "ubuntu"
            img_config_mock.config.project = "myproject"
            img_config_mock.config.registry = "harbor"
            self.ib.prepare_image_signature("image:tag", img_config_mock)

            img_config_mock.config.registry = "myregistry"
            full_digest, sha_sum = self.ib.prepare_image_signature("image:tag", img_config_mock)

            mock_inspect.assert_called()
            mock_podman_client.images.pull.assert_called()
            assert inspect_digest in full_digest
            assert sha_sum == inspect_digest.split(":")[1]

    @patch("factory.update_images.subprocess.check_output")
    @patch("factory.update_images.pformat")
    @patch("factory.update_images.log")
    def test_sign_and_log_image(
        self, mock_log: Mock, mock_pformat: Mock, mock_check_output: Mock
    ) -> None:
        with patch.object(self.ib, "get_signature", return_value={}) as mock_get_signature:

            mock_check_output.side_effect = subprocess.CalledProcessError(returncode=2, cmd=["bad"])
            self.ib.sign_and_log_image(
                "registry",
                "username",
                "password",
                "tuf_url",
                "fulcio_url",
                "rekor_url",
                "full_digest",
                "sha_sum",
                MagicMock(),
            )
            assert mock_log.error.called

            mock_check_output.side_effect = subprocess.SubprocessError()
            self.ib.sign_and_log_image(
                "registry",
                "username",
                "password",
                "tuf_url",
                "fulcio_url",
                "rekor_url",
                "full_digest",
                "sha_sum",
                MagicMock(),
            )
            assert mock_log.exception.called

            mock_check_output.side_effect = None
            self.ib.sign_and_log_image(
                "registry",
                "username",
                "password",
                "tuf_url",
                "fulcio_url",
                "rekor_url",
                "full_digest",
                "sha_sum",
                MagicMock(),
            )
            mock_check_output.assert_called()
            mock_get_signature.assert_called()
            mock_pformat.assert_called()

    def test_sign_image(self) -> None:
        img_config_mock = MagicMock()
        img_config_mock.config.registry = "registry"

        with (
            patch.object(self.ib, "setup_docker_auth") as mock_setup_docker_auth,
            patch.object(
                self.ib, "prepare_image_signature", return_value=("full_digest", "sha_sum")
            ) as mock_prepare_image_signature,
            patch.object(self.ib, "sign_and_log_image") as mock_sign_and_log_image,
        ):
            # Call the method
            self.ib.sign_image("image_name", img_config_mock, "fulcio_url", "rekor_url", "tuf_url")

            # Assertions to verify that the necessary methods are called
            mock_setup_docker_auth.assert_called_once()
            mock_prepare_image_signature.assert_called_once()
            mock_sign_and_log_image.assert_called_once()

    def test_get_id_token_success(self) -> None:
        with patch("factory.update_images.requests.post") as mock_post:
            mock_response = MagicMock()
            mock_response.ok = True
            mock_response.json.return_value = {"id_token": "test_id_token"}
            mock_post.return_value = mock_response

            token = self.ib.get_id_token(
                "test_url", "test_client_id", "test_client_secret", "test_username", "test_password"
            )

            assert token == "test_id_token"
            mock_post.assert_called_with(
                url="test_url",
                data={
                    "grant_type": "password",
                    "client_id": "test_client_id",
                    "client_secret": "test_client_secret",
                    "username": "test_username",
                    "password": "test_password",
                    "scope": "openid",
                },
                headers={"content-type": "application/x-www-form-urlencoded"},
            )

    def test_get_id_token_failure(self) -> None:
        with patch("factory.update_images.requests.post") as mock_post:
            mock_response = MagicMock()
            mock_response.ok = False
            mock_post.return_value = mock_response

            token = self.ib.get_id_token(
                "test_url", "test_client_id", "test_client_secret", "test_username", "test_password"
            )

            assert token is None

    @pytest.mark.parametrize(
        "disable_buildversions, expect_call_count",
        [
            (True, 1),
            (False, 2),
        ],
    )
    def test_process_image_signing(
        self, disable_buildversions: bool, expect_call_count: int
    ) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with (
            patch.object(self.ib, "sign_image") as mock_sign_image,
            patch.object(image_config, "get_buildversion_prod_int"),
        ):
            self.ib.disable_buildversions = disable_buildversions
            self.ib.process_image_signing(
                image_config, "test_id_token", "test_fulcio_url", "test_rekor_url", "test_tuf_url"
            )
            assert mock_sign_image.call_count == expect_call_count
            assert os.environ.get("SIGSTORE_ID_TOKEN", "") == "test_id_token"

    def test_process_sign(self) -> None:
        with (
            patch.object(self.ib, "get_id_token") as mock_get_id_token,
            patch.object(self.ib, "process_image_signing") as mock_process_image_signing,
            patch.dict(
                "os.environ",
                {
                    "KEYCLOAK_SERVER": "keycloak_server",
                    "KEYCLOAK_REALM": "keycloak_realm",
                    "KEYCLOAK_CLIENT_ID": "client_id",
                    "KEYCLOAK_CLIENT_SECRET": "client_secret",
                    "KEYCLOAK_USERNAME": "username",
                    "KEYCLOAK_PASSWORD": "password",
                    "FULCIO_URL": "fulcio_url",
                    "REKOR_URL": "rekor_url",
                    "TUF_URL": "tuf_url",
                },
            ),
        ):
            # Setup
            img_config = MagicMock()
            img_config.config.updated = True
            mock_get_id_token.return_value = "test_id_token"
            self.ib.disable_external_push = False
            self.ib.disable_buildversions = False
            self.ib.stages = ["sign"]

            # Call the process_sign function
            self.ib.process_sign(img_config)

            # Assertions
            mock_get_id_token.assert_called()
            mock_process_image_signing.assert_called()

    @pytest.mark.parametrize(
        "version, expected_rolling_tag",
        [
            ("1.2.3", "1.2"),
            ("3.9", "3"),
        ],
    )
    def test_extract_rolling_tag(self, version: str, expected_rolling_tag: str) -> None:
        """test the correct removal of the last segment of a version"""
        assert self.ib.extract_rolling_tag(version) == expected_rolling_tag

    @pytest.mark.parametrize(
        "disable_external_push, expected_result",
        [
            (False, 2),  # test if internal and external push is called
            (True, 1),  # test if external push is not called on disable_external_push
        ],
    )
    def test_push_tags(self, disable_external_push: bool, expected_result: int) -> None:
        prod_image = MagicMock()
        with patch.object(self.ib.podman_api, "push") as mock_push:
            self.ib.disable_external_push = disable_external_push
            self.ib.push_tags(prod_image, "prod_name_int", "prod_name_ext", "1.2")
            assert mock_push.call_count == expected_result

    @patch("factory.update_images.ImageBuilder.is_latest_in_range", return_value=True)
    def test_handle_rolling_tag_with_is_latest(self, mock_is_latest_in_range: Mock) -> None:
        """test that rolling tag is pushed if latest in range"""
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with patch.object(self.ib, "push_tags") as mock_push_tags:
            self.ib.handle_rolling_tag(
                image_config, "1.2.3-jammy", MagicMock(), "prod_name_int", "prod_name_ext"
            )
            mock_push_tags.assert_called()
            mock_is_latest_in_range.assert_called()

    def test_handle_rolling_tag_not_called_for_not_latest_in_range(self) -> None:
        """test that no rolling tag is pushed if not latest in range"""
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with (
            patch.object(self.ib, "push_tags") as mock_push_tags,
            patch("factory.update_images.ImageBuilder.is_latest_in_range", return_value=False),
        ):
            self.ib.handle_rolling_tag(
                image_config, "1.2.3-jammy", MagicMock(), "prod_name_int", "prod_name_ext"
            )
            mock_push_tags.assert_not_called()

    @pytest.mark.parametrize(
        "is_bot_version, target_tag, expected_result",
        [
            (True, "1.2.3", False),  # bot version
            (False, "1.2.3", True),  # not bot version
            (True, "sometag-1.2", False),  # wrong tag
        ],
    )
    def test_handle_system_package_version_tag(
        self, is_bot_version: bool, target_tag: str, expected_result: bool
    ) -> None:
        """Test if rolling tag is pushed, if bot version or not."""
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with (
            patch.object(self.ib, "push_tags") as mock_push_tags,
            patch("factory.update_images.ImageBuilder.is_bot_version", return_value=is_bot_version),
        ):
            self.ib.handle_system_package_version_tag(
                image_config, target_tag, MagicMock(), "prod_name_int", "prod_name_ext"
            )
        assert mock_push_tags.called == expected_result

    @pytest.mark.parametrize(
        "disable_external_push, stages, types, expect_push_source_called",
        [
            (False, ["push"], set(["production"]), False),
            (False, ["push", "src"], set(["src"]), True),
            (True, ["push", "src"], set(["src"]), False),
            (False, ["push", "src"], set(["production"]), False),
            (False, ["push"], set(["production", "src"]), False),
        ],
    )
    def test_stage_push_source(
        self,
        disable_external_push: bool,
        stages: list[str],
        types: set[str],
        expect_push_source_called: bool,
    ) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        with (
            patch.object(self.ib, "push_latest_tag"),
            patch.object(self.ib, "push_stable_tag"),
            patch.object(self.ib, "push_rolling_and_system_package_version_tags"),
            patch.object(self.ib, "push_commit_tag"),
            patch.object(self.ib, "push_source_tag") as mock_push_source,
            patch.object(self.ib, "push_production_tag"),
            patch.object(self.ib, "push_image_config_tags"),
            patch.object(self.ib, "push_build_version_tags"),
            patch.object(self.ib, "podman_client") as mock_podman_client,
            patch("factory.update_images.log.info"),
            patch.object(image_config, "get_src_image_name"),
            patch.object(image_config, "get_prod_image_name"),
            patch.object(image_config, "get_src_name"),
            patch.object(image_config, "get_image_name_opencode") as mock_get_image_name_opencode,
        ):
            mock_podman_client.images.get.return_value = MagicMock()
            self.ib.disable_external_push = disable_external_push
            self.ib.stages = stages
            self.ib.types = types
            mock_get_image_name_opencode.side_effect = [
                ("somename", "1.2.3-production"),
                ("somename", "1.2.3-src"),
            ]

            self.ib.process_push(image_config)
            assert mock_push_source.called is expect_push_source_called

    @patch("factory.update_images.log")
    @patch("factory.update_images.replace_forbidden_chars")
    def test_push_image_config_tag_internal(self, mock_replace: Mock, mock_log: Mock) -> None:
        self.ib.config.update({"registry": "mock_registry", "project": "mock_project"})
        mock_image = MagicMock()
        # Use patch to mock the push method
        with patch.object(
            self.ib.podman_api, "push", return_value=iter(["pushed successfully"])
        ) as mock_push:
            tag = "internal_tag:latest"
            # Test pushing an internal tag
            self.ib.push_image_config_tag(tag, mock_image)
            mock_log.info.assert_called()
            mock_replace.assert_called()
            mock_push.assert_called()

    @patch("factory.update_images.log")
    @patch("factory.update_images.replace_forbidden_chars")
    def test_push_image_config_tag_external(self, mock_replace: Mock, mock_log: Mock) -> None:
        self.ib.config.update(
            {
                "registry": "mock_registry",
                "project": "mock_project",
                "external_registries": ["external_registry"],
            }
        )
        mock_image = MagicMock()
        with patch.object(self.ib.podman_api, "push", autospec=True) as mock_push:
            tag = "external_registry/external_tag:latest"
            # Test pushing an external tag
            self.ib.push_image_config_tag(tag, mock_image)
            # Assertions
            mock_log.info.assert_called()
            mock_replace.assert_called()
            mock_push.assert_called()

    @patch("factory.update_images.log")
    @patch("factory.update_images.replace_forbidden_chars")
    def test_push_image_config_tag_skip_external(self, mock_replace: Mock, mock_log: Mock) -> None:
        self.ib.config.update(
            {
                "registry": "mock_registry",
                "project": "mock_project",
                "external_registries": ["external_registry"],
            }
        )
        with patch.object(self.ib.podman_api, "push") as mock_push:
            self.ib.disable_external_push = True
            mock_image = MagicMock()
            tag = "external_registry/external_tag:latest"
            # Test skipping pushing an external tag
            self.ib.push_image_config_tag(tag, mock_image)
            mock_push.assert_not_called()

    @pytest.mark.parametrize(
        "is_bot_version, is_stable_version, expected_pushes",
        [
            (True, True, 4),
            (False, False, 4),
            (True, False, 0),
            (False, True, 4),
        ],
    )
    @patch("factory.update_images.ImageConfig")
    def test_push_stable_tag(
        self,
        mock_img_config: Mock,
        is_bot_version: bool,
        is_stable_version: bool,
        expected_pushes: int,
    ) -> None:
        with (
            patch.object(self.ib.podman_api, "push") as mock_push,
            patch(
                "factory.update_images.ImageBuilder.is_bot_version",
                return_value=is_bot_version,
            ),
            patch(
                "factory.update_images.ImageBuilder.is_stable_version",
                return_value=is_stable_version,
            ),
        ):
            self.ib.podman_client = MagicMock()
            self.ib.podman_client.images.get.return_value = MagicMock()
            mock_img_config.return_value.get_prod_image_name.return_value = "prod_image_name"
            mock_img_config.return_value.config.distro = "distro_value"
            mock_img_config.return_value.config.distro_codename = "distro_codename_value"
            mock_img_config.return_value.config.basename = "fake_image"
            self.ib.push_stable_tag(mock_img_config(), MagicMock())
            assert mock_push.call_count == expected_pushes

    @patch("factory.update_images.ImageBuilder.is_latest_version")
    @patch("factory.update_images.ImageBuilder.is_bot_version")
    @patch("factory.update_images.ImageConfig")
    def test_push_build_version_tags(
        self,
        mock_img_config: Mock,
        mock_is_bot_version: Mock,
        mock_is_latest_version: Mock,
    ) -> None:
        mock_img_config.return_value.config = {"updated": True, "basename": "fake_image"}
        mock_img_config.return_value.get_new_image_name.return_value = (
            "image_name",
            "build_version_tag",
        )
        mock_img_config.return_value.get_image_name_opencode.return_value = (
            "opencode_img_name",
            "build_version_tag",
        )
        mock_is_latest_version.return_value = True
        mock_is_bot_version.return_value = False
        prod_image = MagicMock()
        with patch.object(self.ib.podman_api, "push") as mock_push:
            self.ib.disable_external_push = False
            self.ib.push_build_version_tags(mock_img_config(), prod_image)
            assert mock_push.call_count == 2

    def test_push_rolling_and_system_package_version_tags(self) -> None:
        prod_image = MagicMock(spec=Image)
        prod_name_int = "prod_name_int"
        prod_name_ext = "prod_name_ext"
        target_tag = "target_tag"
        config_tag = "config_tag"
        with (
            patch.object(self.ib, "handle_rolling_tag") as mock_handle_rolling_tag,
            patch.object(
                self.ib, "handle_system_package_version_tag"
            ) as mock_handle_system_package_version_tag,
        ):
            self.ib.push_rolling_and_system_package_version_tags(
                self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU],
                config_tag,
                prod_image,
                prod_name_int,
                prod_name_ext,
                target_tag,
            )
            mock_handle_rolling_tag.assert_called()
            mock_handle_system_package_version_tag.assert_called()

    @patch("builtins.open", new_callable=mock_open)
    @patch("factory.util.utils.download_file")
    def test_create_cve_report(self, mock_download_file: Mock, mock_open: Mock) -> None:
        mock_container = MockContainer()
        mock_container.id = "mock_container_id"
        mock_download_file.return_value = True
        with (
            patch.object(self.ib, "check_build_id"),
            patch.object(self.ib, "run_podman") as mock_run_podman,
            patch.object(self.ib, "copy_files_to_container"),
            patch.object(self.ib, "execute_container_commands"),
        ):
            mock_run_podman.return_value.__enter__.return_value = mock_container
            self.ib.prod_img_name = "prod_img_name"
            img_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
            self.ib.create_cve_report(img_config=img_config)
            mock_run_podman.assert_called_with(self.ib.prod_img_name, img_config=img_config)

    @pytest.mark.parametrize(
        "distro_codename, expected_scanned",
        [
            ("jammy", True),
            ("fake", False),
        ],
    )
    def test_process_scan(self, distro_codename: str, expected_scanned: bool) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        image_config.config.updated = True
        image_config.config.distro_codename = distro_codename
        with (
            patch.object(self.ib, "perform_scan") as mock_perform_scan,
            patch.object(self.ib, "process_scan_result") as mock_process_scan_result,
        ):
            self.ib.process_scan(image_config)
            assert mock_process_scan_result.called == expected_scanned
            assert mock_perform_scan.called == expected_scanned

    @patch("factory.update_images.ImageConfig")
    @patch("factory.update_images.ImageBuilder.check_build_id")
    @patch("factory.update_images.subprocess.check_output")
    def test_process_references(
        self, mock_check_output: Mock, mock_check_build_id: Mock, mock_img_config: Mock
    ) -> None:
        mock_img_config.return_value.get_buildversion.return_value = (
            "opencode_img_name:build_version_tag"
        )
        mock_img_config.return_value.get_targetversion.return_value = "opencode_img_name:target_tag"
        mock_img_config.return_value.get_targetversion_prod_ext.return_value = (
            "prod_name_ext:target_tag"
        )
        mock_img_config.return_value.get_buildversion_prod_ext.return_value = (
            "prod_name_ext:build_version_tag"
        )
        mock_img_config.return_value.get_src_image_name.return_value = (
            "src_name:build_version_tag_src"
        )
        with patch.object(self.ib, "run_podman") as mock_run_podman:
            self.ib.process_references(mock_img_config)
            assert mock_run_podman.called
        assert mock_check_output.called

    @pytest.mark.parametrize(
        "returncode, expect_result",
        [
            (0, True),
            (1, False),
        ],
    )
    @patch("factory.update_images.subprocess.run")
    def test_check_image_exists(
        self, mock_subprocess_run: Mock, returncode: int, expect_result: bool
    ) -> None:
        mock_subprocess_run.return_value.returncode = returncode
        result = self.ib.check_image_exists("someimage")
        assert result is expect_result

    @pytest.mark.parametrize(
        "exception",
        [
            FileNotFoundError,
            PermissionError,
            OSError,
            subprocess.SubprocessError,
        ],
    )
    def test_check_image_exists_returns_false_on_exception(self, exception: Exception) -> None:
        with patch("factory.update_images.subprocess.run", side_effect=exception):
            assert not self.ib.check_image_exists("someimage")

    @pytest.mark.parametrize(
        "stages, types, expected_sum",
        [
            # test with all conditions True
            (["src"], {"src"}, 1),
            # test if one condition is not True
            (["src"], {"production"}, 0),
        ],
    )
    @patch("factory.update_images.subprocess.run")
    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    @patch("factory.update_images.log.info")
    def test_check_published_tags_checks_src(
        self,
        mock_log: Mock,
        mock_get_tags: Mock,
        mock_subprocess_run: Mock,
        stages: list[str],
        types: set[str],
        expected_sum: int,
    ) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        app_version = "1.2.3"
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            f"{app_version}-jammy-production-b{build_version}" for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        mock_subprocess_run.return_value.returncode = 0

        self.ib.stages = stages
        self.ib.types = types
        self.ib.check_published_tags(image_config)
        num_src_version = sum("src_version" in args[0] for args, _ in mock_log.call_args_list)
        assert num_src_version == expected_sum

    @pytest.mark.parametrize(
        "disable_buildversions, expect_num_build",
        [
            (True, 0),  # test with all conditions True
            (False, 2),  # test if one condition is not True
        ],
    )
    @patch("factory.update_images.subprocess.run")
    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    @patch("factory.update_images.log.info")
    def test_check_published_tags_checks_build_version(
        self,
        mock_log: Mock,
        mock_get_tags: Mock,
        mock_subprocess_run: Mock,
        disable_buildversions: bool,
        expect_num_build: int,
    ) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        app_version = "1.2.3"
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            f"{app_version}-jammy-production-b{build_version}" for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        mock_subprocess_run.return_value.returncode = 0

        image_config.config.updated = True
        self.ib.disable_external_push = False
        self.ib.disable_buildversions = disable_buildversions
        self.ib.check_published_tags(image_config)
        num_build_version = sum("build_version" in args[0] for args, _ in mock_log.call_args_list)
        assert num_build_version == expect_num_build

    @patch("factory.update_images.subprocess.run")
    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_check_published_tags_throws_exception(
        self, mock_get_tags: Mock, mock_subprocess_run: Mock
    ) -> None:
        app_version = "1.2.3"
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "{}-jammy-production-b{}".format(app_version, build_version)
            for build_version in build_versions
        ]  # NOSONAR
        mock_get_tags.return_value = tags_in_repo
        with pytest.raises(Exception):
            mock_subprocess_run.return_value.returncode = 1
            self.ib.check_published_tags(self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU])

    def test_copy_files_to_container_exception(self) -> None:
        container = cast(Container, MockContainer())

        with pytest.raises(FileNotFoundError):
            self.ib.copy_files_to_container(container, [("somefile", None)])

        with patch.object(container, "put_archive", return_value=False), pytest.raises(Exception):
            self.ib.copy_files_to_container(container, [("README.md", None)])

    def test_copy_files_to_container(self) -> None:
        container = MagicMock()
        container.id = "someid"
        src_file = "tests/test_data/images.yaml"
        dst_file = "/home/tests/test_data/images.yaml"
        self.ib.copy_files_to_container(container, [(src_file, dst_file)])
        container.put_archive.assert_called_once()

    @patch("factory.update_images.ImageBuilder.copy_files_to_container")
    def test_copy_image_license_to_container(self, mock_copy_files_to_container: Mock) -> None:
        container = cast(Container, MockContainer())
        self.ib.copy_image_license_to_container(
            self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU], container
        )
        assert mock_copy_files_to_container.called

    def test_copy_files_to_container_empty_list(self) -> None:
        """Test `copy_files_to_container` with an empty list."""
        container = MagicMock()
        self.ib.copy_files_to_container(container, [])
        container.put_archive.assert_not_called()

    def test_copy_files_to_container_single_file(self) -> None:
        """Test `copy_files_to_container` with a single file."""
        container = MagicMock()
        src_file = "tests/test_data/images.yaml"
        dst_file = "/home/tests/test_data/images.yaml"

        # Patch tarfile and tempfile to simulate tarball creation
        with (
            patch("tarfile.open") as mock_tarfile,
            patch("tempfile.NamedTemporaryFile") as mock_tmpfile,
        ):
            mock_tmpfile.return_value.__enter__.return_value = BytesIO()
            mock_tarfile.return_value.__enter__.return_value.add = MagicMock()

            self.ib.copy_files_to_container(container, [(src_file, dst_file)])

            mock_tarfile.return_value.__enter__.return_value.add.assert_called_once_with(
                f"{self.ib.project_dir}/{src_file}", arcname="home/tests/test_data/images.yaml"
            )
            container.put_archive.assert_called_once()

    def test_copy_files_to_container_multiple_files(self) -> None:
        """Test `copy_files_to_container` with multiple files."""
        container = MagicMock()
        files_to_copy: list[tuple[str, str | None]] = [
            ("tests/test_data/images.yaml", "/home/tests/test_data/images.yaml"),
            ("tests/test_data/config.yaml", None),  # Include None as a valid dst_file
        ]

        # Patch tarfile and tempfile
        with (
            patch("tarfile.open") as mock_tarfile,
            patch("tempfile.NamedTemporaryFile") as mock_tmpfile,
        ):
            mock_tmpfile.return_value.__enter__.return_value = BytesIO()
            mock_tarfile.return_value.__enter__.return_value.add = MagicMock()

            self.ib.copy_files_to_container(container, files_to_copy)

            assert mock_tarfile.return_value.__enter__.return_value.add.call_count == len(
                files_to_copy
            )
            container.put_archive.assert_called_once()

    def test_copy_files_to_container_dst_none(self) -> None:
        """Test `copy_files_to_container` with dst_file as None."""
        container = MagicMock()
        src_file = "tests/test_data/images.yaml"
        dst_file = None

        # Patch tarfile and tempfile
        with (
            patch("tarfile.open") as mock_tarfile,
            patch("tempfile.NamedTemporaryFile") as mock_tmpfile,
        ):
            mock_tmpfile.return_value.__enter__.return_value = BytesIO()
            mock_tarfile.return_value.__enter__.return_value.add = MagicMock()

            self.ib.copy_files_to_container(container, [(src_file, dst_file)])

            mock_tarfile.return_value.__enter__.return_value.add.assert_called_once_with(
                f"{self.ib.project_dir}/{src_file}", arcname="tmp/tests/test_data/images.yaml"
            )
            container.put_archive.assert_called_once()

    def test_copy_files_to_container_put_archive_fails(self) -> None:
        """Test `copy_files_to_container` when put_archive fails."""
        container = MagicMock()
        container.put_archive.return_value = False  # Simulate failure
        src_file = "tests/test_data/images.yaml"
        dst_file = "/home/tests/test_data/images.yaml"

        with (
            patch("tempfile.NamedTemporaryFile") as mock_tmpfile,
            patch("tarfile.open") as mock_tarfile,
        ):
            mock_tmpfile_instance = MagicMock()
            mock_tmpfile.return_value.__enter__.return_value = mock_tmpfile_instance
            mock_tarfile_instance = MagicMock()
            mock_tarfile.return_value.__enter__.return_value = mock_tarfile_instance

            with pytest.raises(Exception, match=f"Put file {src_file} to container .* failed"):
                self.ib.copy_files_to_container(container, [(src_file, dst_file)])

            # Ensure put_archive was called
            container.put_archive.assert_called_once_with("/", mock_tmpfile_instance.read())

    def test_copy_files_to_container_missing_file(self) -> None:
        """Test `copy_files_to_container` with a missing source file."""
        container = MagicMock()
        src_file = "non_existent_file.yaml"
        dst_file = "/home/tests/non_existent_file.yaml"

        with patch("os.path.exists", return_value=False):
            with pytest.raises(FileNotFoundError, match=f"No such file or directory: .*{src_file}"):
                self.ib.copy_files_to_container(container, [(src_file, dst_file)])

        # Ensure put_archive is not called
        container.put_archive.assert_not_called()

    @patch("factory.update_images.sys.exit")
    def test_check_mandatory_image_definition_properties(self, mock_sys_exit: Mock) -> None:
        image_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]
        self.ib.check_mandatory_image_definition_properties(image_config)

        image_config.config.image_license = ""
        self.ib.check_mandatory_image_definition_properties(image_config)
        assert mock_sys_exit.called

    @pytest.mark.parametrize(
        "image_config, tag, expected_result",
        [
            (MOCK_IMAGECONFIG_SYNAPSE, "1.0.0", True),
            (MOCK_IMAGECONFIG_SYNAPSE, "0.0.1", False),
            # ubuntu base ist not part of test data (images.yaml)
            (MOCK_IMAGECONFIG_UBUNTU, "0.0.1", False),
        ],
    )
    def test_is_stable_version(self, image_config: str, tag: str, expected_result: bool) -> None:
        img_config = self.ib.image_configs[image_config]
        # add stable information for test, because it is not set in test data
        self.ib.version_bot_data["images"]["ubuntu"]["jammy"]["synapse"]["stable"] = "1.0.0"

        is_stable = self.ib.is_stable_version(img_config, tag)
        assert is_stable is expected_result

    @pytest.mark.parametrize(
        "image_config, tag, expected_result",
        [
            (MOCK_IMAGECONFIG_SYNAPSE, "1.118.0-jammy", True),
            (MOCK_IMAGECONFIG_SYNAPSE, "1.105.1-jammy", False),
            (MOCK_IMAGECONFIG_SYNAPSE, "1.104.1-jammy", False),
            # ubuntu base ist not part of test data (images.yaml)
            (MOCK_IMAGECONFIG_UBUNTU, "0.0.1-jammy", False),
        ],
    )
    def test_is_latest_version(self, image_config: str, tag: str, expected_result: bool) -> None:
        img_config = self.ib.image_configs[image_config]

        is_latest = self.ib.is_latest_version(img_config, tag)
        assert is_latest is expected_result

    @pytest.mark.parametrize(
        "data, tag, expected_result",
        [
            # versions with 2 digits
            ([{"version": "3.11"}, {"version": "3.12"}, {"version": "4.12"}], "3.12-jammy", True),
            ([{"version": "3.11"}, {"version": "3.12"}, {"version": "4.12"}], "3.12.0-jammy", True),
            ([{"version": "3.11"}, {"version": "3.12"}, {"version": "4.12"}], "3.11-jammy", False),
            # versions with 3 digits
            (
                [{"version": "3.12.1"}, {"version": "3.12.2"}, {"version": "3.13.0"}],
                "3.12.2-jammy",
                True,
            ),
            (
                [{"version": "3.12.1"}, {"version": "3.12.2"}, {"version": "3.13"}],
                "3.12.2-jammy",
                True,
            ),
            (
                [{"version": "3.12.1"}, {"version": "3.12.2"}, {"version": "3.13.0"}],
                "3.12.1-jammy",
                False,
            ),
            # version with pre-release
            (
                [
                    {"version": "3.12.1-pre10"},
                    {"version": "3.12.2-pre1"},
                    {"version": "3.13.0-pre2"},
                ],
                "3.12.2-pre1-jammy",
                True,
            ),
            (
                [{"version": "3.12.1-pre10"}, {"version": "3.12.2-pre1"}, {"version": "3.13-pre2"}],
                "3.12.2-pre1-jammy",
                True,
            ),
            (
                [
                    {"version": "3.12.1-pre10"},
                    {"version": "3.12.2-pre1"},
                    {"version": "3.13.0-pre2"},
                ],
                "3.12.1-pre10-jammy",
                False,
            ),
            (
                [
                    {"version": "3.11.1-pre1"},
                    {"version": "3.11.1-pre2"},
                    {"version": "3.11.1-pre5"},
                ],
                "3.11.1-pre5-jammy",
                True,
            ),
            (
                [
                    {"version": "3.11.1-pre1"},
                    {"version": "3.11.1-pre2"},
                    {"version": "3.11.1-pre5"},
                ],
                "3.11.1-pre2-jammy",
                False,
            ),
            (
                [
                    {"version": "3.11.1-pre1"},
                    {"version": "3.11.2-pre20"},
                    {"version": "3.11.3-pre5"},
                ],
                "3.11.2-pre2-jammy",
                False,
            ),
        ],
    )
    def test_is_latest_in_range(self, data: dict, tag: str, expected_result: bool) -> None:
        self.ib.version_bot_data = {"images": {"ubuntu": {"jammy": {"ubuntu": {"versions": data}}}}}
        img_config = self.ib.image_configs[MOCK_IMAGECONFIG_UBUNTU]

        assert self.ib.is_latest_in_range(img_config, tag) is expected_result

    @pytest.mark.parametrize(
        "image_config, is_bot_version",
        [
            (MOCK_IMAGECONFIG_SYNAPSE, True),
            (MOCK_IMAGECONFIG_UBUNTU, False),
        ],
    )
    def test_is_bot_version(self, image_config: str, is_bot_version: bool) -> None:
        img_config = self.ib.image_configs[image_config]
        assert self.ib.is_bot_version(img_config) is is_bot_version

    def test_load_config(self) -> None:
        test_registry = "registry.example.org"
        # Reset the config and set default for ib.env
        self.ib.config = {}
        self.ib.env = {"registry": "registry.example.com"}

        self.ib.load_config(config_file="tests/test_data/config_rendered.yaml")
        assert self.ib.config["registry"] == test_registry
        assert self.ib.env["registry"] == test_registry

    def test_load_image_definitions(self) -> None:
        """
        Test that both configuration lists are merged and
        duplicated "latest" versions are removed
        """
        self.ib.image_configs = {}
        # create dummy ImageConfigs, only the values are important: distro_codename and basename()
        # name a reserved parameter from MagicMock and needs to set separate
        ubuntu_latest_jammy = MagicMock(basename=lambda: "ubuntu", config=MagicMock())
        ubuntu_latest_jammy.config.configure_mock(
            name="ubuntu:latest-jammy", distro_codename="jammy", depends=[]
        )

        synapse_latest_jammy = MagicMock(basename=lambda: "synapse", config=MagicMock())
        synapse_latest_jammy.config.configure_mock(
            name="synapse:latest-jammy", distro_codename="jammy", depends=["python:3.12"]
        )

        synapse_118_jammy = MagicMock(basename=lambda: "synapse", config=MagicMock())
        synapse_118_jammy.config.configure_mock(
            name="synapse:1.118.0-jammy", distro_codename="jammy", depends=["python:3.12"]
        )

        synapse_latest_bookworm = MagicMock(basename=lambda: "synapse", config=MagicMock())
        synapse_latest_bookworm.config.configure_mock(
            name="synapse:latest-bookworm", distro_codename="bookworm", depends=["python:3.12"]
        )

        with (
            patch(
                "factory.update_images.ImageBuilder.get_image_configs_from_directory",
                return_value={
                    "ubuntu:latest-jammy": ubuntu_latest_jammy,
                    "synapse:latest-jammy": synapse_latest_jammy,
                    "synapse:latest-bookworm": synapse_latest_bookworm,
                },
            ),
            patch(
                "factory.update_images.ImageBuilder.get_image_configs_from_version_bot",
                return_value={"synapse:1.118.0-jammy": synapse_118_jammy},
            ),
        ):
            self.ib.load_image_definitions()

            # tests that:
            #   both dictonaries are merged
            #   "synapse:latest-jammy" was deleted because there is "synapse:1.118.0-jammy" from version bot
            #   "synapse:latest-bookworm" was not deleted because there is no bookworm synapse from version bot
            assert self.ib.image_configs == {
                "ubuntu:latest-jammy": ubuntu_latest_jammy,
                "synapse:1.118.0-jammy": synapse_118_jammy,
                "synapse:latest-bookworm": synapse_latest_bookworm,
            }
            assert self.ib.sorted_names == [
                "ubuntu:latest-jammy",
                "python:3.12",
                "synapse:latest-bookworm",
                "synapse:1.118.0-jammy",
            ]

    def test_create_image_config_from_dir(self) -> None:
        # values that set from environment variables of the test
        # and therefore have to be harmonised with the static test files
        self.ib.config["harbor_api_url"] = "https://registry.example.org/api/v2.0"
        self.ib.config["registry"] = "registry.example.org"
        self.ib.config["registry_url"] = "https://registry.example.org"
        self.ib.config["registry_user"] = "someuser"
        self.ib.config["registry_password"] = "somepassword"
        self.ib.config["project"] = "someproject"

        image_config = self.ib.create_image_config_from_dir("base", "ubuntu", "jammy")
        assert image_config  # may not be None
        expected_data = image_config_ubuntu()

        # check if image_config.config contains expected_data (without directory and tags)
        expected_data.config.directory = ""
        image_config.config.directory = ""
        # we dont know the CI_COMMIT_TAG in rendered_data of the main branch in advance
        # (see the tag property in config.yaml)
        expected_data.config.tags = []
        image_config.config.tags = []
        assert image_config.config == expected_data.config

    def test_create_image_config_from_dir_not_found(self) -> None:
        image_config = self.ib.create_image_config_from_dir("base", "ubuntu", "fake")
        assert image_config is None

    def _patch_image_paths(self, path: str = "tests/test_data/template") -> None:
        for image in self.ib.image_configs:
            self.ib.image_configs[image].config.directory = path

    def test_execute_container_commands_success(self) -> None:
        """Test `execute_container_commands` with successful commands."""
        container: MagicMock = MagicMock()
        commands: list[list[str]] = [["echo", "Hello"], ["ls", "/"]]
        # Mock `exec_run` to simulate successful command execution
        container.exec_run.side_effect = [
            (0, b"Hello\n"),  # Success with bytes output
            (0, b"bin\netc\nhome\n"),  # Success with bytes output
        ]
        # Call the method
        self.ib.execute_container_commands(container, commands)
        # Assert `exec_run` was called for each command
        assert container.exec_run.call_count == len(commands)
        container.exec_run.assert_any_call(["echo", "Hello"], user="root", tty=True)
        container.exec_run.assert_any_call(["ls", "/"], user="root", tty=True)

    def test_execute_container_commands_failed_command(self) -> None:
        """Test `execute_container_commands` when a command fails."""
        container: MagicMock = MagicMock()
        commands: list[list[str]] = [["invalid_command"]]
        # Mock `exec_run` to simulate command failure
        container.exec_run.return_value = (1, b"Command not found\n")
        # Assert exception is raised
        with pytest.raises(Exception, match="Run command .* failed."):
            self.ib.execute_container_commands(container, commands)
        # Assert `exec_run` was called
        container.exec_run.assert_called_once_with(["invalid_command"], user="root", tty=True)

    def test_execute_container_commands_unexpected_output_type(self) -> None:
        """Test `execute_container_commands` with unexpected output type."""
        container: MagicMock = MagicMock()
        commands: list[list[str]] = [["echo", "Hello"]]
        # Mock `exec_run` to simulate unexpected output type
        container.exec_run.return_value = (0, {"unexpected": "output"})
        # Call the method and ensure no exception for unexpected output
        self.ib.execute_container_commands(container, commands)
        # Assert log warning was called for unexpected output
        container.exec_run.assert_called_once_with(["echo", "Hello"], user="root", tty=True)

    def test_execute_container_commands_mixed_outputs(self) -> None:
        """Test `execute_container_commands` with mixed output types."""
        container: MagicMock = MagicMock()
        commands: list[list[str]] = [["echo", "Hello"], ["ls", "/"]]
        # Mock `exec_run` to return mixed outputs
        container.exec_run.side_effect = [
            (0, (b"Hello\n", b"stderr")),  # Tuple output
            (0, "bin\netc\nhome\n"),  # String output
        ]
        # Call the method
        self.ib.execute_container_commands(container, commands)
        # Assert `exec_run` was called for each command
        assert container.exec_run.call_count == len(commands)
        container.exec_run.assert_any_call(["echo", "Hello"], user="root", tty=True)
        container.exec_run.assert_any_call(["ls", "/"], user="root", tty=True)

    def test_run_cve_agent_success(self) -> None:
        """Test `run_cve_agent` with successful execution."""
        container: MagicMock = MagicMock()

        # Mock `exec_run` to simulate successful execution
        container.exec_run.return_value = (0, b"Agent executed successfully\n")

        # Call the method
        result = self.ib.run_cve_agent(container)

        # Assertions
        assert result == "Agent executed successfully\n"
        container.exec_run.assert_called_once_with(
            ["sh", "-c", "cd /tmp && python3.10 -m factory.cve.cve_agent"],
            user="root",
            tty=True,
        )

    def test_run_cve_agent_failure(self) -> None:
        """Test `run_cve_agent` when execution fails."""
        container: MagicMock = MagicMock()

        # Mock `exec_run` to simulate failure
        container.exec_run.return_value = (1, b"Error occurred\n")

        # Call the method
        result = self.ib.run_cve_agent(container)

        # Assertions
        assert result == "Error occurred\n"
        container.exec_run.assert_called_once_with(
            ["sh", "-c", "cd /tmp && python3.10 -m factory.cve.cve_agent"],
            user="root",
            tty=True,
        )

    def test_run_cve_agent_unexpected_output_type(self) -> None:
        """Test `run_cve_agent` with unexpected output type."""
        container: MagicMock = MagicMock()

        # Mock `exec_run` to simulate unexpected output type
        container.exec_run.return_value = (0, {"unexpected": "output"})

        # Call the method
        result = self.ib.run_cve_agent(container)

        # Assertions
        assert result == "Unexpected output type: <class 'dict'>"
        container.exec_run.assert_called_once_with(
            ["sh", "-c", "cd /tmp && python3.10 -m factory.cve.cve_agent"],
            user="root",
            tty=True,
        )


class TestImageBuilderUnpreparedUnit:

    @pytest.mark.parametrize(
        "environ, log_level",
        [
            ({"DEBUG": "0"}, logging.INFO),
            ({"DEBUG": "1"}, logging.DEBUG),
        ],
    )
    def test_log_level(self, environ: dict[str, str], log_level: int) -> None:
        with patch.dict("os.environ", environ):
            create_ib()
        assert logging.getLogger("cf_logger").level == log_level

    @pytest.mark.parametrize(
        "develop_env, expected_verify",
        [
            ("0", True),
            ("1", False),
        ],
    )
    def test_develop_mode(self, develop_env: str, expected_verify: bool) -> None:
        with patch.dict("os.environ", {"DEVELOP": develop_env}):
            image_builder = create_ib()
            img_conf = image_builder.image_configs[MOCK_IMAGECONFIG_UBUNTU]
            assert img_conf.verify is expected_verify
