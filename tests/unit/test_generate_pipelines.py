# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import sys
from unittest.mock import MagicMock, Mock, mock_open, patch

import pytest
import yaml

import factory.generate_pipelines as generate_pipelines
from factory.generate_pipelines import generator
from factory.update_images import ImageBuilder

logging.basicConfig(level=logging.DEBUG)

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


def is_valid_yaml(file_path: str) -> bool:
    try:
        with open(file_path, "r") as file:
            yaml.safe_load(file)
        return True
    except yaml.YAMLError:
        return False
    except FileNotFoundError:
        print("File not found.")
        return False


class TestGeneratePipelinesUnit:
    def test_main_generate_pipelines_creates_ci_file(self, ib: ImageBuilder) -> None:
        filename = "pipelines-gitlab-ci.yml"
        os.chdir(PROJECT_DIR)
        with patch("factory.generate_pipelines.ImageBuilder", return_value=ib):
            generate_pipelines.main()
        assert os.path.exists(filename)
        assert is_valid_yaml(os.path.join(PROJECT_DIR, filename))

    def test_parse_arguments_default_arguments(self) -> None:
        sys.argv = ["program_name"]
        args, unknown = generate_pipelines.parse_arguments()
        assert args.config == "config.yaml"
        assert not args.keep
        assert args.stages is None
        assert args.images is None
        assert not args.internal_scanner
        assert len(unknown) == 0

    def test_parse_arguments_custom_config(self) -> None:
        sys.argv = ["program_name", "--config", "custom.yaml"]
        args, _ = generate_pipelines.parse_arguments()
        assert args.config == "custom.yaml"

    @pytest.mark.parametrize(
        "env_vars",
        [
            {
                "CI_COMMIT_MESSAGE": "Some feature. [all]",
                "CI_COMMIT_BRANCH": "feature/some-feature",
            },
            {"BUILD_ALL": "1", "CI_COMMIT_BRANCH": "feature/some-feature"},
            {"CI_COMMIT_BRANCH": "main"},
        ],
    )
    @patch("builtins.open", new_callable=mock_open)
    def test_generator_build_all(
        self, mock_open: Mock, env_vars: dict[str, str], mock_git_repo: Mock, ib: ImageBuilder
    ) -> None:
        file_obj = mock_open.return_value
        file_obj.write.return_value = MagicMock()
        with patch.dict("os.environ", env_vars):
            changed_images = ib.retrieve_changed_images()
            generator(ib.image_configs, [], changed_images, None)
            expected_call_count = len(ib.image_configs) + 1
            assert file_obj.write.call_count == expected_call_count

    @patch.dict(
        "os.environ",
        {
            "CI_COMMIT_MESSAGE": "Some feature.",
            "CI_COMMIT_BRANCH": "feature/some-feature",
            "BUILD_ALL": "0",
        },
    )
    @patch("builtins.open", new_callable=mock_open)
    def test_generator_build_specified_on_feature_branch(
        self, mock_open: Mock, mock_git_repo: Mock, ib: ImageBuilder
    ) -> None:
        changed_images = ib.retrieve_changed_images()
        file_obj = mock_open.return_value
        file_obj.write.return_value = MagicMock()
        generator(ib.image_configs, ["ubuntu-jammy", "nginx:latest-jammy"], changed_images, None)
        expected_call_count = 2
        assert file_obj.write.call_count == expected_call_count
