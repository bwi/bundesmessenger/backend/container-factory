# Copyright 2025 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from unittest.mock import MagicMock, Mock, patch

import pytest
from podman.errors.exceptions import APIError

from factory.podman_api import BuilderPodmanAPI
from factory.util.exceptions import PushError, TransientError


class TestBuilderPodmanAPIUnit:

    podman_api: BuilderPodmanAPI

    @pytest.fixture(autouse=True)
    def before_test(self) -> None:
        """Function to replace __init__."""

        self.podman_api = BuilderPodmanAPI()

    @patch("factory.podman_api.PodmanClient.login")
    def test_login_successful(self, mock_client_login: Mock) -> None:
        response = "ok"
        mock_client_login.return_value = response
        assert self.podman_api.login("someuser", "somepassword", "someregistry") == response
        assert mock_client_login.call_count == 1
        assert self.podman_api.auth_lookup == {"someregistry": ("someuser", "somepassword")}

    @patch("factory.podman_api.PodmanClient.login")
    @patch("factory.podman_api.log.exception")
    def test_login_retry_login(self, mock_log: Mock, mock_client_login: Mock) -> None:
        attemps = 3
        mock_client_login.side_effect = APIError("POST operation failed")
        with (
            pytest.raises(TransientError),
            patch("factory.podman_api.PODMAN_LOGIN_DELAY", 0),
            patch("factory.podman_api.PODMAN_LOGIN_MAX_ATTEMPTS", attemps),
        ):
            self.podman_api.login("someuser", "somepassword", "someregistry")
        assert mock_log.called
        assert mock_client_login.call_count == attemps

    @patch("factory.podman_api.PodmanClient.login")
    @patch("factory.podman_api.log.exception")
    def test_login_abort_login(self, mock_log: Mock, mock_client_login: Mock) -> None:
        mock_client_login.side_effect = APIError("some other error")
        with pytest.raises(APIError):
            self.podman_api.login("someuser", "somepassword", "someregistry")
        assert mock_client_login.call_count == 1
        assert mock_log.called

    @patch("factory.podman_api.BuilderPodmanAPI.login")
    def test_push(self, mock_login: Mock) -> None:
        mock_login.return_value = "OK"
        mock_image = MagicMock()
        with patch.object(
            self.podman_api.podman_client.images, "push", return_value=["someline"]
        ) as mock_client_images:
            self.podman_api.push(mock_image, "", "", "")
            assert mock_client_images.call_count == 1

    @patch("factory.podman_api.BuilderPodmanAPI.login")
    def test_push_with_error(self, mock_login: Mock) -> None:
        attemps = 5
        mock_login.return_value = "OK"
        mock_image = MagicMock()
        with (
            patch("factory.podman_api.PUSH_MAX_ATTEMPTS", attemps),
            patch("factory.podman_api.PUSH_DELAY", 0),
            patch("factory.podman_api.log") as mock_log,
            pytest.raises(
                PushError,
                match=f"Pushing image to registry failed after {attemps} attempts.",
            ),
            patch.object(
                self.podman_api.podman_client.images, "push", side_effect=APIError("some error")
            ) as mock_client_images,
        ):
            self.podman_api.push(mock_image, "", "", "")

        assert mock_client_images.call_count == attemps
        assert mock_log.info.call_count == (attemps * 2) - 1  # missing last retry message
        assert mock_log.exception.call_count == attemps

    @pytest.mark.parametrize(
        "image_name, expected_result",
        [
            (
                "registry.example.com/repo/image",
                {"username": "fake_user", "password": "fake_password"},
            ),
            ("registry.example.org/repo/image", None),
        ],
    )
    def test_get_auth_config(self, image_name: str, expected_result: dict) -> None:
        self.podman_api.auth_lookup = {"registry.example.com": ("fake_user", "fake_password")}
        result = self.podman_api._get_auth_config(image_name)
        assert result == expected_result
