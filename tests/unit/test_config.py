# Copyright 2024 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from dataclasses import asdict
from typing import Any
from unittest.mock import Mock, patch

import pytest

from factory.config import ImageConfigAttributes
from factory.update_images import ImageConfig
from tests.conftest import image_config_ubuntu


@pytest.fixture
def config_object(required_attributes: dict[str, Any]) -> ImageConfigAttributes:
    return ImageConfigAttributes(**required_attributes)


@pytest.fixture
def required_attributes() -> dict[str, Any]:
    """All required positional arguments as dictionary."""
    return {
        "basename": "test-image",
        "name": "test-image:latest",
        "distro": "ubuntu",
        "distro_codename": "jammy",
        "directory": "/images/test-image",
        "harbor_api_url": "https://harbor.example.com",
        "project": "test-project",
        "registry": "registry.example.com",
        "registry_url": "https://registry.example.com",
        "registry_user": "user",
        "registry_password": "password",
        "rel_path": "path/to/image",
    }


class TestImageConfigAttributesUnit:

    def test_create_from_dict(self, required_attributes: dict[str, Any]) -> None:
        """Test creating the object from create_from_dict."""
        config = ImageConfigAttributes.create_from_dict(required_attributes)

        assert config.basename == "test-image"
        assert config.distro == "ubuntu"
        assert config.distro_codename == "jammy"
        # default values
        assert not config.updated
        assert config.image_license == ""
        assert config.base_images == []
        assert config.disable_external_push is None

    def test_create_from_dict_ignores_unknown_fields(
        self, required_attributes: dict[str, Any]
    ) -> None:
        """Tests that unknown fields are ignored."""
        data = required_attributes | {"unknown_field": "should be ignored"}
        config = ImageConfigAttributes.create_from_dict(data)

        assert config.basename == "test-image"
        assert not hasattr(config, "unknown_field")

    def test_update_existing_fields(self, config_object: ImageConfigAttributes) -> None:
        """Tests that existing fields are updated correctly."""
        update_data = {"distro": "debian", "app_version": "2.0.0", "updated": True}
        config_object.update(update_data)

        # not updated values
        assert config_object.basename == "test-image"
        assert config_object.distro_codename == "jammy"
        # updated values
        assert config_object.distro == "debian"
        assert config_object.app_version == "2.0.0"
        assert config_object.updated is True

    def test_update_does_not_add_new_fields(self, config_object: ImageConfigAttributes) -> None:
        """Tests that `update` does not add any new fields."""
        update_data = {"unknown_field": "this should not be added"}
        config_object.update(update_data)

        assert not hasattr(config_object, "unknown_field")

    def test_asdict(
        self, config_object: ImageConfigAttributes, required_attributes: dict[str, Any]
    ) -> None:
        """Tests that asdict returns all attributes correctly."""
        config_dict = asdict(config_object)

        assert required_attributes.items() <= config_dict.items()

    def test_is_hashable(self, config_object: ImageConfigAttributes) -> None:
        """Test that object is hashable. Expected that is not hashable at the moment."""
        with pytest.raises(TypeError):
            hash(config_object)


class TestImageConfigUnit:

    image_config: ImageConfig

    @pytest.fixture(autouse=True)
    def before_test(self) -> None:
        """Function to replace __init__."""

        self.image_config = image_config_ubuntu()

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_latest_build_version(self, mock_get_tags: Mock) -> None:
        app_version = "1.2.3"
        self.image_config.config.app_version = app_version
        latest_test = 5
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            f"{app_version}-jammy-production-b{build_version}" for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        latest_build_version = self.image_config._get_latest_build_version()
        assert latest_build_version == latest_test

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_latest_build_version_without_app_version(self, mock_get_tags: Mock) -> None:
        latest_test = 5
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "jammy-production-b{}".format(build_version) for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        latest_build_version = self.image_config._get_latest_build_version()
        assert latest_build_version == latest_test

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_new_build_version(self, mock_get_tags: Mock) -> None:
        app_version = "1.2.3"
        self.image_config.config.app_version = app_version
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "{}-jammy-production-b{}".format(app_version, build_version)
            for build_version in build_versions
        ]  # NOSONAR
        mock_get_tags.return_value = tags_in_repo
        new_build_version = self.image_config._get_new_build_version()
        assert new_build_version == latest_test + 1

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_new_build_version_returns_1_if_no_build_version_exists(
        self, mock_get_tags: Mock
    ) -> None:
        app_version = "1.2.3"
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "{}-jammy-production-{}".format(app_version, build_version)
            for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        new_build_version = self.image_config._get_new_build_version()
        assert new_build_version == 1

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_prefix_new_build_tag(self, mock_get_tags: Mock) -> None:
        app_version = "1.2.3"
        self.image_config.config.app_version = app_version
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            f"{app_version}-jammy-production-b{build_version}" for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        new_build_tag = self.image_config._prefix_new_build_tag()
        assert new_build_tag == "1.2.3-jammy-production-b11"

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_new_imagename_internal(self, mock_get_tags: Mock) -> None:
        app_version = "1.2.3"
        self.image_config.config.app_version = app_version
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "{}-jammy-production-b{}".format(app_version, build_version)
            for build_version in build_versions
        ]  # NOSONAR
        mock_get_tags.return_value = tags_in_repo
        new_build_tag = self.image_config.get_new_image_name()
        assert new_build_tag == (
            f"{self.image_config.config.registry}/{self.image_config.config.project}/"
            f"{self.image_config.config.basename}",
            f"{app_version}-jammy-production-b11",
        )

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_new_imagename(self, mock_get_tags: Mock) -> None:
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "jammy-production-b{}".format(build_version) for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        new_build_tag = self.image_config.get_new_image_name()
        assert new_build_tag == (
            f"{self.image_config.config.registry}/{self.image_config.config.project}/"
            f"{self.image_config.config.basename}",
            "jammy-production-b11",
        )

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_new_imagename_of_app_version_with_dash_b_num_pattern(
        self, mock_get_tags: Mock
    ) -> None:
        # this test checks for cases where the app_version contains by itself a dash-b-num pattern
        # such cases must not lead to an increment of the build_version based on the app_version pattern
        existing_build_version = 370
        mock_get_tags.return_value = [f"jammy-production-b{existing_build_version}"]
        new_build_tag = self.image_config.get_new_image_name()
        assert new_build_tag == (
            f"{self.image_config.config.registry}/{self.image_config.config.project}/"
            f"{self.image_config.config.basename}",
            f"jammy-production-b{existing_build_version + 1}",
        )

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_prefix_new_build_tag_without_app_version_return_correct_tag(
        self, mock_get_tags: Mock
    ) -> None:
        latest_test = 10
        build_versions = range(1, latest_test + 1)
        tags_in_repo = [
            "jammy-production-b{}".format(build_version) for build_version in build_versions
        ]
        mock_get_tags.return_value = tags_in_repo
        new_build_tag = self.image_config._prefix_new_build_tag()
        assert new_build_tag == "jammy-production-b11"

    @pytest.mark.parametrize(
        "with_project, with_registry, expected",
        [
            (True, True, "dummy.example.com/project/image"),
            (False, False, "image"),
        ],
    )
    def test_add_project_and_registry(
        self, with_project: bool, with_registry: bool, expected: str
    ) -> None:
        self.image_config.config.registry = "dummy.example.com"
        self.image_config.config.project = "project"
        image_name = self.image_config._add_project_and_registry(
            "image", with_project=with_project, with_registry=with_registry
        )
        assert image_name == expected

    @pytest.mark.parametrize(
        "with_project, with_registry",
        [
            (True, True),
            (False, False),
        ],
    )
    def test_basename(self, with_project: bool, with_registry: bool) -> None:
        basename = self.image_config.basename(
            with_project=with_project, with_registry=with_registry
        )
        assert (self.image_config.config.project in basename) == with_project
        assert (self.image_config.config.registry in basename) == with_registry

    @pytest.mark.parametrize(
        "with_project, with_registry",
        [
            (True, True),
            (False, False),
        ],
    )
    def test_name_with_tag(self, with_project: bool, with_registry: bool) -> None:
        name_with_tag = self.image_config.name_with_tag(
            with_project=with_project, with_registry=with_registry
        )
        assert (self.image_config.config.project in name_with_tag) == with_project
        assert (self.image_config.config.registry in name_with_tag) == with_registry

    @pytest.mark.parametrize(
        "with_project, with_registry",
        [
            (True, True),
            (False, False),
        ],
    )
    def test_name_and_tag(self, with_project: bool, with_registry: bool) -> None:
        name, _ = self.image_config.name_and_tag(
            with_project=with_project, with_registry=with_registry
        )
        assert (self.image_config.config.project in name) == with_project
        assert (self.image_config.config.registry in name) == with_registry

    def test_tag(self) -> None:
        self.image_config.config.name = "image:latest-tag"
        tag = self.image_config.tag()
        assert tag == "latest-tag"

    def test_distro_codename_in_tag_tag(self) -> None:
        tag = self.image_config.tag()
        assert self.image_config.config.distro_codename in tag

    def test_get_safe_version(self) -> None:
        self.image_config.config.app_version = "1.0.0"
        safe_version = self.image_config.get_safe_version()
        assert safe_version == "1.0.0"

        self.image_config.config.app_version = "2.0.0"
        self.image_config.config.safe_version = "3.0.0"
        safe_version = self.image_config.get_safe_version()
        assert safe_version == "3.0.0"

    @patch("factory.update_images.ImageConfig._get_new_build_version")
    def test_get_image_name_opencode_returns_buildtag(
        self, mock_get_new_build_version: Mock
    ) -> None:
        self.image_config.config.app_version = "2.0.0"
        self.image_config.config.safe_version = "3.0.0"
        mock_get_new_build_version.return_value = "10"
        _, tag = self.image_config.get_image_name_opencode()
        assert "-b" in tag

    @patch("factory.update_images.ImageConfig._get_new_build_version")
    def test_get_image_name_opencode_returns_cached_value(
        self, mock_get_new_build_version: Mock
    ) -> None:
        self.image_config.config.app_version = "2.0.0"
        self.image_config.config.safe_version = "3.0.0"
        mock_get_new_build_version.return_value = "10"
        name, tag = self.image_config.get_image_name_opencode()
        name_cached, tag_cached = self.image_config.get_image_name_opencode(cached=True)
        assert name == name_cached
        assert tag == tag_cached

    @patch("factory.update_images.ImageConfig.get_image_name_opencode")
    def test_get_build_version(self, mock_img_config: Mock) -> None:
        mock_img_config.return_value = (
            "opencode_img_name",
            "build_version_tag",
        )
        buildversion = self.image_config.get_buildversion()
        buildversion_cached = self.image_config.get_buildversion(cached=True)
        assert buildversion == buildversion_cached

    @patch("factory.update_images.ImageConfig.get_image_name_opencode")
    def test_get_build_version_prod_ext(self, mock_img_config: Mock) -> None:
        mock_img_config.return_value = (
            "opencode_img_name",
            "build_version_tag",
        )
        buildversion = self.image_config.get_buildversion_prod_ext()
        buildversion_cached = self.image_config.get_buildversion_prod_ext(cached=True)
        assert buildversion == buildversion_cached

    @patch("factory.update_images.ImageConfig.get_tags_for_repository")
    def test_get_build_version_prod_int(self, mock_img_config: Mock) -> None:
        mock_img_config.return_value = (
            "opencode_img_name",
            "build_version_tag",
        )
        buildversion = self.image_config.get_buildversion_prod_int()
        buildversion_cached = self.image_config.get_buildversion_prod_int(cached=True)
        assert buildversion == buildversion_cached

    @patch("factory.update_images.ImageConfig.get_image_name_opencode")
    def test_get_src_image_name(self, mock_img_config: Mock) -> None:
        mock_img_config.return_value = (
            "opencode_img_name",
            "build_version_tag",
        )
        src_image_name = self.image_config.get_src_image_name()
        src_image_name_cached = self.image_config.get_src_image_name(cached=True)
        assert src_image_name == src_image_name_cached

    @patch("factory.update_images.ImageConfig._get_latest_build_version")
    def test_get_existing_build_version_returns_zero_when_no_buildversion_exists(
        self, mock_get_latest_build_version: Mock
    ) -> None:
        mock_get_latest_build_version.return_value = None
        build_version = self.image_config._get_existing_build_version()
        assert build_version == 0

    @patch("factory.update_images.ImageConfig._get_latest_build_version")
    def test_get_existing_build_version(self, mock_get_latest_build_version: Mock) -> None:
        existing_build_version = "123"
        mock_get_latest_build_version.return_value = existing_build_version
        build_version = self.image_config._get_existing_build_version()
        assert build_version == existing_build_version

    @patch("factory.update_images.ImageConfig._get_existing_build_version")
    def test_prefix_build_tag(self, mock_get_existing_build_version: Mock) -> None:
        existing_build_version = "123"
        mock_get_existing_build_version.return_value = existing_build_version
        build_tag = self.image_config._prefix_build_tag()
        assert existing_build_version in build_tag

    def test_get_full_path(self) -> None:
        result = self.image_config._get_full_path(with_registry=False)
        assert self.image_config.config.basename in result
        assert self.image_config.config.project in result

    @patch("factory.update_images.ImageConfig._get_latest_build_version")
    def test_get_build_tag(self, mock_get_latest_build_version: Mock) -> None:
        existing_build_version = "123"
        mock_get_latest_build_version.return_value = existing_build_version
        build_tag = self.image_config.get_build_tag()
        assert existing_build_version in build_tag

    def test_get_targetversion_prod_int(self) -> None:
        target_version = self.image_config.get_targetversion_prod_int()
        assert self.image_config.config.basename in target_version

    @patch("factory.update_images.requests.get")
    def test_get_buildversion_prod_int(self, mock_requests_get: Mock) -> None:
        build_version = self.image_config.get_buildversion_prod_int()
        assert self.image_config.config.basename in build_version

    def test_baseimage_name_with_tag(self) -> None:
        image_name = self.image_config.config.name
        remote_name = self.image_config.baseimage_name_with_tag()
        assert f"{image_name}-unhardened" in remote_name

    def _create_dynamic_mock(self, mock_requests_get: Mock, res_data: dict[str, Any]) -> None:
        first_response = Mock()
        first_response.json.return_value = res_data
        first_response.links = {
            "self": {"url": "https://example.com/self", "rel": "self"},
            "next": {"url": "https://example.com/next", "rel": "next"},
        }
        second_response = Mock()
        second_response.json.return_value = res_data
        second_response.links = {
            "self": {"url": "https://example.com/self", "rel": "self"},
            "next": {"url": "https://example.com/next", "rel": "next"},
        }
        third_response = Mock()
        third_response.json.return_value = res_data
        third_response.links = {
            "self": {"url": "https://example.com/self", "rel": "self"},
            "next": {"url": "https://example.com/next", "rel": "next"},
        }
        fourth_response = Mock()
        fourth_response.json.return_value = res_data
        fourth_response.links = {
            "self": {"url": "https://example.com/self", "rel": "self"},
        }
        responses = [first_response, second_response, third_response, fourth_response]
        mock_requests_get.side_effect = responses

    def test_is_hashable(self) -> None:
        """Test that object is hashable. It is needed for use in set()."""
        assert isinstance(hash(self.image_config), int)
