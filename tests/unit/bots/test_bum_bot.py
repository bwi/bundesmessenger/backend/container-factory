# Copyright 2025 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import date, datetime
from tempfile import NamedTemporaryFile
from unittest.mock import MagicMock, Mock, mock_open, patch

import pytest
import yaml

from factory.bots.bum_bot import BumBot


def create_temp_file(content: list[dict]) -> str:
    """Create temporary file and return the path."""
    with NamedTemporaryFile("w", delete=False) as f:
        yaml.dump(content, f)
        return f.name


@pytest.fixture
def version_file() -> str:
    content = [
        {
            "version": "1.1.0",
            "tag": "v1.1.0",
            "date": "2024-01-01",
            "images": [
                {"image": "registry.example.com/project/image1", "tag": "1.0.0"},
                {"image": "registry.example.com/project/image2", "tag": "2.1.3"},
                {"image": "registry.example.com/another/image3", "tag": "3.2.1"},
            ],
        }
    ]
    return create_temp_file(content)


@pytest.mark.parametrize("load_bot", ["BumBot"], indirect=True)
class TestBumBotUnit:

    bum_bot: BumBot

    @pytest.fixture(autouse=True)
    def before_test(self, load_bot: BumBot) -> None:
        """Function to replace __init__."""
        self.bum_bot = load_bot

    @patch.dict(
        "os.environ",
        {
            "BOT_HELM_CHART_REPO_USER": "testuser",
            "BOT_HELM_CHART_REPO_PASSWORD": "testpass",
            "HELMCHART_REPO": "https://example.com/helmchartrepo",
        },
    )
    @patch("factory.bots.bum_bot.BumBot.get_tags_from_version_file")
    @patch("factory.bots.bum_bot.Repo.clone_from")
    @patch("os.listdir")
    def test_get_used_tags_from_helm_chart_repo(
        self, mock_listdir: Mock, mock_clone_from: Mock, mock_get_tags_from_version_file: Mock
    ) -> None:
        mock_listdir.return_value = ["v1.0.0.yaml", "v2.0.0.yaml"]
        mock_get_tags_from_version_file.side_effect = [
            ["image1:v1.0.0", "image2:v1.0.0"],
            ["image1:v2.0.0", "image2:v2.0.0"],
        ]
        mock_clone_from.return_value = MagicMock()
        tags = self.bum_bot.get_used_tags_from_helm_chart_repo()
        expected_tags = ["image1:v1.0.0", "image2:v1.0.0", "image1:v2.0.0", "image2:v2.0.0"]
        assert tags == expected_tags, "Expected tags did not match the actual tags"

    @patch("yaml.dump")
    @patch("builtins.open", new_callable=mock_open)
    @patch(
        "factory.bots.bum_bot.BumBot.add_missing_source_tags_to_retention_list",
        return_value=[],
    )
    @patch("factory.bots.version_bot.VersionBot.remove_deprecated_versions", return_value=[])
    def test_update_retention_config(
        self,
        mock_remove_deprecated_versions: Mock,
        mock_add_missing_source_tags: Mock,
        mock_file_open: Mock,
        mock_yaml_dump: Mock,
    ) -> None:
        self.bum_bot.retention_config["retention_list"] = [
            "retention:v1.2.3",
            "retention:v1.2.3-production",
        ]
        mock_remove_deprecated_versions.side_effect = lambda x: x  # Pass through
        mock_add_missing_source_tags.side_effect = lambda x: x  # Pass through

        self.bum_bot.update_retention_config()
        # Verify internal functions were called as expected
        mock_remove_deprecated_versions.assert_called()
        mock_add_missing_source_tags.assert_called()
        mock_yaml_dump.assert_called_once()

    @pytest.mark.parametrize(
        "remove_dep_versions_return, expected_extract_call_count, expected_retrieve_call_count",
        [  # Scenario with non-empty list returned by remove_deprecated_versions
            (["tag1:1.0.0", "tag2:1.0.0"], 2, 1),
            # Scenario with empty list returned by remove_deprecated_versions
            ([], 0, 0),
        ],
    )
    @patch("factory.bots.bum_bot.BumBot.write_versions_file")
    @patch("factory.bots.bum_bot.BumBot.retrieve_base_images", return_value=["base_image:tag"])
    @patch("factory.bots.bum_bot.BumBot.extract_version", return_value="1.0.0")
    @patch("factory.bots.bum_bot.BumBot.remove_deprecated_versions")
    @patch(
        "factory.bots.bum_bot.BumBot.get_used_tags_from_helm_chart_repo",
        return_value=["ubuntu:jammy-1.0.0"],
    )
    @patch(
        "factory.bots.bum_bot.BumBot.get_versions_file",
        return_value={
            "images": {
                "ubuntu": {
                    "jammy": {
                        "tag1": {"versions": [{"version": "0.9.0", "base_images": ["base1:0.9.0"]}]}
                    }
                }
            }
        },
    )
    def test_update_versions_file(
        self,
        mock_get_versions_file: Mock,
        mock_get_used_tags_from_helm_chart_repo: Mock,
        mock_remove_deprecated_versions: Mock,
        mock_extract_version: Mock,
        mock_retrieve_base_images: Mock,
        mock_write_versions_file: Mock,
        remove_dep_versions_return: list,
        expected_extract_call_count: int,
        expected_retrieve_call_count: int,
    ) -> None:
        mock_remove_deprecated_versions.return_value = remove_dep_versions_return
        self.bum_bot.update_versions_file()
        assert mock_get_versions_file.called
        assert mock_get_used_tags_from_helm_chart_repo.called
        assert mock_remove_deprecated_versions.called
        assert mock_extract_version.call_count == expected_extract_call_count
        assert mock_retrieve_base_images.call_count == expected_retrieve_call_count
        assert mock_write_versions_file.called

    @pytest.mark.parametrize(
        "mocked_today, expected",
        [
            (
                datetime(2023, 12, 1),
                ["image1:1.0.0", "image2:2.1.3", "image3:3.2.1"],
            ),  # Within support range
            (datetime(2026, 1, 1), []),  # Outside support range
        ],
    )
    def test_get_tags_from_version_file(
        self, version_file: str, mocked_today: date, expected: int
    ) -> None:
        # Patch only datetime.today and preserve strptime
        with patch("factory.bots.bum_bot.datetime", wraps=datetime) as mock_date:
            mock_date.today.return_value = mocked_today
            tags = self.bum_bot.get_tags_from_version_file(version_file)
            assert tags == expected, f"Expected {expected}, but got {tags}"

    @patch("factory.util.utils.requests.get")
    @patch("factory.bots.bum_bot.BumBot.update_versions_file")
    @patch("factory.bots.bum_bot.BumBot.update_retention_config")
    @patch("factory.bots.bum_bot.BumBot.get_used_tags_from_helm_chart_repo")
    def test_bot_pre_process(
        self,
        mock_get_used_tags_from_helm_chart_repo: Mock,
        mock_requests_get: Mock,
        mock_update_versions_file: Mock,
        mock_update_retention_config: Mock,
    ) -> None:
        with (
            patch.object(self.bum_bot, "index") as mock_index,
            patch.object(self.bum_bot, "version_repo") as mock_repo,
        ):
            mock_index.diff.return_value = True
            mock_repo.is_dirty.return_value = True
            self.bum_bot.retention_config["retention_list"] = list()
            self.bum_bot.pre_process()
            mock_index.commit.assert_called()

        with patch.object(self.bum_bot, "index") as mock_index:
            mock_index.diff.return_value = False
            self.bum_bot.pre_process()
            mock_index.commit.assert_not_called()
