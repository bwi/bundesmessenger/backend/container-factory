# Copyright 2024 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from tempfile import NamedTemporaryFile
from typing import Optional
from unittest.mock import MagicMock, Mock, call, mock_open, patch

import pytest
from semver import Version

from factory.bots.version_bot import VersionBot
from factory.util.version_fetcher import (
    GitLabVersionFetcher,
    GitVersionFetcher,
    NexusVersionFetcher,
)


@pytest.mark.parametrize("load_bot", ["VersionBot"], indirect=True)
class TestVersionBotUnit:

    version_bot: VersionBot

    @pytest.fixture(autouse=True)
    def before_test(self, load_bot: VersionBot) -> None:
        """Function to replace __init__."""
        self.version_bot = load_bot

    @pytest.mark.parametrize(
        "tag, expected",
        [
            ("v1.0.0", "1.0.0"),  # Standard version in a tag
            ("version 2.1.3", "2.1.3"),  # Version within text
            ("no version here", None),  # No version pattern
            ("v.3.3.3", "3.3.3"),  # Version with a dot right after v
            ("01.23.456", "01.23.456"),  # Version with leading zeros and more digits
            ("v1.0.0-beta", "1.0.0"),  # Version with pre-release info
            ("update_4.5.6", "4.5.6"),  # Version within an underscored string
            ("123", None),  # Not a version format
            ("v123..", None),  # Incorrect format
            ("v2.0.0+20200101", "2.0.0"),  # Version with build metadata
        ],
    )
    def test_extract_version(self, tag: str, expected: Optional[str]) -> None:
        assert (
            self.version_bot.extract_version(tag) == expected
        ), f"Expected {expected} for tag '{tag}'"

    @patch.dict(
        "os.environ",
        {
            "GIT_SERVER": "https://example.com",
            "PROJECT": "myproject",
            "BOT_USER": "botuser",
            "BOT_PASSWORD": "botpassword",
        },
    )
    @patch("git.Repo.clone_from")
    @patch("os.path.exists", return_value=False)
    def test_download_images_repo(self, mock_exists: Mock, mock_clone_from: Mock) -> None:
        self.version_bot.download_images_repo()
        # Verify that the Repo.clone_from method was called once.
        mock_clone_from.assert_called_once()
        # Check if the local path was checked for existence.
        mock_exists.assert_called_once()

    @pytest.mark.parametrize(
        "retention_list, expected",
        [
            (["image1:1.0.0", "image2:1.5.0", "image3:1.0.0"], ["image2:1.5.0", "image3:1.0.0"]),
            (["image1:2.1.0", "image2:1.4.9", "image3:2.0.0"], ["image1:2.1.0", "image3:2.0.0"]),
            (["image1:2.0.0", "image1:1.9.9", "image2:1.5.0"], ["image1:2.0.0", "image2:1.5.0"]),
            (["image2:1.6.0", "image2:1.4.0", "image4:0.1.0"], ["image2:1.6.0", "image4:0.1.0"]),
        ],
    )
    def test_remove_deprecated_versions(
        self, retention_list: list[str], expected: list[str]
    ) -> None:
        # Define RETENTION_MIN_VERSIONS within the test function
        RETENTION_MIN_VERSIONS = {
            "image1": "2.0.0",
            "image2": "1.5.0",
        }
        # Use patch.dict to mock the RETENTION_MIN_VERSIONS inside your function
        with patch.dict(
            self.version_bot.retention_config, {"min_versions": RETENTION_MIN_VERSIONS}
        ):
            assert self.version_bot.remove_deprecated_versions(retention_list) == expected

    @pytest.mark.parametrize(
        "retention_list, expected",
        [
            (["image1:1.0.0-production"], ["image1:1.0.0-production", "image1:1.0.0-src"]),
            (
                ["image2:2.0.0-src", "image2:2.0.0-production"],
                ["image2:2.0.0-src", "image2:2.0.0-production"],
            ),
            (
                ["image3:3.0.0-production", "image3:3.0.0-src"],
                ["image3:3.0.0-production", "image3:3.0.0-src"],
            ),
            (
                ["image4:4.0.0-production", "image5:5.0.0-production"],
                [
                    "image4:4.0.0-production",
                    "image5:5.0.0-production",
                    "image4:4.0.0-src",
                    "image5:5.0.0-src",
                ],
            ),
            ([], []),
            (["image6:6.0.0-src"], ["image6:6.0.0-src"]),
        ],
    )
    def test_add_missing_source_tags_to_retention_list(
        self, retention_list: list[str], expected: list[str]
    ) -> None:
        result = self.version_bot.add_missing_source_tags_to_retention_list(retention_list)
        assert result == expected, f"Expected {expected} but got {result}"

    @patch(
        "builtins.open",
        new_callable=mock_open,
        read_data="retention_list:\n  - image1:tag1\n  - image2:tag2",
    )
    @patch("yaml.safe_load", return_value={"retention_list": ["image1:tag1", "image2:tag2"]})
    def test_load_retention_config(self, mock_safe_load: Mock, mock_file_open: Mock) -> None:
        # Call the function with the default argument, expecting it to attempt cloning
        self.version_bot.load_retention_config()
        # Validate the retention list matches the expected mocked data
        assert self.version_bot.retention_config["retention_list"] == [
            "image1:tag1",
            "image2:tag2",
        ], "The returned retention list does not match the expected values"

    @patch("builtins.open", new_callable=mock_open, read_data="key: value")
    @patch("yaml.safe_load", return_value={"key": "value"})
    def test_get_versions_file(self, mock_safe_load: MagicMock, mock_file_open: MagicMock) -> None:
        maintenance_dict = self.version_bot.get_versions_file()
        # Validate the maintenance_dict matches the expected mocked data
        assert maintenance_dict == {"key": "value"}

    def test_write_versions_file(self) -> None:
        test_data = {"key": "value", "key2": "value2"}

        with NamedTemporaryFile() as tmp_file:
            self.version_bot.versions_file_filename = tmp_file.name
            self.version_bot.write_versions_file(test_data)

            assert tmp_file.read() == b"key: value\nkey2: value2\n"

    # Test cases for both lower and higher version scenarios
    @pytest.mark.parametrize(
        "used_version, expected",
        [
            ("2.5.0", ["base1:2.0.0", "base2:2.0.0"]),
            ("1.5.0", ["base1:1.0.0", "base2:1.0.0"]),
            ("0.5.0", ["base1:1.0.0", "base2:1.0.0"]),
        ],
    )
    def test_retrieve_base_images(self, used_version: str, expected: list[str]) -> None:
        version_items = [
            {"version": "1.0.0", "base_images": ["base1:1.0.0", "base2:1.0.0"]},
            {"version": "2.0.0", "base_images": ["base1:2.0.0", "base2:2.0.0"]},
            {"version": "3.0.0", "base_images": ["base1:3.0.0", "base2:3.0.0"]},
        ]
        result = self.version_bot.retrieve_base_images(Version.parse(used_version), version_items)
        assert (
            result == expected
        ), f"Expected base images did not match. Got {result}, expected {expected}."

    @pytest.mark.parametrize(
        "data, expected_data",
        [
            (  # add newer latest
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
                {
                    "latest": "2.20.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.20.0"},
                    ],
                },
            ),
            (  # new latest is already the latest version, skip updating
                {
                    "latest": "2.20.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
                {
                    "latest": "2.20.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
        ],
    )
    def test_update_latest_version(
        self, data: dict[str, str | dict], expected_data: dict[str, str | dict]
    ) -> None:
        image = "bundesmessenger-web"
        self.version_bot.data["images"]["ubuntu"]["jammy"][image] = data
        test_latest = Version.parse("2.20.0")

        self.version_bot.update_latest_version("ubuntu", "jammy", image, test_latest)
        assert self.version_bot.data["images"]["ubuntu"]["jammy"][image] == expected_data

    @pytest.mark.parametrize(
        "data, expected_version_items, expected_versions",
        [
            (  # normal, no stable
                {
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
                [
                    {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                    {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                ],
                ["2.10.0", "2.18.0"],
            ),
            (  # normal, with stable
                {
                    "stable": "stable_version",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
                [
                    {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                    {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                ],
                ["2.10.0", "2.18.0"],
            ),
            ({}, [], []),  # no versions
            ({"versions": {}}, [], []),  # empty versions
        ],
    )
    @patch("factory.bots.version_bot.VersionBot.update_version")
    @patch("factory.bots.version_bot.VersionBot.remove_unused_version")
    def test_update_versions_no_delete(
        self,
        mock_remove_unused_version: Mock,
        mock_update_version: Mock,
        data: dict[str, str | dict],
        expected_version_items: list[dict],
        expected_versions: list[str],
    ) -> None:
        """Test where no version is deleted."""
        image = "bundesmessenger-web"
        self.version_bot.data["images"]["ubuntu"]["jammy"][image] = data
        expected_stable = data.get("stable")
        test_latest = Version.parse("2.18.0")
        test_tags = [Version.parse("2.20.0")]

        mock_remove_unused_version.return_value = False
        self.version_bot.update_versions("ubuntu", "jammy", image, test_latest, test_tags)

        for version_item in expected_version_items:
            assert (
                call(version_item, "ubuntu", "jammy", image, test_latest, expected_stable)
                in mock_remove_unused_version.mock_calls
            )

            assert (
                call(version_item, expected_versions, "ubuntu", "jammy", image, test_tags)
                in mock_update_version.mock_calls
            )

    @patch("factory.bots.version_bot.VersionBot.update_version")
    @patch("factory.bots.version_bot.VersionBot.remove_unused_version")
    def test_update_versions_with_delete(
        self,
        mock_remove_unused_version: Mock,
        mock_update_version: Mock,
    ) -> None:
        """Test where versions are deleted."""
        image = "bundesmessenger-web"
        self.version_bot.data["images"]["ubuntu"]["jammy"][image] = {
            "stable": "stable_version",
            "versions": [
                {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
            ],
        }
        expected_version_items = [
            {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
            {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
        ]
        test_latest = Version.parse("2.10.0")
        test_tags = [Version.parse("2.20.0")]

        mock_remove_unused_version.return_value = True
        self.version_bot.update_versions("ubuntu", "jammy", image, test_latest, test_tags)

        for version_item in expected_version_items:
            assert (
                call(version_item, "ubuntu", "jammy", image, test_latest, "stable_version")
                in mock_remove_unused_version.mock_calls
            )
        mock_update_version.assert_not_called()

    @pytest.mark.parametrize(
        "latest, stable, is_used_version, expected_write_file, expected_data",
        [
            (  # remove, is not used version and not latest and not stable
                "999.0.0",
                "stable_version",
                False,
                True,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [{"base_images": ["nginx:latest-jammy"], "version": "2.18.0"}],
                },
            ),
            (  # keep, is not used version, but latest
                "2.10.0",
                "stable_version",
                False,
                False,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
            (  # keep, is not used version, but stable
                "999.0.0",
                "2.10.0",
                False,
                False,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
            (
                # keep, is used version
                "999.0.0",
                "stable_version",
                True,
                False,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
        ],
    )
    @patch("factory.bots.version_bot.VersionBot.is_used_version")
    @patch("builtins.open")
    def test_remove_unused_version(
        self,
        mock_file_open: Mock,
        mock_is_used_version: Mock,
        latest: str,
        stable: str | None,
        is_used_version: bool,
        expected_write_file: bool,
        expected_data: dict[str, str | dict],
    ) -> None:
        image = "bundesmessenger-web"
        self.version_bot.data["images"]["ubuntu"]["jammy"][image] = {
            "latest": "2.18.0-jammy",
            "versions": [
                {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
            ],
        }
        version_item = self.version_bot.data["images"]["ubuntu"]["jammy"][image]["versions"][0]

        mock_is_used_version.return_value = is_used_version
        self.version_bot.remove_unused_version(
            version_item, "ubuntu", "jammy", image, Version.parse(latest), stable
        )
        assert self.version_bot.data["images"]["ubuntu"]["jammy"][image] == expected_data
        assert mock_file_open.called == expected_write_file

    @pytest.mark.parametrize(
        "versions, tags, is_used_version, expected_write_file, expected_data",
        [
            (  # add new version (next version is used, next version in tags and not in versions)
                ["2.10.0", "2.18.0"],
                ["2.10.1", "2.16.0"],
                True,
                True,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.1"},
                    ],
                },
            ),
            (  # version is not used
                ["2.10.0", "2.18.0"],
                ["2.10.1", "2.16.0"],
                False,
                False,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
            (  # next version not in tags)
                ["2.10.0", "2.18.0"],
                ["2.10.0", "2.16.0"],
                True,
                False,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
            (  # next version in versions
                ["2.10.1", "2.18.0"],
                ["2.10.1", "2.16.0"],
                True,
                False,
                {
                    "latest": "2.18.0-jammy",
                    "versions": [
                        {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                        {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
                    ],
                },
            ),
        ],
    )
    @patch("factory.bots.version_bot.VersionBot.is_used_version")
    @patch("builtins.open")
    def test_update_version(
        self,
        mock_file_open: Mock,
        mock_is_used_version: Mock,
        versions: list[str],
        tags: list[str],
        is_used_version: bool,
        expected_write_file: bool,
        expected_data: dict[str, str | dict],
    ) -> None:
        image = "bundesmessenger-web"
        self.version_bot.data["images"]["ubuntu"]["jammy"][image] = {
            "latest": "2.18.0-jammy",
            "versions": [
                {"base_images": ["nginx:latest-jammy"], "version": "2.10.0"},
                {"base_images": ["nginx:latest-jammy"], "version": "2.18.0"},
            ],
        }
        version_item = self.version_bot.data["images"]["ubuntu"]["jammy"][image]["versions"][0]
        version_tags = [tag for tag in (Version.parse(tag) for tag in tags)]

        mock_is_used_version.return_value = is_used_version
        self.version_bot.update_version(
            version_item, versions, "ubuntu", "jammy", image, version_tags
        )
        assert self.version_bot.data["images"]["ubuntu"]["jammy"][image] == expected_data
        assert mock_file_open.called == expected_write_file

    @patch("factory.bots.version_bot.Repo")
    @patch("factory.util.utils.requests.post")
    def test_process(self, mock_requests_post: Mock, mock_repo: Mock) -> None:
        mock_requests_post.return_value.status_code = 201

        def side_effect(remote: str, local_path: str) -> MagicMock:
            some_mock = MagicMock()
            some_mock.tags = ["1.80.0"]
            return some_mock

        mock_repo.clone_from.side_effect = side_effect
        for key in list(self.version_bot.data["images"].keys()):
            if key != "jammy_synapse":
                del self.version_bot.data["images"][key]
        self.version_bot.process()

    def test_previous_version(self) -> None:
        previous_version = self.version_bot.previous_version("2.4.12")
        assert previous_version == "2.4.11"

    @patch("factory.bots.version_bot.VersionBot.get_merge_requests_by_prefix")
    def test_checkout_mr_branch(self, mock_get_merge_requests_by_prefix: Mock) -> None:
        """Check if merge requests are allowed when merge request already opened."""
        mock_get_merge_requests_by_prefix.return_value = [{"title": "", "state": "opened"}]

        self.version_bot.checkout_mr_branch()
        assert self.version_bot.mr_allowed is False

    @patch("factory.util.utils.requests.get")
    @patch("factory.bots.version_bot.VersionBot.update_retention_config")
    def test_bot_pre_process(
        self,
        mock_requests_get: Mock,
        mock_update_retention_config: Mock,
    ) -> None:
        with patch.object(self.version_bot, "index") as mock_index:
            mock_index.diff.return_value = True
            self.version_bot.pre_process()
            mock_index.commit.assert_called()

        with patch.object(self.version_bot, "index") as mock_index:
            mock_index.diff.return_value = False
            self.version_bot.pre_process()
            mock_index.commit.assert_not_called()

    def test_process_calls_essential_functions(self) -> None:
        distro = "ubuntu"
        distro_codename = "jammy"
        # Arrange
        test_data = {
            "images": {
                "ubuntu": {
                    "jammy": {
                        "nginx": {
                            "type": "nexus",
                            "repository": "example-nexus-repo",
                        },
                        "apache": {
                            "type": "gitlab-registry",
                            "remote": "apache-remote",
                        },
                        "someapp": {
                            "type": "gitlab-registry",
                            "remote": "someapp-remote",
                        },
                        "custom-app": {
                            # Default type (git repository)
                            "repository": "example-git-repo",
                        },
                    }
                }
            }
        }
        self.version_bot.data = test_data
        with (
            patch.dict(self.version_bot.retention_config, {"manual_updates": ["apache"]}),
            patch.dict(
                "os.environ",
                {
                    "ARTIFACT_REPO": "",
                    "ARTIFACT_SERVER": "",
                    "UBUNTU_MIRROR_USER": "",
                    "UBUNTU_MIRROR_PASSWORD": "",
                },
            ),
            patch.object(
                NexusVersionFetcher,
                "get_latest_and_tags",
                return_value=("1.2.3", ["1.0.0", "1.1.0", "1.2.3"]),
            ) as mock_get_latest_and_tags_from_nexus,
            patch.object(
                GitLabVersionFetcher,
                "get_latest_and_tags",
                return_value=("2.0.1", ["1.9.0", "2.0.0", "2.0.1"]),
            ) as mock_get_latest_and_tags_from_gitlab_registry,
            patch.object(
                GitVersionFetcher,
                "get_latest_and_tags",
                return_value=("3.3.3", ["3.0.0", "3.2.0", "3.3.3"]),
            ) as mock_get_latest_and_tags_from_git_repository,
            patch.object(self.version_bot, "update_latest_version") as mock_update_latest_version,
            patch.object(self.version_bot, "update_versions") as mock_update_versions,
        ):
            self.version_bot.process()

        # Check if the correct methods were called based on resource_type
        mock_get_latest_and_tags_from_nexus.assert_called_once()
        # only called with "someapp-remote" as apache is manually updated (see patch)
        mock_get_latest_and_tags_from_gitlab_registry.assert_called_once()
        mock_get_latest_and_tags_from_git_repository.assert_called_once()
        # Ensure `update_latest_version` and `update_versions` was called for all images except manual updates
        mock_update_latest_version.assert_any_call(distro, distro_codename, "nginx", "1.2.3")
        mock_update_versions.assert_any_call(
            distro, distro_codename, "nginx", "1.2.3", ["1.0.0", "1.1.0", "1.2.3"]
        )
        mock_update_latest_version.assert_any_call(distro, distro_codename, "custom-app", "3.3.3")
        mock_update_versions.assert_any_call(
            distro, distro_codename, "custom-app", "3.3.3", ["3.0.0", "3.2.0", "3.3.3"]
        )
        # Ensure the undesired call is not present
        assert (distro, distro_codename, "apache", "2.0.1") not in [
            call.args for call in mock_update_latest_version.call_args_list
        ]
        assert (distro, distro_codename, "apache", "2.0.1", ["1.9.0", "2.0.0", "2.0.1"]) not in [
            call.args for call in mock_update_versions.call_args_list
        ]

    @pytest.mark.parametrize(
        "retention_list, image, version, expected",
        [
            # Test direct matches
            (["image:0.10.3"], "image", "0.10.3", True),
            # Test fails when version is not on the retention list
            (["image:0.10.3"], "image", "0.11.3", False),
            # Test alpha version predecessor
            (["image:0.10.3-alpha.1"], "image", "0.10.3-alpha.2", False),
            (["image:0.10.3-alpha.1"], "image", "0.10.3-alpha.3", False),
            # Test custom appendix
            (["image:0.10.3-etke34"], "image", "0.10.3-etke35", False),
            (["image:0.10.3-etke32"], "image", "0.10.3-etke35", False),
            # Test matching alpha with wrong version
            (["image:0.10.3-alpha.1"], "image", "0.10.0", False),
            # Test matching the base version when any alpha exists
            (["image:0.10.0-alpha.1"], "image", "0.10.0", True),
            # Test direct predecessor
            (["image:0.9.117"], "image", "0.10.0", False),
            (["image:0.9.117"], "image", "0.10.1", False),
            # Test that a version with no matches returns False
            (["image:0.9.117"], "image", "0.11.0", False),
            # Test that a version match fails if the image name does not match
            (["other-image:0.10.3"], "image", "0.10.3", False),
            # Test false positive
            (["image:0.10.3"], "image", "0.10", True),
        ],
    )
    def test_is_used_version(
        self, retention_list: list[str], image: str, version: str, expected: bool
    ) -> None:
        """Parametrized test for is_used_version."""
        self.version_bot.retention_config["retention_list"] = retention_list
        assert self.version_bot.is_used_version(image, version) is expected
