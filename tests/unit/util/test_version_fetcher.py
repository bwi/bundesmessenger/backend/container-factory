# Copyright 2025 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess
from unittest.mock import MagicMock, Mock, patch

import pytest
from semver import Version

from factory.util.version_fetcher import (
    GitLabVersionFetcher,
    GitVersionFetcher,
    NexusVersionFetcher,
)


class TestGitVersionFetcherUnit:

    fetcher: GitVersionFetcher

    @pytest.fixture(autouse=True)
    def before_test(self) -> None:
        """Function to replace __init__."""

        self.fetcher = GitVersionFetcher("fake_remote")

    @patch("factory.util.utils.subprocess.run")
    def test_get_remote_tags(self, mock_run: Mock) -> None:
        # Mock subprocess.run to return a MagicMock object with a .stdout attribute
        mock_run.return_value = MagicMock(
            stdout="""\
278ad155a2eecc344d93c6849f7bd887bcc42193	refs/tags/v1.0.0
878ad155a2eecc344d93c6849f7bd887bcc42195	refs/tags/v1.0.1
678ad155a2eecc344d93c6849f7bd887bcc42191	refs/tags/0.34.0rc2
f03c877b32c33275f47ead9e1e01d700a8d049c0	refs/tags/1.7.2
336a5ae1f44d45d49bfabf7788d80871df0e6ace	refs/tags/2017-06-06_1141
e7736668ba0ab2f7954717b6d99dc2f7c69d1845	refs/tags/alpha
b2e8baf8bdb29be0c1095ebc96e5b4ba44f67e71	refs/tags/dinsic_2019-02-22-beta14
e5f40dda27343d618cbb14ac10e449239d30a7f0	refs/tags/dinsic_2020-02-10
c2931a49bf5405f39c8220fbd7c359d474483d68	refs/tags/hhs-9
8d12eff836587ab9aa04e8b24306bdb882d0debe	refs/tags/modular-dev-v1.3.2-alpha.1+modular
87af3d1c05a7329a8fa988ae7f938a8678fa4b18	refs/tags/saml2_auth/181207
f30917b119d583b8ee86bf7982759b504c0e16f8	refs/tags/shhs-v1.1.1.7-opt
"""
        )

        expected_tags = [
            "v1.0.0",
            "v1.0.1",
            "0.34.0rc2",
            "1.7.2",
            "2017-06-06_1141",
            "alpha",
            "dinsic_2019-02-22-beta14",
            "dinsic_2020-02-10",
            "hhs-9",
            "modular-dev-v1.3.2-alpha.1+modular",
            "saml2_auth/181207",
            "shhs-v1.1.1.7-opt",
        ]
        remote_url = "https://github.com/example/repo.git"
        # Call the function with the mocked subprocess.run
        tags = self.fetcher._get_remote_tags(remote_url)

        # Assert that the mock was called with the expected arguments
        mock_run.assert_called_once_with(
            ["git", "ls-remote", "--tags", "--refs", remote_url],
            stdout=subprocess.PIPE,
            text=True,
            check=False,
        )

        # Assert the function returns the correct tags, excluding dereferenced tags
        assert tags == expected_tags

    def test_get_latest_and_tags(self) -> None:
        with patch.object(
            self.fetcher, "_get_remote_tags", return_value=["1.0.0", "1.2.0", "2.0.0"]
        ):
            latest, tags = self.fetcher.get_latest_and_tags()

            assert latest == Version.parse("2.0.0")
            assert tags == [Version.parse("1.0.0"), Version.parse("1.2.0"), Version.parse("2.0.0")]
            assert latest in tags


class TestNexusVersionFetcherUnit:

    fetcher: NexusVersionFetcher
    application = "fake_app"

    @pytest.fixture(autouse=True)
    def before_test(self) -> None:
        """Function to replace __init__."""

        self.fetcher = NexusVersionFetcher(
            self.application, "fake_repo", "fake_server", "fake_user", "fake_password"
        )

    def test_get_latest_and_tags(self) -> None:
        with patch("requests.get") as mock_get:
            mock_response = MagicMock()
            mock_response.json.return_value = {
                "items": [
                    {"path": f"{self.application}-1.0.0.tgz"},
                    {"path": f"{self.application}-2.0.0.tgz"},
                ],
                "continuationToken": None,
            }
            mock_get.return_value = mock_response
            latest, tags = self.fetcher.get_latest_and_tags()

            assert latest == Version.parse("2.0.0")
            assert tags == [Version.parse("1.0.0"), Version.parse("2.0.0")]
            assert latest in tags


class TestGitLabVersionFetcherUnit:

    fetcher: GitLabVersionFetcher

    @pytest.fixture(autouse=True)
    def before_test(self) -> None:
        """Function to replace __init__."""

        self.fetcher = GitLabVersionFetcher("fake_remote")

    def test_fetcher_get_latest_and_tags(self) -> None:
        with patch("requests.get") as mock_get:
            mock_response = MagicMock()
            mock_response.json.return_value = [
                {"version": "1.0.0"},
                {"version": "1.2.0"},
                {"version": "2.0.0"},
            ]
            mock_get.return_value = mock_response
            latest, tags = self.fetcher.get_latest_and_tags()

            assert latest == Version.parse("2.0.0")
            assert tags == [Version.parse("1.0.0"), Version.parse("1.2.0"), Version.parse("2.0.0")]
            assert latest in tags
