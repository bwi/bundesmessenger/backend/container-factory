# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import os
import pathlib
import subprocess
from datetime import datetime, timezone
from unittest.mock import MagicMock, Mock, patch

import pytest
import requests
from rfc3339_validator import validate_rfc3339
from semver import Version

from factory.util.utils import (
    clean_image,
    create_mounts_from_tmpfss,
    create_mounts_from_volumes,
    download_and_extract_image,
    download_file,
    expand_templates,
    filter_history,
    fix_ports_for_podman,
    format_rfc3339,
    get_encrypted_env,
    get_semver_version,
    get_tags_for_repository,
    inspect,
    load_manifest,
    normalize_version,
    pushd,
    repack_and_upload_image,
)

TEST_REPOSITORY = "ubuntu"
TEST_PROJECT = os.environ["PROJECT"]
TEST_REGISTRY = "doesnotexist:5000"
TEST_CONFIG = {
    "registry_user": "testuser",
    "registry_password": "testpassword",
}


class TestUtilsUnit:

    @patch("requests.get")
    def test_get_tags_for_repository(self, mock_requests_get: Mock) -> None:
        registry = TEST_REGISTRY
        project = TEST_PROJECT
        repository = TEST_REPOSITORY
        config = TEST_CONFIG
        tags = get_tags_for_repository(
            registry, project, repository, config["registry_user"], config["registry_password"]
        )
        assert isinstance(tags, list)

    @pytest.mark.parametrize(
        "keep_files",
        [(True), (False)],
    )
    def test_expand_templates(self, keep_files: bool) -> None:
        path = "tests/test_data/template"
        file = pathlib.Path("index.yaml")

        env = {"existing_key": "value"}
        overwrite_env = {"somenewkey": "somenewvalue"}
        with pushd(path):
            with expand_templates(
                ".", env, recurse=True, keep_files=keep_files, overwrite_env=overwrite_env
            ):
                assert os.path.exists(file)
                assert (
                    file.read_text(encoding="utf-8")
                    == 'distro: ubuntu\ndistro_codename: jammy\napp_version: "value"\nsafe_version: "somenewvalue"'
                )

            assert os.path.exists(file) == keep_files

    def test_create_mounts_from_volumes(self) -> None:
        docker_volume = {
            "/tmp/scratch/jammy-base/volume": {
                "bind": "/opt/nginx",
                "mode": "rw",
            }
        }
        mounts = create_mounts_from_volumes(docker_volume)
        assert mounts[0]["source"] in docker_volume.keys()

    def test_create_mounts_from_tmpfss(self) -> None:
        docker_tmpfs = {"/home/synapse/synapse": ""}
        mounts = create_mounts_from_tmpfss(docker_tmpfs)
        assert mounts[0]["target"] in docker_tmpfs.keys()

    def test_fix_ports_for_podman(self) -> None:
        ports = {"8080": "8080/tcp"}
        fixed_ports = fix_ports_for_podman(ports)
        for port in fixed_ports:
            assert "/tcp" not in fixed_ports[port]

    def test_get_encrypted_env(self) -> None:
        res = get_encrypted_env()
        assert isinstance(res, str)

    def test_format_rfc3339(self) -> None:
        current_datetime = datetime.now(timezone.utc)
        result = format_rfc3339(current_datetime)
        assert validate_rfc3339(result)

    @patch("podman.domain.images_manager")
    @patch("factory.util.utils.tarfile.open")
    @patch("builtins.open")
    def test_download_and_extract_image(
        self, mock_open: Mock, mock_tarfile_open: Mock, mock_images_manager: Mock
    ) -> None:
        mock_tar = MagicMock()
        mock_tar.extractall = MagicMock()
        mock_tar.close = MagicMock()
        mock_tarfile_open.return_value = mock_tar
        base_dir = "/test/dir"
        tar_name = "test_tar"
        image_name = "remote_image"

        download_and_extract_image(mock_images_manager, image_name, base_dir, tar_name)
        mock_images_manager.get(image_name).save.assert_called_once_with(named=True)
        mock_open.assert_called_once_with(f"{base_dir}/{tar_name}.tar", "wb")
        mock_tarfile_open.assert_called_once_with(f"{base_dir}/{tar_name}.tar")
        mock_tar.extractall.assert_called_once_with(f"{base_dir}/{tar_name}")
        mock_tar.close.assert_called()

    @patch("factory.util.utils.os.path.exists")
    @patch("factory.util.utils.json.load")
    @patch("builtins.open")
    def test_load_manifest_when_manifest_exists(
        self, mock_open: Mock, mock_json_load: Mock, mock_exists: Mock
    ) -> None:
        mock_exists.return_value = True
        base_dir = "/test/dir"
        tar_name = "test_tar"
        load_manifest(base_dir, tar_name)
        mock_open.assert_called_once_with(f"{base_dir}/{tar_name}/manifest.json")
        mock_json_load.assert_called()

    @patch("factory.util.utils.os.path.exists")
    def test_load_manifest_when_manifest_not_exists(self, mock_exists: Mock) -> None:
        mock_exists.return_value = False
        base_dir = "/test/dir"
        tar_name = "test_tar"
        result = load_manifest(base_dir, tar_name)
        assert result is None

    @patch("factory.util.utils.json.load")
    @patch("factory.util.utils.os.remove")
    @patch("factory.util.utils.json.dump")
    @patch("builtins.open")
    def test_filter_history(
        self, mock_open: Mock, mock_json_dump: Mock, mock_remove: Mock, mock_json_load: Mock
    ) -> None:
        mock_config_data = {
            "history": [
                {"created_by": "command1"},
                {"created_by": "command2 REMOVE=1"},
                {"created_by": "command3"},
            ]
        }
        expected_history = [
            {"created_by": "command1"},
            {
                "created_by": '/bin/sh -c RUN echo "This history item was removed by the build process."'
            },
            {"created_by": "command3"},
        ]
        mock_json_load.return_value = mock_config_data
        base_dir = "/test/dir"
        tar_name = "test_tar"
        config_file = "config.json"
        filter_history(base_dir, tar_name, config_file)
        assert mock_open.call_count == 2
        mock_json_dump.assert_called_once()
        mock_remove.assert_called()
        mock_json_load.assert_called_once()
        for fcall in mock_open().write.call_args_list:
            written_data_str = fcall[0][0]
            written_data = json.loads(written_data_str)
            assert written_data["history"] == expected_history

    @patch("builtins.open")
    @patch("podman.domain.images_manager")
    @patch("factory.util.utils.shutil.make_archive")
    def test_repack_and_upload_image(
        self, mock_make_archive: Mock, mock_images_manager: Mock, mock_open: Mock
    ) -> None:
        base_dir = "/test/dir"
        tar_name = "test_tar"
        repack_and_upload_image(mock_images_manager, base_dir, tar_name)
        mock_make_archive.assert_called()
        mock_open.assert_called_once()
        mock_images_manager.load.assert_called_once()

    @patch("podman.domain.images_manager")
    @patch("factory.util.utils.download_and_extract_image")
    @patch("factory.util.utils.load_manifest")
    @patch("factory.util.utils.filter_history")
    @patch("factory.util.utils.repack_and_upload_image")
    def test_clean_image(
        self,
        mock_repack_and_upload_image: Mock,
        mock_filter_history: Mock,
        mock_load_manifest: Mock,
        mock_download_and_extract_image: Mock,
        mock_images_manager: Mock,
    ) -> None:
        image_name = "test_image"
        # Call the overarching function
        with patch("tempfile.mkdtemp"):
            clean_image(mock_images_manager, image_name)
        # Assert that each component function was called
        mock_download_and_extract_image.assert_called()
        mock_load_manifest.assert_called()
        mock_filter_history.assert_called()
        mock_repack_and_upload_image.assert_called()

        # test case when load_manifest returns None
        mock_load_manifest.return_value = None
        mock_filter_history.reset_mock()
        mock_repack_and_upload_image.reset_mock()

        with patch("tempfile.mkdtemp"):
            clean_image(mock_images_manager, image_name)
        mock_filter_history.assert_not_called()
        mock_repack_and_upload_image.assert_not_called()

    @pytest.mark.parametrize(
        "version, expected_version",
        [
            ("1", "1.0.0"),
            ("1.1", "1.1.0"),
            ("1.1.1", "1.1.1"),
            ("1-dev", "1.0.0-dev"),
            ("1-dev1.2.3", "1.0.0-dev1.2.3"),
        ],
    )
    def test_normalize_version(self, version: str, expected_version: str) -> None:
        assert str(normalize_version(version)) == expected_version

    @pytest.mark.parametrize(
        "version, expected_result",
        [
            ("1.0.0", Version(major=1, minor=0, patch=0)),
            ("v1.0.0", Version(major=1, minor=0, patch=0)),
            ("1.0.0-prerelease1", Version(major=1, minor=0, patch=0, prerelease="prerelease1")),
            ("v1.0.0-prerelease1", Version(major=1, minor=0, patch=0, prerelease="prerelease1")),
            ("1.0", None),
            ("1.0.0.0", None),
            ("bla1.0.0", None),
        ],
    )
    def test_get_semver_version(self, version: str, expected_result: Version | None) -> None:
        # complex comparison, as the type union is (Version | None)
        if expected_result:
            result = get_semver_version(version)
            assert result
            assert result == expected_result
        else:
            assert get_semver_version(version) is None

    @pytest.mark.parametrize(
        "exception",
        [subprocess.CalledProcessError(1, "command"), FileNotFoundError, OSError],
    )
    def test_inspect_run_skopeo_exception(self, exception: Exception) -> None:
        with patch("subprocess.check_output", side_effect=exception):
            assert inspect("registry", "project", "repository", "tag") is None

    def test_inspect_invalid_json_exception(self) -> None:
        with patch("subprocess.check_output", return_value="invalid json"):
            assert inspect("registry", "project", "repository", "tag") is None

    @pytest.mark.parametrize(
        "exception",
        [
            requests.HTTPError,
            requests.ConnectionError,
            requests.Timeout,
            FileNotFoundError,
            PermissionError,
            IsADirectoryError,
            OSError,
        ],
    )
    def test_download_file_exception(self, exception: Exception) -> None:
        with patch("requests.get", side_effect=exception):
            assert not download_file("url", "filename", "directory")
