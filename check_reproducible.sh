source_date_epoch=$(podman inspect $1 -f "{{index .Labels \"de.bwi.baseimage.source-date-epoch\"}}")

project_dir=`pwd`

export SOURCE_DATE_EPOCH=$source_date_epoch

if [ -z "$source_date_epoch" ]
then
	echo "The image contains no LABEL de.bwi.baseimage.source-date-epoch."
	exit
fi

echo "LABEL de.bwi.baseimage.source-date-epoch has the value: $source_date_epoch"

mkdir image_chroot
mkdir orig_layer
mkdir test_layer
echo "Pull and extract image..."
podman pull -q $1 > /dev/null 2>&1
podman save $1 -q -o image.tar > /dev/null 2>&1
tar xf image.tar -C image_chroot
cd image_chroot
image_layer_tar=$(cat manifest.json | jq '.[].Layers[0]' 2>&1)
image_layer_dir=${image_layer_tar::-4}
# echo "Found first layer in image: $image_layer_tar"
eval "tar xf $image_layer_tar -C $project_dir/orig_layer" > /dev/null 2>&1
cd $project_dir

echo "Create test image from snapshot..."
mmdebstrap -q --components=main,universe --variant=minbase --include=apt,dirmngr,apt-transport-https jammy 2>/dev/null | podman import - strap-test > /dev/null 2>&1
mkdir test_chroot
podman save strap-test -q -o test.tar > /dev/null 2>&1
tar xf test.tar -C test_chroot
cd test_chroot
test_layer_tar=$(cat manifest.json | jq '.[].Layers[0]')
test_layer_dir=${test_layer_tar::-4}
mkdir test_layer
eval "tar xf $test_layer_tar -C $project_dir/test_layer" > /dev/null 2>&1
cd ..

rm -rf $image_layer_tar
rm -rf $image_layer_dir

cd $project_dir

diff -ur --no-dereference test_layer orig_layer &>/dev/null

if [ $? -eq 0 ]; then
  echo 'The image is bit-by-bit reproducible with the original debian snapshot.'
else
  echo 'The image is not bit-by-bit reproducible with the original debian snapshot.'
  echo " "
  echo "The image and the produced test image from mmdebstrap have the following diff: "
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
  diff -ur --no-dereference test_layer orig_layer
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
fi

# clean up
rm test.tar
rm image.tar
rm -rf test_chroot
rm -rf image_chroot
rm -rf image_layer
rm -rf test_layer
rm -rf orig_layer
