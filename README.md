# Container Image Factory

Toolchain to build basic Docker images and push them to a registry.

All images are currently based on Ubuntu releases and packages are pulled from a server configured
in the environment variable `UBUNTU_MIRROR`. Since this mirror will be included in the repository
configuration of all images, for images released to the public, the configured mirror should be
reachable from the public internet.

<!-- TOC_START -->
## Table of contents

- [Setup and Configuration](#setup-and-configuration)
  - [Layout](#layout)
  - [Hardware Requirements](#hardware-requirements)
  - [Prerequisites](#prerequisites)
  - [Optional tools](#optional-tools)
- [Installation Guide](#installation-guide)
  - [Installation](#installation)
- [Building and Testing](#building-and-testing)
  - [Build pipeline stages](#build-pipeline-stages)
  - [Build stages in depth](#build-stages-in-depth)
    - [Render stage](#render-stage)
    - [Scan stage](#scan-stage)
    - [Sign stage](#sign-stage)
- [Development](#development)
  - [Unittests, integrationtests and functional tests](#unittests-integrationtests-and-functional-tests)
  - [Coverage](#coverage)
  - [Gitlab repo configuration](#gitlab-repo-configuration)
  - [Gitlab Runner configuration](#gitlab-runner-configuration)
  - [Container logs during the build process](#container-logs-during-the-build-process)
  - [Adding images to the container factory](#adding-images-to-the-container-factory)
- [Buildsystem operation](#buildsystem-operation)
<!-- TOC_END -->

## Setup and Configuration

### Layout

The top-level directory contains the tool to control the build (`update_images.py`) process and a
YAML configuration file (`config.yaml`) holding variables to be referenced during build time.

The list of all maintained images must be defined in the `maintenance_list.yaml` file.
The file specifies all images under control of the container factory.
Maintenance modules like `delete_images.py` use this list to restrict the processes to the
defined images.

Images which are maintained by the version bot are defined in the `images.yaml` file.

Below directory `images` there is one directory for each image to build. These directories contain
at least the following files:

- `build`: Program to build the image
- `image.yaml`: Configuration file holding at least image name, dependencies, short description, and
  information needed by [podman-py](https://github.com/containers/podman-py) to run the container.
  For that purpose keys prefixed with `docker_` are stripped and passed to the `run` method of the
  container API.
- `needs_update`: Program to check, if the image needs to be updated. It should return 0, if an
  update is required. When started, the program get the id of a running container of the existing
  image as only parameter.
- `tests`: Program to run final tests on the newly created image. It should return 0, if all tests
  have passed. It gets the id of a running container of the fresh image as only parameter. Only if
  `tests` returned 0, the image is pushed to the registry.  If `tests` outputs lines in the form

          ENV NAME=VALUE

  the `NAME` will be added to the environment and the templates in the current working directories
  are updates with the corresponding values.  This allows to include information extracted from the
  container to be used e.g. in tags.

- `build_bom`: An optional program to be used to add BOM, sources and other additional information
  to an image. A sample implementation is provided in the focal-base configuration. The script is
  called with two command line parameters

  1. container id
  2. image name

  If the file is missing, that stage will be skipped.

- `build_ref`: Program to add the readme_references.txt file to the image.

- `build_src`: Program to add the sources to the source image.

- `cve_ignores.yaml`: An optional list of CVEs to ignore if detected in an image during security
  scan. If the file does not exist, no CVEs are ignored. File structure is a dict with the CVE as
  key and at least a short description behind key `sd`, e.g.:

```yaml
- id: 'CVE-2022-48522'
  reason: 'app_dependency'
  description: 'Python3.9 needs the libperl5.34 package by the following dependency chain: python3 mime-support mailcap perl libperl5.34.'
```

- `strip.yaml`: An optional set of commands to remove unused
  components from an image. The following ations are supported:
  1. `action: script`: A shell script to be run by a given shell
  2. `action: remove_deb_packages`: Remove packages using `apt`.
  3. `action: remove_apt`: Remove apt package manager using `dpkg`.

  Each `action` is called with a set of `params`, where type and content depends on the action, e.g.:

```yaml
steps:
  - remove_deb_packages
  - remove_apt
  - remove_files
remove_deb_packages:
  action: remove_deb_packages
  params: # list of packages, wildcards are supported
    - .*-dev(:amd64)?
    - cpp
    - pip
remove_apt:
  action: remove_apt
  params: null
remove_files:
  action: script
  params:
    shell: /bin/bash
    script:
      - echo "do anything"
```

Instead of the configuration files templates may be used. All files ending in `.template` are
processed by the [Jinja2](https://jinja.palletsprojects.com/en/3.1.x/) templating engine and the
suffix is stripped from the name.

While processing the top-level `config.yaml.template` all current operating system environment
veriables are available. The templates in image directories have also access to the keys specified
in `config.yaml`.

### Hardware Requirements

The container image factory consists of the following components:

- GitLab repository with activated ci pipelines
- central container image registry (f.e. harbor)
- second container image registry for usage in feature branch and development pipelines (too keep
  test builds ouside the the central registry)
- GitLab runner and a docker or podman executor to provide the builder container for the build
  pipelines

For an on-premise GitLab instance see the official hardware requirements of the GitLab project.

https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/install/requirements.md

For the central registry we recommend a minimum of:

- 512GB disk space
- 8GB Memory
- 4 cpus

For the test registry we recomment a minimum of:

- 512 GB disk space
- 8GB Memory
- 4 cpus

For the Gitlab Runner VM / Server we recommend a minimum of

- 512GB diskspace
- 16GB Memory
- 8 cpus

### Prerequisites

- Gitlab instance has ci pipelines activated
- User account in GitLab with permissions to change ci variables
- Gitlab Runner installed
- A docker executor is registered with the GitLab repository on the GitLab runner

### Optional tools

- Sonatype Nexus Repository Manager
- Keycloak as identity provider for the keyless signing process
- Sigstore stack for the signing process

## Installation Guide

### Installation

Clone the repository to your local folder.

```bash
git clone https://gitlab.opencode.de/bwi/bundesmessenger/backend/container-factory/base-images.git
```

Copy helper files from provided default files to set environment variables for development.

```bash
cd base-images
cp .env_default .env
cp .env_container_default .env_container
cp setenv_container_default.sh setenv_container.sh
```

Modify the the files and insert information about registries, credentials and gitservers used by the
build process.

`.env` is used to set environment variables on the host running the builder.

`.env_container` is used to set the environment variables inside the builder container.

`setenv.sh` sets the environment variables defined in the .env file.
`setenv_container.sh` sets the environment variables defined in the .env_container file.

The auth string for the registries must be provided inside the setenv_container.sh script.

To create the auth code in setenv_container.sh, encode the usename and password in base64:

```bash
# The use of printf (as opposed to echo) prevents encoding a newline in the password.

printf "my_username:my_password" | openssl base64 -A

# Example output to copy

bXlfdXNlcm5hbWU6bXlfcGFzc3dvcmQ=
```

Add the output to setenv_container.sh:

```bash
export DOCKER_AUTH_CONFIG='{"auths": {"registry.example.com": {"auth": "(Base64 content from above)"}}}'
```

## Building and Testing

### Build pipeline stages

The build pipeline consists of the following stages, which the build system will process for each
image:

- render (render configuration templates)
- lintcheck (dockerfiles lint check)
- spellcheck (entrypoint spell check)
- check (Check, if image must be updated)
- build (build image)
- test (test image)
- strip (strip unnessary and critical files)
- ref (create references file to production and source images)
- src (build source image)
- bom (create bill of materials)
- qa (qa check for mandatory files)
- push (push image to registry)
- scan (scan image for vulnerabilities)
- sign (sign image)

### Build stages in depth

#### Render stage

The render stage is processed as part of the build pipeline and renders all jinja2 template files in
the images directory, which are used to generate Dockerfiles, build- and test-files.  The rendering
of the files can be executed independent from the pipeline by using the stages parameter of the
update_images script:

```bash
python3 -m factory.update_images --stages=render
```

The rendered files can be downloaded as stage artifacts in GitLab.

#### Scan stage

The scan stage performs a security scan and creates a comprehensive CVE report in the Pipeline Job Log.
Two different security scanners can be configured in the container factory:

- Internal scanner of the builder image (grype)
- External scanner integrated in harbor triggered by API call (f.e. trivy)

The scanner can be selected with the CI Variable `VULNERABILITY_SCANNER`.

A CVE report is created for the built image at the end of the pipeline job log.
The CVE agent processes the built image and upstream CVE status data to provide useful overview and
assistance for necessary CVE mitigations in the CVE report.

#### Sign stage

##### Introduction

In the build pipeline, the "sign" stage plays a critical role in ensuring the authenticity and
integrity of container images before deployment. This stage utilizes the keyless signing process,
along with the Sigstore stack, to enhance security, transparency, and verifiability in the software
supply chain.

The documentation for the keyless signing process can be found here:

[Keyless signing](docs/keyless_signing.md)

## Development

Usually the build process is triggered from GitLab, e.g. through the web UI at CI/CD - Pipelines -
Run Pipeline. During image development local builds are also possible.

Instead of using environment variables provided by the GitLab runner, you can either modify the
environment manually or provide key=value pairs on the `update_images.py` command line, e.g.:

```bash
python3 -m factory.update_images DOCKER_REGISTRY=localhost:5000 key=value
```

Command line option `--keep` will make `update_images.py` keep the files generated from templates,
leaving e.g. an executable `build` command with all substitutions for local execution.

If local image creation works, it still needs to be tested inside the podman container used by
GitLab-runner to build the images. Provided you have a working podman installation, from the
top-level directory of the checked out repository, run e.g.

```bash
./local_pinp_buildimage.sh
```

This will start a pinp (podman inside podman) instance of the build image with the sources, which
will be copied to `/ci_project_dir`.  The script will adjust the ownership of the copied directory
to the mapped user und group (setuid/setgid) for the user "build" inside the container.

### Unittests, integrationtests and functional tests

Start the unit tests with the following command:

```bash
pytest tests/test_unit.py
```

### Coverage

Create a coverage report with the following commands:

```bash
pytest --cov --cov-report term
```

### Gitlab repo configuration

The environment variables described before in the chapter "development" must be configured as ci
variables in GitLab.

### Gitlab Runner configuration

We recommend a "concurrent" setting for the GitLab runner of 8 or higher for a fast build process.
But this value depends on the number of cpus your runner machine provides and your network
bandwidth.  A higher concurrent setting leads to much faster build times, because the container
image factory creates multiple ci pipelines jobs to build different images.  On the other hand this
leads to higher network bandwidth usage when a higher number of build jobs push to a registry at the
same time.  So this value should be balanced.

### Container logs during the build process

The container logs during the build process are included in the GitLab ci job log.  You can find the
container logs by searching the string "Container logs:" in the job log.  If a container crashed
before starting a new stage, the container logs are included at the end of the job log.

### Adding images to the container factory

[Adding images](docs/adding_images.md)

## Buildsystem operation

The documentation for the operation of the buildsystem can be found here:

[Buildsystem operation](docs/buildsystem_operation.md)
