# Keyless signing

The keyless signing process is a significant advancement in software supply
chain security. Instead of generating and managing traditional cryptographic
key pairs for each signer, the keyless approach relies on Fulcio, a secure
Certificate Authority (CA), and a centralized signing service.

<!-- TOC_START -->
## Table of contents

- [Keyless Signing Process](#keyless-signing-process)
  - [Identity Management with Keycloak](#identity-management-with-keycloak)
  - [Certificate Issuance](#certificate-issuance)
  - [Signature Generation](#signature-generation)
  - [Log and Publish](#log-and-publish)
- [Advantages of Keyless Signing](#advantages-of-keyless-signing)
  - [Enhanced Security](#enhanced-security)
  - [Simplified Key Management](#simplified-key-management)
[The Sigstore Stack](#the-sigstore-stack)
  - [Cosign](#cosign)
  - [Fulcio](#fulcio)
  - [Rekor](#rekor)
  - [CTLog (Certificate Transparency Log)](#ctlog-certificate-transparency-log-)
  - [Trillian](#trillian)
- [Verify images with the generated signatures](#verify-images-with-the-generated-signatures)
- [Contents of the SIG File](#contents-of-the-sig-file)
- [Verify signature of image with cosign](#verify-signature-of-image-with-cosign)
- [Manual verification of image](#manual-verification-of-image)
  - [Check contents of signature](#check-contents-of-signature)
  - [Download signature](#download-signature)
  - [Download certificate](#download-certificate)
  - [Check contents of certificate](#check-contents-of-certificate)
  - [Extract public key from certificate](#extract-public-key-from-certificate)
  - [Verify signature of image with cosign without ctlog](#verify-signature-of-image-with-cosign-without-ctlog)
  - [Verify signature of image with openssl using the signature in the registry](#verify-signature-of-image-with-openssl-using-the-signature-in-the-registry)
  - [Verify signature of image with openssl using rekor's entry](#verify-signature-of-image-with-openssl-using-rekor-s-entry)
- [Managing disconnected signatures using cosign](#managing-disconnected-signatures-using-cosign)
- [Installation of prerequisites for keyless signing](#installation-of-prerequisites-for-keyless-signing)
  - [Keycloak as identity provider for the signing process](#keycloak-as-identity-provider-for-the-signing-process)
  - [Sigstore stack for the signing process](#sigstore-stack-for-the-signing-process)
<!-- TOC_END -->

## Keyless Signing Process

### Identity Management with Keycloak

Keycloak acts as the identity provider, managing user identities and access
control. Developers are authenticated and authorized through Keycloak, ensuring
strong identity verification.

### Certificate Issuance

Fulcio issues X.509 certificates to individual signers, ensuring strong
identity verification.

### Signature Generation

During the signing process, a container image is signed using Cosign, and the
cryptographic signing operation is performed by a centralized and dedicated
signing service.

### Log and Publish

The signed container image and its corresponding signature metadata are
recorded in the Rekor transparency log and published to CTLog.

## Advantages of Keyless Signing

### Enhanced Security

Keyless signing mitigates the risk of key compromise, as the cryptographic
signing operations are isolated within the centralized signing service. It
ensures that signing keys are not distributed or stored locally, reducing the
attack surface.

### Simplified Key Management

Developers are relieved from the burden of managing and safeguarding
cryptographic key pairs. This significantly simplifies the signing process and
reduces the chances of accidental key exposure.

## The Sigstore Stack

The Sigstore stack is a set of open-source tools and services that work
together to provide secure and verifiable software supply chain signatures.
Let's briefly explain each component:

### Cosign

Cosign is a tool used for container image signing and verification. It employs
cryptographic signatures to validate the integrity and authenticity of
container images. With Cosign, developers can sign container images locally and
then store the signatures in a tamper-evident manner.

### Fulcio

Fulcio serves as a secure Certificate Authority (CA) designed to issue X.509
certificates. These certificates are used in the signing process to establish
trust and identity for signers, ensuring a robust signing infrastructure.

### Rekor

Rekor is a transparency log designed to store and publish metadata about
software artifacts and signatures. It enables a transparent and accountable
record of all signed artifacts, enhancing the visibility of the signing
process.

### CTLog (Certificate Transparency Log)

CTLog is a critical component of the Sigstore stack that provides a
tamper-evident and publicly auditable log of certificates. It helps ensure the
integrity and authenticity of certificates issued by Fulcio.

### Trillian

Trillian is a verifiable data structure that acts as an append-only log. In the
Sigstore context, it is used to store and secure the CTLog data, enabling a
secure and scalable logging infrastructure.

## Verify images with the generated signatures

Cosign creates a signature for a container images. The build pipeline signs a
specific image by digest not by a tag, which would be critical as tags are
arbitrary and the underlying image could be replaced.

For example to sign the image ubuntu:latest-jammy-production the container
image factory looks up the digest for this image tag:
ubuntu@sha256:726332b3e7e42879dca811d4510c33693faf49397b0b27b974a0d7a0f0479199

The signature will be created and stored in the registry at the following
location:

`/v2/[project]/ubuntu/manifests/sha256-[image digest].sig`

F.e. (set 'project' to your local project name for the container image factory)

`/v2/bundesmessenger/ubuntu/manifests/sha256-726332b3e7e42879dca811d4510c33693faf49397b0b27b974a0d7a0f0479199.sig`

## Contents of the SIG File

The SIG file, published to the container image registry, contains essential
metadata about the signed container image. It includes the following
information:

- Image Digest (The cryptographic digest (hash) of the container image,
  ensuring its integrity.)
- Signature (The cryptographic signature generated during the signing process,
  verifying the authenticity of the container image.)
- Signer Identity (Information about the signer's identity, which is
  established through the Keycloak identity provider and Fulcio certificate
  issuance.)
- Timestamp (The timestamp of the signing operation, providing additional
  accountability and enabling auditing.)

By including this information in the SIG file, users can easily verify the
integrity and authenticity of the container image, ensuring a secure software
supply chain.

## Verify signature of image with cosign

```bash
cosign verify --offline=false --certificate-identity='signer@example.com' \
--certificate-oidc-issuer='https://keycloak.example.com/realms/imagesigning' --allow-insecure-registry --rekor-url=$REKOR_URL \
registry.example.com/project/ubuntu@sha256:726332b3e7e42879dca811d4510c33693faf49397b0b27b974a0d7a0f0479199
```

The following checks were performed on each of these signatures:

- The cosign claims were validated
- Existence of the claims in the transparency log was verified offline
- The code-signing certificate was verified using trusted certificate authority
  certificates

## Manual verification of image

### Check contents of signature

```bash
skopeo inspect --raw docker://$(cosign triangulate $IMAGE) | jq -r
```

### Download signature

```bash
cosign triangulate $IMAGE

curl -u 'user:password' -H 'Accept: application/vnd.oci.image.manifest.v1+json' https://example/v2/project/ubuntu/manifests/sha256-726332b3e7e42879dca811d4510c33693faf49397b0b27b974a0d7a0f0479199.sig | jq -r '.layers[0].annotations."dev.cosignproject.cosign/signature"' > image.sig
```

### Download certificate

```bash
curl -u 'username:password' \
-H 'Accept: application/vnd.oci.image.manifest.v1+json' \
https://registry.example.com/v2/project/ubuntu/manifests/sha256-726332b3e7e42879dca811d4510c33693faf49397b0b27b974a0d7a0f0479199.sig \
| jq -r '.layers[0].annotations."dev.sigstore.cosign/certificate"' > cert.pem
```

### Check contents of certificate

```bash
openssl x509 -in cert.pem -text
```

### Extract public key from certificate

```bash
openssl x509 -in cert.pem -pubkey -noout -out pub.pem
```

### Verify signature of image with cosign without ctlog

```bash
cosign verify --insecure-ignore-tlog --signature=image.sig --key=pub.pem $IMAGE
```

WARNING: Skipping tlog verification is an insecure practice that lacks of
transparency and auditability verification for the signature.

The following checks were performed on each of these signatures:

- The cosign claims were validated
- The signatures were verified against the specified public key

### Verify signature of image with openssl using the signature in the registry

To create signature and public key files, follow the chapter "Manual verification of image".

```bash
cosign verify --insecure-ignore-tlog --signature=image.sig --key=pub.pem $IMAGE
Verified OK
```

### Verify signature of image with openssl using rekor's entry

####### Retrieve rekor's logIndex for the signature

Get the logIndex number from the build log of the GitLab pipeline log.

Access rekor's entry for the signature and download signed entry timestamp
(SET), then verify against the signed entry timestamp and the signature of the
signed entry timestamp:

```bash
curl https://rekor.example.com/api/v1/log/entries?logIndex=11 > entry.json
cat entry.json | jq -r '.[].verification.signedEntryTimestamp' | base64 -d > set.sig
jq -cj '.[] | del(.attestation, .verification)' entry.json > set.json

openssl dgst -sha256 -verify rekor.pub -signature set.sig set.json
Verified OK
```

## Managing disconnected signatures using cosign

When verifying content, cosign by, default, communicates with the remote
registry to obtain the resources needed to perform the verification process.
Since we have a method of retrieving the signature, we can instruct cosign to
refrain from involving the remote signature store and to instead provide it
from a local source.

The ability to accomplish the same steps to verify the integrity of an image
using cosign without referring to a remote signature source affords additional
options when determining how image signatures will be managed. There are
several situations for which the default functionality of cosign of storing
signing assets within an associated tag within the same repository cannot be
facilitated. Examples include avoiding having container image content and
signature content stored within the same repository or the fact that some
container registries still do not support the use of OCI artifacts resulting in
failures when attempting to store signatures.

The cosign parameters to manage disconnected signatures are described in the
following article:

https://medium.com/@sabre1041/managing-disconnected-signatures-using-cosign-27f157e82f88

## Installation of prerequisites for keyless signing

### Keycloak as identity provider for the signing process

Installation f.e. with helm:

https://bitnami.com/stack/keycloak/helm

### Sigstore stack for the signing process

Create a scaffold values file: scaffold.values.yaml

```yaml
copySecretJob:
  enabled: true

rekor:
  server:
    attestation_storage:
      enabled: false
      persistence:
        enabled: false
    ingress:
      className: nginx
      hosts:
        - host: rekor.example.com
          path: /
      tls:
        - secretName: rekor-tls
          hosts:
            - rekor.example.com
fulcio:
  createcerts:
    enabled: true
  server:
    ingress:
      className: nginx
      http:
        hosts:
          - host: fulcio.example.com
            path: /
      tls:
        - secretName: fulcio-tls
          hosts:
            - fulcio.example.com
  config:
    contents:
      OIDCIssuers:
        https://keycloak.example.com/realms/imagesigning:
          IssuerURL: https://keycloak.example.com/realms/imagesigning
          ClientID: sigstore
          Type: email
        https://dl-gitlab.example.com:
          IssuerURL: https://dl-gitlab.example.com
          ClientID: sigstore
          Type: email

tuf:
  enabled: true
  ingress:
    className: nginx
    http:
      hosts:
        - host: tuf.example.com
          path: /
    tls:
      - secretName: tuf-tls
        hosts:
          - "tuf.example.com"
```

Clone the scaffold helm chart of the sigstore stack:

```bash
git clone https://github.com/sigstore/helm-charts.git .
```

If your clustername is not the default cluster name then edit
`charts/scaffold/templates/copy-secrets-job.yaml` and change
`.svc.cluster.local` to your clustername f.e.
`.svc.yourclustername-cluster.local`

Install the scaffold helm chart of the sigstore stack:

```bash
helm upgrade -i scaffold  ./helm/charts/scaffold -n sigstore --create-namespace  --values scaffold.values.yaml
```
