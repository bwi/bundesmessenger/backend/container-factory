# Adding images to the container factory

<!-- TOC_START -->
## Table of contents

- [Utilizing Existing Images as Templates for Custom Definitions](#utilizing-existing-images-as-templates-for-custom-definitions)
- [Supported Image Types in the Container Factory](#supported-image-types-in-the-container-factory)
  - [System Package Images](#system-package-images)
  - [VersionBot Images](#versionbot-images)
- [Creating an image definition from scratch](#creating-an-image-definition-from-scratch)
  - [Location of image definitions](#location-of-image-definitions)
  - [Required parts of an image definition](#required-parts-of-an-image-definition)
  - [Required parts of the image configuration](#required-parts-of-the-image-configuration)
  - [Required parts of a dockerfile](#required-parts-of-a-dockerfile)
<!-- TOC_END -->

## Utilizing Existing Images as Templates for Custom Definitions

In anticipation of future enhancements to the container factory, we are
actively exploring the incorporation of definition templates and a
comprehensive scaffolding tool. These additions are designed to streamline the
process of creating new image definitions, fostering an environment of
efficiency and customization.

**Current State and Interim Solutions:**

At present, the container factory has yet to implement definition templates or
a scaffolding mechanism tailored to the distinct types of images we support.
Recognizing the importance of agility and flexibility in development workflows,
we offer a pragmatic interim solution for developers seeking to create new
image definitions.

Developers can leverage existing images within the container factory as
foundational templates. This approach enables the creation of new, customized
image definitions by using an existing image as a baseline. This method not
only saves time but also ensures consistency and reliability by building upon
proven configurations.

**Procedure:**

1. **Identify an Existing Image:** Select an existing image that closely aligns
   with your requirements. This image will serve as the starting point for your
   new image definition.
   For different image types in the container factory see the chapter:
   [Supported Image Types in the Container Factory](#supported-image-types-in-the-container-factory)
2. **Duplicate and Customize:** Copy the chosen image definition and modify it
   according to your specific needs. This may involve adjusting configurations,
   installing additional packages, or integrating custom scripts.
3. **Validation and Testing:** Thoroughly test the newly created image to
   ensure it meets your expectations and functions as intended within your
   deployment environment.

**Benefits:**

- **Efficiency:** Quickly generate new image definitions without starting from
  scratch.
- **Consistency:** Maintain a high level of reliability and standardization by
  using tested and proven image configurations as your foundation.
- **Customization:** Tailor the image definitions to meet the exact needs of
  your project, allowing for greater flexibility and optimization.

We understand the critical role that efficient development tools and processes
play in the success of your projects. As we work towards integrating definition
templates and scaffolding capabilities into the container factory, we encourage
you to utilize this method for creating new image definitions. This approach
not only streamlines your development process but also aligns with best
practices for maintaining high-quality, reliable container images.

## Supported Image Types in the Container Factory

The container factory is designed to accommodate a diverse range of image types
to suit various development and deployment needs. Below, we outline the
categories of images currently supported, along with examples to illustrate
their application and integration strategies.

### System Package Images

System package images serve as foundational application images, where the
application is installed using the system package manager specific to the
image's distribution. These images are ideal for scenarios where the
reliability and standardization of system packages are preferred for
application deployment.

**Examples:** nginx, clamav, icap, redis

These images leverage the native package management system to ensure that the
application is integrated seamlessly with the underlying operating system,
benefiting from the package manager's handling of dependencies, versioning, and
updates.

### VersionBot Images

VersionBot images are meticulously maintained by the VersionBot, ensuring they
are up-to-date and secure. These images are further subdivided into three
categories, each serving a unique purpose within the development pipeline:

#### Artifact Images

Artifact images are specialized for incorporating external application
artifacts. These artifacts, whether frontend or backend components, are either
extracted and served (in the case of frontend artifacts) or directly executed
as the container's primary process (for backend artifacts). This approach is
particularly beneficial for deploying applications that consist of pre-built
binaries or assets that require a straightforward extraction or execution
environment.

**Frontend Artifact Images**

Designed specifically for frontend applications, these images are optimized for
serving static assets or executing frontend frameworks. They ensure that the
deployment of user interfaces and web applications is both efficient and
scalable.

**Examples:** bundesmessenger-web, bundesmessenger-call, synapse-admin

**Backend Artifact Images**

Focused on backend services, these images are tailored for running backend
applications and services directly from external artifacts. This is especially
useful for deploying applications that are distributed as pre-compiled binaries
or that need to be deployed without a full build environment.

**Examples:** kubectl

**Custom Build Images**

Custom build images integrate the application build process directly within the
container factory, offering a highly flexible solution for applications
requiring custom build steps or environments. This category is ideal for
scenarios where the application development involves unique build processes,
dependencies, or environments that are not adequately addressed by standard
system package images.

These images empower developers to fine-tune the build environment,
dependencies, and process to suit the specific needs of their application,
resulting in a container image that is both optimized for deployment and
tailored to the application's requirements.

***Examples:*** synapse, matrix-content-scanner

## Creating an image definition from scratch

The following chapters describe how you can create an image definition.
The image definition is a directory containing files with informations
for the container factory, how to build and maintain the image.

### Location of image definitions

The image definitions are located in a folder

```
images/[distro]/[distroversion]
f.e.
images/ubuntu/jammy
```

So the nginx image is located in the directory

```
images/ubuntu/jammy/nginx
```

### Required parts of an image definition

The image definition directoy contains at least the following files:

- `build.template`: Program to build the image

- `build_bom.template`: An optional program to be used to add BOM, sources and
  other additional information to an image. A sample implementation is provided
  in the focal-base configuration. The script is called with two command line
  parameters

  1. container id
  2. image name

  If the file is missing, that stage will be skipped.

- `build_ref.template`: Program to add the readme_references.txt file to the image.

- `cve_ignores.yaml`: An optional list of CVEs to ignore if detected in an
  image during security scan. If the file does not exist, no CVEs are ignored.
  File structure is a dict with the CVE as key and at least a short description
  behind key `sd`, e.g.:

```yaml
- id: 'CVE-2022-48522'
  reason: 'app_dependency'
  description: 'Python3.9 needs the libperl5.34 package by the following dependency chain: python3 mime-support mailcap perl libperl5.34.'
```

- `Dockerfile.template`: The dockerfile to use for the image build process. For
  the different image types of the container factory, the dockerfile has
  specific parts the container factory needs to build and maintain the image.
  See chapter "Required parts of a dockerfile".

- `image.yaml`: Image configuration file holding at least image name,
  dependencies, short description, and information needed by
  [podman-py](https://github.com/containers/podman-py) to run the container.
  For that purpose keys prefixed with `docker_` are stripped and passed to the
  `run` method of the container API. For a description of the required parts of
  the image configuration file, see Chapter "Required parts of the image
  configuration".

- `strip.yaml`: An optional set of commands to remove unused components from an
  image. The following ations are supported:
  1. `action: script`: A shell script to be run by a given shell
  2. `action: remove_deb_packages`: Remove packages using `apt`.
  3. `action: remove_apt`: Remove apt package manager using `dpkg`.

  Each `action` is called with a set of `params`, where type and content
  depends on the action, e.g.:

```yaml
steps:
  - remove_deb_packages
  - remove_apt
  - remove_files
remove_deb_packages:
  action: remove_deb_packages
  params: # list of packages, wildcards are supported
    - .*-dev(:amd64)?
    - cpp
    - pip
remove_apt:
  action: remove_apt
  params: null
remove_files:
  action: script
  params:
    shell: /bin/bash
    script:
      - rm -rf /usr/share/man
```

After the container is built using the Dockerfile, a strip step is performed.
This process involves starting a container from the newly built image and
executing the steps defined in strip.yaml. These steps typically include
actions such as removing unnecessary files.

It is crucial to understand that the resulting image retains all its layers.
Consequently, any files removed during the strip step remain accessible within
the image's history. These files can be extracted from the image, meaning that
the strip step does not serve as a secure method for deleting sensitive or
security-critical data.

**Key Points for the strip step:**

 1. Post-Build Strip Execution: The strip step is executed after the initial
    image build. A container is instantiated from the built image, and the
    defined strip actions are applied.

 2. Layer Retention: The image maintains all layers created during the build
    process. The strip step modifies only the top layer, while previous layers,
    including the ones containing the removed files, remain intact.

 3. Data Extractability: Files deleted in the strip step can still be retrieved
    from the underlying layers. This characteristic underscores that the strip
    step is not a mechanism for secure data removal.

 4. Security Implications: Be aware that relying on the strip step for removing
    sensitive information is insufficient. Sensitive data should be managed
    appropriately during the build process, ensuring it is not included in the
    image layers.

 5. Best Practices: To enhance security and minimize risks:
    - Avoid including sensitive data in the Dockerfile and build context.
    - Use multi-stage builds to reduce the final image size and exclude
      unnecessary files from the final stage.
    - Regularly audit and clean up Docker images to ensure no unintended data
      is present.

By comprehending these intricacies, you can better manage your container images
and avoid potential security pitfalls associated with the post-build strip
step.


- `tests`: Program to run final tests on the newly created image. It should
  return 0, if all tests have passed. It gets the id of a running container of
  the fresh image as only parameter. Only if `tests` returned 0, the image is
  pushed to the registry. If `tests` outputs lines in the form

          ENV NAME=VALUE

  the `NAME` will be added to the environment and the templates in the current
  working directories are updates with the corresponding values.  This allows
  to include information extracted from the container to be used e.g. in tags.

- `tools/collect_app_sources.sh`: Contains the commands to add the applications
  source to the image. As this process might differ from image to image, we
  provide this file for each image.

Instead of the configuration files templates may be used. All files ending in
`.template` are processed by the [Jinja2](https://jinja.palletsprojects.com/)
templating engine and the suffix is stripped from the name.

While processing the top-level `config.yaml.template` all current operating
system environment veriables are available. The templates in image directories
have also access to the keys specified in `config.yaml`.

### Required parts of the image configuration

`distro`: The distrubution of the image f.e. "ubuntu"

`distro_codename`: The distribution version of the image f.e. "jammy".
At the current state of the container factory you must use the codename and not
the numeric version to maintain unique tags for multiple distributions for the following reason:
Codenames are unique and lead to unique tags . The container factory currently
uses only the code_version property to create tags and not a combination of distribution and version.

`basename`: The basename of the image f.e. "synapse"
`name`: The name of the image including the image tag.

**Specific required parts for VersionBot images**

`install_version`: defines the version part of the image tag for VersionBot images.
In the current state of the container factory this is predefined by the
container factory to the version of the application. So we use the following
property in the image definition file to load the predifined value into the
image definition:

```yaml
install_version: "{{ install_version }}"
```

The value of `install_version` is set by the container factory to the value of
install_version, which is extracted from the tag of the images parameter of the
update_images script.

### Required parts of a dockerfile

1. The FROM statement

For multistage images you can define a baseimage for your build requirements:

```
FROM {{ registry }}/{{ project }}/python:3.9-{{ distro_codename }}-unhardened as requirements
```
Your final stage should reference your configured base images (from the
definition in images.yaml) like this:

```
FROM {{ registry }}/{{ project }}/{{ base_images[0] }}-unhardened
```

2. The build_id build argument and label

The build_id will be injected by the central build.template script.
The definition of the build argument must be present in each dockerfile.

```
ARG BUILD_ID
```

The following statement MUST be placed at the end of the dockerfile.

```
LABEL de.bwi.baseimage.build-id=$BUILD_ID
```

3. The license of the application

The license MUST be present in the `/` directory. Our existing
images copy the license from the first stage of a multistage dockefile where it
is downloaded in most cases with curl from a git repository. Other options are
to copy it from a dist-packages folder of a pypi package installation or from
your used package system for the application.

```
COPY --from=requirements /LICENSE /LICENSE
```

The following is an example how to download a license from a git repository.

```
RUN curl -L -o /LICENSE https://raw.githubusercontent.com/element-hq/synapse/v{{ install_version }}/LICENSE || exit 1
```

4. The install version

Use the jinja variable tag `{{ install_version }}` to reference the application
version, which is defined in the images.yaml file.

5. The user statement

As all images should be non-root, you MUST define a USER statement. Reference a
user uid with a variable tag for a variable you defined in the
image.yaml.template

```
USER {{ uid }}
```

## Required parts of the image item in the images.yaml file

### Example for an image item in the images.yaml

The images are defined in the images.yaml in the distinct subtree for the distribution,
f.e. `images/ubuntu/jammy`.

Following you find an example for an image item:

```yaml
      synapse:
        latest: 1.118.0-jammy
        remote: https://github.com/element-hq/synapse
        versions:
        - base_images:
          - python:3.12-jammy
          version: 1.94.0
        - base_images:
          - python:3.12-jammy
          version: 1.103.0
        - base_images:
          - python:3.12-jammy
          version: 1.105.1
        - base_images:
          - python:3.12-jammy
          version: 1.118.0
```

### Definition of the latest version

The `latest` property defines the latest version tag of the image.

### Definition of the maintained versions of an image

The `versions` property defined the maintained version of an image.
Its `base_images` subproperty lists the base images which are used to build the image.
The `version` subproperty of each versions items defined the semver version of the images application.

### Optional properties:

The optional `type` property defines the type of resource where the published versions of an applications
are controlled.

Possible values:
- `git-repository` (also default Value, if no `type` property is set)
- `gitlab-registry`
- `nexus`

The resource type defines the type of resource in which the versions for the image are published.
Resource type can be "nexus", "gitlab-registry" or undefined (no "type" property in the
images.yaml for the image). An undefined value of type is the default case where the resource
type is a git repository with the versions published as tags.

#### Definition of the remote

The `remote` property defined the url where the published versions are retrieved by the version bot to
maintain updates.

##### Setting the remote for type gitlab-registry

Set up the GitLab API URL in the property `remote`:

e.g. `https://gitlab.example.org/api/v4/projects/[your_project_id]/packages`

##### Setting the remote for type git-repository

Set up the GitLab Repository URL in the property `remote`:

e.g. `https://github.com/kubernetes/kubectl`

#### Setting the repository for type nexus

Set the property `repository` to the repository name in nexus.
