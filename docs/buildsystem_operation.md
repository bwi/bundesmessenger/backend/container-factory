# Buildsystem operation

<!-- TOC_START -->
## Table of contents

- [Environment variables](#environment-variables)
- [CI files](#ci-files)
- [Configuration of stages of the build process](#configuration-of-stages-of-the-build-process)
  - [Force build of images](#force-build-of-images)
    - [Buildsystem stages](#buildsystem-stages)
    - [Restrict build process to build a single or a defined set of images](#restrict-build-process-to-build-a-single-or-a-defined-set-of-images)
    - [Restriction of the build process to changed images in feature branches](#restriction-of-the-build-process-to-changed-images-in-feature-branches)
- [Images](#images)
  - [Base images](#base-images)
    - [jammy-base](#jammy-base)
    - [jammy-builder](#jammy-builder)
    - [jammy-nginx, jammy-nginx-production](#jammy-nginx-jammy-nginx-production)
  - [Images for the BundesMessenger](#images-for-the-bundesmessenger)
    - [jammy-synapse](#jammy-synapse)
    - [jammy-redis](#jammy-redis)
    - [jammy-clamav](#jammy-clamav)
    - [jammy-sygnal](#jammy-sygnal)
    - [jammy-kubectl](#jammy-kubectl)
    - [jammy-synapse-admin](#jammy-synapse-admin)
- [In-depth view into the intricacies of proxy environment variables during the build process and runtime](#in-depth-view-into-the-intricacies-of-proxy-environment-variables-during-the-build-process-and-runtime)
  - [Managing Proxy Environment Variables in Container Image Creation](#managing-proxy-environment-variables-in-container-image-creation)
  - [Buildah and Proxy Variables](#buildah-and-proxy-variables)
  - [Podman, `commit`, and Environment Variables](#podman-commit-and-environment-variables)
  - [Running Containers: Proxy Variables in Docker vs. Podman](#running-containers-proxy-variables-in-docker-vs-podman)
  - [Key Takeaways: Balancing Functionality and Security with Proxy Variables](#key-takeaways-balancing-functionality-and-security-with-proxy-variables)
- [Operation and extension of the version bot](#operation-and-extension-of-the-version-bot)
- [Checking reproducible builds](#checking-reproducible-builds)
- [Debug Pipeline Environment](#debug-pipeline-environment)
  - [Versioning and tagging system](#versioning-and-tagging-system)
    - [Production tags](#production-tags)
    - [Immutable tags (build version tags)](#immutable-tags-build-version-tags-)
    - [Source images](#source-images)
    - [References to all images](#references-to-all-images)
  - [Build process schedule](#build-process-schedule)
  - [Retention list, deletion process and retention duration for the images](#retention-list-deletion-process-and-retention-duration-for-the-images)
  - [Deployment and Operation](#deployment-and-operation)
    - [Nginx and nginx base images](#nginx-and-nginx-base-images)
      - [List of relevant Images](#list-of-relevant-images)
      - [Technical guidelines for deployments and compliance](#technical-guidelines-for-deployments-and-compliance)
<!-- TOC_END -->

## Environment variables

At time of writing the following environment variables are used, but others may
be added to the configuration file.

|Variable                 |Values                                 |Description|
|-------------------------|---------------------------------------|-----------|
|ARTIFACT_PATH            |repository/bum-relase/@bwi/bum/-       |Path of artifact server repo for BundesMessenger web artifact|
|ARTIFACT_REPO            |bundesmessenger-npm-h-release          |Name of artifact server repo for BundesMessenger web artifact|
|ARTIFACT_SERVER          |nexus.example.com                      |The artifact server (nexus) domain|
|BOT_USER                 |version-bot                            |The name of the access token for the version bot|
|BOT_PASSWORD             |somepassword                           |The access token for the version bot|
|BUILDER_IMAGE            |registry.example.org/project/<br>builder:5.3.0-jammy-production-b1 |The container image for the build process pipeline.|
|CI_PROJECT_DIR           |(this is set by GitLab)                |The CI project directory. `/var/build` for local development.|
|                         |                                       |In GitLab this is set to /build/...Directory of cloned git project|
|DEVELOP                  |0, 1                                   |The setting to activate the mode for local development. (to be used|
|                         |                                       |with the local_pinp_buildimage.sh script)|
|DISABLE_EXTERNAL_PUSH    |0, 1                                   |Set to 1 to disable all pushs to external registries. This will also|
|                         |                                       |prevent the creation of source images (images containing source codes)|
|                         |                                       |if the buildsystem is configured to use an external registry as the|
|                         |                                       |store for the source images.|
|DOCKER_REGISTRY          |registry.example.com:5000              |The hostname of the central registry (single point of truth for the buildversions|
|FULCIO_URL               |https://fulcio.example.com             |[See chapter "Sign stage"](./keyless_signing.md)|
|GIT_SERVER               |gitlab.example.com                     |The domain of the GitLab server|
|KEYCLOAK_CLIENT_ID       |sigstore                               |[See chapter "Sign stage"](./keyless_signing.md)|
|KEYCLOAK_CLIENT_SECRET   |somesecret                             |[See chapter "Sign stage"](./keyless_signing.md)|
|KEYCLOAK_REALM           |imagesigning                           |[See chapter "Sign stage"](./keyless_signing.md)|
|KEYCLOAK_SERVER          |keycloak.example.com                   |[See chapter "Sign stage"](./keyless_signing.md)|
|KEYCLOAK_USERNAME        |someusername                           |[See chapter "Sign stage"](./keyless_signing.md)|
|KEYCLOAK_PASSWORD        |somepassword                           |[See chapter "Sign stage"](./keyless_signing.md)|
|OPENCODE_CONTAINER_PATH  |registry.example.com/orga/project/path |The domain and path of the external registry|
|PROJECT                  |bwmessenger                            |Path to images in registry (without registry domain)|
|REKOR_URL                |https://rekor.example.com              |[See chapter "Sign stage"](./keyless_signing.md)|
|REGISTRY_USER            |someuser                               |The user for login to the central registry|
|REGISTRY_PASSWORD        |somepassword                           |The password for the login to the central registry|
|TEST_BUILD               |0, 1                                   |Set to 1 to speed up the build process by restricting the collection|
|                         |                                       |of sources to one package|
|TUF_URL                  |https://tuf.example.com                |[See chapter "Sign stage"](./keyless_signing.md)|
|UBUNTU_MIRROR            |de.archive.ubuntu.com/ubuntu           |The official or private ubuntu mirror to be used for ubuntu package installation|
|VULNERABILITY_SCANNER    |grype, trivy-harbor                    |The scanner to be used for CVE scanning|

## CI files

- **.default_env**

  Contains the default environment variables for the build process.

- **.main.env**

  Contains the environment variables for the main branch. Overwrites the
  existing default environment variables.

- **.develop.env**

  Contains the environment variables for all branches which are not named
  "main" e.g. all feature/hotfix branches and the develop branch.

- **.gitlab-ci.yml**

  Contains the ci stages generate-pipelines, run-pipelines and mirror.

  The **generate-pipelines** stage creates all jobs to build one image in each
  job.  This allows a parallel processing of build jobs for multiple images,
  which speeds up the build process and makes it scalable.

  The **run-pipelines** stage runs all created pipelines from the
  generate-pipelines stage.

  The **mirror** stage mirrors code to an external repository.

## Configuration of stages of the build process

The configuration of the active stages of the build process in defined in the
file *.gitlab-ci.yml*.

```bash
python3 -m factory.generate_pipelines --stages=check,build,test,strip,ref,src,bom,push,scan
```

### Force build of images

If the buildsystem detects no security patches or code changes for an image,
the image will not be built. Sometimes it is necessary to force the build of an
image. This can be achieved by removing the check stage from the list of build
stages which are defined in the file *.gitlab-ci.yml*

e.g. all build stages defined:

```bash
python3 -m factory.generate_pipelines --stages=check,build,test,strip,ref,src,bom,qa,push,scan
```

With removed check stage (no check for updates, force build):

```bash
python3 -m factory.generate_pipelines --stages=build,test,strip,ref,src,bom,qa,push,scan
```

## Buildsystem stages

- check

  Checks for updates of system packages and changed code in the image folder
  for this image (f.e. changes to image.yaml or Dockerfile).

- build

  Builds the image.

- test

  Runs the new image and calls tests.

- strip

  Removes (security critical) packages.

- ref

  Adds `/readme_references.txt` to images, containing references to the
  immutable tag-versions, the production and source tags of an image and the
  specific version tag which a rolling tag points to.

- bom

  Adds a bill of material to the image.

- push

  Pushes image to internal and external registries.

- scan

  Executes security scan for image.

## Restrict build process to build a single or a defined set of images

Sometimes it is convenient to build a single image or a set of images f.e.
during the development process to test a single feature, or to provide a new
build of a single image for the users during production in a fast way.

To build a single image:

```bash
python3 -m factory.generate_pipelines --images=ubuntu --stages=check,build,test,strip,ref,src,bom,qa,push,scan
```

To build a set of images (currently you must add all parent images to the
list):

```bash
.python3 -m factory.generate_pipelines --images=ubuntu,python:3.9-jammy,synapse --stages=check,build,test,strip,ref,src,bom,qa,push,scan
```

To build a single or defined set of images by using special tags in the commit
message: The export command is only necessary on local development.  On GitLab
the last commit message will be set to the CI_COMMIT_MESSAGE pipeline variable.

```bash
export CI_COMMIT_MESSAGE='Restrict pipelines. [images=python:3.9-jammy,synapse]'
python3 -m factory.generate_pipelines
```

The pipeline generator will add all needed base images automatically. Note that
the --images parameter to the generate_pipelines.py script has precedence over
the special tags in the commit message.

## Restriction of the build process to changed images in feature branches

In feature branches (beginning with 'feature/' the ImageBuilder will restrict
the build_process to the changed images.

The list of changed images is determined by all changed file paths in the
feature branch.

You can prevent that default behaviour of feature branch pipelines by using the
special tag '[all]' in the commit message. With that special commit tag, all
images will be build.

```bash
git commit -m "Some new feature. [all]"
```

Alternatively you can set the CI- or environment-variable `BUILD_ALL` to `1`.

## Images

### Base images

#### ubuntu

Base image containing the minimal set of Ubuntu 22.04 `Jammy Jellyfish`
packages. BOM and source is made available in `/root/container_data`.

#### builder

PinP image ready to build the configured images based on `ubuntu`.

This image is used to run the updater to build all other images. It contains a
docker client installation and all libraries needed to run the updater. If new
images require more tools, they need to be added to this image or they have to
be pulled at runtime.

The container also brings tools to scan sourcecode for used licenses
(`scancode-toolkit`), create a BoM including package names and licenses
(`syft`), and scan installed packages for known vulnerabilities (`grype`).

#### nginx

A small nginx installation based on jammy-base. Configuration is expected in
`/etc/nginx`.Ports 8080 and 8443 are exposed.

```bash
$ docker run -p 8080:8080 -v /tmp/xx:/etc/nginx jammy-nginx:latest
$ curl -i http://localhost:8080/
HTTP/1.1 200 OK
Server: nginx/1.18.0 (Ubuntu)
```

#### python Image based on `ubuntu`. The python versions for the different tags
of this image are installed from deadsnakes.

### Images for the BundesMessenger

#### bundesmessenger-web

Image based on `nginx`. Includes an installation of
gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web. This image
expects the default nginx configuration in
`/etc/nginx/sites-available/default`.

The webclient configuration must be overriden in `/app/config.json`.

#### bundesmessenger-call

Image based on `nginx`.
Includes an installation of bundesmessenger-call.
This image expects the default nginx configuration in
`/etc/nginx/sites-available/default`.

The client's configuration must be overriden in
`/app/public/config.json`

#### clamav

Image based on stock `ubuntu` with added clamav. The image expects the files to
be scanned in `/scan`, the signatures in `/clamav/database` and the
configuration for signature updates in `/clamav/config/freshclam.conf`. A
sample configuration file will be provided.

The container accepts command line options for `clamscan` except
`--database`.Also the scan directory is `/scan` and should not be passed to the
container.

#### icap

Image based in `ubuntu` with added icap. The configuration must be overriden in
`/clamav/config/clamdcheck.conf` and `/clamav/config/c-icap.conf`.

#### kubectl

Image based on `ubuntu` with added kubectl. kubectl expects its configuration
in directory `/home/kube/.kube`. A sample invocation might look like this:

```bash
# docker run -v /home/kube/.kube:/tmp/config registry.example.com:5000/projectname/jammy-kubectl:latest --help

# The path segment 'projectname' should equalize to the PROJECT environment variable.
```

kubectl controls the Kubernetes cluster manager.

Find more information at:
https://kubernetes.io/docs/reference/kubectl/overview/

This image includes BOM and source code.

#### matrix-authentication-service

Image based on `ubuntu`. The configuration must be overriden in:
`/mas/config/conf.d/config.yaml`.

The secret must be set in: `/mas/config/conf.d/mas-secret.yaml`.

#### matrix-content-scanner

Image based in `python`. The configuration must be overriden in:
`/config/config.yaml`

#### redis

Redis server based on `ubuntu`. Configuration is expected in
`/etc/redis/redis.conf`. If that file does not exist, a default is created at
container start. Database files will be created in `/var/lib/redis` by default
and logs are written to `/var/log/redis`.

#### synapse

Image based on `python`, matrix-synapse. Synapse version, user, uid, and gid
are configured in image.yaml.template. Configuration is expected in `/data/`.
Ports 8008 and 8448 are exported.

#### sygnal

The matrix push gateway reference implementation based on `python`.
Configuration is expected in `/home/sygnal/config/sygnal.yaml` but may be
overridden with environment variable `SYGNAL_CONF`.


#### synapse-admin

This is a mostly unchanged `nginx` image with a compiled version of
`Awesome-Technologies/synapse-admin` installed in `/opt/sadm`. At startup a
minimal (e.g. port 80 only and no SSL) configuration is created and enabled for
this site as `sites-available/sadm`.

BOM and sourcecode collection is disabled by configuration.

## In-depth view into the intricacies of proxy environment variables during the
build process and runtime

### Managing Proxy Environment Variables in Container Image Creation

When creating container images using tools like Buildah and Podman,
understanding the intricacies of proxy environment variables is essential.
These variables, including `http_proxy` and `https_proxy`,
can greatly influence the build process and the behavior of
running containers. Furthermore, if not managed carefully, these variables
might lead to unintentional leakage of infrastructure information into the
built image, potentially exposing sensitive details.

### Buildah and Proxy Variables

1. **Inherited Variables**:
   - While Buildah doesn't embed the host's environment variables into the
     image by default, commands executed during the build process (e.g., `wget`
     or `curl`) might use the host's proxy settings.
   - If a base image has proxy environment variables set, derived images will
     inherit them unless explicitly overridden.

2. **The Empty vs. Unset Dilemma**:
   - An environment variable set to an empty value (`""`) isn't the same as an
     unset variable. The former exists but holds no data, while the latter
     doesn't exist in the environment at all. This distinction can lead to
     different behaviors in derived containers or images.

## Podman, `commit`, and Environment Variables

1. **Committing with Proxy Variables**:
   - Using `podman commit` can inadvertently embed environment variables, like
     proxy settings, from the building container into the new image. This
     behavior can introduce undesired configurations into the resulting image
     and may also leak infrastructure information.

2. **Cleanup Before Committing**:
   - Before leveraging `podman commit`, ensure removal or appropriate setting
     of proxy variables to avoid carrying over environment-specific
     configurations into generalized images.

3. **Preventing leakage beforehand**:
   - Alternatively use build arguments instead of environment variables where
     applicable.
   - F.e. use the following code in the build template and Dockerfile to
     provide a setting for the build process without using an environment
     variable which would leak into the image, if not overwritten:

Providing a host environment variable / ci variable  as a build parameter in a
build.template:
```
buildah build --build-arg MYSETTING=$MYSETTING -t someimage
```

Using the build parameter in the Dockerfile:
```
FROM someimage

ARG MYSETTING

RUN curl ${MYSETTING}/somepath
```

This way the setting is not leaked into the image.

Don't do this, as this would leak the setting into the image, if not
overwritten:

```
FROM someimage

ARG MYSETTING

ENV MYSETTING=${MYSETTING}

RUN curl ${MYSETTING}/somepath
```

### Running Containers: Proxy Variables in Docker vs. Podman

1. **Docker**:
   - Docker doesn't automatically propagate host environment variables to
     running containers. However, variables set using the `ENV` directive in
     the Dockerfile will be present in the container.  - Override image-defined
     environment variables using the `-e` flag during runtime.

2. **Podman**:
   - Podman, being more "rootless", might inherit environment variables more
     closely from the host. Still, it also respects `ENV` directives from the
     Dockerfile.  - Overriding image-defined variables is similar to Docker,
     using the `-e` flag.

### Key Takeaways: Balancing Functionality and Security with Proxy Variables

Proxy environment variables play a crucial role in building and running
container images. By understanding their nuances in Buildah and Podman,
developers can ensure that their containers function predictably and securely
across various environments. Additionally, it's vital to be vigilant about the
potential leakage of infrastructure details into the final images, maintaining
a balance between functionality and security.

## Operation and Extension of the Version Bot

### Overview

The Version Bot is responsible for updating the `images.yaml` file and the `retention_list.yaml` file. These files track image versions and their retention policies. The bot ensures that the images listed in `images.yaml` stay up to date and that retention policies are followed.

### Structure of `images.yaml`

The `images.yaml` file contains structured information about available images, their versions, and related metadata. Below is an example of its format:

```yaml
images:
  ubuntu:
    jammy:
      python:
        latest: 3.12-jammy
        remote: https://github.com/python/cpython
        versions:
        - base_images:
          - ubuntu:latest-jammy
          version: '3.11'
        - base_images:
          - ubuntu:latest-jammy
          version: '3.12'
      synapse:
        latest: 1.118.0-jammy
        remote: https://github.com/element-hq/synapse
        versions:
        - base_images:
          - python:3.12-jammy
          version: 1.105.1
        - base_images:
          - python:3.12-jammy
          version: 1.118.0
```

### Structure of `retention_config.yaml`

The `retention_config.yaml` file contains the retention configuration and maintains a list of image versions that should be retained based on business rules.

```yaml
manual_updates:
  - 'python'
  - 'node'
  - 'builder'
min_versions:
  synapse: "1.78.0"
retention_list:
  - 'example-image:2.16.0-jammy-production'                                                        
  - 'example-image:2.16.0-jammy-src'
  - 'example-image:2.18.0-jammy-production'
  - 'example-image:2.18.0-jammy-src'
...
```

### Existing Extensions of the Version Bot

The Version Bot has an extension for BundesMessenger located at `factory/bots/bum_bot_py`.  
This extension is implemented as the `BumBot` class and works  
alongside the `VersionBot` class in `factory/bots/version_bot_py`.

#### Functionality of the BumBot Extension

The `BumBot` extension enhances the Version Bot by checking the BundesMessenger HelmChart  
repository for referenced images. It then updates both `images.yaml` and `retention_list.yaml`  
according to organizational policies, such as maximum retention periods and support timelines.

### Extending the Version Bot

To customize the Version Bot for additional processing logic, two hooks are available:

#### Available Hooks

- `pre_process_pre_hook`: Called before the preprocessing of `images.yaml` and `retention_list.yaml`.
- `pre_process_post_hook`: Called after preprocessing but before the main processing of the Version Bot.
  This allows for additional filtering or transformations before the main workflow runs.

#### Adding Custom Logic

To extend the Version Bot, implement custom logic inside these hooks in a new subclass or within an existing bot extension. Depending on the use case, the hooks can be used to:

- Enforce additional business rules.
- Integrate with external APIs.
- Modify retention policies dynamically.

By leveraging these hooks, developers can ensure the Version Bot aligns with their specific requirements while maintaining its core functionality.

## Checking reproducible builds

The base images contains a LABEL de.bwi.baseimage.source-date-epoch with a
value set to the timestamp of the creation time.

It can be used to reproduce the build of the image.  We included a script,
which reads the value of this label and create a test image with mmdebstrap
containing the exact bit-by-bit copy of the artifact from the creation date
provided by snapshot.debian.org. The script will compare all files of the base
layer of a given image to the files of the snapshot.

Usage:

```bash
./check_reproducible.sh registry.example.com/projectname/jammy-base
```

The path segment 'projectname' should equalize to the PROJECT environment
variable.

```bash
-> LABEL de.bwi.baseimage.source-date-epoch has the value: 1665919188
-> Pull and extract image...
-> Create test image from snapshot...
-> The image is bit-by-bit reproducible with the original debian snapshot.
```

With this result message, you can be sure to have a bit-by-bit copy of the base
layer we created for you at build time.

## Debug Pipeline Environment

1. Set the environment variable `DEBUG=1` in the GitLab ci pipeline script.

- `export DEBUG=1`

2. Copy the cipher between the \*-markers in the pipeline log to a file
   'encrypted_log' into a directory.

```bash
SECURELOG**************************************************************
[some cipher]
***********************************************************************
```

3. Copy private_key.pem into the same directory.

4. Execute the decrypt_log script:

Usage:

```bash
./scripts/decrypt_log.py
```

to display the decrypted pipeline environment.

If you need to create your own key pair e.g. for debugging in a feature branch,
your can create your own key pair:

```python
from Crypto.PublicKey import RSA

key = RSA.generate(4096)
private_key = key.export_key()
file_out = open("private_key.pem", "wb")
file_out.write(private_key)
file_out.close()

public_key = key.publickey().export_key()
file_out = open("public_key.pem", "wb")
file_out.write(public_key)
file_out.close()
```

### Versioning and tagging system


#### Unhardened tags

The buildsystem uses unhardened tags of the base images for the different
application images in order to be able f.e. to install packages for the
application.

#### Production tags

The buildsystem provides rolling and immutable tags. The rolling tags include
the version of the integrated application. The production images are stripped
from insecure and unnecessary packages and files during the strip stage from
the final image. The packages and files will be deleted in an additional image
layer, so even deleted in the final image, they exist in the preceding image
layers. Insecure packages are also removed beforehand during CVE mitigations.

Example: `synapse:1.108.0-jammy-production`

#### Immutable tags (build version tags)

The immutable tags include the full version and will be appended with the
appendix `-b<build_number>`.

Example: `synapse:1.108.0-jammy-production-b2`

The buildsystem executes a nightly build, which checks, if updates for the
system packages or application modules of an image are available (f.e. system
package updates of ubuntu).  If this is the case, the image will be built and
published in the configured registries.  A rolling tag will be set to this new
image and the build_number of the immutable tag will be increased.

#### Source images

For each image there is a source image, which contains the sources of the
system packages and the installed application.

Example: `synapse:1.108.0-jammy-src-b2`

#### References to all images

In all images the file `/root/readme_references.txt` containins references to
the immutable tag-versions, the production and source tags of an image and the
specific version tag which a rolling tag points to.

### Build process schedule

The build process will be triggered by the GitLab scheduler once in a week on
mondays.  The build cycle may vary in the future depending on the chosen
retention duration for the images and the usable registry space.

### Retention list, deletion process and retention duration for the images

The file retention_list.yaml contains tags of images, which are retained (not
deleted by the deletion process to safe space) for a specified period of time.
Currently the RETENTION_DAYS setting is set to 30 days.  The deletion process
will be triggered by the GitLab scheduler once a day.  All images with no
definition in the retention list in the file retention_list.yaml will be
deleted, if the image is older than the number of days defined with the
RETENTION_DAYS constant in the delete_images.py script.

### Deployment and Operation

#### Nginx and nginx base images

##### List of relevant Images

The following images include the nginx server:

- nginx
- synapse-admin
- bundesmessenger-web

##### Technical guidelines for deployments and compliance

For these images you must care for a secure deployment. For BSI conform
deployments the BSI minimum standards (Section 8 (1) Sent. 1 of the German
Federal Office for Information Security Act (BSI Act)) - Mindeststandard
Version 2.4) defines requirements for the compliance. The Technical guideline
TR-02102-2 defines recommended cipher suites for secure key exchange and
encrypted communication. The document Mindeststandard Version 2.4 demands the
usage of the recommended cipher suites, defined in TR-02102-2. (MUST)

For the ssl termination with a proxy or ingress we provide an example nginx
configuration with a setting for the ssl ciphers with compliance to the BSI
guidelines.

The cipher configuration is based on the moz://a SSL Configuration Generator
(as of 23.6.2023) intermediate config. We removed all cipher suites from that
base config, which are NOT listed in the recommendations of TR-02102-2 as the
strict usage of TR-02102-2 recommended cipher suites is a MUST requirement of
the BSI minimum standards.

Please note that the ordering of the cipher suites is based on multiple
factors, including encryption algorithm strength, key size, integrity
protection mechanism, security level, implementation quality, and vulnerability
status.

The Order should be adapted to the application context and compliance
considerations.

```
server {
    listen 8443 ssl default_server;
    listen [::]:8443 ssl
    default_server;

    gzip on;

    ssl_protocols       TLSv1.3 TLSv1.2;

    ssl_certificate     /etc/nginx/certs/example.crt;
    ssl_certificate_key /etc/nginx/certs/example.key;

    ssl_dhparam /etc/nginx/certs/dhparam.pem;

    # intermediate configuration ssl_ciphers
    ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    root /var/www/html;

    index index.html index.htm index.nginx-debian.html;

    server_name _;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

}

server {
  listen 8080;
  listen [::]:8080;

  server_name localhost;

  return 302 https://$server_name$request_uri;
}

```

https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Mindeststandards/Mindeststandard_BSI_TLS_Version_2_4.pdf

https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR02102/BSI-TR-02102-2.pdf

https://ssl-config.mozilla.org/#server=nginx&version=1.17.7&config=intermediate&openssl=1.1.1k&ocsp=false&guideline=5.7

https://www.ssllabs.com/ssltest/
